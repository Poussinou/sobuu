package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.BookProgress
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.LocalDateTime

val bookProgressReadingMiddle = BookProgress(
    id = "753",
    finished = false,
    giveUp = false,
    page = 65,
    progressInPercentage = 44.0,
    finishedToRead = null,
    percentage = null,
    startedToRead = LocalDateTime.now().minusDays(3).toKotlinLocalDateTime(),
)

val bookProgressReadingStarting = BookProgress(
    id = "349",
    finished = false,
    giveUp = false,
    page = null,
    progressInPercentage = 1.0,
    finishedToRead = null,
    percentage = 1.0,
    startedToRead = LocalDateTime.now().minusMinutes(5).toKotlinLocalDateTime(),
)

val bookProgressReadingAlmostFinished = BookProgress(
    id = "943",
    finished = false,
    giveUp = false,
    page = null,
    progressInPercentage = 95.0,
    finishedToRead = null,
    percentage = 95.0,
    startedToRead = LocalDateTime.now().minusDays(6).toKotlinLocalDateTime(),
)

val bookProgressFinished = BookProgress(
    id = "357",
    finished = true,
    giveUp = false,
    page = null,
    progressInPercentage = 100.0,
    finishedToRead = LocalDateTime.now().minusDays(9).toKotlinLocalDateTime(),
    percentage = 100.0,
    startedToRead = LocalDateTime.now().minusMonths(1).toKotlinLocalDateTime(),
)

val bookProgressGaveUp = BookProgress(
    id = "153",
    finished = false,
    giveUp = true,
    page = 66,
    progressInPercentage = 42.0,
    finishedToRead = LocalDateTime.now().minusMonths(1).toKotlinLocalDateTime(),
    percentage = null,
    startedToRead = LocalDateTime.now().minusMonths(2).toKotlinLocalDateTime(),
)