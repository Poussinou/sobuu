package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.LocalDateTime


val userRating1 = UserBookRating(
    id = "",
    date = LocalDateTime.now().minusDays(10).toKotlinLocalDateTime(),
    rating = 4.5,
    review = "Loved it",
    user = Profile(
        id = "",
        createdAt = LocalDateTime.now().minusMonths(9).toString(),
        email = "fake@email.com",
        firstName = "John",
        lastName = "Doe",
        username = "DoeJohn",
    )
)

val userRating2 = UserBookRating(
    id = "",
    date = LocalDateTime.now().minusMonths(10).toKotlinLocalDateTime(),
    rating = 3.5,
    review = "I find it a bit weak at the end. Didn't make sense",
    user = Profile(
        id = "",
        createdAt = LocalDateTime.now().minusMonths(19).toString(),
        email = "fake@email.com",
        firstName = "John A.",
        lastName = "Doe",
        username = "DoeAJohn",
    )
)

val userRating3 = UserBookRating(
    id = "",
    date = LocalDateTime.now().minusMonths(6).toKotlinLocalDateTime(),
    rating = 4.0,
    review = "I cannot understand how at the end the protagonist couldn't do the right thing. But, for the rest, very rounded story",
    user = Profile(
        id = "",
        createdAt = LocalDateTime.now().minusMonths(19).toString(),
        email = "fake@email.com",
        firstName = "John A.",
        lastName = "Doe",
        username = "DoeAJohn",
    )
)