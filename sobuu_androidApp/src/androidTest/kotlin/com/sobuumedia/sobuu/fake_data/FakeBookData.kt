package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.LocalDateTime

    val book1: Book = Book(
        id = "123",
        title = "From the Earth to the Moon",
        authors = listOf("Jules Verne"),
        bookDescription = "The Baltimore Gun Club, a society dedicated to the design of weapons of all kinds (especially cannons), comes together when Impey Barbicane, its president, calls them to support his latest idea. He's done some calculations, and believes that they could construct a cannon capable of shooting a projectile to the Moon.",
        picture = "https://upload.wikimedia.org/wikipedia/commons/c/c4/From_the_Earth_to_the_Moon_Jules_Verne.jpg",
        thumbnail = "https://upload.wikimedia.org/wikipedia/commons/c/c4/From_the_Earth_to_the_Moon_Jules_Verne.jpg",
        publisher = "Pierre-Jules Hetzel",
        credits = CreditsBO(
            translators = arrayOf("J. K. Hoyt")
        ),
        totalPages = 123,
        isbn = Pair("1598184547", "9781598184549"),
        publishedDate = "1869",
        genres = listOf("Science Fiction"),
        totalComments = 0,
        peopleReadingIt = 0,
        readingStatus = BookReadingStatus.FINISHED,
        allReviews = listOf(),
        userRating = UserBookRating(
            id = "951",
            date = LocalDateTime.now().minusDays(10).toKotlinLocalDateTime(),
            rating = 4.5,
            review = "Loved it",
            user = Profile(
                id = "",
                createdAt = LocalDateTime.now().minusMonths(9).toString(),
                email = "fake@email.com",
                firstName = "John",
                lastName = "Doe",
                username = "DoeJohn",
            )
        ),
        totalRating = 4.5,
        serie = "Voyages Extraordinaires",
        serieNumber = 4,
        lang = "en",
    )

    val book2: Book = Book(
        id = "456",
        title = "The Time Machine",
        authors = listOf("H.G.Wells"),
        bookDescription = "Utilizing a frame story set in then-present Victorian England, Wells' text focuses on a recount of the otherwise anonymous Time Traveller's journey into the far future.",
        picture = "https://upload.wikimedia.org/wikipedia/en/2/2f/Timemachinebook.JPG",
        thumbnail = "https://upload.wikimedia.org/wikipedia/en/2/2f/Timemachinebook.JPG",
        publisher = "William Heinemann",
        credits = CreditsBO(),
        totalPages = 108,
        isbn = Pair("1984561376", "1354366874654"),
        publishedDate = "1895",
        genres = listOf("Science Fiction"),
        totalComments = 60,
        peopleReadingIt = 4,
        readingStatus = BookReadingStatus.NOT_READ,
        allReviews = listOf(),
        userRating = null,
        totalRating = 4.9,
        serie = "",
        serieNumber = 0,
        lang = "en",
    )

    val book3: Book = Book(
        id = "789",
        title = "The Black Corsair",
        authors = listOf("Emilio Salgari"),
        bookDescription = "Two pirates, Carmaux and Van Stiller, are rescued by the Thunder, a pirate ship under the command of Emilio di Roccabruna of Roccanera, Lord of Valpenta and of Ventimiglia, and feared throughout the Caribbean as the Black Corsair...",
        picture = "https://upload.wikimedia.org/wikipedia/en/e/ec/The_Black_Corsair_Book_Cover.jpg",
        thumbnail = "https://upload.wikimedia.org/wikipedia/en/e/ec/The_Black_Corsair_Book_Cover.jpg",
        publisher = "ROH Press",
        credits = CreditsBO(
            translators = arrayOf("Nico Lorenzutti")
        ),
        totalPages = 272,
        isbn = Pair("5469456165", "7232154212654"),
        publishedDate = "2011",
        genres = listOf("Adventure, Young Adult"),
        totalComments = 10,
        peopleReadingIt = 0,
        readingStatus = BookReadingStatus.GIVE_UP,
        allReviews = listOf(),
        userRating = null,
        totalRating = 6.0,
        serie = "",
        serieNumber = 0,
        lang = "en",
    )

    val book4: Book = Book(
        id = "987",
        title = "A Study in Scarlet",
        authors = listOf("Sir Arthur Conan Doyle"),
        bookDescription = "In 1881, Doctor John Watson returns to London after serving in the Second Anglo-Afghan War and starts looking for a place to live.",
        picture = "https://live.staticflickr.com/8502/8347312582_9eb9c8170f.jpg",
        thumbnail = "https://live.staticflickr.com/8502/8347312582_9eb9c8170f.jpg",
        publisher = "Ward Lock & Co",
        credits = CreditsBO(),
        totalPages = 123,
        isbn = Pair("9546512145", "1965426541235"),
        publishedDate = "1888",
        genres = listOf("Detective"),
        totalComments = 100,
        peopleReadingIt = 30,
        readingStatus = BookReadingStatus.READING,
        allReviews = listOf(),
        userRating = null,
        totalRating = 5.0,
        serie = "Sherlock Holmes",
        serieNumber = 1,
        lang = "en",
    )

    val book5: Book = Book(
        id = "654",
        title = "Death on the Nile",
        authors = listOf("Agatha Christie"),
        bookDescription = "While on holiday in Aswan to board the steamer Karnak, set to tour along the Nile River from Shellal to Wadi Halfa, Hercule Poirot is approached by successful socialite Linnet Doyle née Ridgeway.",
        picture = "https://upload.wikimedia.org/wikipedia/en/9/96/Death_on_the_Nile_First_Edition_Cover_1937.jpg",
        thumbnail = "https://upload.wikimedia.org/wikipedia/en/9/96/Death_on_the_Nile_First_Edition_Cover_1937.jpg",
        publisher = "Collins Crime Club",
        credits = CreditsBO(),
        totalPages = 288,
        isbn = Pair("1354215498", "6545542154321"),
        publishedDate = "1937",
        genres = listOf("Crime, Detective"),
        totalComments = 76,
        peopleReadingIt = 10,
        readingStatus = BookReadingStatus.NOT_READ,
        allReviews = listOf(),
        userRating = null,
        totalRating = 4.6,
        serie = "Hercule Poirot",
        serieNumber = 18,
        lang = "en",
    )