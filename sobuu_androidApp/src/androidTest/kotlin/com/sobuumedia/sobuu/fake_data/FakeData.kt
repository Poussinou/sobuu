package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook

val bookWithProgressReading: BookWithProgress = BookWithProgress(
    book = book4,
    bookProgress = bookProgressReadingMiddle,
    bookProgressComments = null
)

val bookWithProgressReading2: BookWithProgress = BookWithProgress(
    book = book3,
    bookProgress = bookProgressReadingMiddle,
    bookProgressComments = null
)

val bookWithProgressFinished: BookWithProgress = BookWithProgress(
    book = book1,
    bookProgress = bookProgressFinished,
    bookProgressComments = null
)

val bookWithProgressGaveUp: BookWithProgress = BookWithProgress(
    book = book3,
    bookProgress = bookProgressGaveUp,
    bookProgressComments = null
)

val bookFinished: FinishedReadingBook = FinishedReadingBook(
    bookWithProgress = bookWithProgressFinished,
    userRating = userRating1,
)

val bookFinished2: FinishedReadingBook = FinishedReadingBook(
    bookWithProgress = bookWithProgressFinished,
    userRating = userRating2,
)

val bookFinished3: FinishedReadingBook = FinishedReadingBook(
    bookWithProgress = bookWithProgressFinished,
    userRating = userRating3,
)