package com.sobuumedia.sobuu.fake_data

import com.sobuumedia.sobuu.models.bo_models.Profile
import java.time.LocalDateTime

val profile1 = Profile(
    id = "877",
    createdAt = LocalDateTime.now().minusMonths(19).toString(),
    email = "faking@email.com",
    firstName = "Jane",
    lastName = "Doe",
    username = "DoeJane",
)

val profile2 = Profile(
    id = "947",
    createdAt = LocalDateTime.now().minusMonths(19).toString(),
    email = "fake@email.com",
    firstName = "John A.",
    lastName = "Doe",
    username = "DoeAJohn",
)