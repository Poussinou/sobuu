package com.sobuumedia.sobuu.authentication.login

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createNewAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_forgotPassword
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.navigation.Navigation
import com.sobuumedia.sobuu.android.navigation.Screen
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.withContentDescription
import junit.framework.TestCase.assertEquals
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
internal class LoginScreenNavigationTests {

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var navController: TestNavHostController

    @Before
    fun setUp() {
        composeTestRule.setContent {
            navController = TestNavHostController(ApplicationProvider.getApplicationContext())
            navController.navigatorProvider.addNavigator(ComposeNavigator())
            Navigation(navController = navController, NapierLogs())
        }
        sleep(1200)
    }

    @Test
    fun testTermsButtonNavigates() {
        composeTestRule.onNode(settings_main_terms.withContentDescription()).performClick()
        val route = navController.currentBackStackEntry?.destination?.route
        assertEquals(route, "${Screen.LongTextScreen.route}/{text_type}")
    }

    @Test
    fun testPrivacyButtonNavigates() {
        composeTestRule.onNode(settings_main_privacy.withContentDescription()).performClick()
        val route = navController.currentBackStackEntry?.destination?.route
        assertEquals(route, "${Screen.LongTextScreen.route}/{text_type}")
    }

    @Test
    fun testForgotButtonNavigate() {
        composeTestRule.onNode(authorization_auth_forgotPassword.withContentDescription()).performClick()
        val route = navController.currentBackStackEntry?.destination?.route
        assertEquals(route, Screen.ForgotPasswordScreen.route)
    }

    @Test
    fun testRegistrationButtonNavigates() {
        composeTestRule.onNode(authorization_auth_createNewAccount.withContentDescription()).performClick()
        val route = navController.currentBackStackEntry?.destination?.route
        assertEquals(route, Screen.RegistrationScreen.route)
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }

        @ClassRule
        @JvmField
        val localeTestRule = LocaleTestRule()
    }
}