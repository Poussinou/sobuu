package com.sobuumedia.sobuu.authentication.register

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_confirmPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createNewAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_firstNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_lastNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_password
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_username
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_registration_privacySwitch
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_backButton
import com.sobuumedia.sobuu.android.navigation.Navigation
import com.sobuumedia.sobuu.android.navigation.Screen
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.withContentDescription
import junit.framework.TestCase
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.time.Instant

@RunWith(AndroidJUnit4::class)
class RegistrationScreenNavigationTests {

    private val fakeName = "test${Instant.now().toEpochMilli()}@sobuu.com"
    private val fakeLastname = "test${Instant.now().toEpochMilli()}@sobuu.com"
    private val fakeUsername = "test${Instant.now().toEpochMilli()}@sobuu.com"
    private val fakeEmail = "test${Instant.now().toEpochMilli()}@sobuu.com"
    private val fakePassword = "asdf1234"

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var navController: TestNavHostController

    @Before
    fun setUp() {
        composeTestRule.setContent {
            navController = TestNavHostController(ApplicationProvider.getApplicationContext())
            navController.navigatorProvider.addNavigator(ComposeNavigator())
            Navigation(navController = navController, NapierLogs())
        }
        Thread.sleep(100)

        composeTestRule.onNode(authorization_auth_createNewAccount.withContentDescription()).performClick()

        Thread.sleep(120)
    }

    @Test
    fun testTermsButtonNavigates() {
        // TODO I'm not sure how to test this special case
    }

    @Test
    fun testPrivacyButtonNavigates() {
        // TODO I'm not sure how to test this special case
    }

    @Test
    fun testCreateAccountButtonNavigates() {
        composeTestRule.onNode(authorization_auth_username.withContentDescription()).performTextInput(fakeUsername)
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).performTextInput(fakePassword)
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performTextInput(fakeEmail)
        composeTestRule.onNode(authorization_auth_confirmPassword.withContentDescription()).performTextInput(fakePassword)
        composeTestRule.onNode(authorization_auth_firstNameAccount.withContentDescription()).performTextInput(fakeName)
        composeTestRule.onNode(authorization_auth_lastNameAccount.withContentDescription()).performTextInput(fakeLastname)
        Espresso.closeSoftKeyboard()

        Thread.sleep(1200)
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()

        Thread.sleep(1200)
        val route = navController.currentBackStackEntry?.destination?.route
        TestCase.assertEquals(route, "${Screen.SentEmailScreen.route}/{email_type}")
    }

    @Test
    fun testBackButtonNavigates() {
        composeTestRule.onNode(contentDescription_topBar_backButton.withContentDescription()).performClick()

        Thread.sleep(400)
        val route = navController.currentBackStackEntry?.destination?.route
        TestCase.assertEquals(route, Screen.LoginScreen.route)
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }

        @ClassRule
        @JvmField
        val localeTestRule = LocaleTestRule()
    }
}