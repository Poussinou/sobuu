package com.sobuumedia.sobuu.authentication.register

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.isEnabled
import androidx.compose.ui.test.isNotEnabled
import androidx.compose.ui.test.isOff
import androidx.compose.ui.test.isOn
import androidx.compose.ui.test.isToggleable
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_confirmPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_firstNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_lastNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_password
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_username
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emailAlreadyUsed
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyRegistrationFields
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_registration
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_shortPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_usernameAlreadyTaken
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_wrongEmailFormat
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_registration_privacySwitch
import com.sobuumedia.sobuu.android.authentication.register.RegistrationScreen
import com.sobuumedia.sobuu.android.authentication.register.RegistrationViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import com.sobuumedia.sobuu.withContentDescription
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.lang.Thread.sleep


@RunWith(AndroidJUnit4::class)
internal class RegistrationScreenInstrumentationTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<IAuthenticationRepository>()

    /**
     * TODO
     * - Take screenshots in other languages
     * - Test visibility of elements in tablet landscape
     * - Tests errors:
     *     - An error in emails, password or username is reflected by a text message and the red border of the textfield
     */

    @Before
    fun setUp() {
        val mockViewModel = RegistrationViewModel(mockRepo)

        composeTestRule.setContent {
            SobuuAuthTheme {
                RegistrationScreen(
                    navigateToSentEmailScreen = {},
                    navigateBack = {},
                    navigateToLegalText = {},
                    viewModel = mockViewModel
                )
            }
        }
        sleep(120)
    }

    @Test
    fun takeScreenshot() {
        Screengrab.screenshot("create_account")
    }

    @Test
    fun testUsernameDisplayed() {
        composeTestRule.onNode(authorization_auth_username.withContentDescription()).isDisplayed()
    }

    @Test
    fun testUsernameDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_username.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_username.withContentDescription()).isDisplayed()
    }

    @Test
    fun testPasswordDisplayed() {
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).isDisplayed()
    }

    @Test
    fun testPasswordDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmailDisplayed() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmailDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).isDisplayed()
    }

    @Test
    fun testConfirmationPasswordDisplayed() {
        composeTestRule.onNode(authorization_auth_confirmPassword.withContentDescription()).isDisplayed()
    }

    @Test
    fun testConfirmationPasswordDisplayedWithOpenKeyboard() {
        composeTestRule.onNode(authorization_auth_confirmPassword.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_confirmPassword.withContentDescription()).isDisplayed()
    }

    @Test
    fun testFirstNameDisplayed() {
        composeTestRule.onNode(authorization_auth_firstNameAccount.withContentDescription()).isDisplayed()
    }

    @Test
    fun testLastNameDisplayed() {
        composeTestRule.onNode(authorization_auth_lastNameAccount.withContentDescription()).isDisplayed()
    }

    @Test
    fun testPrivacySwitchDisplayedUnchecked() {
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).isDisplayed()
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).assert(isToggleable() and isOff())
    }

    @Test
    fun testPrivacySwitchDisplayedChecked() {
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).assert(isToggleable() and isOn())
    }

    @Test
    fun testCreateAccountButtonDisplayed() {
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).isDisplayed()
    }

    @Test
    fun testCreateAccountButtonIsDisabled() {
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).assert(isNotEnabled())
    }

    @Test
    fun testCreateAccountButtonEmptyErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).isDisplayed()
    }

    @Test
    fun testCreateAccountButtonIsEnabledAfterPrivacySwitchDisplayedChecked() {
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).assert(isToggleable() and isOn())
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).assert(isEnabled())
    }

    @Test
    fun testEmptyFieldsErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "","en") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)
        coEvery { mockRepo.registerUser("", "", "", "", "","es") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)
        coEvery { mockRepo.registerUser("", "", "", "", "","de") } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_emptyRegistrationFields.withContentDescription()).isDisplayed()
    }

    @Test
    fun testWrongEmailFormatErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "","de") } returns AuthenticationResult.Error(
            AuthenticationError.WrongEmailFormatError)
        coEvery { mockRepo.registerUser("", "", "", "", "","es") } returns AuthenticationResult.Error(
            AuthenticationError.WrongEmailFormatError)
        coEvery { mockRepo.registerUser("", "", "", "", "","en") } returns AuthenticationResult.Error(
            AuthenticationError.WrongEmailFormatError)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_wrongEmailFormat.withContentDescription()).isDisplayed()
    }

    @Test
    fun testTimeOutErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.TimeOutError)

        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.TimeOutError)

        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.TimeOutError)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_timeout.withContentDescription()).isDisplayed()
    }

    @Test
    fun testPasswordTooShortErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.PasswordTooShort)

        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.PasswordTooShort)

        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.PasswordTooShort)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_shortPassword.withContentDescription()).isDisplayed()
    }

    @Test
    fun testRegistrationErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.RegistrationError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.RegistrationError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.RegistrationError)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_registration.withContentDescription()).isDisplayed()
    }

    @Test
    fun testUsernameTakenErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.UsernameAlreadyTaken)
        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.UsernameAlreadyTaken)
        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.UsernameAlreadyTaken)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_usernameAlreadyTaken.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmailTakenErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.EmailAlreadyTaken)
        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.EmailAlreadyTaken)
        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.EmailAlreadyTaken)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_emailAlreadyUsed.withContentDescription()).isDisplayed()
    }

    @Test
    fun testUnknownErrorIsDisplayed() {
        coEvery { mockRepo.registerUser("", "", "", "", "", "de") } returns AuthenticationResult.Error(
            AuthenticationError.UnknownError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "es") } returns AuthenticationResult.Error(
            AuthenticationError.UnknownError)
        coEvery { mockRepo.registerUser("", "", "", "", "", "en") } returns AuthenticationResult.Error(
            AuthenticationError.UnknownError)

        composeTestRule.onNode(contentDescription_registration_privacySwitch.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_auth_createAccount.withContentDescription()).performClick()
        composeTestRule.onNode(authorization_errors_unknown.withContentDescription()).isDisplayed()
    }


    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}