package com.sobuumedia.sobuu.authentication.reset_pass

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_forgotPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_resetPassword
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_backButton
import com.sobuumedia.sobuu.android.navigation.Navigation
import com.sobuumedia.sobuu.android.navigation.Screen
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.withContentDescription
import junit.framework.TestCase
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.time.Instant


@RunWith(AndroidJUnit4::class)
internal class ResetPasswordScreenNavigationTest {

    private val fakeEmail = "test${Instant.now().toEpochMilli()}@sobuu.com"

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var navController: TestNavHostController

    @Before
    fun setUp() {
        composeTestRule.setContent {
            navController = TestNavHostController(ApplicationProvider.getApplicationContext())
            navController.navigatorProvider.addNavigator(ComposeNavigator())
            Navigation(navController = navController, NapierLogs())
        }
        Thread.sleep(100)

        composeTestRule.onNode(authorization_auth_forgotPassword.withContentDescription()).performClick()

        Thread.sleep(120)
    }

    @Test
    fun testCreateAccountButtonNavigates() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performTextInput(fakeEmail)
        Espresso.closeSoftKeyboard()

        Thread.sleep(1200)
        composeTestRule.onNode(authorization_auth_resetPassword.withContentDescription()).performClick()

        Thread.sleep(1200)
        val route = navController.currentBackStackEntry?.destination?.route
        TestCase.assertEquals(route, "${Screen.SentEmailScreen.route}/{email_type}")
    }

    @Test
    fun testBackButtonNavigates() {
        composeTestRule.onNode(contentDescription_topBar_backButton.withContentDescription()).performClick()

        Thread.sleep(400)
        val route = navController.currentBackStackEntry?.destination?.route
        TestCase.assertEquals(route, Screen.LoginScreen.route)
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }

        @ClassRule
        @JvmField
        val localeTestRule = LocaleTestRule()
    }
}