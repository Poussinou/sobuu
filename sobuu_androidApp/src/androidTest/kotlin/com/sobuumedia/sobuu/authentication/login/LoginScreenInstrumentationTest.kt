package com.sobuumedia.sobuu.authentication.login

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertHasClickAction
import androidx.compose.ui.test.hasContentDescription
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createNewAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_forgotPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_login
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_password
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyCredentials
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_invalidCredentials
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_login_arrow
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_login_loading
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.authentication.login.LoginScreen
import com.sobuumedia.sobuu.android.authentication.login.LoginViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import com.sobuumedia.sobuu.withContentDescription
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule
import java.lang.Thread.sleep

const val fakeEmail = "test@sobuu.com"
const val fakePassword = "asdf1234"


@RunWith(AndroidJUnit4::class)
internal class LoginScreenInstrumentationTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<IAuthenticationRepository>()

    /**
     * TODO
     * - Test visibility of elements in tablet landscape
     */

    @Before
    fun setUp() {
        coEvery { mockRepo.authenticate() } returns AuthenticationResult.Unauthorized()
        val mockViewModel = LoginViewModel(mockRepo)

        composeTestRule.setContent {
            SobuuAuthTheme {
                LoginScreen(
                    navigateToLegalTextScreen = {},
                    navigateToRegistrationScreen = {},
                    navigateToForgotPasswordScreen = {},
                    navigateToHomeScreen = {},
                    viewModel = mockViewModel
                )
            }
        }
        sleep(1200)
    }

    @Test
    fun takeScreenshot() {
        coEvery { mockRepo.loginUser(any(), any()) } returns AuthenticationResult.Authorized()
        Screengrab.screenshot("Login")

        composeTestRule.onNode(
            authorization_auth_email.withContentDescription()
        ).performTextInput(fakeEmail)
        composeTestRule.onNode(
            authorization_auth_password.withContentDescription()
        ).performTextInput(fakePassword)
        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).performClick()

        sleep(1200)
    }

    @Test
    fun testUsernameAndPasswordDisplayed() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).isDisplayed()
    }

    @Test
    fun testLoginButtonDisplayed() {
        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).isDisplayed()
    }

    @Test
    fun testLoginButtonDisplayedWhenKeyboardOpen() {
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performClick()
        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).isDisplayed()
    }

    @Test
    fun testLoginTextIsDisplayed() {
        composeTestRule.onNode(authorization_auth_login.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_auth_login.withContentDescription()).assert(hasText(authorization_auth_login.stringResource()))
    }

    @Test
    fun testLoadingIndicatorIsDisplayed() {
        coEvery { mockRepo.loginUser(any(), any()) } returns AuthenticationResult.Authorized()

        composeTestRule.onNode(
            authorization_auth_email.withContentDescription()
        ).performTextInput(fakeEmail)
        composeTestRule.onNode(
            authorization_auth_password.withContentDescription()
        ).performTextInput(fakePassword)
        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).performClick()

        sleep(100)

        composeTestRule.onNode(contentDescription_login_loading.withContentDescription()).isDisplayed()
    }

    @Test
    fun testTermsButtonIsDisplayed() {
        composeTestRule.onNode(settings_main_terms.withContentDescription()).isDisplayed()
        composeTestRule.onNode(settings_main_terms.withContentDescription()).assert(hasText(settings_main_terms.stringResource()))
        composeTestRule.onNode(settings_main_terms.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testPrivacyButtonIsDisplayed() {
        composeTestRule.onNode(settings_main_privacy.withContentDescription()).isDisplayed()
        composeTestRule.onNode(settings_main_privacy.withContentDescription()).assert(hasText(settings_main_privacy.stringResource()))
        composeTestRule.onNode(settings_main_privacy.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testForgotButtonIsDisplayed() {
        composeTestRule.onNode(authorization_auth_forgotPassword.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_auth_forgotPassword.withContentDescription()).assert(hasText(authorization_auth_forgotPassword.stringResource()))
        composeTestRule.onNode(authorization_auth_forgotPassword.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testRegistrationButtonIsDisplayed() {
        composeTestRule.onNode(authorization_auth_createNewAccount.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_auth_createNewAccount.withContentDescription()).assert(hasText(authorization_auth_createNewAccount.stringResource()))
        composeTestRule.onNode(authorization_auth_createNewAccount.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testAppNameLogoIsDisplayed() {
        composeTestRule.onNode(hasContentDescription(SharedRes.strings.general_appName.stringResource())).isDisplayed()
    }

    @Test
    fun testErrorDisplayIfLoginWithEmptyTextFields() {
        coEvery { mockRepo.loginUser(any(), any()) } returns AuthenticationResult.Error(
            AuthenticationError.EmptyCredentialsError)

        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).performClick()

        composeTestRule.onNode(authorization_errors_emptyCredentials.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_errors_emptyCredentials.withContentDescription()).assert(
            hasText(authorization_errors_emptyCredentials.stringResource())
        )
    }

    @Test
    fun testErrorDisplayIfErrorLogin() {
        coEvery { mockRepo.loginUser(any(), any()) } returns AuthenticationResult.Error(
            AuthenticationError.InvalidCredentials)
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performTextInput(
            fakeEmail
        )
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).performTextInput(
            fakePassword
        )

        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).performClick()

        composeTestRule.onNode(authorization_errors_invalidCredentials.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_errors_invalidCredentials.withContentDescription()).assert(
            hasText(authorization_errors_invalidCredentials.stringResource())
        )
    }

    @Test
    fun testErrorDisplayIfTimeOutError() {
        coEvery { mockRepo.loginUser(any(), any()) } returns AuthenticationResult.Error(
            AuthenticationError.TimeOutError)
        composeTestRule.onNode(authorization_auth_email.withContentDescription()).performTextInput(
            fakeEmail
        )
        composeTestRule.onNode(authorization_auth_password.withContentDescription()).performTextInput(
            fakePassword
        )

        composeTestRule.onNode(contentDescription_login_arrow.withContentDescription()).performClick()

        composeTestRule.onNode(authorization_errors_timeout.withContentDescription()).isDisplayed()
        composeTestRule.onNode(authorization_errors_timeout.withContentDescription()).assert(
            hasText(authorization_errors_timeout.stringResource())
        )
    }


    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}