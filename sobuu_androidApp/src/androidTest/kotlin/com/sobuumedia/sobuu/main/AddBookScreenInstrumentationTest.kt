package com.sobuumedia.sobuu.main

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.android.add_book.AddBookScreen
import com.sobuumedia.sobuu.android.add_book.AddBookViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.book2
import com.sobuumedia.sobuu.fake_data.book4
import com.sobuumedia.sobuu.fake_data.book5
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule

@RunWith(AndroidJUnit4::class)
class AddBookScreenInstrumentationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockBookRepo = mockk<IBookRepository>()
    private lateinit var mockAddViewModel: AddBookViewModel

    /**
     *
     */
    @Before
    fun setUp() {
        mockAddViewModel = AddBookViewModel(mockBookRepo)

        mockAddViewModel.booksList = listOf(book1, book2, book4, book5)

        composeTestRule.setContent {
            SobuuTheme {
                AddBookScreen(
                    addBookViewModel = mockAddViewModel,
                    navigateBackOrHome = {},
                    navigateToBookScreen = { _, _ -> },
                    isManually = false
                )
            }
        }

        Thread.sleep(120)
    }

    @Test
    fun takeScreenshotFromSearchList() {
        Thread.sleep(500)
        Screengrab.screenshot("search_isbn")
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}