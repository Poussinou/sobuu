package com.sobuumedia.sobuu.main

import androidx.compose.ui.test.assertHasClickAction
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.isNotDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_home_finishedBooksCarousel
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_home_gaveUpBooksCarousel
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_home_readingBooksCarousel
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_profileButton
import com.sobuumedia.sobuu.SharedRes.strings.general_appName
import com.sobuumedia.sobuu.SharedRes.strings.home_main_alreadyRead
import com.sobuumedia.sobuu.SharedRes.strings.home_main_giveUp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_reading
import com.sobuumedia.sobuu.SharedRes.strings.home_main_searchBook
import com.sobuumedia.sobuu.android.authentication.login.LoginViewModel
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.BookStatusType
import com.sobuumedia.sobuu.android.main.HomeScreen
import com.sobuumedia.sobuu.android.main.HomeViewModel
import com.sobuumedia.sobuu.android.main.SearchViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.fake_data.bookFinished
import com.sobuumedia.sobuu.fake_data.bookFinished2
import com.sobuumedia.sobuu.fake_data.bookFinished3
import com.sobuumedia.sobuu.fake_data.bookWithProgressGaveUp
import com.sobuumedia.sobuu.fake_data.bookWithProgressReading
import com.sobuumedia.sobuu.fake_data.bookWithProgressReading2
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.withContentDescription
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule

@RunWith(AndroidJUnit4::class)
class HomeScreenInstrumentationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockRepo = mockk<IAuthenticationRepository>()
    private val mockBookRepo = mockk<IBookRepository>()
    private lateinit var mockHomeViewModel: HomeViewModel
    private lateinit var mockSearchViewModel: SearchViewModel

    /**
     * TODO test:
     * - No books in all the sections
     * - One book in all the sections
     * - More than one book in all the sections
     * - Error getting book data
     * - Error timeout
     * - Change sections
     */
    @Before
    fun setUp() {
        coEvery { mockRepo.authenticate() } returns AuthenticationResult.Unauthorized()
        coEvery { mockBookRepo.getUserCurrentReadingBook() } returns BookResult.Success(listOf(
            bookWithProgressReading, bookWithProgressReading2
        ))
        coEvery { mockBookRepo.getUserGiveUpBook() } returns BookResult.Success(listOf(
            bookWithProgressGaveUp
        ))
        coEvery { mockBookRepo.getUserFinishedReadingBook() } returns BookResult.Success(listOf(
            bookFinished, bookFinished2, bookFinished3
        ))
        coEvery { mockRepo.authenticate() } returns AuthenticationResult.Unauthorized()
        val mockLoginViewModel = LoginViewModel(mockRepo)
        mockHomeViewModel = HomeViewModel(mockBookRepo, logs = NapierLogs())
        mockSearchViewModel = SearchViewModel(mockBookRepo)

        composeTestRule.setContent {
            SobuuTheme {
                HomeScreen(
                    navigateToLoginScreen = {},
                    navigateToProfileScreen = {},
                    navigateToCurrentlyReadingBook = {},
                    navigateToBookScreen = {},
                    navigateToAddBookScreen = {},
                    loginViewModel = mockLoginViewModel,
                    searchViewModel = mockSearchViewModel,
                    homeViewModel = mockHomeViewModel,
                    logs = NapierLogs()
                )
            }
        }

        Thread.sleep(120)
    }

    @Test
    fun takeScreenshot() {
        Thread.sleep(500)
        Screengrab.screenshot("home")
    }

    @Test
    fun takeScreenshotWithGaveUpBooks() {
        Thread.sleep(120)
        composeTestRule.onNode(home_main_giveUp.withContentDescription()).performClick()
        Thread.sleep(100)
        mockHomeViewModel.currentSection = BookStatusType.GIVE_UP
        Thread.sleep(500)
        Screengrab.screenshot("home_gave_up")
    }

    @Test
    fun takeScreenshotWithFinishedBooks() {
        Thread.sleep(120)
        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).performClick()
        Thread.sleep(100)
        mockHomeViewModel.currentSection = BookStatusType.ALREADY_READ
        Thread.sleep(500)
        Screengrab.screenshot("home_finished")
    }

    @Test
    fun testAppNameIsDisplayed() {
        composeTestRule.onNode(general_appName.withContentDescription()).isDisplayed()
    }

    @Test
    fun testSearchBarWithHintIsDisplayed() {
        composeTestRule.onNode(home_main_searchBook.withContentDescription()).isDisplayed()
        composeTestRule.onNode(home_main_searchBook.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testProfileButtonIsDisplayed() {
        composeTestRule.onNode(contentDescription_topBar_profileButton.withContentDescription()).isDisplayed()
        composeTestRule.onNode(contentDescription_topBar_profileButton.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testCurrentlyReadingSectionButtonIsDisplayed() {
        composeTestRule.onNode(home_main_reading.withContentDescription()).isDisplayed()
        composeTestRule.onNode(home_main_reading.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testCurrentlyReadingSectionBookIsDisplayed() {
        composeTestRule.onNode(contentDescription_home_readingBooksCarousel.withContentDescription()).isDisplayed()
    }

    @Test
    fun testAlreadyReadSectionButtonIsDisplayed() {
        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).isDisplayed()
        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testAlreadyReadSectionBookIsDisplayed() {
        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.ALREADY_READ

        composeTestRule.onNode(contentDescription_home_finishedBooksCarousel.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmptyAlreadyReadSectionBookIsDisplayed() {
        coEvery { mockBookRepo.getUserFinishedReadingBook() } returns BookResult.Success(listOf())
        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.ALREADY_READ

        composeTestRule.onNode(contentDescription_home_finishedBooksCarousel.withContentDescription()).isNotDisplayed()
    }

    @Test
    fun testGaveUpReadSectionButtonIsDisplayed() {
        composeTestRule.onNode(home_main_giveUp.withContentDescription()).isDisplayed()
        composeTestRule.onNode(home_main_giveUp.withContentDescription()).assertHasClickAction()
    }

    @Test
    fun testGaveUpReadSectionBookIsDisplayed() {
        composeTestRule.onNode(home_main_giveUp.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.GIVE_UP

        composeTestRule.onNode(contentDescription_home_gaveUpBooksCarousel.withContentDescription()).isDisplayed()
    }

    @Test
    fun testEmptyGaveUpReadSectionBookIsDisplayed() {
        coEvery { mockBookRepo.getUserGiveUpBook() } returns BookResult.Success(listOf(
            bookWithProgressGaveUp
        ))
        composeTestRule.onNode(home_main_giveUp.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.GIVE_UP

        composeTestRule.onNode(contentDescription_home_gaveUpBooksCarousel.withContentDescription()).isNotDisplayed()
    }

    @Test
    fun testChangeFromAlreadyReadSectionToGaveUpReadSection() {

        composeTestRule.onNode(home_main_alreadyRead.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.ALREADY_READ

        composeTestRule.onNode(contentDescription_home_finishedBooksCarousel.withContentDescription()).isDisplayed()

        composeTestRule.onNode(home_main_giveUp.withContentDescription()).performClick()

        mockHomeViewModel.currentSection = BookStatusType.GIVE_UP

        composeTestRule.onNode(contentDescription_home_gaveUpBooksCarousel.withContentDescription()).isDisplayed()
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}