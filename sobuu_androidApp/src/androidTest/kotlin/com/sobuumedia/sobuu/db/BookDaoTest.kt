package com.sobuumedia.sobuu.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.book2
import com.sobuumedia.sobuu.fake_data.book3
import com.sobuumedia.sobuu.features.book.database.toBookDB
import com.sobuumedia.sobuu.features.book.database.toCreditDB
import com.sobuumedia.sobuu.features.book.database.toUserBookRatingDB
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import com.sobuumedia.sobuu.models.db_models.BookDB
import com.sobuumedia.sobuu.models.db_models.UserBookRatingDB
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.junit.runner.RunWith
import kotlin.test.Test

@RunWith(AndroidJUnit4::class)
class BookDaoTest: DaoTestCase() {

    // Test that insert a book works
    @Test
    fun testInsertingBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()

        // When
        bookDao.saveData(fakeBook)

        val result = bookDao.getAllData()

        // Then
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
    }

    // Test that insert a second book doesn't replace the first one
    @Test
    fun testInsertingSeveralBooks() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()

        // When
        bookDao.saveData(fakeBook)
        bookDao.saveData(fakeBook2)

        val result = bookDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test that insert a list of books work
    @Test
    fun testInsertingAListOfBooks() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)

        // When
        bookDao.saveDataList(booksList)

        val result = bookDao.getAllData()

        // Then
        assertTrue(result.size == 3)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[1].bookId, book2.id)
        assertEquals(result[2].bookId, book3.id)
    }

    // Test that insert the same book with updated data update the data in the db
    @Test
    fun testInsertingUpdatedBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()

        // When
        bookDao.saveData(fakeBook)

        val bookInDB = bookDao.getData(fakeBook.bookId)
        val fakeUpdatedBook = bookInDB?.copy(totalComments = 100) ?: BookDB()

        bookDao.updateData(fakeUpdatedBook)

        val result = bookDao.getAllData()

        // Then
        println("-------Result size: ${result.size}")
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[0].totalComments, 100)
    }

    // Test getting a book by the bookId
    @Test
    fun testGettingBookById() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)
        val bookId = book1.id

        // When
        bookDao.saveDataList(booksList)

        val result = bookDao.getData(bookId)

        // Then
        assertEquals(result?.bookId, book1.id)
        assertEquals(result?.isbn10, book1.isbn.first)
    }

    // Test deleting a book
    @Test
    fun testDeleteABook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)

        // When
        bookDao.saveDataList(booksList)
        bookDao.removeById(fakeBook.bookId)

        val result = bookDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test deleting all books
    @Test
    fun testDeleteAllBooks() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)

        // When
        bookDao.saveDataList(booksList)

        bookDao.removeAll()

        val result = bookDao.getAllData()

        // Then
        assertTrue(result.isEmpty())
    }

    // Test getting credits
    @Test
    fun testGettingCreditsFromBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)
        val fakeBookCredits = book1.credits.toCreditDB(bookId = book1.id)
        val fakeBook2Credits = book2.credits.toCreditDB(bookId = book2.id)
        val fakeBook3Credits = book3.credits.toCreditDB(bookId = book3.id)
        val creditsList = listOf(fakeBookCredits, fakeBook2Credits, fakeBook3Credits)
        val bookId = book2.id

        // When
        bookDao.saveDataList(booksList)
        creditDao.saveDataList(creditsList)

        val result = bookDao.getBookWithCredits(bookId)

        // Then
        assertEquals(result.book.bookId, book2.id)
        assertEquals(result.credits?.bookId, book2.id)
    }

    // Test getting user rating
    @Test
    fun testGettingUserFromBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)
        val fakeBookCredits = book1.credits.toCreditDB(bookId = book1.id)
        val fakeBook2Credits = book2.credits.toCreditDB(bookId = book2.id)
        val fakeBook3Credits = book3.credits.toCreditDB(bookId = book3.id)
        val creditsList = listOf(fakeBookCredits, fakeBook2Credits, fakeBook3Credits)
        val userRating = book1.userRating?.toUserBookRatingDB(book1.id) ?: UserBookRatingDB()
        val bookId = book1.id

        // When
        bookDao.saveDataList(booksList)
        creditDao.saveDataList(creditsList)
        bookRatingDao.saveData(userRating)

        val result = bookDao.getBookWithRating(bookId)

        // Then
        assertEquals(result.book.bookId, book1.id)
        assertEquals(result.userRating?.rating, userRating.rating)
    }

    // Test getting non user rating
    @Test
    fun testGettingNonUserFromBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)
        val fakeBookCredits = book1.credits.toCreditDB(bookId = book1.id)
        val fakeBook2Credits = book2.credits.toCreditDB(bookId = book2.id)
        val fakeBook3Credits = book3.credits.toCreditDB(bookId = book3.id)
        val creditsList = listOf(fakeBookCredits, fakeBook2Credits, fakeBook3Credits)
        val bookId = book2.id

        // When
        bookDao.saveDataList(booksList)
        creditDao.saveDataList(creditsList)

        val result = bookDao.getBookWithRating(bookId)

        // Then
        assertEquals(result.book.bookId, book2.id)
        assertEquals(result.userRating, null)
    }

    // Test getting several reviews
    @Test
    fun testGettingReviewsFromBook() = runBlocking {
        // Given
        val fakeBook = book1.toBookDB()
        val fakeBook2 = book2.toBookDB()
        val fakeBook3 = book3.toBookDB()
        val booksList = listOf(fakeBook, fakeBook2, fakeBook3)
        val fakeBookCredits = book1.credits.toCreditDB(bookId = book1.id)
        val fakeBook2Credits = book2.credits.toCreditDB(bookId = book2.id)
        val fakeBook3Credits = book3.credits.toCreditDB(bookId = book3.id)
        val creditsList = listOf(fakeBookCredits, fakeBook2Credits, fakeBook3Credits)
        val fakeBookReview = UserBookRating(
            id = "852",
            user = Profile(
                email = "",
                firstName = "",
                id = "",
                lastName = "",
                createdAt = "",
                username = "Test1"
            ),
            rating = 4.5,
            review = "Test text",
            date = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
        )
        val fakeBookReview2 = fakeBookReview.copy(id = "710", rating = 3.6, review = "Text test 2")
        val fakeBookReview3 = fakeBookReview.copy(id = "693", rating = 1.6, review = "Text test 5")
        val reviewsList = listOf(
            fakeBookReview.toUserBookRatingDB(book1.id),
            fakeBookReview2.toUserBookRatingDB(book1.id),
            fakeBookReview3.toUserBookRatingDB(book1.id)
        )
        val bookId = book1.id

        // When
        bookDao.saveDataList(booksList)
        creditDao.saveDataList(creditsList)
        bookRatingDao.saveDataList(reviewsList)

        val result = bookDao.getBookWithReviews(bookId)

        // Then
        assertEquals(result[0].allReviews.size, 3)
        assertEquals(result[0].allReviews[1].review, fakeBookReview2.review)
    }
}