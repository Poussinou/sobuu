package com.sobuumedia.sobuu.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.book2
import com.sobuumedia.sobuu.fake_data.book3
import com.sobuumedia.sobuu.fake_data.comment1
import com.sobuumedia.sobuu.fake_data.comment2
import com.sobuumedia.sobuu.fake_data.comment3
import com.sobuumedia.sobuu.features.comments.database.toCommentDB
import kotlinx.coroutines.runBlocking
import org.junit.runner.RunWith
import kotlin.test.Test

@RunWith(AndroidJUnit4::class)
class CommentDaoTest: DaoTestCase() {

    // Test that insert a comment works
    @Test
    fun testInsertingComment() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)

        // When
        commentDao.saveData(fakeComment)

        val result = commentDao.getAllData()

        // Then
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
    }

    // Test that insert a second comment doesn't replace the first one
    @Test
    fun testInsertingSeveralBooksProgress() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)
        val fakeComment2 = comment2.toCommentDB(book2.id)

        // When
        commentDao.saveData(fakeComment)
        commentDao.saveData(fakeComment2)

        val result = commentDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test that insert a list of books progress work
    @Test
    fun testInsertingAListOfBooksProgress() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)
        val fakeComment2 = comment2.toCommentDB(book2.id)
        val fakeComment3 = comment3.toCommentDB(book3.id)
        val booksList = listOf(fakeComment, fakeComment2, fakeComment3)

        // When
        commentDao.saveDataList(booksList)

        val result = commentDao.getAllData()

        // Then
        assertTrue(result.size == 3)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[1].bookId, book2.id)
        assertEquals(result[2].bookId, book3.id)
    }

    // Test that insert the same comment with updated data update the data in the db
    @Test
    fun testInsertingUpdatedComment() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)

        // When
        commentDao.saveData(fakeComment)

        val commentInDB = commentDao.getDataByBookId(book1.id)
        val fakeUpdatedComment = commentInDB[0].copy(text = "Updated text")

        commentDao.updateData(fakeUpdatedComment)

        val result = commentDao.getAllData()

        // Then
        println("-------Result size: ${result.size}")
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[0].percentage, 47.4)
        assertEquals(result[0].text, "Updated text")
    }

    // Test getting a comment by the bookId
    @Test
    fun testGettingCommentById() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)
        val fakeComment2 = comment2.toCommentDB(book2.id)
        val fakeComment3 = comment3.toCommentDB(book3.id)
        val booksList = listOf(fakeComment, fakeComment2, fakeComment3)
        val bookId = book3.id

        // When
        commentDao.saveDataList(booksList)

        val result = commentDao.getDataByBookId(bookId)

        // Then
        assertEquals(result[0].bookId, book3.id)
        assertEquals(result[0].percentage, 50.5)
        assertTrue(result[0].hasSpoilers)
    }

    // Test deleting a comment
    @Test
    fun testDeleteAComment() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)
        val fakeComment2 = comment2.toCommentDB(book2.id)
        val fakeComment3 = comment3.toCommentDB(book3.id)
        val booksList = listOf(fakeComment, fakeComment2, fakeComment3)

        // When
        commentDao.saveDataList(booksList)
        commentDao.removeById(comment1.id)

        val result = commentDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test deleting all books progress
    @Test
    fun testDeleteAllBooksProgress() = runBlocking {
        // Given
        val fakeComment = comment1.toCommentDB(book1.id)
        val fakeComment2 = comment2.toCommentDB(book2.id)
        val fakeComment3 = comment3.toCommentDB(book3.id)
        val booksList = listOf(fakeComment, fakeComment2, fakeComment3)

        // When
        commentDao.saveDataList(booksList)

        commentDao.removeAll()

        val result = commentDao.getAllData()

        // Then
        assertTrue(result.isEmpty())
    }
}