package com.sobuumedia.sobuu.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.sobuumedia.sobuu.SobuuDB
import com.sobuumedia.sobuu.core.database.BookDBApi
import com.sobuumedia.sobuu.core.database.BookProgressDBApi
import com.sobuumedia.sobuu.core.database.BookRatingDBApi
import com.sobuumedia.sobuu.core.database.CommentDBApi
import com.sobuumedia.sobuu.core.database.CreditsDBApi
import junit.framework.TestCase
import org.junit.After
import org.junit.Before

open class DaoTestCase: TestCase() {

    // Get reference to the SobuuDB and the Dao Classes
    private lateinit var db: SobuuDB

    lateinit var bookDao: BookDBApi
    lateinit var bookProgressDao: BookProgressDBApi
    lateinit var bookRatingDao: BookRatingDBApi
    lateinit var commentDao: CommentDBApi
    lateinit var creditDao: CreditsDBApi


    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        db = Room.inMemoryDatabaseBuilder(context, SobuuDB::class.java).build()
        bookDao = db.getBookDao()
        bookProgressDao = db.getBookProgressDao()
        bookRatingDao = db.getBookRatingsDao()
        creditDao = db.getCreditsDao()
        commentDao = db.getCommentDao()
    }

    @After
    fun closeDB() {
        db.close()
    }
}