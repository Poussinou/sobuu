package com.sobuumedia.sobuu.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.book2
import com.sobuumedia.sobuu.fake_data.book3
import com.sobuumedia.sobuu.fake_data.bookProgressFinished
import com.sobuumedia.sobuu.fake_data.bookProgressReadingAlmostFinished
import com.sobuumedia.sobuu.fake_data.bookProgressReadingMiddle
import com.sobuumedia.sobuu.features.book.database.toBookProgressDB
import com.sobuumedia.sobuu.models.db_models.BookProgressDB
import kotlinx.coroutines.runBlocking
import org.junit.runner.RunWith
import kotlin.test.Test

@RunWith(AndroidJUnit4::class)
class BookProgressDaoTest: DaoTestCase() {

    // Test that insert a book progress works
    @Test
    fun testInsertingBookProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)

        // When
        bookProgressDao.saveData(fakeBookProgress)

        val result = bookProgressDao.getAllData()

        // Then
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
    }

    // Test that insert a second book progress doesn't replace the first one
    @Test
    fun testInsertingSeveralBooksProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)
        val fakeBookProgress2 = bookProgressReadingMiddle.toBookProgressDB(book2.id)

        // When
        bookProgressDao.saveData(fakeBookProgress)
        bookProgressDao.saveData(fakeBookProgress2)

        val result = bookProgressDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test that insert a list of books progress work
    @Test
    fun testInsertingAListOfBooksProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)
        val fakeBookProgress2 = bookProgressReadingAlmostFinished.toBookProgressDB(book2.id)
        val fakeBookProgress3 = bookProgressFinished.toBookProgressDB(book3.id)
        val booksList = listOf(fakeBookProgress, fakeBookProgress2, fakeBookProgress3)

        // When
        bookProgressDao.saveDataList(booksList)

        val result = bookProgressDao.getAllData()

        // Then
        assertTrue(result.size == 3)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[1].bookId, book2.id)
        assertEquals(result[2].bookId, book3.id)
    }

    // Test that insert the same book progress with updated data update the data in the db
    @Test
    fun testInsertingUpdatedBookProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)

        // When
        bookProgressDao.saveData(fakeBookProgress)

        val bookProgressInDB = bookProgressDao.getDataByBookId(book1.id)
        val fakeUpdatedBookProgress = bookProgressInDB?.copy(percentage = 88.7, page = 368) ?: BookProgressDB()

        bookProgressDao.updateData(fakeUpdatedBookProgress)

        val result = bookProgressDao.getAllData()

        // Then
        println("-------Result size: ${result.size}")
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[0].percentage, 88.7)
    }

    // Test getting a book progress by the bookId
    @Test
    fun testGettingBookProgressById() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)
        val fakeBookProgress2 = bookProgressReadingAlmostFinished.toBookProgressDB(book2.id)
        val fakeBookProgress3 = bookProgressFinished.toBookProgressDB(book3.id)
        val booksList = listOf(fakeBookProgress, fakeBookProgress2, fakeBookProgress3)
        val bookId = book3.id

        // When
        bookProgressDao.saveDataList(booksList)

        val result = bookProgressDao.getDataByBookId(bookId)

        // Then
        assertEquals(result?.bookId, book3.id)
        assertEquals(result?.percentage, 100.0)
    }

    // Test deleting a book progress
    @Test
    fun testDeleteABookProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)
        val fakeBookProgress2 = bookProgressReadingAlmostFinished.toBookProgressDB(book2.id)
        val fakeBookProgress3 = bookProgressFinished.toBookProgressDB(book3.id)
        val booksList = listOf(fakeBookProgress, fakeBookProgress2, fakeBookProgress3)

        // When
        bookProgressDao.saveDataList(booksList)
        bookProgressDao.removeById(fakeBookProgress.bookId)

        val result = bookProgressDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test deleting all books progress
    @Test
    fun testDeleteAllBooksProgress() = runBlocking {
        // Given
        val fakeBookProgress = bookProgressReadingMiddle.toBookProgressDB(book1.id)
        val fakeBookProgress2 = bookProgressReadingAlmostFinished.toBookProgressDB(book2.id)
        val fakeBookProgress3 = bookProgressFinished.toBookProgressDB(book3.id)
        val booksList = listOf(fakeBookProgress, fakeBookProgress2, fakeBookProgress3)

        // When
        bookProgressDao.saveDataList(booksList)

        bookProgressDao.removeAll()

        val result = bookProgressDao.getAllData()

        // Then
        assertTrue(result.isEmpty())
    }
}