package com.sobuumedia.sobuu.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.fake_data.book2
import com.sobuumedia.sobuu.fake_data.book3
import com.sobuumedia.sobuu.features.book.database.toCreditDB
import com.sobuumedia.sobuu.models.db_models.CreditsDB
import kotlinx.coroutines.runBlocking
import org.junit.runner.RunWith
import kotlin.test.Test

@RunWith(AndroidJUnit4::class)
class CreditsDBTest: DaoTestCase() {

    // Test that insert credits works
    @Test
    fun testInsertingCredit() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)

        // When
        creditDao.saveData(fakeCredit)

        val result = creditDao.getAllData()

        // Then
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
    }

    // Test that insert a second credits doesn't replace the first one
    @Test
    fun testInsertingSeveralCredits() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)
        val fakeCredit2 = book2.credits.toCreditDB(book2.id)

        // When
        creditDao.saveData(fakeCredit)
        creditDao.saveData(fakeCredit2)

        val result = creditDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test that insert a list of credits work
    @Test
    fun testInsertingAListOfCredits() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)
        val fakeCredit2 = book2.credits.toCreditDB(book2.id)
        val fakeCredit3 = book3.credits.toCreditDB(book3.id)
        val creditsList = listOf(fakeCredit, fakeCredit2, fakeCredit3)

        // When
        creditDao.saveDataList(creditsList)

        val result = creditDao.getAllData()

        // Then
        assertTrue(result.size == 3)
        assertEquals(result[0].bookId, book1.id)
        assertEquals(result[1].bookId, book2.id)
        assertEquals(result[2].bookId, book3.id)
    }

    // Test that insert the same credits with updated data update the data in the db
    @Test
    fun testInsertingUpdatedCredit() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)

        // When
        creditDao.saveData(fakeCredit)

        val creditInDB = creditDao.getData(fakeCredit.bookId)
        val fakeUpdatedBook = creditInDB?.copy(illustrators = listOf("Test McTester")) ?: CreditsDB()

        creditDao.updateData(fakeUpdatedBook)

        val result = creditDao.getAllData()

        // Then
        println("-------Result size: ${result.size}")
        assertTrue(result.size == 1)
        assertEquals(result[0].bookId, book1.id)
        assertTrue(result[0].illustrators.contains("Test McTester"))
    }

    // Test getting a credits by the bookId
    @Test
    fun testGettingCreditById() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)
        val fakeCredit2 = book2.credits.toCreditDB(book2.id)
        val fakeCredit3 = book3.credits.toCreditDB(book3.id)
        val creditsList = listOf(fakeCredit, fakeCredit2, fakeCredit3)
        val bookId = book1.id

        // When
        creditDao.saveDataList(creditsList)

        val result = creditDao.getData(bookId)

        // Then
        assertEquals(result?.bookId, book1.id)
        assertEquals(result?.translators?.get(0), book1.credits.translators[0])
    }

    // Test deleting a credits
    @Test
    fun testDeleteACredit() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)
        val fakeCredit2 = book2.credits.toCreditDB(book2.id)
        val fakeCredit3 = book3.credits.toCreditDB(book3.id)
        val creditsList = listOf(fakeCredit, fakeCredit2, fakeCredit3)

        // When
        creditDao.saveDataList(creditsList)
        creditDao.removeById(fakeCredit.bookId)

        val result = creditDao.getAllData()

        // Then
        assertTrue(result.size == 2)
    }

    // Test deleting all credits
    @Test
    fun testDeleteAllCredits() = runBlocking {
        // Given
        val fakeCredit = book1.credits.toCreditDB(book1.id)
        val fakeCredit2 = book2.credits.toCreditDB(book2.id)
        val fakeCredit3 = book3.credits.toCreditDB(book3.id)
        val creditsList = listOf(fakeCredit, fakeCredit2, fakeCredit3)

        // When
        creditDao.saveDataList(creditsList)

        creditDao.removeAll()

        val result = creditDao.getAllData()

        // Then
        assertTrue(result.isEmpty())
    }
}