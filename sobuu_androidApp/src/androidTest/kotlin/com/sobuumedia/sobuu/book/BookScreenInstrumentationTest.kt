package com.sobuumedia.sobuu.book

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sobuumedia.sobuu.android.book.BookScreen
import com.sobuumedia.sobuu.android.book.BookViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.fake_data.book1
import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar
import tools.fastlane.screengrab.locale.LocaleTestRule

@RunWith(AndroidJUnit4::class)
class BookScreenInstrumentationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    /**
     * Used to translate the screen for the Fastlane screenshots
     */
    @get:Rule
    val localeTestRule = LocaleTestRule()

    private val mockBookRepo = mockk<IBookRepository>()
    private lateinit var mockBookViewModel: BookViewModel

    @Before
    fun setUp() {
        coEvery { mockBookRepo.getBookByIdFromDB(any()) } returns BookResult.Success(book1)

        mockBookViewModel = BookViewModel(mockBookRepo, logs = NapierLogs())

        composeTestRule.setContent {
            SobuuTheme {
                BookScreen(
                    navigateBackOrHome = {},
                    navigateToHomeScreen = {},
                    navigateToBookCoverScreen = {},
                    bookId = "1235",
                    bookViewModel = mockBookViewModel
                )
            }
        }

        Thread.sleep(120)
    }

    @Test
    fun takeScreenshot() {
        Thread.sleep(500)
        Screengrab.screenshot("book")
    }


    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeAll() {
            CleanStatusBar.enableWithDefaults()
        }

        @JvmStatic
        @AfterClass
        fun afterAll() {
            CleanStatusBar.disable()
        }
    }
}