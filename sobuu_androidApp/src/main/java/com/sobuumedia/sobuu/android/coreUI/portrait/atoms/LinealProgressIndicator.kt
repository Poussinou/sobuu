package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.times
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun LinealProgressIndicator(
    modifier: Modifier = Modifier,
    progress: Double,
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        val barWidth: Dp = 250.dp
        val barHeight: Dp = 35.dp
        val startCornerRadius: Dp = if(progress < 3) 0.dp else 10.dp
        val endCornerRadius: Dp = 10.dp
        val activeProgressBarWidth: Dp = (progress * barWidth) / 100

        val backBarColor = MaterialTheme.colorScheme.primary
        val frontBarColor = MaterialTheme.colorScheme.error
        val textColor = MaterialTheme.colorScheme.background

        Box(
            modifier = Modifier
                .height(barHeight)
                .width(barWidth)
                .clip(RoundedCornerShape(
                    topStart = startCornerRadius,
                    bottomStart = startCornerRadius,
                    bottomEnd = endCornerRadius,
                    topEnd = endCornerRadius,
                ))
                .background(backBarColor),
        )

        Box(
            modifier = Modifier
                .height(barHeight)
                .width(activeProgressBarWidth)
                .clip(RoundedCornerShape(
                    topStart = startCornerRadius,
                    bottomStart = startCornerRadius,
                    bottomEnd = endCornerRadius,
                    topEnd = endCornerRadius,
                ))
                .background(frontBarColor)
                .align(Alignment.CenterStart),
        )

        Text(
            text = "$progress%",
            style = Typography.bodyLarge.copy(color = textColor)
        )
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CurrentlyReadingViewDarkPreview() {
    SobuuTheme {
        LinealProgressIndicator(progress = 6.50)
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun CurrentlyReadingViewPreview() {
    SobuuTheme {
        LinealProgressIndicator(progress = 34.65)
    }
}