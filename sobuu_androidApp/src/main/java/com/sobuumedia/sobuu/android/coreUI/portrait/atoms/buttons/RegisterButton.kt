package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createNewAccount
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun RegisterButton(onClick: () -> Unit) {
    val textColor = MaterialTheme.colorScheme.background
    val contentColor = MaterialTheme.colorScheme.secondary
    val disabledColor = MaterialTheme.colorScheme.tertiary

    FilledTonalButton(
        modifier = Modifier.fillMaxWidth(),
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            containerColor = contentColor,
            contentColor = textColor,
            disabledContainerColor = disabledColor,
            disabledContentColor = textColor,
        )
    ) {
        Text(
            text = authorization_auth_createNewAccount.stringResource(context = LocalContext.current),
            modifier = Modifier.semantics { this.contentDescription = authorization_auth_createNewAccount.stringResource() },
            style = Typography.bodyLarge.copy(color = textColor),
            textAlign = TextAlign.Center,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun RegisterButtonPreview() {
    RegisterButton(
        onClick = {}
    )
}