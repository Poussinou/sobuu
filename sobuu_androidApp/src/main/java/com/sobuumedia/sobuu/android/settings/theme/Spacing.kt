package com.sobuumedia.sobuu.android.settings.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val compactNone: Dp = 0.dp
val compactExtraSmall: Dp = 2.dp
val compactSmall: Dp = 4.dp
val compactDefault: Dp = 8.dp
val compactMedium: Dp = 16.dp
val compactLarge: Dp = 32.dp
val compactExtraLarge: Dp = 64.dp

val mediumNone: Dp = 0.dp
val mediumExtraSmall: Dp = 2.dp
val mediumSmall: Dp = 4.dp
val mediumDefault: Dp = 8.dp
val mediumMedium: Dp = 16.dp
val mediumLarge: Dp = 32.dp
val mediumExtraLarge: Dp = 64.dp

val expandedNone: Dp = 0.dp
val expandedExtraSmall: Dp = 2.dp
val expandedSmall: Dp = 4.dp
val expandedDefault: Dp = 8.dp
val expandedMedium: Dp = 16.dp
val expandedLarge: Dp = 32.dp
val expandedExtraLarge: Dp = 64.dp

val roundedCorner: Dp = 10.dp

data class Spacing(
    val none: Dp = compactNone,
    val extraSmall: Dp = compactExtraSmall,
    val small: Dp = compactSmall,
    val default: Dp = compactDefault,
    val medium: Dp = compactMedium,
    val large: Dp = compactLarge,
    val extraLarge: Dp = compactExtraLarge,
)


val LocalSpacing = compositionLocalOf { Spacing() }

val MaterialTheme.spacing: Spacing
    @Composable
    @ReadOnlyComposable
    get() = LocalSpacing.current