package com.sobuumedia.sobuu.android.authentication.login

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class LoginViewModel(private val auth: IAuthenticationRepository): ViewModel() {
    var state by mutableStateOf(LoginState())

    private val resultChannel = Channel<AuthenticationResult<Unit>>()
    val authResult = resultChannel.receiveAsFlow()

    init {
        authentication()
    }

    fun onEvent(event: LoginUIEvent) {
        when(event) {
            is LoginUIEvent.LoginPasswordChanged -> {
                state = state.copy(loginPassword = event.value)
            }
            is LoginUIEvent.LoginUsernameChanged -> {
                state = state.copy(error = null, loginUsername = event.value)
            }
            is LoginUIEvent.loginUser -> login()
            is LoginUIEvent.removeErrorState -> removeErrorState()
        }
    }

    fun handleError(error: AuthenticationError?) {
        state = state.copy(error = error)
    }

    private fun login() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.loginUser(
                username = state.loginUsername.lowercase(),
                password = state.loginPassword
            )

            resultChannel.send(result)

            state = state.copy(isLoading = false)
        }
    }

    private fun authentication() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.authenticate()
            resultChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun removeErrorState() {
        viewModelScope.launch {
            state = state.copy(error = null)
        }
    }
}