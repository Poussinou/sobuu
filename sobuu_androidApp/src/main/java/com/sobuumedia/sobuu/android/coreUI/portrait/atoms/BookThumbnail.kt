package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun BookThumbnail(
    modifier: Modifier = Modifier,
    imageURL: String
) {
    AsyncImage(
        modifier = Modifier
            .fillMaxSize()
            .then(modifier),
        model = imageURL,
        placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
        contentDescription = null
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun BookThumbnailPreview() {
    SobuuTheme {
        BookThumbnail(
            imageURL = ""
        )
    }
}