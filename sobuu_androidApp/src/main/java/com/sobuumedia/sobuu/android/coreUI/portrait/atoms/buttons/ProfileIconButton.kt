package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import android.content.res.Configuration
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_profileButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun ProfileIconButton(
    modifier: Modifier = Modifier,
    navigateToProfileScreen: () -> Unit
) {

    val profileIconColor = MaterialTheme.colorScheme.tertiary
    val iconSize = 52.dp

    IconButton(
        modifier = Modifier
            .fillMaxWidth()
            .height(iconSize)
            .semantics {
                this.contentDescription = contentDescription_topBar_profileButton.stringResource()
            }
            .then(modifier),
        onClick = {
            navigateToProfileScreen()
        }
    ) {
        Icon(
            modifier = Modifier
                .fillMaxWidth()
                .height(iconSize),
            imageVector = Icons.Filled.AccountCircle,
            contentDescription = Icons.Filled.AccountCircle.name,
            tint = profileIconColor,
        )
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ProfileIconButtonPreview() {
    SobuuTheme {
        ProfileIconButton(
            navigateToProfileScreen = {}
        )
    }
}