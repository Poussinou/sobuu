package com.sobuumedia.sobuu.android.book

sealed class BookUIEvent {
    object DisplayUserShelves: BookUIEvent()
    object CancelAllErrors: BookUIEvent()
    object CloseReportedMissingDataDialog: BookUIEvent()
    data class AddBookToShelf(val bookId: String, val shelfId: String): BookUIEvent()
    data class StartToReadBook(val bookId: String, val bookTitle: String, val bookISBN: String?): BookUIEvent()
    data class ReportMissingDataInBook(val bookId: String, val bookTitle: String, val bookISBN: String?): BookUIEvent()
    data class StartScreen(val bookId: String): BookUIEvent()
}