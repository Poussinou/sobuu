package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Book
import androidx.compose.material.icons.filled.Groups
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_books
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_challenges
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_friends
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

enum class BottomMenuItems {
    Books, Shelves, Friends, Challenges
}

@Composable
fun BottomMenuBar(
    modifier: Modifier = Modifier,
    displayLabels: Boolean = true,
    currentScreen: String? = ""
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.primary
    val selectedBackgroundColor = MaterialTheme.colorScheme.error
    val textColor = MaterialTheme.colorScheme.background
    val selectedTextColor = MaterialTheme.colorScheme.secondary
    val iconSize = 36.dp
    val bottomMenuHeight = 90.dp
    val selectedMenuItem = getMenuItemFromScreen(currentScreen)

    NavigationBar(
        modifier = Modifier.fillMaxWidth()
            .height(bottomMenuHeight)
            .then(modifier),
        containerColor = backgroundColor,
    ) {
        val config = LocalConfiguration.current
        val screenWidth = config.screenWidthDp.dp
        val itemWidth = screenWidth / 4

        Column(
            modifier = Modifier
                .background(if (selectedMenuItem == BottomMenuItems.Books) selectedBackgroundColor else backgroundColor)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(ShelvesScreenDestination)
//                    currentRoute = ShelvesScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Book,
                contentDescription = Icons.Filled.Book.name,
                modifier = Modifier.size(iconSize),
                tint = if (selectedMenuItem == BottomMenuItems.Books) selectedTextColor else textColor
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_books.stringResource(context = context),
                    style = Typography.bodySmall.copy(color = if (selectedMenuItem == BottomMenuItems.Books) selectedTextColor else textColor)
                )
            }
        }

        Column(
            modifier = Modifier
                .background(if (selectedMenuItem == BottomMenuItems.Shelves) selectedBackgroundColor else backgroundColor)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(ShelvesScreenDestination)
//                    currentRoute = ShelvesScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_bookshelf),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
//                tint = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = SharedRes.strings.home_bottomMenuLabels_shelves.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
//                        fontSize = if (currentRoute == ShelvesScreenDestination.route) 16.sp else 12.sp,
//                        color = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background,
//                        fontWeight = if (currentRoute == ShelvesScreenDestination.route) FontWeight.Medium else FontWeight.Normal,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(if (selectedMenuItem == BottomMenuItems.Friends) selectedBackgroundColor else backgroundColor)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Groups,
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_friends.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(if (selectedMenuItem == BottomMenuItems.Challenges) selectedBackgroundColor else backgroundColor)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_trophy),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )

            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_challenges.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }
    }
}

private fun getMenuItemFromScreen(screen: String?): BottomMenuItems {
    return if (screen != null && screen.equals("shelves")) {
        BottomMenuItems.Shelves
    } else {
        BottomMenuItems.Books
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BottomMenuBarPreview() {
    SobuuTheme {
        BottomMenuBar()
    }
}