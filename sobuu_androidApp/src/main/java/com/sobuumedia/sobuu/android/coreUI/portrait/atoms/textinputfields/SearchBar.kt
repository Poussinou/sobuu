package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_searchBook
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextMedium
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchBar(
    modifier: Modifier = Modifier,
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearText: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit
) {
    val focusManager = LocalFocusManager.current
    val focused = remember { mutableStateOf(false) }
    val keyboardController = LocalSoftwareKeyboardController.current

    val textColor = MaterialTheme.colorScheme.secondary
    val hintColor = MaterialTheme.colorScheme.tertiary
    val background = MaterialTheme.colorScheme.background
    val errorColor = MaterialTheme.colorScheme.error

    ProvideTextStyle(value = Typography.bodyMedium.copy(color = textColor)) {
        OutlinedTextField(
            value = searchFieldValue,
            onValueChange = onSearchFieldValueChange,
            modifier = Modifier
                .background(
                    shape = RoundedCornerShape(5.dp),
                    color = background,
                )
                .fillMaxWidth()
                .semantics { this.contentDescription = searchFieldValue }
                .then(modifier)
                .onFocusChanged { focusState ->
                    focused.value = focusState.isFocused
                    onSearchFieldFocusChange(focusState.isFocused)
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = textColor,
                unfocusedBorderColor = textColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
            keyboardActions = KeyboardActions(
                onSearch = {
                    onSearchButtonClick()
                    keyboardController?.hide()
                }
            ),
            placeholder = {
                PlaceholderTextMedium(placeholderText = home_main_searchBook.stringResource(context = LocalContext.current))
            },
            shape = RoundedCornerShape(5.dp),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = Icons.Filled.Search.name,
                    tint = hintColor,
                )
            },
            trailingIcon = if(focused.value) {
                {
                    IconButton(
                        onClick = {
                            if (searchFieldValue.isBlank()) {
                                focusManager.clearFocus()
                            } else {
                                clearText()
                            }
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Close,
                            tint = textColor,
                            contentDescription = Icons.Filled.Close.name,
                        )
                    }
                }
            } else {
                {}
            }
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun SearchBarPreview() {
    SobuuTheme {
        SearchBar(
            searchFieldValue = "",
            onSearchFieldValueChange = {},
            onSearchButtonClick = {},
            clearText = {},
            onSearchFieldFocusChange = {}
        )
    }
}