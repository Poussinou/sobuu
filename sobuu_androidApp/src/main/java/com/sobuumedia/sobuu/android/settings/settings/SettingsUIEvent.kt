package com.sobuumedia.sobuu.android.settings.settings

import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions

sealed class SettingsUIEvent {
    object displayThemeModeDialog: SettingsUIEvent()
    object displayAnalyticsDialog: SettingsUIEvent()
    object getThemeMode: SettingsUIEvent()
    object getAnalyticsEnabled: SettingsUIEvent()

    data class SetAnalyticsEnabled(val value: Boolean): SettingsUIEvent()
    data class SetThemeMode(val value: ThemeOptions): SettingsUIEvent()
}