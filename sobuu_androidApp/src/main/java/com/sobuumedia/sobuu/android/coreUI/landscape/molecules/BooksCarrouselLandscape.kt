package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerScope
import androidx.compose.foundation.pager.PagerState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun BooksCarouselLandscape(
    pagerState: PagerState,
    count: Int,
    content: @Composable PagerScope.(page: Int) -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val verticalPadding = 16.dp
    val horizontalPadding = 100.dp
    val pageSpacing = 0.dp

    HorizontalPager(
        state = pagerState,
        beyondBoundsPageCount = count,
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
        contentPadding = PaddingValues(
            horizontal = horizontalPadding,
            vertical = verticalPadding
        ),
        pageSpacing = pageSpacing,
    ) { page ->
        content(page)
    }
}