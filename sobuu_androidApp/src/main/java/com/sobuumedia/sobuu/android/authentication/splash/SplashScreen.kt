package com.sobuumedia.sobuu.android.authentication.splash

import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.general_appName
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_noConnection
import com.sobuumedia.sobuu.android.navigation.Screen
import com.sobuumedia.sobuu.android.settings.theme.Solway
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import org.koin.androidx.compose.koinViewModel

@Composable
fun SplashScreen(
    nav: NavController?,
    viewModel: SplashViewModel = koinViewModel()
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primary),
        contentAlignment = Alignment.Center,
    ) {
        val state = viewModel.state
        val context = LocalContext.current

        LaunchedEffect(viewModel, context) {
            if (!state.disconnected) {
                viewModel.authResult.collect { result ->
                    when (result) {
                        is AuthenticationResult.Authorized -> {
                            nav?.navigate(Screen.HomeScreen.route) {
                                popUpTo(Screen.SplashScreen.route) {
                                    inclusive = true
                                }
                            }
                        }
                        is AuthenticationResult.Unauthorized -> {
                            nav?.navigate(Screen.LoginScreen.route) {
                                popUpTo(Screen.SplashScreen.route) {
                                    inclusive = true
                                }
                            }
                        }
                        else -> {
                            viewModel.handleError(result.error)
                        }
                    }
                }
            }
        }

        Column(
            modifier = Modifier.background(MaterialTheme.colorScheme.primary),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            SplashContent()

            if (state.disconnected) {
                Spacer(modifier = Modifier.height(25.dp))

                Text(
                    general_errors_noConnection.stringResource(context = context),
                    modifier = Modifier
                        .padding(start = 32.dp, end = 32.dp),
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.error,
                        fontFamily = SourceSans,
                        fontSize = 20.sp
                    ),
                    textAlign = TextAlign.Center,
                )

                Spacer(modifier = Modifier.height(5.dp))
            } else if (state.error != null) {
                Spacer(modifier = Modifier.height(25.dp))

                Text(
                    getStringFromError(error = state.error),
                    modifier = Modifier
                        .padding(start = 32.dp, end = 32.dp),
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.error,
                        fontFamily = SourceSans,
                        fontSize = 20.sp
                    ),
                    textAlign = TextAlign.Center,
                )

                Spacer(modifier = Modifier.height(5.dp))
            } else {
                Spacer(modifier = Modifier.height(45.dp))
            }
        }
    }
}

@Composable
fun SplashContent() {
    Box(
        modifier = Modifier.background(MaterialTheme.colorScheme.primary),
        contentAlignment = Alignment.Center,
    ) {
        Pulsating(
            pulseFraction = 1.2f
        ) {
            Text(
                general_appName.stringResource(context = LocalContext.current),
                modifier = Modifier
                    .align(Alignment.Center)
                    .background(MaterialTheme.colorScheme.primary),
                style = TextStyle(
                    fontSize = 80.sp,
                    fontFamily = Solway,
                    color = MaterialTheme.colorScheme.secondary,
                ),
                textAlign = TextAlign.Center,
            )
        }
    }
}

@Composable
fun Pulsating(pulseFraction: Float = 1.2f, content: @Composable () -> Unit) {
    val infiniteTransition = rememberInfiniteTransition()

    val scale by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = pulseFraction,
        animationSpec = infiniteRepeatable(
            animation = tween(1000),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(modifier = Modifier.scale(scale)) {
        content()
    }
}

@Composable
fun getStringFromError(error: AuthenticationError?): String {
    val context = LocalContext.current
    return when (error) {
        is AuthenticationError.TimeOutError -> {
            authorization_errors_timeout.stringResource(context = context)
        }
        else -> {
            authorization_errors_unknown.stringResource(context = context)
        }
    }
}

@Preview(showSystemUi = true, showBackground = true, group = "Done")
@Composable
fun ComposableSplashContentPreview() {
    SplashContent()
}