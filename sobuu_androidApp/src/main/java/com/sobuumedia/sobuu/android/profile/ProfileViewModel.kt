package com.sobuumedia.sobuu.android.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.features.profile.remote.ProfileResult
import com.sobuumedia.sobuu.features.profile.repository.IProfileRepository
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val profileRepo: IProfileRepository
): ViewModel() {

    var state by mutableStateOf(ProfileState())
        private set

    init {
        loadUserProfile()
    }

    fun onEvent(event: ProfileUIEvent) {
        when(event) {
            ProfileUIEvent.startScreen -> loadUserProfile()
            ProfileUIEvent.logout -> logout()
        }
    }

    private fun loadUserProfile() {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true,
                error = null,
            )

            state = when(val result = profileRepo.getUserProfile()) {
                is ProfileResult.Error -> {
                    state.copy(
                        isLoading = false,
                        error = result.error
                    )
                }
                is ProfileResult.Success -> {
                    state.copy(
                        profileInfo = result.data,
                        isLoading = false,
                    )
                }
            }
        }
    }

    private fun logout() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            profileRepo.logout()

            state = state.copy(isLoading = false, logout = true)
        }
    }
}