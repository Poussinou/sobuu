package com.sobuumedia.sobuu.android.utils

import android.content.Context
import android.net.Uri
import androidx.compose.runtime.Composable
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_selectDate
import com.sobuumedia.sobuu.StringsHelper
import com.sobuumedia.sobuu.android.SobuuApp
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.authentication.TextType
import dev.icerock.moko.resources.StringResource
import java.io.File
import java.io.FileOutputStream
import java.sql.Date
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.Locale

fun <T> Iterable<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
    return map {
        if (block(it)) newValue else it
    }
}

fun Uri.getImageFile(context: Context): File {

    val file = File(context.filesDir, "image.png")
    val inputStream = context.contentResolver.openInputStream(this)
    val outputStream = FileOutputStream(file)

    inputStream!!.copyTo(outputStream)
    inputStream.close()

    return file
}

fun SharedRes.strings.getAllGenres(): List<String> {
    return listOf(
        genres_Fiction.stringResource(),
        genres_Study_Aids.stringResource(),
        genres_Romance.stringResource(),
        genres_Historical.stringResource(),
        genres_Mystery.stringResource(),
        genres_Historical_Fiction.stringResource(),
        genres_Fantasy.stringResource(),
        genres_Dark.stringResource(),
        genres_Dark_Fantasy.stringResource(),
        genres_Crime.stringResource(),
        genres_LGBT.stringResource(),
        genres_Contemporary.stringResource(),
        genres_Adult.stringResource(),
        genres_Classic.stringResource(),
        genres_Adventure.stringResource(),
        genres_Sports.stringResource(),
        genres_Short_Stories.stringResource(),
        genres_Novella.stringResource(),
        genres_Novels.stringResource(),
        genres_Suspense.stringResource(),
        genres_Chick_Lit.stringResource(),
        genres_Music.stringResource(),
        genres_Young_Adult.stringResource(),
        genres_Epic_Fantasy.stringResource(),
        genres_Epic.stringResource(),
        genres_High_Fantasy.stringResource(),
        genres_Magic.stringResource(),
        genres_Adult_Fiction.stringResource(),
        genres_War.stringResource(),
        genres_Westerns.stringResource(),
        genres_Anthologies.stringResource(),
        genres_Queer.stringResource(),
        genres_Magical_Realism.stringResource(),
        genres_Thriller.stringResource(),
        genres_Time_Travel.stringResource(),
        genres_Science_Fiction.stringResource(),
        genres_Science_Fiction_Fantasy.stringResource(),
        genres_Space.stringResource(),
        genres_Speculative_Fiction.stringResource(),
    )
}

/**
 * Convert a string into a Text type for the legal documents.
 * If the text is not one of the three options, return LICENSE, as default
 */
fun String?.toTextType(): TextType {
    return when {
        this.equals(TextType.PRIVACY_POLICY.name) -> TextType.PRIVACY_POLICY
        this.equals(TextType.TERMS_AND_CONDITIONS.name) -> TextType.TERMS_AND_CONDITIONS
        else -> TextType.LICENSES
    }
}

/**
 * Convert a string into a Email type for the screen shown after sending an emai
 * There are only two options, being the default the verification option
 */
fun String?.toEmailType(): EmailType {
    return if (this.equals(EmailType.RESET_PASSWORD.name)) {
        EmailType.RESET_PASSWORD
    } else {
        EmailType.VERIFICATION
    }
}

/**
 * Extension function to get easily the shared texts from MOKO to use it
 * in a composable
 */
@Composable
fun StringResource.stringResource(context: Context, vararg args: Any): String {
    return StringsHelper(context).get(this, args.toList())
}

/**
 * Extension function to get easily the shared texts from MOKO to use it
 * in a no composable
 */
fun StringResource.stringResource(vararg args: Any): String {
    return StringsHelper(SobuuApp.context).get(this, args.toList())
}

/**
 * Extension function to get a date formatted with the current locale from millis
 */
fun Long?.getDateFromMillis(): String {
    return if (this == null) {
        addBook_main_selectDate.stringResource()
    } else {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return dateFormat.format(Date(this))
    }
}

/**
 * Extension function to calculate the page from the percentage
 */
fun Double.getPage(totalPages: Int): Int {
    val page = (this / 100.0) * totalPages
    return page.toInt() + 1
}

/**
 * Extension function to calculate the percentage from the page
 */
fun Int.getPercentage(totalPages: Int): Double {
    val percentage = (this.toDouble() / totalPages.toDouble()) * 100.0
    return if (percentage > 100.0) 100.0 else percentage
}

/**
 * Extension function to give only two numbers after a point in a decimal number passed as string
 */
fun String.limitToTwoDecimals(): String {
    return this.substring(0, this.indexOf('.') + 3)
}

/**
 * Extension function to give only two numbers after a point in a decimal number passed as double
 */
fun Double.limitToTwoDecimals(): String {
    val dec = DecimalFormat("#.##")
    val limitedNumber = dec.format(this)

    return if(limitedNumber.contains(',')) limitedNumber.replace(',', '.') else limitedNumber
}