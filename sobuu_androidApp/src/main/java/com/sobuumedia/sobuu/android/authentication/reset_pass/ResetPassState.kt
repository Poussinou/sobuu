package com.sobuumedia.sobuu.android.authentication.reset_pass

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError

data class ResetPassState(
    val isLoading: Boolean = false,
    val forgotEmail: String = "",
    val error: AuthenticationError? = null,
)
