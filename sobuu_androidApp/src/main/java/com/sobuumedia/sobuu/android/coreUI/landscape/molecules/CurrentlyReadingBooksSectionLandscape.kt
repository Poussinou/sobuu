package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.lerp
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlin.math.absoluteValue

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CurrentlyReadingBooksSectionLandscape(
    bookList: List<BookWithProgress>?,
    onClick: (String) -> Unit,
) {
    val pagerState = rememberPagerState(pageCount = { bookList?.size ?: 0 })

    val colorTextEmptyState = MaterialTheme.colorScheme.primary
    val colorBackgroundTextEmptyState = MaterialTheme.colorScheme.background
    val emptyTextPadding = 80.dp
    val textEmptyState =
        SharedRes.strings.home_currentlyReading_noReadingBookText.stringResource(context = LocalContext.current)

    if (!bookList.isNullOrEmpty()) {
        BooksCarouselLandscape(
            pagerState = pagerState,
            count = bookList.size
        ) { page ->
            val book = bookList[page]

            BookCardLandscape(
                modifier = Modifier
                    .graphicsLayer {
                        val pageOffset = pagerState.getOffsetFractionForPage(page).absoluteValue

                        lerp(
                            start = 0.85f,
                            stop = 1f,
                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                        ).also { scale ->
                            scaleX = scale
                            scaleY = scale
                        }

                        // We animate the alpha, between 50% and 100%
                        alpha = lerp(
                            start = 0.5f,
                            stop = 1f,
                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                        )
                    }
                    .clickable { onClick.invoke(book.book.id) },
                picture = book.book.picture,
                progress = book.bookProgress.progressInPercentage,
                startedToRead = book.bookProgress.startedToRead,
                finishedToRead = book.bookProgress.finishedToRead,
                title = book.book.title,
                authors = book.book.authors.joinToString(", "),
                finished = book.bookProgress.finished,
                giveUp = book.bookProgress.giveUp
            )
        }
    } else {
        Text(
            modifier = Modifier
                .background(colorBackgroundTextEmptyState)
                .padding(emptyTextPadding),
            text = textEmptyState,
            textAlign = TextAlign.Center,
            style = Typography.bodyLarge.copy(color = colorTextEmptyState)
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun CurrentlyReadingBooksSectionLandscapePreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        CurrentlyReadingBooksSectionLandscape(
            bookList = listOf(book, book),
            onClick = {}
        )
    }
}

@Preview(
    showSystemUi = false,
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    widthDp = 720,
    heightDp = 360
)
@Composable
fun CurrentlyReadingBooksSectionLandscapeEmptyPreview() {
    SobuuTheme {
        CurrentlyReadingBooksSectionLandscape(
            bookList = listOf(),
            onClick = {}
        )
    }
}