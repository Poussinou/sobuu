package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_confirmPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_firstNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_lastNameAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_password
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_username
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.CreateAccountButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.BottomRoundedOutlinedTextField
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.SquareOutlinedTextField
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.TopRoundedOutlinedTextField
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LegalButtons
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.LegalTextAcceptanceSwitch
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun RegistrationView(
    usernameValue: String,
    usernameOnValueChange: (String) -> Unit,
    emailValue: String,
    emailOnValueChange: (String) -> Unit,
    passwordValue: String,
    passwordOnValueChange: (String) -> Unit,
    confirmationPasswordValue: String,
    confirmationPasswordOnValueChange: (String) -> Unit,
    firstNameValue: String,
    firstNameOnValueChange: (String) -> Unit,
    lastNameValue: String,
    lastNameOnValueChange: (String) -> Unit,
    nameHasError: Boolean,
    emailHasError: Boolean,
    passwordHasError: Boolean,
    confirmationPassHasError: Boolean,
    errorText: String?,
    isLoading: Boolean,
    privacyPolicyConfirmationSwitchValue: Boolean,
    privacyPolicyConfirmationSwitchOnValueChange: (Boolean) -> Unit,
    registerButtonOnClick: () -> Unit,
    termsButtonOnClick: () -> Unit,
    privacyButtonOnClick: () -> Unit,
) {
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current

    val backgroundColor = MaterialTheme.colorScheme.primary
    val errorColor = MaterialTheme.colorScheme.error

    if (isLoading) {
        CircularLoadingIndicator()
    } else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor)
                .padding(horizontal = 30.dp),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Spacer(Modifier.height(1.dp))

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                TopRoundedOutlinedTextField(
                    fieldValue = usernameValue,
                    onFieldValueChange = usernameOnValueChange,
                    placeholderText = authorization_auth_username.stringResource(context = context),
                    isError = nameHasError
                )
                SquareOutlinedTextField(
                    fieldValue = emailValue,
                    onFieldValueChange = emailOnValueChange,
                    placeholderText = authorization_auth_email.stringResource(context = context),
                    icon = Icons.Filled.Mail,
                    isError = emailHasError
                )
                SquareOutlinedTextField(
                    fieldValue = passwordValue,
                    onFieldValueChange = passwordOnValueChange,
                    placeholderText = authorization_auth_password.stringResource(context = context),
                    passwordField = true,
                    icon = Icons.Filled.Lock,
                    isError = passwordHasError
                )
                BottomRoundedOutlinedTextField(
                    fieldValue = confirmationPasswordValue,
                    onFieldValueChange = confirmationPasswordOnValueChange,
                    placeholderText = authorization_auth_confirmPassword.stringResource(context = context),
                    passwordField = true,
                    onKeyboardActionClicked = { focusManager.moveFocus(FocusDirection.Down) },
                    isError = confirmationPassHasError
                )
            }

            Column {
                TopRoundedOutlinedTextField(
                    fieldValue = firstNameValue,
                    onFieldValueChange = firstNameOnValueChange,
                    placeholderText = authorization_auth_firstNameAccount.stringResource(context = context)
                )
                BottomRoundedOutlinedTextField(
                    fieldValue = lastNameValue,
                    onFieldValueChange = lastNameOnValueChange,
                    placeholderText = authorization_auth_lastNameAccount.stringResource(context = context),
                    passwordField = false,
                    onKeyboardActionClicked = {
                        keyboardController?.hide()

                        if(privacyPolicyConfirmationSwitchValue) {
                            registerButtonOnClick()
                        }
                    }
                )
            }

            LegalTextAcceptanceSwitch(
                navigateToTermsText = termsButtonOnClick,
                navigateToPrivacyText = privacyButtonOnClick,
                privacyPolicyConfirmationSwitchValue = privacyPolicyConfirmationSwitchValue,
                onPrivacyPolicyConfirmationSwitchValueChange = privacyPolicyConfirmationSwitchOnValueChange
            )

            if (errorText != null) {
                Text(
                    text = errorText,
                    modifier = Modifier.semantics { this.contentDescription = errorText },
                    style = Typography.bodyMedium.copy(color = errorColor),
                    textAlign = TextAlign.Center,
                )
            } else {
                Spacer(modifier = Modifier.height(35.dp))
            }

            CreateAccountButton(
                onClick = registerButtonOnClick,
                isEnabled = privacyPolicyConfirmationSwitchValue
            )

            LegalButtons(
                onTermsAndConditionsButtonClick = termsButtonOnClick,
                onPrivacyPolicyButtonClick = privacyButtonOnClick
            )

            Spacer(Modifier.height(1.dp))
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun RegistrationViewLightPreview() {
    SobuuAuthTheme {
        RegistrationView(
            usernameValue = "",
            usernameOnValueChange = {},
            emailValue = "",
            emailOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            confirmationPasswordValue = "",
            confirmationPasswordOnValueChange = {},
            firstNameValue = "",
            firstNameOnValueChange = {},
            lastNameValue = "",
            lastNameOnValueChange = {},
            nameHasError = false,
            emailHasError = false,
            passwordHasError = false,
            confirmationPassHasError = false,
            errorText = null,
            isLoading = false,
            privacyPolicyConfirmationSwitchValue = false,
            privacyPolicyConfirmationSwitchOnValueChange = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun RegistrationViewDarkPreview() {
    SobuuAuthTheme {
        RegistrationView(
            usernameValue = "",
            usernameOnValueChange = {},
            emailValue = "",
            emailOnValueChange = {},
            passwordValue = "",
            passwordOnValueChange = {},
            confirmationPasswordValue = "",
            confirmationPasswordOnValueChange = {},
            firstNameValue = "",
            firstNameOnValueChange = {},
            lastNameValue = "",
            lastNameOnValueChange = {},
            nameHasError = false,
            emailHasError = false,
            passwordHasError = false,
            confirmationPassHasError = false,
            errorText = null,
            isLoading = false,
            privacyPolicyConfirmationSwitchValue = false,
            privacyPolicyConfirmationSwitchOnValueChange = {},
            registerButtonOnClick = {},
            termsButtonOnClick = {},
            privacyButtonOnClick = {}
        )
    }
}