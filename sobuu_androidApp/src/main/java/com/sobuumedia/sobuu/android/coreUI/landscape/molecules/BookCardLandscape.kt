package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_finishedBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_gaveUpBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_startedBookOn
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.BookThumbnail
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularProgressIndicator
import com.sobuumedia.sobuu.android.main.toStringDateWithDayAndTime
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun BookCardLandscape(
    modifier: Modifier = Modifier,
    picture: String,
    progress: Double,
    startedToRead: LocalDateTime?,
    finishedToRead: LocalDateTime?,
    title: String,
    authors: String,
    finished: Boolean,
    giveUp: Boolean,
) {
    val context = LocalContext.current
    val textColor = MaterialTheme.colorScheme.background
    val backgroundColor = MaterialTheme.colorScheme.secondary
    val startedDateText = "${home_main_startedBookOn.stringResource(context = context)} ${
        startedToRead?.toStringDateWithDayAndTime() ?: Clock.System.now()
            .toLocalDateTime(TimeZone.UTC).toStringDateWithDayAndTime()
    }"
    val finishedDateText = if (finished || giveUp) {
        "${
            if (finished) {
                home_main_finishedBookOn.stringResource(context = context)
            } else {
                home_main_gaveUpBookOn.stringResource(context = context)
            }
        } ${
            finishedToRead?.toStringDateWithDayAndTime()
        }"
    } else {
        ""
    }

    Box(
        modifier = Modifier.then(modifier)
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .clip(RoundedCornerShape(20.dp))
                .background(backgroundColor)
                .padding(10.dp)
        ) {
            Box(
                modifier = Modifier.fillMaxWidth(0.4f),
                contentAlignment = Alignment.Center
            ) {
                BookThumbnail(
                    modifier = Modifier.padding(10.dp),
                    imageURL = picture
                )

                Box(
                    modifier = Modifier
                        .fillMaxWidth(0.6f)
                        .fillMaxHeight(0.8f),
                    contentAlignment = Alignment.BottomEnd
                ) {
                    CircularProgressIndicator(progress = progress)
                }
            }

            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.Top
                ) {
                    Text(
                        text = startedDateText,
                        style = Typography.bodySmall.copy(color = textColor),
                        modifier = Modifier
                            .padding(10.dp)
                            .semantics { this.contentDescription = startedDateText }
                    )

                    if (finishedToRead != null && (finished || giveUp)) {
                        Text("|",
                            style = Typography.bodySmall.copy(color = textColor),
                            modifier = Modifier
                                .padding(10.dp)
                                .semantics { this.contentDescription = finishedDateText }
                        )

                        Text(finishedDateText,
                            style = Typography.bodySmall.copy(color = textColor),
                            modifier = Modifier
                                .padding(10.dp)
                                .semantics { this.contentDescription = finishedDateText }
                        )
                    }
                }

                Spacer(modifier = Modifier.weight(0.5f))

                Text(
                    text = title,
                    style = Typography.titleMedium.copy(color = textColor),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .semantics { this.contentDescription = title }
                )

                Text(
                    text = authors,
                    style = Typography.bodyLarge.copy(color = textColor),
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp)
                        .semantics { this.contentDescription = authors }
                )

                Spacer(modifier = Modifier.weight(0.5f))
            }
        }
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO, widthDp = 720, heightDp = 360)
@Composable
fun BookCardLandscapePreview() {
    SobuuTheme {
        BookCardLandscape(
            progress = 93.0,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            finished = true,
            finishedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            picture = "",
            giveUp = false,
            authors = listOf("Steph Ryan, Blake Sorgenson, Jeffrey O'Connell ").joinToString(", "),
            title = "Testing books with an extra large name and more than one author"
        )
    }
}