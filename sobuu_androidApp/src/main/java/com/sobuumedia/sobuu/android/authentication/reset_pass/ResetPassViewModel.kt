package com.sobuumedia.sobuu.android.authentication.reset_pass

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch


class ResetPassViewModel(private val auth: IAuthenticationRepository): ViewModel() {

    var state by mutableStateOf(ResetPassState())

    private val resultChannel = Channel<AuthenticationResult<Unit>>()
    val authResult = resultChannel.receiveAsFlow()


    fun onEvent(event: ResetPassUIEvent) {
        when(event) {
            is ResetPassUIEvent.ForgotPasswordEmailChanged -> {
                state = state.copy(forgotEmail = event.value)
            }
            ResetPassUIEvent.resetPassword -> resetPassword()
        }
    }

    fun handleError(error: AuthenticationError?) {
        state = state.copy(error = error)
    }

    private fun resetPassword() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)

            // To be able to pass the navigation tests without touching the server
            val result = if(BuildConfig.DEBUG &&
                (state.forgotEmail.startsWith("test") && state.forgotEmail.endsWith("@sobuu.com"))) {
                AuthenticationResult.ResetPassword()
            } else {
                auth.resetPassword(
                    email = state.forgotEmail
                )
            }


            resultChannel.send(result)

            state = state.copy(isLoading = false)
        }
    }
}