package com.sobuumedia.sobuu.android.comments

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentWithSpoilers
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentWithoutSpoilers
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsOnPercentage
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_sendComment
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_writeComment
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_writeCommentBeforeStart
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_writeCommentOnPage
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.LinealProgressIndicator
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.custom_widgets.TopAppBarWithMenu
import com.sobuumedia.sobuu.android.custom_widgets.WideTextField
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.SobuuLogs
import org.koin.androidx.compose.koinViewModel

@Composable
fun AddNewCommentScreen(
    navigateBackOrHome: () -> Unit,
    navigatePopBackStack: () -> Unit,
    bookId: String,
    parentCommentId: String?,
    viewModel: CommentsViewModel = koinViewModel(),
    logs: SobuuLogs
) {
    val state = viewModel.state
    val context = LocalContext.current

    if(state.navigateBack) {
        navigatePopBackStack()
    }

    LaunchedEffect(viewModel, context) {
        viewModel.onEvent(CommentsUIEvents.GetBookData(bookId))
    }

    Scaffold(
        topBar = {
            TopAppBarWithMenu(
                navigateBackOrHome = navigateBackOrHome,
                title = if (state.book?.bookProgress?.page != null) {
                    if ((state.book.bookProgress.page ?: 0) <= 0) {
                        comments_comment_writeCommentBeforeStart.stringResource(context = context)
                    } else {
                        "${comments_comment_writeCommentOnPage.stringResource(context = context)} ${state.book.bookProgress.page}"
                    }
                } else {
                    if ((state.book?.bookProgress?.percentage ?: 0.0) <= 0.0) {
                        comments_comment_writeCommentBeforeStart.stringResource(context = context)
                    } else {
                        "${comments_comment_commentsOnPercentage.stringResource(context = context)} ${state.book?.bookProgress?.percentage}%"
                    }
                },
                titleSize = 20.sp,
                listItems = listOf(
                    MenuItemData(
                        text = comments_comment_sendComment.stringResource(context = context),
                        icon = Icons.AutoMirrored.Filled.Send,
                        action = {
                            viewModel.onEvent(
                                CommentsUIEvents.CreateNewComment(
                                    bookId = bookId,
                                    page = state.book?.bookProgress?.page,
                                    percentage = state.book?.bookProgress?.percentage,
                                    totalPages = state.book?.book?.totalPages ?: 0,
                                    hasSpoilers = viewModel.state.spoilers,
                                    commentText = viewModel.state.newComment,
                                    parentCommentId = parentCommentId,
                                )
                            )
                        }
                    )
                ),
                showCollapseMenu = false,
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .padding(
                        start = 20.dp,
                        top = it.calculateTopPadding(),
                        bottom = it.calculateBottomPadding(),
                        end = 20.dp
                    )
            ) {
                if (viewModel.state.isLoading) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(MaterialTheme.colorScheme.background),
                        contentAlignment = Alignment.Center,
                    ) {
                        CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                    }
                } else if (viewModel.state.error != null) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(MaterialTheme.colorScheme.background),
                        contentAlignment = Alignment.Center,
                    ) {
                        Text(
                            text = getStringFromError(error = viewModel.state.error),
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.error,
                                fontSize = 18.sp
                            )
                        )
                    }
                } else {
                    BookInfo(
                        title = state.book?.book?.title ?: "",
                        authors = state.book?.book?.authors?.joinToString(",") ?: "",
                        totalPages = state.book?.book?.totalPages ?: 0,
                        page = state.book?.bookProgress?.page,
                        percentage = state.book?.bookProgress?.percentage
                    )
                    Spacer(modifier = Modifier.padding(vertical = 5.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.End
                    ) {
                        Text(
                            modifier = Modifier.padding(10.dp),
                            text = if(state.spoilers) {
                                comments_comment_commentWithSpoilers.stringResource(context = context)
                            } else {
                                comments_comment_commentWithoutSpoilers.stringResource(context = context)
                            },
                            style = TextStyle(
                                fontSize = 16.sp,
                                color = MaterialTheme.colorScheme.secondary,
                            )
                        )
                        Switch(
                            checked = state.spoilers,
                            onCheckedChange = { newValue ->
                                viewModel.onEvent(
                                    CommentsUIEvents.SpoilerSwitchChanged(newValue)
                                )
                            },
                            colors = SwitchDefaults.colors(
                                checkedThumbColor = MaterialTheme.colorScheme.error,
                                uncheckedThumbColor = MaterialTheme.colorScheme.secondary,
                                checkedTrackColor = MaterialTheme.colorScheme.primary,
                                uncheckedTrackColor = MaterialTheme.colorScheme.tertiary,
                            )
                        )
                    }
                    WideTextField(
                        modifier = Modifier
                            .fillMaxSize(),
                        fieldValue = state.newComment,
                        onFieldValueChange = { newCommentText ->
                            viewModel.onEvent(
                                CommentsUIEvents.NewCommentTextChanged(
                                    newCommentText
                                )
                            )
                        },
                        placeholderText = comments_comment_writeComment.stringResource(context = context),
                        onKeyboardActionClicked = ({
                            viewModel.onEvent(
                                CommentsUIEvents.NewCommentTextChanged(
                                    "${state.newComment}\r\n"
                                )
                            )
                        }),
                    )
                }
            }
        }
    )
}

@Composable
private fun BookInfo(
    modifier: Modifier = Modifier,
    title: String, authors: String,
    totalPages: Int, page: Int?, percentage: Double?
) {
    val progress = if ((page == null || page == -1) && (percentage == null || percentage == -1.0)) {
        0
    } else if (page == null) {
        percentage
    } else {
        (page * 100) / totalPages
    }

    Box(
        modifier = Modifier
            .then(modifier)
            .fillMaxWidth()
            .border(
                border = BorderStroke(2.dp, color = MaterialTheme.colorScheme.secondary),
                shape = RoundedCornerShape(20.dp)
            )
            .clip(RoundedCornerShape(20.dp))
            .background(MaterialTheme.colorScheme.secondary)
            .padding(10.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.padding(all = 5.dp),
                text = title,
                style = TextStyle(
                    fontSize = 24.sp,
                    color = MaterialTheme.colorScheme.background,
                    fontFamily = SourceSans,
                    fontWeight = FontWeight.Medium,
                ),
                textAlign = TextAlign.Center,
            )
            Text(
                modifier = Modifier.padding(all = 5.dp),
                text = authors,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = MaterialTheme.colorScheme.background,
                    fontFamily = SourceSans,
                    fontWeight = FontWeight.Medium,
                ),
                textAlign = TextAlign.Center,
            )
            Spacer(modifier = Modifier.padding(vertical = 10.dp))
            LinealProgressIndicator(progress = progress?.toDouble() ?: 0.0)
            Spacer(modifier = Modifier.padding(vertical = 5.dp))
        }
    }
}


@Preview
@Composable
fun BookInfoPreview() {
    BookInfo(
        title = "Los siete maridos de Eva Salazar",
        authors = "George Orwell",
        totalPages = 275,
        page = 69,
        percentage = null
    )
}