package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CustomTextButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    color: Color = MaterialTheme.colorScheme.background,
    text: String,
) {
    TextButton(
        modifier = Modifier
            .fillMaxWidth()
            .semantics { this.contentDescription = text }
            .then(modifier),
        onClick = onClick
    ) {
        Text(
            text = text,
            style = Typography.bodySmall.copy(color = color),
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CustomTextButtonPreview() {
    CustomTextButton(
        onClick = {},
        text = "Privacy policy"
    )
}