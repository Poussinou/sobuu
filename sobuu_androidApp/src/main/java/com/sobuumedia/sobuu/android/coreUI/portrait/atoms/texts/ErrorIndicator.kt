package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun ErrorIndicator(
        modifier: Modifier = Modifier,
        errorText: String?
    ) {
        val horizontalPadding = 32.dp
        val textColor = MaterialTheme.colorScheme.error

        Text(
            modifier = modifier
                .padding(horizontal = horizontalPadding)
                .then(modifier),
            text = errorText ?: "",
            style = Typography.bodyMedium.copy(color = textColor)
        )
}

@Preview(showSystemUi = false, showBackground = false, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ErrorIndicatorPreview() {
    SobuuAuthTheme {
        ErrorIndicator(
            errorText = "There was an error with the request"
        )
    }
}