package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SquareOutlinedTextField(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    icon: ImageVector?,
    passwordField: Boolean = false,
    contentDescription: String = "",
    isError: Boolean = false,
) {
    val focusManager = LocalFocusManager.current
    val bringIntoViewRequester = remember { BringIntoViewRequester() }
    val coroutineScope = rememberCoroutineScope()

    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(color = backgroundColor)
                .semantics { this.contentDescription = contentDescription }
                .onFocusEvent { focusState ->
                    if (focusState.isFocused) {
                        coroutineScope.launch {
                            bringIntoViewRequester.bringIntoView()
                        }
                    }
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            visualTransformation = if (passwordField) PasswordVisualTransformation() else VisualTransformation.None,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = if (passwordField) KeyboardType.Password else KeyboardType.Text
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                }
            ),
            isError = isError,
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            shape = RoundedCornerShape(0.dp),
            singleLine = true,
            leadingIcon = {
                if(icon != null) {
                    Icon(
                        imageVector = icon,
                        contentDescription = icon.name,
                        tint = placeholderColor,
                    )
                }
            }
        )
    }
}


@Preview(showSystemUi = false, showBackground = true)
@Composable
fun SquareOutlinedTextFieldPreview() {
    SquareOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Email",
        icon = Icons.Filled.Mail
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun SquareOutlinedTextFieldErrorPreview() {
    SquareOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Email",
        icon = Icons.Filled.Mail,
        isError = true
    )
}