package com.sobuumedia.sobuu.android.settings.theme

import androidx.compose.ui.graphics.Color

val DarkLava = Color(0xFF483C32)
val GreenSheen = Color(0xFF79AEA3)
val Vermilion = Color(0xFFCD4631)
val SpanishGray = Color(0xFF969A97)
val WhiteBlue = Color(0xFFF6FBFF)

val DarkCharcoalGray = Color(0XFFA9A19D)
val DarkGreen = Color(0xFF3B6D63)
val DarkRed = Color(0xFFB53324)
val LightGray = Color(0xFF75787B)
val LightBlue = Color(0xFF1A1C23)
