package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextMedium
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun NoBorderLongTextInputField(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    contentDescription: String = "",
    isError: Boolean = false)
{
    val keyboardController = LocalSoftwareKeyboardController.current
    var offset by remember { mutableFloatStateOf(0f) }

    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error

    ProvideTextStyle(
        value = Typography.bodyMedium.copy(color = elementColor)
    ) {
        TextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(color = backgroundColor)
                .scrollable(
                    orientation = Orientation.Horizontal,
                    state = rememberScrollableState { delta ->
                        offset += delta
                        delta
                    }
                )
                .semantics { this.contentDescription = contentDescription },
            textStyle = Typography.bodyLarge.copy(color = elementColor),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            isError = isError,
            placeholder = { PlaceholderTextMedium(placeholderText = placeholderText, isError = isError) },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Default,
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences
            ),
            singleLine = false,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun NoBorderLongTextInputFieldPreview() {
    NoBorderLongTextInputField(
        fieldValue = "",
        onFieldValueChange = ({}),
        placeholderText = "Write a long text that takes several lines so it is better read by the users",
        onKeyboardActionClicked = { }
    )
}