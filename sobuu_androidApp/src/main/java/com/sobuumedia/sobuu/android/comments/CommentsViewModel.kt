package com.sobuumedia.sobuu.android.comments

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.features.comments.remote.CommentError
import com.sobuumedia.sobuu.features.comments.repository.ICommentRepository
import com.sobuumedia.sobuu.models.bo_models.ReportReason
import kotlinx.coroutines.launch
import org.acra.ACRA

private const val TAG = "CommentsViewModel"

class CommentsViewModel(
    private val commentsRepo: ICommentRepository,
    private val booksRepo: IBookRepository,
    private val logs: SobuuLogs
): ViewModel() {

    var state by mutableStateOf(CommentsState())

    fun onEvent(event: CommentsUIEvents) {
        when(event) {
            is CommentsUIEvents.FilterByType -> {
                state = state.copy(applyFilter = true, filterType = event.type)
            }
            is CommentsUIEvents.DisplayCommentsScreen -> fetchCommentsForPageOrPercentage(
                bookId = event.bookId
            )
            is CommentsUIEvents.DisplayFilterScreen -> TODO()
            is CommentsUIEvents.CreateNewComment -> addNewCommentToPageOrPercentage(
                bookId = event.bookId,
                page = event.page,
                percentage = event.percentage,
                totalPages = event.totalPages,
                parentCommentId = event.parentCommentId,
                commentText = event.commentText,
                hasSpoilers = event.hasSpoilers
            )
            is CommentsUIEvents.NewCommentTextChanged -> {
                state = state.copy(newComment = event.value)
            }
            is CommentsUIEvents.SpoilerSwitchChanged -> {
                state = state.copy(spoilers = event.value)
            }
            is CommentsUIEvents.DecreaseVote -> voteInComment(commentId = event.commentId,-1)
            is CommentsUIEvents.IncreaseVote -> voteInComment(commentId = event.commentId, 1)
            is CommentsUIEvents.DisplayReportReasonsDialog -> {
                state = state.copy(displayReportReasonsDialog = Pair(!state.displayReportReasonsDialog.first, event.commentId))
            }
            is CommentsUIEvents.ReportComment -> reportComment(event.commentId, event.reportReason)
            is CommentsUIEvents.CloseResultDialog -> {
                state = state.copy(displayErrorDialog = false, displaySuccessDialog = false)
            }
            is CommentsUIEvents.GetBookData -> getBookByIdInDB(event.bookId)
        }
    }

    fun handleError(error: CommentError?) {
        when(error) {
            is CommentError.UnknownError -> {
                ACRA.errorReporter.putCustomData("Handle error in CommentViewModel", error.message ?: "Unknown error with empty message")
            }
            else -> {
                state = state.copy(error = error)
            }
        }
    }

    private fun getBookByIdInDB(bookId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = booksRepo.getBookWithProgressByIdFromDB(bookId)

            if(result.error != null) {
                handleError(CommentError.BookDataNotFound)
            } else {
                handleError(null)
                logs.info(TAG, "---Got Book data in Comments page: ${result.data?.book?.title}")
                state = state.copy(book = result.data)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun fetchCommentsForPageOrPercentage(bookId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)

            val bookWithProgress = commentsRepo.getBookInfoById(bookId)
            val progress = bookWithProgress.data?.bookProgress
            val book = bookWithProgress.data?.book

            val result = commentsRepo.getCommentsInPageOrPercentage(
                bookId = bookId,
                page = if (progress?.page == -1) null else progress?.page,
                percentage = if(progress?.percentage == -1.0) null else progress?.percentage,
                totalPages = book?.totalPages ?: 0
            )

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                logs.info(TAG, "---List of comments in page size: ${result.data?.size}")
            }

            state = state.copy(isLoading = false, listOFComments = result.data ?: emptyList())
        }
    }

    private fun addNewCommentToPageOrPercentage(bookId: String, totalPages: Int,
                                                page: Int? = null, percentage: Double? = null,
                                                hasSpoilers: Boolean, commentText: String,
                                                parentCommentId: String?) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = commentsRepo.addComment(
                bookId = bookId,
                page = page,
                percentage = percentage,
                bookTotalPages = totalPages,
                text = commentText,
                hasSpoilers = hasSpoilers,
                parentCommentId = parentCommentId
            )

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(navigateBack = true)
            }

            state = state.copy(isLoading = false, listOFComments = result.data ?: emptyList())
        }
    }

    private fun voteInComment(commentId: String, value: Int) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)

            val result = if(value > 0) {
                commentsRepo.increaseCommentVoteCounter(commentId = commentId)
            } else {
                commentsRepo.decreaseCommentVoteCounter(commentId = commentId)
            }

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
            }

            state = state.copy(isLoading = false, listOFComments = result.data ?: emptyList())
        }
    }

    private fun reportComment(commentId: String, reportReason: ReportReason) {
        viewModelScope.launch {
            state = state.copy(isLoading = true, displayReportReasonsDialog = Pair(false, ""))

            val result = commentsRepo.reportComment(
                commentId = commentId,
                reason = reportReason
            )

            state = if(result.error != null) {
                handleError(result.error)
                state.copy(error = result.error, displayErrorDialog = true)
            } else {
                handleError(null)
                state.copy(displaySuccessDialog = true)
            }

            state = state.copy(isLoading = false)
        }
    }
}