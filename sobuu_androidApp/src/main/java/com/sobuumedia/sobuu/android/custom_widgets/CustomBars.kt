package com.sobuumedia.sobuu.android.custom_widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Book
import androidx.compose.material.icons.filled.Groups
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.PopupProperties
import androidx.navigation.NavController
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_backButton
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_books
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_challenges
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_friends
import com.sobuumedia.sobuu.SharedRes.strings.home_bottomMenuLabels_shelves
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ProfileIconButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.SearchBar
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.TopBarAppName
import com.sobuumedia.sobuu.android.settings.theme.Solway
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomTopAppBar(
    navigateBack: () -> Unit,
    title: String,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    titleSize: TextUnit = 24.sp,
    iconColor: Color = MaterialTheme.colorScheme.secondary,
    titleColor: Color = MaterialTheme.colorScheme.secondary
) {
    CenterAlignedTopAppBar(
        title = {
            Text(
                title,
                style = TextStyle(
                    color = titleColor,
                    fontSize = titleSize,
                    fontFamily = SourceSans
                )
            )
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        modifier = Modifier.fillMaxWidth(),
        navigationIcon = {
            IconButton(
                modifier = Modifier
                    .semantics {
                        this.contentDescription = contentDescription_topBar_backButton.stringResource()
                    },
                onClick = {
                    navigateBack()
                }
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = "",
                    tint = iconColor,
                )
            }
        }
    )
}

@Composable
fun TopAppBarWithSearchAndProfile(
    navigateToProfileScreen: () -> Unit,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearTextButtonClick: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
) {
    Surface(
        modifier = Modifier
            .background(backgroundColor)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(backgroundColor),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TopBarAppName()
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 12.dp, end = 12.dp)
            ) {
                SearchBar(
                    searchFieldValue = searchFieldValue,
                    onSearchFieldValueChange = onSearchFieldValueChange,
                    modifier = Modifier
                        .weight(3f)
                        .semantics { this.contentDescription = searchFieldValue },
                    onSearchButtonClick = onSearchButtonClick,
                    clearText = clearTextButtonClick,
                    onSearchFieldFocusChange = onSearchFieldFocusChange
                )

                ProfileIconButton(
                    modifier = Modifier.weight(1.0f),
                    navigateToProfileScreen = navigateToProfileScreen
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SimpleTransparentTopAppBar(
    modifier: Modifier = Modifier,
    nav: NavController?,
    backgroundColor: Color = MaterialTheme.colorScheme.background.copy(alpha = 0f),
) {
    TopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .then(modifier),
        title = {
            Text("")
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        navigationIcon = {
            IconButton(onClick = {
                nav?.navigateUp()
            }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopAppBarWithMenu(
    modifier: Modifier = Modifier,
    navigateBackOrHome: () -> Unit,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    title: String? = null,
    titleSize: TextUnit = 38.sp,
    titleColor: Color = MaterialTheme.colorScheme.secondary,
    listItems: List<MenuItemData> = listOf(),
    showCollapseMenu: Boolean = true,
) {
    var expanded by remember { mutableStateOf(false) }

    CenterAlignedTopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .then(modifier),
        title = {
            Text(
                text = title ?: "",
                style = TextStyle(
                    color = titleColor,
                    fontSize = titleSize,
                    fontFamily = Solway,
                    fontWeight = FontWeight.Medium,
                ),
                textAlign = TextAlign.Center,
            )
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        actions = {
            if (showCollapseMenu) {
                IconButton(onClick = {
                    expanded = true
                }) {
                    Icon(
                        imageVector = Icons.Default.MoreVert,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary
                    )
                }

                DropdownMenu(
                    modifier = Modifier
                        .background(backgroundColor)
                        .width(width = 200.dp),
                    expanded = expanded,
                    onDismissRequest = {
                        expanded = false
                    },
                    offset = DpOffset(x = (-102).dp, y = (-64).dp),
                    properties = PopupProperties()
                ) {
                    listItems.forEach { menuItemData ->
                        if(!menuItemData.displayIt) {
                            return@forEach
                        }
                        DropdownMenuItem(
                            onClick = {
                                menuItemData.action()
                                expanded = false
                            },
                            enabled = true,
                            text = {
                                Text(
                                    text = menuItemData.text,
                                    fontWeight = FontWeight.Medium,
                                    fontSize = 16.sp,
                                    color = MaterialTheme.colorScheme.secondary
                                )
                            },
                            leadingIcon = {
                                Icon(
                                    imageVector = menuItemData.icon,
                                    contentDescription = menuItemData.text,
                                    tint = MaterialTheme.colorScheme.secondary,
                                )
                            }
                        )
                    }
                }
            } else {
                if(listItems.isNotEmpty()) {
                    val menuItem = listItems[0]

                    IconButton(
                        onClick = {
                            menuItem.action.invoke()
                        },
                    ) {
                        Icon(
                            imageVector = menuItem.icon,
                            contentDescription = menuItem.text,
                            tint = MaterialTheme.colorScheme.secondary,
                        )
                    }
                }
            }
        },
        navigationIcon = {
            IconButton(onClick = {
                navigateBackOrHome()
            }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    )
}


@Composable
fun CustomBottomAppBar(
    modifier: Modifier = Modifier,
    nav: NavController? = null,
    displayLabels: Boolean = true,
) {
    val context = LocalContext.current
    NavigationBar(
        modifier = Modifier
            .fillMaxWidth(),
        containerColor = MaterialTheme.colorScheme.primary,
    ) {
//        var currentRoute by remember { mutableStateOf(HomeScreenDestination.route) }

        val config = LocalConfiguration.current
        val screenWidth = config.screenWidthDp.dp
        val itemWidth = screenWidth / 4

        Column(
            modifier = Modifier
//                .background(if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(HomeScreenDestination)
//                    currentRoute = HomeScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Book,
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
//                tint = if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_books.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
//                        fontSize = if (currentRoute == HomeScreenDestination.route) 16.sp else 12.sp,
//                        color = if (currentRoute == HomeScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background,
//                        fontWeight = if (currentRoute == HomeScreenDestination.route) FontWeight.Medium else FontWeight.Normal,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
//                .background(if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
//                    nav?.navigate(ShelvesScreenDestination)
//                    currentRoute = ShelvesScreenDestination.route
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_bookshelf),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
//                tint = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_shelves.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
//                        fontSize = if (currentRoute == ShelvesScreenDestination.route) 16.sp else 12.sp,
//                        color = if (currentRoute == ShelvesScreenDestination.route) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.background,
//                        fontWeight = if (currentRoute == ShelvesScreenDestination.route) FontWeight.Medium else FontWeight.Normal,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                imageVector = Icons.Filled.Groups,
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )
            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_friends.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.primary)
                .width(itemWidth)
                .fillMaxHeight()
                .clickable {
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_trophy),
                contentDescription = "",
                modifier = Modifier
                    .width(36.dp)
                    .height(36.dp),
                tint = MaterialTheme.colorScheme.background
            )

            if(displayLabels) {
                Text(
                    text = home_bottomMenuLabels_challenges.stringResource(context = context),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    )
                )
            }
        }
    }
}

data class MenuItemData(val text: String, val icon: ImageVector, val action: () -> Unit, val displayIt: Boolean = true)

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "Custom")
@Composable
fun ComposableHomeScreenTopBarCustomPreview() {
    CustomTopAppBar(
        navigateBack = {},
        title = "Sobuu"
    )
}

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "Transparent")
@Composable
fun ComposableHomeScreenTopBarTransparentPreview() {
    SimpleTransparentTopAppBar(
        nav = null,
    )
}

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "WithMenu")
@Composable
fun ComposableHomeScreenTopBarWithMenuPreview() {
    TopAppBarWithMenu(
        navigateBackOrHome = {},
        title = "Sobuu"
    )
}

@Preview(showSystemUi = true, showBackground = true, group = "Done", name = "WithSearchAndProfile")
@Composable
fun ComposableHomeScreenTopBarPreview() {
    TopAppBarWithSearchAndProfile(
        {},
        searchFieldValue = "",
        onSearchFieldValueChange = {},
        onSearchButtonClick = {},
        clearTextButtonClick = {},
        onSearchFieldFocusChange = {}
    )
}