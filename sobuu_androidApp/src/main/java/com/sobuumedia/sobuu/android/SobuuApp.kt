package com.sobuumedia.sobuu.android

import android.app.Application
import android.content.Context
import android.content.pm.PackageInfo
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.analytics.Analytics
import com.sobuumedia.sobuu.android.analytics.SobuuAnalytics
import com.sobuumedia.sobuu.android.di.androidModule
import com.sobuumedia.sobuu.di.appModule
import org.acra.BuildConfig
import org.acra.config.httpSender
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import org.acra.sender.HttpSender
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SobuuApp : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
        startKoin {
            androidContext(this@SobuuApp)
//            androidLogger()
            modules(appModule() + androidModule)
        }

        //Analytics data
        val screenHeight = resources.configuration.screenHeightDp.dp
        val screenWidth = resources.configuration.screenWidthDp.dp

        val pInfo: PackageInfo = this.packageManager.getPackageInfo(this.packageName, 0)
        val version = pInfo.versionName

        val resolution = "{ Height: $screenHeight, Width $screenWidth}"
        val operativeSystem = "Android"
        val osVersion = android.os.Build.VERSION.RELEASE
        val language = Locale.current.language

        analytics = SobuuAnalytics().startAnalytics(
            resolution = resolution,
            operativeSystem = operativeSystem,
            osVersion = osVersion,
            appVersion = version,
            language = language
        )
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        initAcra {
            //core configuration:
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON

            httpSender {
                //required. Https recommended
                //uri = "https://crash.getsobuu.com/"
                //uri = "/report" /*best guess, you may need to adjust this*/
                uri = "https://crash.getsobuu.com/report"
                basicAuthLogin = "ovTHs9SRWc8Ner69"
                basicAuthPassword = "acQ8XGnbovM9DL96"
                httpMethod = HttpSender.Method.POST
                connectionTimeout = 5000
                socketTimeout = 20000
                dropReportsOnTimeout = false
                compress = false
            }
        }
    }

    companion object {
        lateinit var analytics: Analytics
        lateinit var context: Context
    }
}