package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.search_main_getMoreBooks
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun GetMoreBooksText(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    displayMoreInfo: Boolean
) {
    val textColor = MaterialTheme.colorScheme.secondary
    val paddingSize = 10.dp

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .then(modifier)
            .padding(paddingSize),
        horizontalArrangement = Arrangement.End,
    ) {
        Text(
            modifier = Modifier.align(Alignment.CenterVertically),
            text = search_main_getMoreBooks.stringResource(context = LocalContext.current),
            style = Typography.bodySmall.copy(color = textColor)
        )
        if (displayMoreInfo) {
            Icon(
                imageVector = Icons.Filled.ArrowDropUp,
                contentDescription = Icons.Filled.ArrowDropUp.name,
                tint = textColor
            )
        } else {
            Icon(
                imageVector = Icons.Filled.ArrowDropDown,
                contentDescription = Icons.Filled.ArrowDropDown.name,
                tint = textColor
            )
        }
    }
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun GetMoreBooksTextPreview() {
    SobuuTheme {
        GetMoreBooksText(
            modifier = Modifier.background(Color.White),
            onClick = {},
            displayMoreInfo = false
        )
    }
}