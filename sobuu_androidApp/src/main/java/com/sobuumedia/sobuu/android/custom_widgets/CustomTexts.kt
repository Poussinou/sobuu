package com.sobuumedia.sobuu.android.custom_widgets

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.android.settings.theme.SourceSans

@Composable
fun IconAndText(
    modifier: Modifier = Modifier,
    text: String,
    textMaxLines: Int = 1,
    textColor: Color = MaterialTheme.colorScheme.secondary,
    fontSize: TextUnit,
    icon: ImageVector? = null,
    iconPainter: Painter? = null,
    customIcon: @Composable () -> Unit = {},
    iconColor: Color = MaterialTheme.colorScheme.secondary,) {

    Row (
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ){
        if(icon == null && iconPainter != null) {
            Icon(
                painter = iconPainter,
                modifier = Modifier.size(16.dp),
                contentDescription = "",
                tint = iconColor,
            )
        } else if(icon != null && iconPainter == null) {
            Icon(
                imageVector = icon,
                modifier = Modifier.size(16.dp),
                contentDescription = "",
                tint = iconColor,
            )
        } else {
            customIcon()
        }

        SobuuText(
            text = text,
            modifier = Modifier.padding(start = 2.dp),
            textColor = textColor,
            textSize = fontSize,
            maxLines = textMaxLines
        )
    }
}

@Composable
fun SobuuText(
    modifier: Modifier = Modifier,
    text: String,
    textSize: TextUnit = 14.sp,
    textColor: Color = MaterialTheme.colorScheme.secondary,
    textWeight: FontWeight = FontWeight.Normal,
    maxLines: Int = 1,
    textAlign: TextAlign = TextAlign.Justify
) {
    Text(
        text = text,
        modifier = Modifier.padding(start = 2.dp).then(modifier),
        maxLines = maxLines,
        style = TextStyle(
            fontFamily = SourceSans,
            color = textColor,
            fontSize = textSize,
            fontWeight = textWeight,
            textAlign = textAlign
        ),
    )
}

@Composable
fun Chip(
    text: String,
    backgroundColor: Color,
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier,
        shape = RoundedCornerShape(20.dp),
        color = backgroundColor
    ) {
        Text(
            text = text,
            style = TextStyle(
                fontFamily = SourceSans,
                fontSize = 16.sp,
            ),
            color = MaterialTheme.colorScheme.background,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp, top = 4.dp, bottom = 4.dp)
        )
    }
}