package com.sobuumedia.sobuu.android.authentication

sealed class AuthenticationUIEvent {
    object logoutUser: AuthenticationUIEvent()
}
