package com.sobuumedia.sobuu.android.navigation

sealed class Screen(val route: String) {
    object SplashScreen : Screen("splash")
    object LoginScreen : Screen("login")
    object HomeScreen : Screen("home")
    object ForgotPasswordScreen : Screen("forgot_password")
    object RegistrationScreen : Screen("registration")
    object LongTextScreen : Screen("long_text")
    object SentEmailScreen : Screen("sent_email")
    object CurrentlyReadingBookScreen : Screen("current_book")
    object CommentsScreen : Screen("comments") {
        fun String.passPageAndPercentage(page: Int?, percentage: Double?): String {
            return buildString {
                append(this@passPageAndPercentage)
                append("?book_page=$page&book_percentage=$percentage")
            }
        }
    }
    object AddNewCommentScreen : Screen("add_new_comment"){
        fun String.passStringPageAndPercentage(page: String?, percentage: String?): String {
            return buildString {
                append(this@passStringPageAndPercentage)
                append("?book_page=$page&book_percentage=$percentage")
            }
        }
        fun String.passParentCommentId(parentCommentId: String?): String {
            return buildString {
                append(this@passParentCommentId)
                append("&parent_comment_id=$parentCommentId")
            }
        }
        fun String.passBookData(title: String, authors: String): String {
            return buildString {
                append(this@passBookData)
                append("&book_title=$title&book_authors=$authors")
            }
        }
    }
    object BookScreen : Screen("book")
    object AddBookScreen : Screen("add_book")
    object BookCoverScreen : Screen("book_cover")
    object ProfileScreen : Screen("profile")
    object SettingsScreen : Screen("settings")
    object AboutScreen : Screen("about")

    fun withArgs(vararg args: String): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }

    fun withArgs(vararg args: Boolean): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}