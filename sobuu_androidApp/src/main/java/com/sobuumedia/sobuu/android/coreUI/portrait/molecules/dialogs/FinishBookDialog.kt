package com.sobuumedia.sobuu.android.coreUI.portrait.molecules.dialogs

import android.content.res.Configuration
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.RatingStarsBar
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingUIEvent
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingViewModel
import com.sobuumedia.sobuu.android.custom_widgets.CustomTextButton
import com.sobuumedia.sobuu.android.custom_widgets.WideTextField
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs

private const val TAG = "FinishBookDialog"

@Composable
fun FinishBookDialog(
    modifier: Modifier = Modifier,
    showDialogState: MutableState<Boolean> = mutableStateOf(false),
    bookTitle: String,
    bookId: String,
    logger: SobuuLogs
) {

    val context = LocalContext.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val textColor = MaterialTheme.colorScheme.secondary
    val ratingColor = MaterialTheme.colorScheme.tertiary
    val padding = 10.dp
    val roundCornerRadius = 10.dp
    
    var ratingText: String by remember { mutableStateOf("") }

    var emptyRatingWarning: Boolean by remember { mutableStateOf(false) }

    Dialog(
        onDismissRequest = { showDialogState.value = false },
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
    ) {
        Box {
            Column(
                Modifier
                    .fillMaxWidth()
                    .background(backgroundColor)
                    .padding(padding)
                    .clip(shape = RoundedCornerShape(roundCornerRadius))
                    .then(modifier),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Title
                Text(
                    modifier = Modifier.padding(padding * 2),
                    text = "${SharedRes.strings.home_currentlyReading_finishTitle.stringResource(context = context)} \"$bookTitle\"",
                    style = Typography.titleSmall.copy(color = textColor)
                )

                // Text 1
                Text(
                    modifier = Modifier.padding(vertical = padding),
                    text = SharedRes.strings.home_currentlyReading_finishText1.stringResource(context = context),
                    style = Typography.bodyMedium.copy(color = textColor)
                )

                // Text 2
                Text(
                    modifier = Modifier.padding(vertical = padding),
                    text = SharedRes.strings.home_currentlyReading_finishText2.stringResource(context = context),
                    style = Typography.bodyMedium.copy(color = textColor)
                )

                // Stars line
                RatingStarsBar()
            }
        }
    }
}



@Composable
fun FinishBookDialog(
    modifier: Modifier = Modifier,
    onShowDialogState: MutableState<Boolean> = mutableStateOf(true),
    bookTitle: String,
    bookId: String,
    viewModel: CurrentlyReadingViewModel
) {
    val context = LocalContext.current
    var rating: Double by remember { mutableDoubleStateOf(0.0) }
    var ratingText: String by remember { mutableStateOf("") }
    var emptyRatingWarning: Boolean by remember { mutableStateOf(false) }

    Dialog(
        onDismissRequest = {
            onShowDialogState.value = false
        },
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
        content = {
            Box {
                if (emptyRatingWarning) {
                    Toast.makeText(
                        context,
                        SharedRes.strings.home_currentlyReading_emptyRateNumber.stringResource(context = context),
                        Toast.LENGTH_LONG
                    ).show()
                }
                Column(
                    Modifier
                        .fillMaxWidth()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(10.dp)
                        .clip(shape = RoundedCornerShape(10.dp))
                        .then(modifier),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {

                    // Text field
                    Box {
                        WideTextField(
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight(0.3f),
                            fieldValue = ratingText,
                            onFieldValueChange = {
                                ratingText = it
                            },
                            placeholderText = SharedRes.strings.home_currentlyReading_rateBookHint.stringResource(
                                context = context
                            ),
                            onKeyboardActionClicked = {
                                ratingText = "${ratingText}\r\n"
                            })
                    }

                    // Yes - No Buttons
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.End,
                        verticalAlignment = Alignment.Bottom
                    ) {
                        CustomTextButton(
                            modifier = Modifier.padding(vertical = 10.dp),
                            onClick = { onShowDialogState.value = false },
                            color = MaterialTheme.colorScheme.error,
                            text = SharedRes.strings.general_no.stringResource(context = context),
                            fontSize = 20.sp
                        )
                        CustomTextButton(
                            modifier = Modifier.padding(vertical = 10.dp),
                            onClick = {
                                if (rating <= 0) {
                                    emptyRatingWarning = true
                                } else {
                                    emptyRatingWarning = false
                                    viewModel.onEvent(
                                        CurrentlyReadingUIEvent.FinishBook(
                                            bookId = bookId,
                                            rate = rating.toFloat(),
                                            ratingText = ratingText,
                                        )
                                    )
                                    onShowDialogState.value = false
                                }
                            },
                            color = MaterialTheme.colorScheme.primary,
                            text = SharedRes.strings.general_yes.stringResource(context = context),
                            fontSize = 20.sp
                        )
                    }
                }
            }
        }
    )
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun FinishBookDialogPreview() {
    SobuuTheme {
        FinishBookDialog(
            bookTitle = "Sharp knifes",
            bookId = "e98hinwe98h3ews",
            logger = NapierLogs()
        )
    }
}
