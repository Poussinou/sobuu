package com.sobuumedia.sobuu.android.coreUI.landscape.molecules

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.util.lerp
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlin.math.absoluteValue

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FinishedBooksSectionLandscape(
    bookList: List<FinishedReadingBook>?
) {
    val pagerState = rememberPagerState(pageCount = { bookList?.size ?: 0 })

    BooksCarouselLandscape(
        pagerState = pagerState,
        count = bookList?.size ?: 0
    ) { page ->
        val book = bookList?.get(page)?.bookWithProgress ?: return@BooksCarouselLandscape

        BookCardLandscape(
            modifier = Modifier
                .graphicsLayer {
                    val pageOffset = pagerState.getOffsetFractionForPage(page).absoluteValue

                    lerp(
                        start = 0.85f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    ).also { scale ->
                        scaleX = scale
                        scaleY = scale
                    }

                    // We animate the alpha, between 50% and 100%
                    alpha = lerp(
                        start = 0.5f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    )
                },
            picture = book.book.picture,
            progress = book.bookProgress.progressInPercentage,
            startedToRead = book.bookProgress.startedToRead,
            finishedToRead = book.bookProgress.finishedToRead,
            title = book.book.title,
            authors = book.book.authors.joinToString(", "),
            finished = book.bookProgress.finished,
            giveUp = book.bookProgress.giveUp
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO, widthDp = 720, heightDp = 360)
@Composable
fun FinishedBooksSectionLandscapePreview() {
    val book = FinishedReadingBook(
        bookWithProgress = BookWithProgress(
            bookProgress = BookProgress(
                id = "",
                giveUp = false,
                finishedToRead = null,
                finished = false,
                startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
                percentage = 0.0,
                page = 0,
                progressInPercentage = 0.0
            ),
            book = Book(
                title = "Test Book",
                authors = listOf("Flame McGuy"),
                picture = "",
                thumbnail = "",
                userRating = null,
                id = "",
                allReviews = emptyList(),
                credits = CreditsBO(),
                bookDescription = "",
                genres = listOf(),
                isbn = Pair("", ""),
                lang = "en",
                peopleReadingIt = 6,
                publishedDate = "",
                publisher = "",
                readingStatus = BookReadingStatus.NOT_READ,
                serie = "",
                serieNumber = -1,
                totalComments = 0,
                totalPages = 654,
                totalRating = 0.0
            ),
            bookProgressComments = listOf()
        ),
        userRating = UserBookRating(
            id = "",
            user = Profile("", "Test", "Quan", "Qant", "quant@testt.com", ""),
            review = null,
            rating = 3.3,
            date = Clock.System.now().toLocalDateTime(TimeZone.UTC)
        )
    )

    SobuuTheme {
        FinishedBooksSectionLandscape(
            bookList = listOf(book, book)
        )
    }
}