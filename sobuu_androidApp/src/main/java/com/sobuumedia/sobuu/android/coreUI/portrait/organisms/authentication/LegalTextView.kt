package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun LegalTextView(
    textType: TextType
) {
    val backgroundColor = MaterialTheme.colorScheme.background

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
    ) {

        val path: String = when(textType) {
            TextType.LICENSES -> "app-licenses"
            TextType.TERMS_AND_CONDITIONS -> "app-terms"
            TextType.PRIVACY_POLICY -> "app-privacy"
        }

        val url = "https://getsobuu.com/$path"

        AndroidView(factory = {
            WebView(it).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                settings.javaScriptEnabled = true
                webViewClient = WebViewClient()
                loadUrl(url)
            }
        }, update = {
            it.loadUrl(url)
        })
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun LegalTextViewPreview() {
    SobuuAuthTheme {
        LegalTextView(
            textType = TextType.TERMS_AND_CONDITIONS
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun LegalTextViewDarkPreview() {
    SobuuAuthTheme {
        LegalTextView(
            textType = TextType.LICENSES
        )
    }
}