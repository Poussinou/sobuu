package com.sobuumedia.sobuu.android.authentication.login

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError

data class LoginState(
    val isLoading: Boolean = false,
    val loginUsername: String = /*if(BuildConfig.DEBUG) "supabase.z2yho@aleeas.com" else*/ "",
    val loginPassword: String = /*if(BuildConfig.DEBUG) "asdf1234" else*/ "",
    val error: AuthenticationError? = null,
)
