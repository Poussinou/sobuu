package com.sobuumedia.sobuu.android.settings.settings

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForwardIos
import androidx.compose.material.icons.filled.TripOrigin
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_title
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_1
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_2
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_analytics_explanation_data_3
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_forwardArrow
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_licenses
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_setThemeButton
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_theme
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_themeDark
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_themeLight
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_themeSystem
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_title
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.custom_widgets.CustomTopAppBar
import com.sobuumedia.sobuu.android.navigation.Screen
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.settings.theme.SpanishGray
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions
import org.koin.androidx.compose.koinViewModel

@Composable
fun SettingsScreen(
    navigator: NavController,
    navigateBack: () -> Unit,
    viewModel: SettingsViewModel = koinViewModel()
) {
    val context = LocalContext.current

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                CustomTopAppBar(
                    navigateBack = { navigateBack() },
                    title = settings_main_title.stringResource(context = context),
                )
            },
            content = { padding ->
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(padding),
                ) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        Column (modifier = Modifier.fillMaxWidth()) {
                            AboutButton(navigator = navigator)
                            // DarkModeButton(viewModel = viewModel)
                            AnalyticsButton(viewModel = viewModel)
                        }

                        Column(modifier = Modifier.fillMaxWidth()) {
                            PrivacyPolicyButton(navigator = navigator)
                            TermsAndConditionsButton(navigator = navigator)
                            LicensesButton(navigator = navigator)
                        }
                    }

//                    TODO Currently it's not ready for production
//                    if (viewModel.state.displayThemeDialog) {
//                        PickThemeDialog(viewModel)
//                    }

                    if (viewModel.state.displayAnalyticsExplanationDialog) {
                        AnalyticsExplanationDialog(viewModel = viewModel)
                    }
                }
            }
        )
    }
}

@Composable
fun PrivacyPolicyButton(navigator: NavController) {
    BaseSettingsButton(text = settings_main_privacy.stringResource(context = LocalContext.current)) {
        navigator.navigate(Screen.LongTextScreen.withArgs(TextType.PRIVACY_POLICY.name))
    }
}

@Composable
fun TermsAndConditionsButton(navigator: NavController) {
    BaseSettingsButton(text = settings_main_terms.stringResource(context = LocalContext.current)) {
        navigator.navigate(Screen.LongTextScreen.withArgs(TextType.TERMS_AND_CONDITIONS.name))
    }
}

@Composable
fun LicensesButton(navigator: NavController) {
    BaseSettingsButton(text = settings_main_licenses.stringResource(context = LocalContext.current)) {
        navigator.navigate(Screen.LongTextScreen.withArgs(TextType.LICENSES.name))
    }
}

@Composable
fun AboutButton(navigator: NavController) {
    BaseSettingsButton(text = settings_about_title.stringResource(context = LocalContext.current)) {
        navigator.navigate(Screen.AboutScreen.route)
    }
}

@Composable
fun DarkModeButton(viewModel: SettingsViewModel) {
    BaseSettingsButton(text = settings_main_theme.stringResource(context = LocalContext.current)) {
        viewModel.onEvent(SettingsUIEvent.displayThemeModeDialog)
    }
}

@Composable
fun AnalyticsButton(viewModel: SettingsViewModel) {
    viewModel.onEvent(SettingsUIEvent.getAnalyticsEnabled)
    BaseSettingsSwitch(
        text = settings_main_analytics.stringResource(context = LocalContext.current),
        switchDefaultValue = viewModel.state.currentAnalyticsEnabled,
        action = {
            viewModel.onEvent(SettingsUIEvent.displayAnalyticsDialog)
        },
        switchAction = {
            viewModel.onEvent(SettingsUIEvent.SetAnalyticsEnabled(it))
        })
}

@Composable
fun PickThemeDialog(viewModel: SettingsViewModel) {
    val context = LocalContext.current
    val themeOptions = listOf(
        settings_main_themeLight.stringResource(context = context),
        settings_main_themeDark.stringResource(context = context),
        settings_main_themeSystem.stringResource(context = context)
    )

    val selectedCurrentTheme: Int = when(viewModel.state.currentTheme) {
        ThemeOptions.LIGHT -> 0
        ThemeOptions.DARK -> 1
        ThemeOptions.SYSTEM -> 2
        else -> 0
    }

    val (selectedOption, onOptionSelected) = remember { mutableStateOf(themeOptions[selectedCurrentTheme]) }

    Dialog(
        onDismissRequest = {
            viewModel.onEvent(SettingsUIEvent.displayThemeModeDialog)
        },
        DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
                .background(MaterialTheme.colorScheme.background, shape = RoundedCornerShape(8.dp))
                .padding(20.dp),
        ) {
            themeOptions.forEach { text ->
                Row(
                    Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = (text == selectedOption),
                            onClick = {
                                onOptionSelected(text)
                            }
                        )
                        .padding(horizontal = 16.dp)
                ) {
                    RadioButton(
                        colors = RadioButtonDefaults.colors(
                            unselectedColor = MaterialTheme.colorScheme.secondary,
                            selectedColor = MaterialTheme.colorScheme.error
                        ),
                        selected = (text == selectedOption),
                        onClick = { onOptionSelected(text) }
                    )
                    Text(
                        text = text,
                        style = TextStyle(
                            fontFamily = SourceSans,
                            fontSize = 16.sp,
                            color = MaterialTheme.colorScheme.secondary
                        ),
                        modifier = Modifier.padding(start = 16.dp, top = 10.dp)
                    )
                }
            }

            Spacer(modifier = Modifier.padding(horizontal = 30.dp))

            FilledTonalButton(
                modifier = Modifier
                    .clip(RoundedCornerShape(10.dp)),
                onClick = {
                    val saveOption: ThemeOptions = when (selectedOption) {
                        themeOptions[0] -> ThemeOptions.LIGHT
                        themeOptions[1] -> ThemeOptions.DARK
                        themeOptions[2] -> ThemeOptions.SYSTEM
                        else -> ThemeOptions.LIGHT
                    }

                    viewModel.onEvent(SettingsUIEvent.SetThemeMode(saveOption))
                },
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.error,
                    contentColor = MaterialTheme.colorScheme.background,
                    disabledContainerColor = MaterialTheme.colorScheme.tertiary,
                    disabledContentColor = MaterialTheme.colorScheme.background,
                )
            ) {
                Text(
                    settings_main_setThemeButton.stringResource(context = context),
                    style = TextStyle(
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.onError,
                        fontWeight = FontWeight.Normal,
                        fontFamily = SourceSans
                    ),
                    textAlign = TextAlign.Center,
                )
            }
        }
    }
}

@Composable
fun AnalyticsExplanationDialog(viewModel: SettingsViewModel) {
    val context = LocalContext.current
    Dialog(
        onDismissRequest = {
            viewModel.onEvent(SettingsUIEvent.displayAnalyticsDialog)
        },
        DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
                .background(MaterialTheme.colorScheme.background, shape = RoundedCornerShape(8.dp))
                .padding(20.dp),
        ) {

            val textStyle = TextStyle(
                fontFamily = SourceSans,
                fontSize = 16.sp,
                color = MaterialTheme.colorScheme.secondary
            )

            Text(text = settings_main_analytics_explanation.stringResource(context = context), style = textStyle)

            Spacer(modifier = Modifier.padding(horizontal = 30.dp))

            BulletPointText {
                Text(text = settings_main_analytics_explanation_data_1.stringResource(context = context), style = textStyle)
            }
            BulletPointText {
                Text(text = settings_main_analytics_explanation_data_2.stringResource(context = context), style = textStyle)
            }
            BulletPointText {
                Text(text = settings_main_analytics_explanation_data_3.stringResource(context = context), style = textStyle)
            }
        }
    }
}

@Composable
fun BulletPointText(
    text: @Composable () -> Unit
) {
    Row(modifier = Modifier.padding(start = 5.dp, top = 4.dp)) {
        Icon(
            modifier = Modifier.padding(5.dp),
            imageVector = Icons.Filled.TripOrigin,
            contentDescription = "")
        text()
    }
}

@Composable
fun BaseSettingsButton(
    text: String,
    action: () -> Unit
) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .border(border = BorderStroke(color = SpanishGray, width = 1.dp))
        .background(MaterialTheme.colorScheme.background)
        .clickable {
            action()
        }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth()
        ) {
            Text(
                text = text,
                style = TextStyle(
                    color = MaterialTheme.colorScheme.secondary,
                    fontFamily = SourceSans,
                    fontSize = 20.sp
                )
            )
            Icon(
                imageVector = Icons.AutoMirrored.Filled.ArrowForwardIos,
                contentDescription = settings_main_forwardArrow.stringResource(context = LocalContext.current),
                tint = MaterialTheme.colorScheme.secondary
            )
        }
    }
}


@Composable
fun BaseSettingsSwitch(
    text: String,
    switchDefaultValue: Boolean,
    action: () -> Unit,
    switchAction: (value: Boolean) -> Unit,
) {
    val switchValue = remember { mutableStateOf(switchDefaultValue) }
    Box(modifier = Modifier
        .fillMaxWidth()
        .border(border = BorderStroke(color = SpanishGray, width = 1.dp))
        .background(MaterialTheme.colorScheme.background)
        .clickable {
            action()
        }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth()
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(0.5f),
                text = text,
                maxLines = 2,
                style = TextStyle(
                    color = MaterialTheme.colorScheme.secondary,
                    fontFamily = SourceSans,
                    fontSize = 20.sp
                )
            )
            Switch(
                checked = switchValue.value,
                onCheckedChange = {
                    switchValue.value = it
                    switchAction(it)
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = MaterialTheme.colorScheme.error,
                    uncheckedThumbColor = MaterialTheme.colorScheme.secondary,
                    checkedTrackColor = MaterialTheme.colorScheme.primary,
                    uncheckedTrackColor = MaterialTheme.colorScheme.tertiary,
                )
            )
        }
    }
}


@Preview(showSystemUi = true, showBackground = true)
@Composable
fun DarkModeButtonPreview() {
    BaseSettingsButton(
        text = settings_main_theme.stringResource(context = LocalContext.current)
    ) {}
}

@Preview
@Composable
fun ThemeDialogPreview() {
    PickThemeDialog(viewModel = koinViewModel())
}