package com.sobuumedia.sobuu.android.currently_reading


sealed class CurrentlyReadingUIEvent {
    data class UpdateProgress(val bookID: String, val page: Int? = null,
                              val percentage: Double? = null): CurrentlyReadingUIEvent()
    data class UpdateProgressChanged(val page: Int? = null,
                                     val percentage: Double? = null): CurrentlyReadingUIEvent()
    data class FetchBookProgressData(val bookId: String): CurrentlyReadingUIEvent()
    data class StartScreen(val bookId: String): CurrentlyReadingUIEvent()
    data class GiveUpBook(val bookId: String): CurrentlyReadingUIEvent()
    data class FinishBook(val bookId: String, val rate: Float,
                          val ratingText: String): CurrentlyReadingUIEvent()
}