package com.sobuumedia.sobuu.android.main

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_processingQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unauthorizedQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.search_error_errorSearch
import com.sobuumedia.sobuu.SharedRes.strings.shelf_errors_errorEmptyTerm
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.home.SearchListViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.SearchListView
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.book.remote.BookError
import org.koin.androidx.compose.koinViewModel

@Composable
fun SearchListScreen(
    modifier: Modifier = Modifier,
    navigateToBookScreen: (String) -> Unit,
    navigateToAddBookScreen: (Boolean) -> Unit,
    viewModel: SearchViewModel = koinViewModel()
) {
    val backgroundColor = MaterialTheme.colorScheme.background

    val state = viewModel.state
    val listState = rememberLazyListState()
    val configuration = LocalConfiguration.current

    when(configuration.orientation) {
        Configuration.ORIENTATION_LANDSCAPE -> {
            SearchListViewLandscape(
                modifier = Modifier.then(modifier).background(backgroundColor),
                isLoading = state.isLoading,
                errorText = getStringFromSearchError(state.error),
                bookList = viewModel.booksList,
                navigateToBookScreen = { navigateToBookScreen(it) },
                navigateToAddBookScreen = { navigateToAddBookScreen(it) },
                onMoreInfoClick = { viewModel.onEvent(SearchUIEvent.clickOnMoreInfo) },
                listState = listState,
                emptyResult = state.emptyResult,
                displayMoreInfo = state.displayMoreInfo,
                displaySearchFurther = state.displaySearchFurther,
                displayWarningWithEmail = state.displayWarningWithEmail
            )
        }
        else -> {
            SearchListView(
                modifier = Modifier.then(modifier).background(backgroundColor),
                isLoading = state.isLoading,
                errorText = getStringFromSearchError(state.error),
                bookList = viewModel.booksList,
                navigateToBookScreen = { navigateToBookScreen(it) },
                navigateToAddBookScreen = { navigateToAddBookScreen(it) },
                onMoreInfoClick = { viewModel.onEvent(SearchUIEvent.clickOnMoreInfo) },
                listState = listState,
                emptyResult = state.emptyResult,
                displayMoreInfo = state.displayMoreInfo,
                displaySearchFurther = state.displaySearchFurther,
                displayWarningWithEmail = state.displayWarningWithEmail
            )
        }
    }
}

@Composable
private fun getStringFromSearchError(error: BookError?): String {
    val context = LocalContext.current
    if (error == null) return ""
    return when (error) {
        BookError.EmptySearchTermError -> shelf_errors_errorEmptyTerm.stringResource(context = context)
        BookError.SearchBookError -> search_error_errorSearch.stringResource(context = context)
        BookError.InvalidSessionTokenError -> general_errors_invalidSessionToken.stringResource(
            context = context
        )

        BookError.ProcessingQueryError -> general_errors_processingQuery.stringResource(context = context)
        BookError.TimeOutError -> general_errors_timeout.stringResource(context = context)
        BookError.UnauthorizedQueryError -> general_errors_unauthorizedQuery.stringResource(context = context)
        else -> general_errors_unknown.stringResource(context = context)
    }
}