package com.sobuumedia.sobuu.android.authentication.register

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.intl.Locale
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.BuildConfig
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class RegistrationViewModel(private val auth: IAuthenticationRepository) : ViewModel() {

    var state by mutableStateOf(RegistrationState())

    private val resultChannel = Channel<AuthenticationResult<Unit>>()
    val registrationResult = resultChannel.receiveAsFlow()

    fun onEvent(event: RegistrationUIEvent) {
        when (event) {
            is RegistrationUIEvent.RegistrationPasswordChanged -> {
                state = state.copy(registrationPassword = event.value)
            }

            is RegistrationUIEvent.RegistrationUsernameChanged -> {
                state = state.copy(registrationUsername = event.value)
            }

            is RegistrationUIEvent.registerUser -> registration()
            is RegistrationUIEvent.RegistrationEmailChanged -> {
                state = state.copy(registrationEmail = event.value)
            }

            is RegistrationUIEvent.RegistrationFirstNameChanged -> {
                state = state.copy(registrationFirstname = event.value)
            }

            is RegistrationUIEvent.RegistrationLastNameChanged -> {
                state = state.copy(registrationLastname = event.value)
            }

            is RegistrationUIEvent.RegistrationPasswordConfirmationChanged -> {
                state = state.copy(registrationPasswordConfirmation = event.value)
            }

            is RegistrationUIEvent.PrivacyPolicyConfirmationSwitchChanged -> {
                state = state.copy(privacyPolicyConfirmationSwitch = event.value)
            }
        }
    }

    fun handleError(error: AuthenticationError?) {
        state = when (error) {
            is AuthenticationError.InvalidEmailError,
            is AuthenticationError.EmailAlreadyTaken,
            is AuthenticationError.WrongEmailFormatError -> {
                state.copy(error = error, emailError = true, usernameError = false, passwordError = false, confirmationPassError = false)
            }

            is AuthenticationError.InvalidCredentials,
            is AuthenticationError.EmptyCredentialsError,
            is AuthenticationError.UsernameAlreadyTaken -> {
                state.copy(error = error, usernameError = true, passwordError = false, confirmationPassError = false, emailError = false)
            }

            is AuthenticationError.ResetPassword,
            is AuthenticationError.PasswordTooShort,
            is AuthenticationError.WeakPassword -> {
                state.copy(error = error, passwordError = true, emailError = false, usernameError = false)
            }

            is AuthenticationError.PasswordConfirmationDifferent -> {
                state.copy(error = error, passwordError = true, confirmationPassError = true, emailError = false, usernameError = false)
            }

            else -> {
                state.copy(error = error, passwordError = false, emailError = false, confirmationPassError = false, usernameError = false)
            }
        }
    }

    private fun registration() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)

            if(state.registrationPassword != state.registrationPasswordConfirmation) {
                handleError(error = AuthenticationError.PasswordConfirmationDifferent)
            } else {
                // Trick to run UI tests.
                val result = if (BuildConfig.DEBUG &&
                    (state.registrationEmail.startsWith("test") && state.registrationEmail.endsWith(
                        "@sobuu.com"
                    ))
                ) {
                    AuthenticationResult.Registered()
                } else {
                    auth.registerUser(
                        username = state.registrationUsername,
                        email = state.registrationEmail.lowercase(),
                        password = state.registrationPassword,
                        firstname = state.registrationFirstname,
                        lastname = state.registrationLastname,
                        language = Locale.current.language
                    )
                }

                resultChannel.send(result)
            }

            state = state.copy(isLoading = false)
        }
    }
}