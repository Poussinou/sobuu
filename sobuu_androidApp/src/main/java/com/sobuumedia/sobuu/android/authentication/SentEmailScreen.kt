package com.sobuumedia.sobuu.android.authentication

import android.content.res.Configuration
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.authentication.SentEmailViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication.SentEmailView

enum class EmailType {
    RESET_PASSWORD, VERIFICATION
}

@Composable
fun SentEmailScreen(
    navToLogin: () -> Unit,
    emailType: EmailType,
) {
    val configuration = LocalConfiguration.current

    when (configuration.orientation) {
        Configuration.ORIENTATION_LANDSCAPE -> {
            SentEmailViewLandscape(navToLoginScreen = navToLogin, emailType = emailType)
        }
        else -> {
            SentEmailView(navToLoginScreen = navToLogin, emailType = emailType)
        }
    }
}