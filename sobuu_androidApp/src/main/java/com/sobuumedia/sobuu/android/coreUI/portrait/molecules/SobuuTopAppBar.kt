package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_topBar_backButton
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SobuuTopAppBar(
    navigateBack: () -> Unit,
    title: String,
    backgroundColor: Color = MaterialTheme.colorScheme.background,
    iconColor: Color = MaterialTheme.colorScheme.secondary,
    titleColor: Color = MaterialTheme.colorScheme.secondary
) {
    CenterAlignedTopAppBar(
        title = {
            Text(
                text = title,
                style = Typography.titleSmall.copy(color = titleColor)
            )
        },
        colors = TopAppBarDefaults.largeTopAppBarColors(backgroundColor),
        modifier = Modifier.fillMaxWidth(),
        navigationIcon = {
            IconButton(
                modifier = Modifier
                    .semantics {
                        this.contentDescription = contentDescription_topBar_backButton.stringResource()
                    },
                onClick = {
                    navigateBack()
                }
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = Icons.AutoMirrored.Filled.ArrowBack.name,
                    tint = iconColor,
                )
            }
        }
    )
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun TopAppBarLightPreview() {
    SobuuAuthTheme {
        SobuuTopAppBar(
            navigateBack = {},
            title = "Create Account"
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun TopAppBarDarkPreview() {
    SobuuAuthTheme {
        SobuuTopAppBar(
            navigateBack = {},
            title = "Create Account"
        )
    }
}