package com.sobuumedia.sobuu.android.di

import com.sobuumedia.sobuu.android.add_book.AddBookViewModel
import com.sobuumedia.sobuu.android.analytics.SobuuAnalytics
import com.sobuumedia.sobuu.android.authentication.login.LoginViewModel
import com.sobuumedia.sobuu.android.authentication.register.RegistrationViewModel
import com.sobuumedia.sobuu.android.authentication.reset_pass.ResetPassViewModel
import com.sobuumedia.sobuu.android.authentication.splash.ConnectivityObserver
import com.sobuumedia.sobuu.android.authentication.splash.NetworkConnectivityObserver
import com.sobuumedia.sobuu.android.authentication.splash.SplashViewModel
import com.sobuumedia.sobuu.android.book.BookViewModel
import com.sobuumedia.sobuu.android.comments.CommentsViewModel
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingViewModel
import com.sobuumedia.sobuu.android.main.HomeViewModel
import com.sobuumedia.sobuu.android.main.SearchViewModel
import com.sobuumedia.sobuu.android.profile.ProfileViewModel
import com.sobuumedia.sobuu.android.settings.settings.SettingsViewModel
import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.getDatabaseBuilder
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val androidModule = module {

    viewModelOf(::LoginViewModel)

    single<ConnectivityObserver> { NetworkConnectivityObserver(androidApplication()) }

    viewModel { RegistrationViewModel(get()) }

    viewModel { ResetPassViewModel(get()) }

    viewModel { SplashViewModel(get(), get()) }

    single { KMMStorage(androidApplication()) }

    single { getDatabaseBuilder(androidApplication()) }

    viewModel { HomeViewModel(get(), get()) }

    viewModel { SearchViewModel(get()) }

    viewModel { CurrentlyReadingViewModel(get()) }

    viewModel { CommentsViewModel(get(), get(), get()) }

    viewModel { BookViewModel(get()/*, get()*/, get()) }

    viewModel { ProfileViewModel(get()) }

    viewModel { SettingsViewModel(get()) }

    viewModel { AddBookViewModel(get()) }

    single { SobuuAnalytics() }
}