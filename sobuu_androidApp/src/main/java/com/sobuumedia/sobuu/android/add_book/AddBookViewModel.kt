package com.sobuumedia.sobuu.android.add_book

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.analytics.AnalyticsEvent
import com.sobuumedia.sobuu.analytics.events.SaveBookManuallyEvent
import com.sobuumedia.sobuu.android.SobuuApp.Companion.analytics
import com.sobuumedia.sobuu.android.currently_reading.isNumber
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import kotlinx.coroutines.launch
import org.acra.ACRA

class AddBookViewModel(private val book: IBookRepository) : ViewModel() {

    var state by mutableStateOf(AddBookState())
    var booksList by mutableStateOf<List<Book>?>(emptyList())

    fun onEvent(event: AddBookUIEvent) {
        when (event) {
            is AddBookUIEvent.SearchISBNChanged -> {
                state = state.copy(
                    searchISBN = event.value,
                )
            }

            AddBookUIEvent.cleanSearchISBN -> {
                state = state.copy(
                    searchISBN = "",
                )
                booksList = emptyList()
            }

            is AddBookUIEvent.removeErrorState -> TODO()
            is AddBookUIEvent.searchISBN -> searchISBN()
            is AddBookUIEvent.SaveBookData -> saveData(event.book)
            // Manually introduced data
            is AddBookUIEvent.BookTitleChanged -> {
                state = state.copy(bookName = event.value)
            }

            is AddBookUIEvent.BookAuthorsChanged -> {
                state = state.copy(bookAuthors = event.value)
            }

            is AddBookUIEvent.BookDescriptionChanged -> {
                state = state.copy(bookDescription = event.value)
            }

            is AddBookUIEvent.emptyBookIdData -> {
                state = state.copy(bookId = null)
            }

            is AddBookUIEvent.BookGenresChanged -> {
                state = if (state.bookGenres.contains(event.value)) {
                    state.copy(bookGenres = state.bookGenres - event.value)
                } else {
                    state.copy(bookGenres = state.bookGenres + event.value)
                }
            }

            is AddBookUIEvent.BookPictureChanged -> {
                state = state.copy(bookImage = event.value)
            }

            is AddBookUIEvent.BookISBNChanged -> {
                state = state.copy(bookISBN = event.value)
            }

            is AddBookUIEvent.BookLanguageChanged -> {
                state = state.copy(bookLanguage = event.value)
            }

            is AddBookUIEvent.BookPublishedDateChanged -> {
                state = state.copy(bookPublishedDate = event.value)
            }

            is AddBookUIEvent.BookPublisherChanged -> {
                state = state.copy(bookPublisher = event.value)
            }

            is AddBookUIEvent.BookSerieChanged -> {
                state = state.copy(bookIsSerie = event.value)
            }

            is AddBookUIEvent.BookSerieNameChanged -> {
                state = state.copy(bookSerieName = event.value)
            }

            is AddBookUIEvent.BookSerieNumberChanged -> {
                state = state.copy(bookSerieNumber = event.value)
            }

            is AddBookUIEvent.BookTotalPagesChanged -> {
                if(event.value.isNumber()) {
                    state = state.copy(bookTotalPages = event.value.toInt())
                }
            }

            is AddBookUIEvent.BookTranslatedChanged -> {
                state = state.copy(bookIsTranslated = event.value)
            }

            is AddBookUIEvent.BookTranslatorChanged -> {
                state = state.copy(bookTranslator = event.value)
            }

            is AddBookUIEvent.saveBookManuallyData -> {
                saveBookDataManually()
            }

            is AddBookUIEvent.closeErrorDialog -> {
                state = state.copy(displayErrorDialog = false)
            }

            is AddBookUIEvent.Error -> {
                state = state.copy(displayErrorDialog = true, error = event.bookError)
            }

            is AddBookUIEvent.LogDebug -> println("Debug: AddBookScreen -> ${event.log}")
            is AddBookUIEvent.LogError -> println("Error: AddBookScreen -> ${event.log}")
            is AddBookUIEvent.LogInfo -> println("Info: AddBookScreen -> ${event.log}")
        }
    }

    fun onAnalyticsEvent(event: AnalyticsEvent) {
        when (event) {
            is SaveBookManuallyEvent -> sendAnalyticData(event)
        }
    }

    private fun handleError(error: BookError?) {
        when(error) {
            is BookError.UnknownError -> {
                ACRA.errorReporter.putCustomData(
                    "Handle error in AddBookViewModel",
                    error.message ?: "Unknown error with empty message"
                )
            }
            else -> {
                state = state.copy(error = error)
            }
        }
    }

    private fun saveData(bookData: Book) {
        viewModelScope.launch {

            state = state.copy(isLoading = true)

            val result = book.saveBook(
                title = bookData.title,
                authors = bookData.authors,
                description = bookData.bookDescription,
                credits = bookData.credits,
                picture = bookData.picture,
                thumbnail = bookData.thumbnail,
                isbn = bookData.isbn,
                genres = bookData.genres,
                publisher = bookData.publisher,
                publishedDate = bookData.publishedDate,
                totalPages = bookData.totalPages,
                language = bookData.lang,
                serie = bookData.serie,
                seriesNumber = bookData.serieNumber
            )

            if (result.data == null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(bookId = result.data?.id)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun saveBookDataManually() {
        val title = state.bookName
        val authors = state.bookAuthors
        val publisher = state.bookPublisher
        val publishedDate = state.bookPublishedDate
        val description = state.bookDescription
        val pages = state.bookTotalPages
        val translator = state.bookTranslator
        val isbn = state.bookISBN
        val serieName = state.bookSerieName
        val serieNumber = state.bookSerieNumber
        val language = state.bookLanguage
        val genres = state.bookGenres
        val cover = state.bookImage

        if (title.isBlank() || authors.isEmpty() || publisher.isBlank() || publishedDate.isBlank() ||
            pages <= 0 || isbn.isBlank() || isbn.length < 10 || isbn.length > 13 || language.isBlank() ||
            genres.isEmpty()
        ) {
            state = state.copy(error = BookError.MissingRequiredDataError, displayErrorDialog = true)
        } else {

            viewModelScope.launch {
                state = state.copy(isLoading = true, displayErrorDialog = false)

                val savedBook = book.saveBookManually(
                    title = title,
                    authors = authors.split(","),
                    description = description,
                    picture = cover?.readBytes() ?: byteArrayOf(),
                    thumbnail = cover?.readBytes() ?: byteArrayOf(),
                    publisher = publisher,
                    credits = CommonItemApi.CreditsItemApi(
                        translators = listOf(translator),
                        illustrators = listOf(),
                        contributors = listOf(),
                        others = listOf()
                    ),
                    totalPages = pages,
                    isbn = Pair(
                        if(isbn.length == 10) state.bookISBN else "",
                        if(isbn.length == 13) state.bookISBN else ""
                    ),
                    publishedDate = publishedDate,
                    genres = genres,
                    serie = serieName,
                    seriesNumber = serieNumber,
                    language = language,
                )

                state = if(savedBook.error == null) {
                    state.copy(manuallySavedBook = savedBook.data, isLoading = false)
                } else {
                    state.copy(error = savedBook.error, isLoading = false, displayErrorDialog = true)
                }
            }
        }
    }

    private fun searchISBN() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = book.searchISBN(
                isbn = state.searchISBN
            )

            if (result.data.isNullOrEmpty()) {
                handleError(result.error)
                state = state.copy(displayBookListResult = false)
            } else {
                booksList = result.data
                handleError(null)
                state = state.copy(displayBookListResult = true)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun sendAnalyticData(event: AnalyticsEvent) {
        viewModelScope.launch {
            analytics.track(event)
        }
    }
}