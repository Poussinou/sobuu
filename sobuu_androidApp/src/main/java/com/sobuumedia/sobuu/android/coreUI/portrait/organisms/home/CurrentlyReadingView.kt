package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home

import android.content.res.Configuration
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun CurrentlyReadingView() {

}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CurrentlyReadingViewDarkPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        CurrentlyReadingView(
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun CurrentlyReadingViewPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        CurrentlyReadingView()
    }
}