package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.contentDescription_login_arrow
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun LoginCircleButton(
    onLoginButtonClick: () -> Unit
) {
    val iconColor = MaterialTheme.colorScheme.background
    val buttonColor = MaterialTheme.colorScheme.error
    val disabledButtonColor = MaterialTheme.colorScheme.tertiary

    FilledIconButton(
        modifier = Modifier
            .size(62.dp)
            .semantics {
                this.contentDescription = contentDescription_login_arrow.stringResource()
            },
        onClick = onLoginButtonClick,
        shape = CircleShape,
        colors = IconButtonDefaults.filledIconButtonColors(
            contentColor = iconColor,
            containerColor = buttonColor,
            disabledContainerColor = disabledButtonColor,
            disabledContentColor = iconColor,
        )
    ) {
        Icon(
            modifier = Modifier.size(32.dp),
            imageVector = Icons.AutoMirrored.Filled.ArrowForward,
            contentDescription = Icons.AutoMirrored.Filled.ArrowForward.name,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LoginCircleButtonPreview() {
    LoginCircleButton(
        onLoginButtonClick = {}
    )
}