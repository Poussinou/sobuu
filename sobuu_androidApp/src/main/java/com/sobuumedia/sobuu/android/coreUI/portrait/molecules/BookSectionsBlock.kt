package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.ErrorIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.BookStatusType
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun BookSectionsBlock(
    modifier: Modifier = Modifier,
    isLoading: Boolean,
    errorText: String?,
    currentSection: BookStatusType,
    onCurrentBookClicked: (String) -> Unit,
    onDisplayCurrentlyReadingMenuClicked: () -> Unit,
    onDisplayFinishedMenuClicked: () -> Unit,
    onDisplayGaveUpMenuClicked: () -> Unit,
    currentlyReadingBookList: List<BookWithProgress>?,
    gaveUpBookList: List<BookWithProgress>?,
    finishedBooksList: List<FinishedReadingBook>?
) {
    val backgroundColor = MaterialTheme.colorScheme.background

    when {
        isLoading -> CircularLoadingIndicator()
        errorText != null -> ErrorIndicator(errorText = errorText)
        else -> {
            Column(
                modifier = Modifier.background(backgroundColor).then(modifier)
            ) {
                BookSectionsButtonBar(
                    displayCurrentlyReading = onDisplayCurrentlyReadingMenuClicked,
                    displayFinished = onDisplayFinishedMenuClicked,
                    displayGaveUp = onDisplayGaveUpMenuClicked,
                    currentSection = currentSection
                )

                when (currentSection) {
                    BookStatusType.CURRENTLY_READING -> {
                        CurrentlyReadingBooksSection(
                            bookList = currentlyReadingBookList,
                            onClick = onCurrentBookClicked
                        )
                    }
                    BookStatusType.ALREADY_READ -> {
                        FinishedBooksSection(bookList = finishedBooksList)
                    }
                    BookStatusType.GIVE_UP -> {
                        GaveUpBooksSection(bookList = gaveUpBookList)
                    }
                }
            }
        }
    }
}

@Preview(showSystemUi = false, showBackground = false, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BookSectionsBlockPreview() {
    val book = FinishedReadingBook(
        bookWithProgress = BookWithProgress(
            bookProgress = BookProgress(
                id = "",
                giveUp = false,
                finishedToRead = null,
                finished = false,
                startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
                percentage = 0.0,
                page = 0,
                progressInPercentage = 0.0
            ),
            book = Book(
                title = "Test Book",
                authors = listOf("Flame McGuy"),
                picture = "",
                thumbnail = "",
                userRating = null,
                id = "",
                allReviews = emptyList(),
                credits = CreditsBO(),
                bookDescription = "",
                genres = listOf(),
                isbn = Pair("", ""),
                lang = "en",
                peopleReadingIt = 6,
                publishedDate = "",
                publisher = "",
                readingStatus = BookReadingStatus.NOT_READ,
                serie = "",
                serieNumber = -1,
                totalComments = 0,
                totalPages = 654,
                totalRating = 0.0
            ),
            bookProgressComments = listOf()
        ),
        userRating = UserBookRating(
            id = "",
            user = Profile("", "Test", "Quan", "Qant", "quant@testt.com", ""),
            review = null,
            rating = 3.3,
            date = Clock.System.now().toLocalDateTime(TimeZone.UTC)
        )
    )

    SobuuTheme {
        BookSectionsBlock(
            isLoading = false,
            errorText = null,
            currentlyReadingBookList = listOf(),
            gaveUpBookList = listOf(),
            currentSection = BookStatusType.ALREADY_READ,
            finishedBooksList = listOf(book, book),
            onCurrentBookClicked = {},
            onDisplayCurrentlyReadingMenuClicked = {},
            onDisplayFinishedMenuClicked = {},
            onDisplayGaveUpMenuClicked = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = false, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun BookSectionsBlockEmptyPreview() {
    SobuuTheme {
        BookSectionsBlock(
            isLoading = false,
            errorText = null,
            currentlyReadingBookList = listOf(),
            gaveUpBookList = listOf(),
            currentSection = BookStatusType.CURRENTLY_READING,
            finishedBooksList = emptyList(),
            onCurrentBookClicked = {},
            onDisplayCurrentlyReadingMenuClicked = {},
            onDisplayFinishedMenuClicked = {},
            onDisplayGaveUpMenuClicked = {}
        )
    }
}