package com.sobuumedia.sobuu.android.profile

sealed class ProfileUIEvent {
    object startScreen: ProfileUIEvent()
    object logout: ProfileUIEvent()
}
