package com.sobuumedia.sobuu.android.settings.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.android.R

val SourceSans = FontFamily(
    Font(R.font.source_sans3_regular),
    Font(R.font.source_sans3_bold, FontWeight.Bold),
    Font(R.font.source_sans3_black, FontWeight.Black),
    Font(R.font.source_sans3_light, FontWeight.Light),
    Font(R.font.source_sans3_extra_light, FontWeight.ExtraLight),
    Font(R.font.source_sans3_semibold, FontWeight.SemiBold),
    Font(R.font.source_sans3_it, FontWeight.Normal),
)

val Solway = FontFamily(
    Font(R.font.solway_regular),
    Font(R.font.solway_bold, FontWeight.Bold),
    Font(R.font.solway_light, FontWeight.Light),
    Font(R.font.solway_extra_bold, FontWeight.ExtraBold),
    Font(R.font.solway_medium, FontWeight.Medium),
)

// Set of Material typography styles to start with

//val displayLarge: TextStyle = TypographyTokens.DisplayLarge,
//val displayMedium: TextStyle = TypographyTokens.DisplayMedium,
//val displaySmall: TextStyle = TypographyTokens.DisplaySmall,
//val headlineLarge: TextStyle = TypographyTokens.HeadlineLarge,
//val headlineMedium: TextStyle = TypographyTokens.HeadlineMedium,
//val headlineSmall: TextStyle = TypographyTokens.HeadlineSmall,
//val titleLarge: TextStyle = TypographyTokens.TitleLarge,
//val titleMedium: TextStyle = TypographyTokens.TitleMedium,
//val titleSmall: TextStyle = TypographyTokens.TitleSmall,
//val bodyLarge: TextStyle = TypographyTokens.BodyLarge,
//val bodyMedium: TextStyle = TypographyTokens.BodyMedium,
//val bodySmall: TextStyle = TypographyTokens.BodySmall,
//val labelLarge: TextStyle = TypographyTokens.LabelLarge,
//val labelMedium: TextStyle = TypographyTokens.LabelMedium,
//val labelSmall: TextStyle = TypographyTokens.LabelSmall,

val Typography = Typography(
    // Used in Logo in the Login screen
    headlineLarge = TextStyle(
        fontFamily = Solway,
        fontWeight = FontWeight.Normal,
        fontSize = 100.sp,
    ),
    headlineMedium = TextStyle(
        fontFamily = Solway,
        fontWeight = FontWeight.Normal,
        fontSize = 50.sp,
    ),
    headlineSmall = TextStyle(
        fontFamily = Solway,
        fontWeight = FontWeight.Medium,
        fontSize = 38.sp,
    ),
    bodyLarge = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),
    bodyMedium = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    bodySmall = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    ),
    titleLarge = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Medium,
        fontSize = 32.sp
    ),
    titleMedium = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Medium,
        fontSize = 28.sp
    ),
    titleSmall = TextStyle(
        fontFamily = SourceSans,
        fontWeight = FontWeight.Medium,
        fontSize = 24.sp
    ),
    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
    */
)
