package com.sobuumedia.sobuu.android.custom_widgets

import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.android.settings.theme.SourceSans

@Composable
fun RoundedFillButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    color: Color = MaterialTheme.colorScheme.error,
    text: String,
    isEnabled: Boolean = true
) {
    FilledTonalButton(
        modifier = modifier,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            containerColor = color,
            contentColor = MaterialTheme.colorScheme.background,
            disabledContainerColor = MaterialTheme.colorScheme.tertiary,
            disabledContentColor = MaterialTheme.colorScheme.background,
        ),
        enabled = isEnabled
    ) {
        Text(
            text = text,
            modifier = Modifier.semantics { this.contentDescription = text },
            style = TextStyle(
                fontSize = 22.sp,
                color = MaterialTheme.colorScheme.background,
                fontWeight = FontWeight.Normal,
                fontFamily = SourceSans
            ),
            textAlign = TextAlign.Center,
        )
    }
}

@Composable
fun CustomTextButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    color: Color = MaterialTheme.colorScheme.background,
    text: String,
    fontSize: TextUnit = 14.sp,
) {
    TextButton(
        modifier = modifier,
        onClick = onClick
    ) {
        Text(
            text,
            style = TextStyle(
                fontSize = fontSize,
                fontFamily = SourceSans,
                color = color
            ),
        )
    }
}