package com.sobuumedia.sobuu.android.authentication.register

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError

data class RegistrationState (
    val isLoading: Boolean = false,
    val registrationUsername: String = "",
    val registrationEmail: String = "",
    val registrationPassword: String = "",
    val registrationPasswordConfirmation: String = "",
    val registrationFirstname: String = "",
    val registrationLastname: String = "",
    val privacyPolicyConfirmationSwitch: Boolean = false,
    val error: AuthenticationError? = null,
    val usernameError: Boolean = false,
    val emailError: Boolean = false,
    val passwordError: Boolean = false,
    val confirmationPassError: Boolean = false,
)
