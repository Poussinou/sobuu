package com.sobuumedia.sobuu.android.book

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Comment
import androidx.compose.material.icons.automirrored.filled.Rule
import androidx.compose.material.icons.filled.Brush
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.material.icons.filled.Domain
import androidx.compose.material.icons.filled.ImportContacts
import androidx.compose.material.icons.filled.Translate
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import coil.compose.AsyncImage
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.google.accompanist.flowlayout.FlowRow
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_invalidBookId
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_invalidBookRate
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_invalidProgress
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_processingQuery
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_timeout
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_titleBook
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_unauthorized
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_unknown
import com.sobuumedia.sobuu.SharedRes.strings.book_details_informationIncomplete
import com.sobuumedia.sobuu.SharedRes.strings.book_details_reportedBookInformationIncompleteDialogText
import com.sobuumedia.sobuu.SharedRes.strings.book_details_startToRead
import com.sobuumedia.sobuu.SharedRes.strings.book_details_startedReadSuccessfully
import com.sobuumedia.sobuu.SharedRes.strings.book_details_startedToReadSuccessfullyButton
import com.sobuumedia.sobuu.SharedRes.strings.general_ok
import com.sobuumedia.sobuu.SharedRes.strings.search_main_comment
import com.sobuumedia.sobuu.SharedRes.strings.search_main_comments
import com.sobuumedia.sobuu.SharedRes.strings.search_main_moreThan999
import com.sobuumedia.sobuu.SharedRes.strings.search_main_nobodyReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_onePersonReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_pluralPersonsReadingBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_review
import com.sobuumedia.sobuu.SharedRes.strings.search_main_reviews
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userGaveUpBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userHasAlreadyReadBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userHasNotReadBook
import com.sobuumedia.sobuu.SharedRes.strings.search_main_userIsReadingBook
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.custom_widgets.Chip
import com.sobuumedia.sobuu.android.custom_widgets.IconAndText
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.custom_widgets.TopAppBarWithMenu
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.Profile
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import com.sobuumedia.sobuu.utils.RemoteDataUtils
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import org.koin.androidx.compose.koinViewModel
import java.time.format.DateTimeFormatter

enum class ISBNType {
    ISBN10, ISBN13
}

@Composable
fun BookScreen(
    navigateBackOrHome: () -> Unit,
    navigateToHomeScreen: () -> Unit,
    navigateToBookCoverScreen: (String) -> Unit,
    bookId: String,
    bookViewModel: BookViewModel = koinViewModel()
) {
    val context = LocalContext.current
    val bookState = bookViewModel.state

    LaunchedEffect(bookViewModel, context) {
        bookViewModel.onEvent(BookUIEvent.StartScreen(bookId))
    }

    Scaffold(
        topBar = {
            if (bookState.book?.readingStatus != BookReadingStatus.READING) {
                TopAppBarWithMenu(
                    navigateBackOrHome = navigateBackOrHome,
                    listItems = listOf(
//                    MenuItemData(
//                        text = stringResource(id = R.string.add_to_shelf),
//                        icon = Icons.Filled.Add,
//                        action = {
//                            bookViewModel.onEvent(BookUIEvent.DisplayUserShelves)
//                        }
//                    ),
                        MenuItemData(
                            text = book_details_startToRead.stringResource(context = context),
                            icon = Icons.Filled.ImportContacts,
                            action = {
                                bookViewModel.onEvent(
                                    BookUIEvent.StartToReadBook(
                                        bookId = bookId,
                                        bookTitle = bookState.book?.title ?: "",
                                        bookISBN = if (bookState.book?.isbn?.first.isNullOrBlank()) {
                                            bookState.book?.isbn?.second
                                        } else {
                                            bookState.book?.isbn?.first
                                        }
                                    )
                                )
                            },
                            displayIt = true,
                        ),
                        MenuItemData(
                            text = book_details_informationIncomplete.stringResource(context = context),
                            icon = Icons.AutoMirrored.Filled.Rule,
                            action = {
                                bookViewModel.onEvent(
                                    BookUIEvent.ReportMissingDataInBook(
                                        bookId = bookId,
                                        bookTitle = bookState.book?.title ?: "",
                                        bookISBN = if (bookState.book?.isbn?.first.isNullOrBlank()) {
                                            bookState.book?.isbn?.second
                                        } else {
                                            bookState.book?.isbn?.first
                                        }
                                    )
                                )
                            },
                            displayIt = true,
                        ),
//                    MenuItemData(
//                        text = stringResource(id = R.string.search_in_map),
//                        icon = Icons.Filled.Map,
//                        action = {
//
//                        }
//                    )
                    )
                )
            }
        },
        content = {

            if (bookViewModel.state.isLoading) {
                Dialog(
                    onDismissRequest = { !bookViewModel.state.isLoading },
                    DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
                ) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .size(100.dp)
                            .background(
                                MaterialTheme.colorScheme.background,
                                shape = RoundedCornerShape(8.dp)
                            )
                            .padding(20.dp)
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }

            if (bookViewModel.state.displayStartedToRead) {
                SuccessStartToReadDialog(
                    isLoading = !bookViewModel.state.isLoading,
                    bookTitle = bookViewModel.state.bookStartedReadingTitle ?: "",
                    onClickButton = { navigateToHomeScreen() }
                )
            }

            if (bookViewModel.state.displayReportedMissingDataMessage) {
                ReportedMissingDataDialog(
                    isLoading = !bookViewModel.state.isLoading,
                    onClickButton = {
                        bookViewModel.onEvent(BookUIEvent.CloseReportedMissingDataDialog)
                    }
                )
            }

            if (bookViewModel.state.error != null) {
                AlertDialog(
                    title = {
                        Text(
                            text = book_detailsError_titleBook.stringResource(context = context),
                            style = TextStyle(
                                fontSize = 18.sp,
                                fontFamily = SourceSans,
                                color = MaterialTheme.colorScheme.secondary
                            )
                        )
                    },
                    text = {
                        Text(
                            text = getStringFromBookError(bookViewModel.state.error),
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontFamily = SourceSans,
                                color = MaterialTheme.colorScheme.secondary
                            )
                        )
                    },
                    containerColor = MaterialTheme.colorScheme.background,
                    textContentColor = MaterialTheme.colorScheme.secondary,
                    confirmButton = {
                        TextButton(
                            onClick = {
                                bookViewModel.onEvent(BookUIEvent.CancelAllErrors)
                            }) {
                            Text(general_ok.stringResource(context = context))
                        }
                    },
                    shape = RoundedCornerShape(8.dp),
                    onDismissRequest = {}
                )
            }

            BookScreenContent(
                navigateToBookCoverScreen = navigateToBookCoverScreen,
                modifier = Modifier.padding(it),
                peopleReadingIt = bookState.book?.peopleReadingIt ?: 0,
                cover = bookState.book?.picture ?: "",
                title = bookState.book?.title ?: "",
                authors = bookState.book?.authors?.joinToString(",") ?: "",
                description = bookState.book?.bookDescription ?: "",
                credits = bookState.book?.credits ?: CreditsBO(),
                publisher = bookState.book?.publisher ?: "",
                publishedDate = bookState.book?.publishedDate ?: "",
                totalPages = bookState.book?.totalPages?.toString() ?: "-1",
                isbn10 = bookState.book?.isbn?.first,
                isbn13 = bookState.book?.isbn?.second,
                genres = bookState.book?.genres ?: emptyList(),
                userHasReadTheBook = bookState.book?.readingStatus ?: BookReadingStatus.NOT_READ,
                totalComments = bookState.book?.totalComments ?: 0,
                rating = bookState.book?.totalRating ?: 0.0,
                reviews = bookState.book?.allReviews ?: emptyList(),
                userReview = bookState.book?.userRating,
            )
        }
    )
}

@Composable
fun BookScreenContent(
    navigateToBookCoverScreen: (String) -> Unit,
    modifier: Modifier = Modifier,
    peopleReadingIt: Int,
    cover: String,
    title: String,
    authors: String,
    description: String,
    credits: CreditsBO,
    publisher: String,
    publishedDate: String,
    totalPages: String,
    isbn10: String?,
    isbn13: String?,
    genres: List<String>,
    userHasReadTheBook: BookReadingStatus,
    totalComments: Int,
    rating: Double,
    reviews: List<UserBookRating>,
    userReview: UserBookRating? = null,
) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .background(MaterialTheme.colorScheme.background)
            .padding(12.dp)
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .then(modifier),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        PeopleReadingItSign(
            peopleReadingIt = peopleReadingIt
        )
        if(cover.isEmpty()) {
            Image(
                painter = painterResource(id = R.drawable.ic_cover_placeholder),
                contentDescription = null,
                modifier = Modifier
                    .width(120.dp)
                    .height(200.dp),
            )
        } else {
            AsyncImage(
                model = cover,
                placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                contentDescription = null,
                modifier = Modifier
                    .width(120.dp)
                    .height(200.dp)
                    .clickable { navigateToBookCoverScreen(cover) },
            )
        }

        Text(
            title,
            modifier = Modifier
                .padding(5.dp),
            style = TextStyle(
                fontFamily = SourceSans,
                color = MaterialTheme.colorScheme.secondary,
                fontSize = 22.sp,
            ),
            maxLines = 5,
            overflow = TextOverflow.Ellipsis,
        )

        Text(
            authors,
            modifier = Modifier
                .padding(5.dp),
            style = TextStyle(
                fontFamily = SourceSans,
                color = MaterialTheme.colorScheme.secondary,
                fontSize = 16.sp,
            ),
            maxLines = 5,
            overflow = TextOverflow.Ellipsis,
        )

        Spacer(modifier = Modifier.padding(top = 15.dp))

        IconAndText(
            text = totalPages,
            fontSize = 16.sp,
            iconPainter = painterResource(id = R.drawable.ic_book_open_page_variant),
            modifier = Modifier.padding(top = 5.dp, bottom = 5.dp, start = 5.dp, end = 5.dp),
        )

        FlowRow {
            if (credits.translators.isNotEmpty()) {
                IconAndText(
                    text = credits.translators.joinToString(","),
                    fontSize = 14.sp,
                    icon = Icons.Filled.Translate,
                    modifier = Modifier
                        .padding(top = 5.dp, bottom = 5.dp, start = 5.dp, end = 5.dp),
                )
            }

            if (credits.illustrators.isNotEmpty()) {
                IconAndText(
                    text = credits.illustrators.joinToString(","),
                    fontSize = 14.sp,
                    icon = Icons.Filled.Brush,
                    modifier = Modifier
                        .padding(top = 5.dp, bottom = 5.dp, start = 5.dp, end = 5.dp),
                )
            }
        }

        FlowRow(modifier = Modifier.padding(top = 5.dp)) {
            IconAndText(
                text = publisher,
                fontSize = 16.sp,
                icon = Icons.Filled.Domain,
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
                    .weight(1f),
            )
            IconAndText(
                text = publishedDate,
                fontSize = 16.sp,
                icon = Icons.Filled.CalendarToday,
                modifier = Modifier
                    .padding(start = 5.dp, end = 5.dp)
                    .weight(0.5f),
            )
        }

        FlowRow(modifier = Modifier.padding(bottom = 10.dp)) {
            if (!isbn10.isNullOrBlank()) {
                IconAndText(
                    text = isbn10,
                    fontSize = 16.sp,
                    customIcon = { CreateISBNIcon(type = ISBNType.ISBN10) },
                    modifier = Modifier
                        .padding(top = 5.dp, start = 5.dp, end = 5.dp),
                )
            }
            if (!isbn13.isNullOrBlank()) {
                IconAndText(
                    text = isbn13,
                    fontSize = 16.sp,
                    customIcon = { CreateISBNIcon(type = ISBNType.ISBN13) },
                    modifier = Modifier
                        .padding(top = 5.dp, start = 5.dp, end = 5.dp),
                )
            }
        }

        Text(
            description,
            modifier = Modifier
                .padding(5.dp),
            style = TextStyle(
                fontFamily = SourceSans,
                color = MaterialTheme.colorScheme.secondary,
                fontSize = 18.sp,
            ),
            textAlign = TextAlign.Justify,
        )

        CreateGenresChips(genres = RemoteDataUtils().translateGenres(context, genres))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp, bottom = 5.dp),
            horizontalArrangement = Arrangement.SpaceBetween,

            ) {
            Text(
                text = when (userHasReadTheBook) {
                    BookReadingStatus.NOT_READ -> search_main_userHasNotReadBook.stringResource(context = context)
                    BookReadingStatus.READING -> search_main_userIsReadingBook.stringResource(context = context)
                    BookReadingStatus.FINISHED -> search_main_userHasAlreadyReadBook.stringResource(context = context)
                    BookReadingStatus.GIVE_UP -> search_main_userGaveUpBook.stringResource(context = context)
                },
                style = TextStyle(
                    fontFamily = SourceSans,
                    color = MaterialTheme.colorScheme.secondary,
                    fontSize = 16.sp,
                ),
            )

            IconAndText(
                text = when {
                    totalComments == 1 -> "$totalComments ${search_main_comment.stringResource(context = context)}"
                    totalComments > 999 -> "${search_main_moreThan999.stringResource(context = context)} ${search_main_comments.stringResource(context = context)}"
                    else -> "$totalComments ${search_main_comments.stringResource(context = context)}"
                },
                fontSize = 16.sp,
                icon = Icons.AutoMirrored.Filled.Comment,
                iconColor = MaterialTheme.colorScheme.secondary,
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp, bottom = 15.dp),
            horizontalArrangement = Arrangement.Center,
        ) {
            CalculateRateIcons(rate = rating, color = MaterialTheme.colorScheme.secondary)

            Text(
                text = when {
                    reviews.size == 1 -> "${reviews.size} ${search_main_review.stringResource(context = context)}"
                    reviews.size > 999 -> "${search_main_moreThan999.stringResource(context = context)} ${search_main_reviews.stringResource(context = context)}"
                    else -> "${reviews.size} ${search_main_reviews.stringResource(context = context)}"
                },
                modifier = Modifier.padding(start = 10.dp),
                style = TextStyle(
                    fontFamily = SourceSans,
                    color = MaterialTheme.colorScheme.secondary,
                    fontSize = 18.sp,
                ),
            )
        }

        if (userReview != null) {
            ReviewItem(review = userReview)
        } else {
            Spacer(Modifier.height(30.dp))
        }

        if (reviews.isNotEmpty()) {
            Spacer(Modifier.height(20.dp))

            reviews.forEach {
                ReviewItem(review = it)
            }
        } else {
            Spacer(Modifier.height(20.dp))
        }

    }
}

@Composable
fun CreateISBNIcon(
    type: ISBNType,
    modifier: Modifier = Modifier,
) {
    Box(modifier = modifier, contentAlignment = Alignment.Center) {
        Icon(
            painter = painterResource(id = R.drawable.ic_code_bar),
            tint = MaterialTheme.colorScheme.secondary,
            modifier = Modifier.size(16.dp),
            contentDescription = null,
        )
        Text(
            text = if (type == ISBNType.ISBN10) "10" else "13",
            style = TextStyle(
                fontFamily = SourceSans,
                color = MaterialTheme.colorScheme.primary.copy(alpha = 0.8f),
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold
            ),
        )
    }
}

@Composable
fun PeopleReadingItSign(
    modifier: Modifier = Modifier,
    peopleReadingIt: Int
) {
    val context = LocalContext.current
    Row(
        modifier = Modifier
            .background(MaterialTheme.colorScheme.tertiary)
            .fillMaxWidth()
            .height(40.dp)
            .offset(x = (-10).dp)
            .then(modifier),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_currently_reading),
            contentDescription = null,
            tint = Color.Unspecified,
            modifier = Modifier.padding(start = 12.dp),
        )
        Text(
            text = when {
                peopleReadingIt == 0 -> search_main_nobodyReadingBook.stringResource(context = context)
                peopleReadingIt == 1 -> "$peopleReadingIt ${search_main_onePersonReadingBook.stringResource(context = context)}"
                peopleReadingIt > 999 -> "${search_main_moreThan999.stringResource(context = context)} ${search_main_pluralPersonsReadingBook.stringResource(context = context)}"
                else -> "$peopleReadingIt ${search_main_pluralPersonsReadingBook.stringResource(context = context)}"
            },
            modifier = Modifier.padding(start = 12.dp, end = 12.dp),
            style = TextStyle(
                color = MaterialTheme.colorScheme.background,
                fontSize = 16.sp,
                fontFamily = SourceSans
            ),

            maxLines = 2,
        )
    }
}

@Composable
fun CalculateRateIcons(
    modifier: Modifier = Modifier,
    rate: Double?,
    color: Color,
    starSize: Dp = 24.dp,
) {
    RatingBar(
        modifier = modifier,
        value = rate?.toFloat() ?: 0.0f,
        config = RatingBarConfig()
            .activeColor(color)
            .hideInactiveStars(false)
            .inactiveColor(MaterialTheme.colorScheme.background)
            .stepSize(StepSize.HALF)
            .numStars(5)
            .isIndicator(true)
            .size(starSize)
            .padding(2.dp)
            .style(RatingBarStyle.HighLighted),
        onValueChange = {},
        onRatingChanged = {}
    )
}

@Composable
fun ReviewItem(
    review: UserBookRating,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.secondary)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Text(
                text = "${review.user?.firstName} ${review.user?.lastName}",
                modifier = Modifier.padding(start = 5.dp),
                style = TextStyle(
                    fontFamily = SourceSans,
                    color = MaterialTheme.colorScheme.error,
                    fontSize = 12.sp,
                ),
            )

            CalculateRateIcons(
                modifier = Modifier.padding(5.dp),
                starSize = 16.dp,
                rate = review.rating,
                color = MaterialTheme.colorScheme.error
            )

            if(review.date != null) {
                Text(
                    text = review.date!!.toJavaLocalDateTime()
                        .format(DateTimeFormatter.ofPattern("dd MMMM, yyyy, HH:mm")),
                    modifier = Modifier.padding(end = 5.dp),
                    style = TextStyle(
                        fontFamily = SourceSans,
                        color = MaterialTheme.colorScheme.background,
                        fontSize = 12.sp,
                    ),
                )
            }
        }

        if(review.review?.isNotBlank() == true) {
            Text(
                text = review.review ?: "",
                modifier = Modifier.padding(10.dp),
                style = TextStyle(
                    fontFamily = SourceSans,
                    color = MaterialTheme.colorScheme.background,
                    fontSize = 16.sp,
                ),
            )
        }
    }
}

@Composable
fun CreateGenresChips(
    genres: List<String>,
    modifier: Modifier = Modifier,
) {
    FlowRow(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier)
    ) {
        for (element in genres) {
            Chip(
                text = element,
                backgroundColor = MaterialTheme.colorScheme.primary,
                modifier = Modifier.padding(4.dp)
            )
        }
    }

}

@Composable
fun SuccessStartToReadDialog(isLoading: Boolean, bookTitle: String, onClickButton: () -> Unit) {
    val context = LocalContext.current
    var showDialog by remember { mutableStateOf(isLoading) }

    if (showDialog) {
        Dialog(
            onDismissRequest = { showDialog = false },
            DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(IntrinsicSize.Min)
                    .background(
                        MaterialTheme.colorScheme.background,
                        shape = RoundedCornerShape(8.dp)
                    )
                    .padding(20.dp),
            ) {
                BookAnimation()

                Spacer(modifier = Modifier.padding(10.dp))

                Text(
                    modifier = Modifier.padding(10.dp),
                    text = "${book_details_startedReadSuccessfully.stringResource(context = context)} '$bookTitle'",
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 18.sp,
                        color = MaterialTheme.colorScheme.secondary
                    ),
                )

                FilledTonalButton(
                    modifier = Modifier
                        .clip(RoundedCornerShape(10.dp)),
                    onClick = {
                        showDialog = false
                        onClickButton.invoke()
                    },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.error,
                        contentColor = MaterialTheme.colorScheme.background,
                        disabledContainerColor = MaterialTheme.colorScheme.tertiary,
                        disabledContentColor = MaterialTheme.colorScheme.background,
                    )
                ) {
                    Text(
                        book_details_startedToReadSuccessfullyButton.stringResource(context = context),
                        style = TextStyle(
                            fontSize = 16.sp,
                            color = MaterialTheme.colorScheme.background,
                            fontWeight = FontWeight.Normal,
                            fontFamily = SourceSans
                        ),
                        textAlign = TextAlign.Center,
                    )
                }
            }
        }
    }
}

@Composable
fun ReportedMissingDataDialog(isLoading: Boolean, onClickButton: () -> Unit) {
    val context = LocalContext.current
    var showDialog by remember { mutableStateOf(isLoading) }

    if (showDialog) {
        Dialog(
            onDismissRequest = { showDialog = false },
            DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(IntrinsicSize.Min)
                    .background(
                        MaterialTheme.colorScheme.background,
                        shape = RoundedCornerShape(8.dp)
                    )
                    .padding(20.dp),
            ) {
                Text(
                    modifier = Modifier.padding(10.dp),
                    text = book_details_reportedBookInformationIncompleteDialogText.stringResource(context = context),
                    textAlign = TextAlign.Justify,
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 18.sp,
                        color = MaterialTheme.colorScheme.secondary
                    ),
                )

                Spacer(modifier = Modifier.padding(10.dp))

                FilledTonalButton(
                    modifier = Modifier
                        .clip(RoundedCornerShape(10.dp)),
                    onClick = {
                        showDialog = false
                        onClickButton.invoke()
                    },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.error,
                        contentColor = MaterialTheme.colorScheme.background,
                        disabledContainerColor = MaterialTheme.colorScheme.tertiary,
                        disabledContentColor = MaterialTheme.colorScheme.background,
                    )
                ) {
                    Text(
                        general_ok.stringResource(context = context),
                        style = TextStyle(
                            fontSize = 16.sp,
                            color = MaterialTheme.colorScheme.background,
                            fontWeight = FontWeight.Normal,
                            fontFamily = SourceSans
                        ),
                        textAlign = TextAlign.Center,
                    )
                }
            }
        }
    }
}

@Composable
private fun getStringFromBookError(error: BookError?): String {
    val context = LocalContext.current
    if (error == null) return ""
    return when (error) {
        BookError.InvalidBookIdError -> book_detailsError_invalidBookId.stringResource(context = context)
        BookError.InvalidDoubleValueError,
        BookError.InvalidFinishedAndGiveUpBookValuesError,
        BookError.InvalidPageNumberError,
        BookError.InvalidPercentageNumberError -> book_detailsError_invalidProgress.stringResource(context = context)
        BookError.InvalidRateIdError,
        BookError.InvalidRateNumberError -> book_detailsError_invalidBookRate.stringResource(context = context)
        BookError.InvalidSessionTokenError -> book_detailsError_invalidSessionToken.stringResource(context = context)
        BookError.ProcessingQueryError -> book_detailsError_processingQuery.stringResource(context = context)
        BookError.TimeOutError -> book_detailsError_timeout.stringResource(context = context)
        BookError.UnauthorizedQueryError -> book_detailsError_unauthorized.stringResource(context = context)
        else -> book_detailsError_unknown.stringResource(context = context)
    }
}

@Composable
fun BookAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.book_animation))
    val progress by animateLottieCompositionAsState(composition)

    LottieAnimation(
        modifier = Modifier
            .width(200.dp)
            .height(200.dp),
        composition = composition,
        progress = { progress },
    )
}

@Preview(showSystemUi = true, showBackground = true, group = "Done")
@Composable
fun BookScreenContentPreview() {
    BookScreenContent(
        navigateToBookCoverScreen = {},
        peopleReadingIt = 56,
        cover = "",
        title = "El imperio final",
        authors = "Brandon Sanderson",
        description = "Durante mil años han caído cenizas del cielo. Durante mil años nada ha " +
                "florecido. Durante mil años los skaa han sido esclavizados y viven en la miseria, " +
                "sumidos en un miedo inevitable. Durante mil años el Lord Legislador ha reinado con " +
                "poder absoluto, dominando gracias al terror, a sus poderes y a su inmortalidad, " +
                "ayudado por «obligadores» e «inquisidores», junto a la poderosa magia de la " +
                "alomancia.\n" +
                "\n" +
                "Pero los nobles a menudo han tenido trato sexual con jóvenes skaa y, aunque la " +
                "ley lo prohíbe, algunos de sus bastardos han sobrevivido y heredado los poderes " +
                "alománticos: son los «Nacidos de la Bruma» (Mistborn).\n" +
                "\n" +
                "Ahora, Kelsier, el «superviviente», el único que ha logrado huir de los " +
                "Pozos de Hathsin, ha encontrado a Vin, una pobre chica skaa con mucha suerte... " +
                "Tal vez los dos, con el mejor equipo criminal jamás reunido, unidos a la rebelión " +
                "que los skaa intentan desde hace mil años, logren cambiar el mundo y acabar con " +
                "la atroz mano de hierro del Lord Legislador.",
        credits = CreditsBO(
            translators = arrayOf("Armando Fuerte Bulla")
        ),
        publisher = "Nova",
        publishedDate = "2006",
        totalPages = "623",
        isbn10 = "5746854987",
        isbn13 = "1698465465475",
        genres = listOf(
            "Fantasy",
            "Science fiction",
            "Contemporary fiction",
            "Horror",
            "Fiction",
            "Middle Age"
        ),
        userHasReadTheBook = BookReadingStatus.FINISHED,
        totalComments = 211,
        rating = 2.5,
        reviews = emptyList(),
        userReview = UserBookRating(
            id = "",
            rating = 4.5,
            review = "I love it.",
//            book = null,
            user = Profile(
                id = "",
                firstName = "Paco",
                lastName = "Jones",
                username = "CoJones",
                email = "",
                createdAt = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()).toString()
            ),
            date = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
        )
    )
}

@Preview(group = "Undone")
@Composable
fun PeopleReadingItSignPreview() {
    PeopleReadingItSign(
        peopleReadingIt = 56,
    )
}

@Preview(group = "Done")
@Composable
fun ISBNIconPreview() {
    CreateISBNIcon(type = ISBNType.ISBN13)
}

@Preview(group = "Done")
@Composable
fun GenreChipsPreview() {
    CreateGenresChips(
        genres = listOf(
            "Fantasy",
            "Science fiction",
            "Contemporary fiction",
            "Horror",
            "Fiction",
            "Middle Age"
        )
    )
}

@Preview(group = "Done")
@Composable
fun ReviewItemPreview() {
    ReviewItem(
        review = UserBookRating(
            id = "",
            rating = 4.5,
            review = "I love it.",
//            book = null,
            user = Profile(
                id = "",
                firstName = "Paco",
                lastName = "Jones",
                username = "CoJones",
                email = "",
                createdAt = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()).toString()
            ),
            date = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
        )
    )
}

@Preview
@Composable
fun StartToReadSuccessDialogPreview() {
    SuccessStartToReadDialog(
        isLoading = true,
        bookTitle = "El imperio final",
        onClickButton = {}
    )
}

@Preview
@Composable
fun ReportedMissingDataDialogPreview() {
    ReportedMissingDataDialog(
        isLoading = true,
        onClickButton = {}
    )
}