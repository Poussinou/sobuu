package com.sobuumedia.sobuu.android.authentication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.compose.rememberNavController
import com.sobuumedia.sobuu.android.navigation.Navigation
import com.sobuumedia.sobuu.android.settings.settings.SettingsViewModel
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs
import org.koin.android.ext.android.inject

class LoginActivity: ComponentActivity() {
    private val settingsVieWModel: SettingsViewModel by inject()
    private val logs: SobuuLogs by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        val themeOption = settingsVieWModel.state.currentTheme

//        enableEdgeToEdge()
        installSplashScreen()
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()

            // TODO Not ready for production
            SobuuAuthTheme(false) {
                Navigation(navController, logs)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val navController = rememberNavController()
    SobuuAuthTheme {
        Navigation(navController, NapierLogs())
    }
}