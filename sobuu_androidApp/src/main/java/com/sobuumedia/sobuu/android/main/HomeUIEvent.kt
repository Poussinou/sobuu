package com.sobuumedia.sobuu.android.main

sealed class HomeUIEvent {
    data class DisplaySearch(val focused: Boolean, val emptyTerm: Boolean): HomeUIEvent()
    object DisplayGiveUp: HomeUIEvent()
    object DisplayFinished: HomeUIEvent()
    object DisplayCurrentlyReading: HomeUIEvent()
}