package com.sobuumedia.sobuu.android.settings.settings

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_addBooksToDB
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_addBooksToDBTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_appCode
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_contactTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_emailContact
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_goals
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_goalsTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_help
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_helpTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_idea
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_ideaTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_inGermany
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_love
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_madeWithLove
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_share
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialButtonTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaButton
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaButtonsTitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_socialMediaShareText
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_title
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whoAmI
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whoAmITitle
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whyIDo
import com.sobuumedia.sobuu.SharedRes.strings.settings_about_whyIDoTitle
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.custom_widgets.CustomTopAppBar
import com.sobuumedia.sobuu.android.custom_widgets.IconAndText
import com.sobuumedia.sobuu.android.custom_widgets.SobuuText
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.HeartShape
import com.sobuumedia.sobuu.android.utils.stringResource
import org.koin.androidx.compose.koinViewModel

@Composable
fun AboutTheAppScreen(
    navigateBack: () -> Unit,
    viewModel: SettingsViewModel = koinViewModel()
) {
    val context = LocalContext.current

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                CustomTopAppBar(
                    navigateBack = { navigateBack() },
                    title = settings_about_title.stringResource(context = context),
                )
            },
            content = { padding ->
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(padding)
                        .verticalScroll(rememberScrollState())
                        .padding(horizontal = 10.dp),
                ) {
                    // About the app (idea and goals)
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_ideaTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_idea.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_goalsTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_goals.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )

                    // About me (who I am and why I do the app)
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_whoAmITitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_whoAmI.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_whyIDoTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_whyIDo.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )

                    // How to contact me
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_contactTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_emailContact.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )
                    // TODO Uncomment this when the Mastodon account is created
//                    SobuuText(
//                        modifier = Modifier.padding(5.dp),
//                        text = settings_about_socialMediaContact.stringResource(context = context),
//                        textSize = 16.sp,
//                        maxLines = 10,
//                    )
//                    Row {
//                        MastodonButton()
                        EmailButton("info@getsobuu.com")
//                    }

                    // Where to find the code of the app
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_contactTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_appCode.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )
                    GitlabButton()

                    // How to help (donations, translations, adding books to the database, sharing in social media)
                    Spacer(modifier = Modifier.padding(vertical = 10.dp))
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_helpTitle.stringResource(context = context),
                        textSize = 18.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(5.dp),
                        text = settings_about_help.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )

                    SobuuText(
                        modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp, end = 5.dp),
                        text = settings_about_socialMediaButtonsTitle.stringResource(context = context),
                        textSize = 16.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp, end = 5.dp),
                        text = settings_about_socialMediaButton.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )

                    ShareButton()

                    SobuuText(
                        modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp, end = 5.dp),
                        text = settings_about_addBooksToDBTitle.stringResource(context = context),
                        textSize = 16.sp,
                        textWeight = FontWeight.Bold
                    )
                    SobuuText(
                        modifier = Modifier.padding(start = 10.dp, top = 5.dp, bottom = 5.dp, end = 5.dp),
                        text = settings_about_addBooksToDB.stringResource(context = context),
                        textSize = 16.sp,
                        maxLines = 10,
                    )
                    EmailButton("bookrequest@getsobuu.com")

                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                    MadeWithLove()
                    Spacer(modifier = Modifier.padding(vertical = 20.dp))
                }
            }
        )
    }
}

@Composable
fun MastodonButton() {
    val context: Context = LocalContext.current
    Button(
        modifier = Modifier.padding(start = 10.dp, top = 10.dp, bottom = 10.dp, end = 5.dp),
        onClick = {
        val intent = Intent(Intent.ACTION_VIEW)
        val data = Uri.parse("https://mastodon.social/@sobuumedia")
        intent.data = data
        startActivity(context, intent, null)
    }) {
        IconAndText(
            text = "@sobuumedia",
            fontSize = 14.sp,
            customIcon = {
                Image(
                    modifier = Modifier.size(24.dp, 24.dp),
                    painter = painterResource(id = R.drawable.mastodon),
                    contentDescription = "Mastodon"
                )
            }
        )
    }
}

@Composable
fun EmailButton(text: String) {
    val context: Context = LocalContext.current
    Button(
        modifier = Modifier.padding(start = 10.dp, top = 10.dp, bottom = 10.dp, end = 5.dp),
        onClick = {
        val intent = Intent(Intent.ACTION_VIEW)
        val data = Uri.parse("mailto:$text")
        intent.data = data
        startActivity(context, intent, null)
    }) {
        IconAndText(
            text = text,
            fontSize = 14.sp,
            customIcon = {
                Image(
                    modifier = Modifier.size(24.dp, 24.dp),
                    imageVector = Icons.Filled.Mail,
                    contentDescription = authorization_auth_email.stringResource(context = context),
                    colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.background)
                )
            }
        )
    }
}

@Composable
fun GitlabButton() {
    val context: Context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
        val intent = Intent(Intent.ACTION_VIEW)
        val data = Uri.parse("https://gitlab.com/sobuumedia/sobuu")
        intent.data = data
        startActivity(context, intent, null)
    }) {
        IconAndText(
            text = "Sobuu",
            fontSize = 14.sp,
            customIcon = {
                Image(
                    modifier = Modifier.size(24.dp, 24.dp),
                    painter = painterResource(id = R.drawable.gitlab),
                    contentDescription = "Gitlab",
                )
            }
        )
    }
}

@Composable
fun ShareButton() {
    val context: Context = LocalContext.current
    Button(
        modifier = Modifier.padding(start = 10.dp, top = 10.dp, bottom = 10.dp, end = 5.dp),
        onClick = {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, settings_about_socialMediaShareText.stringResource())
            startActivity(context, Intent.createChooser(intent, settings_about_socialButtonTitle.stringResource()), null)
    }) {
        IconAndText(
            text = settings_about_share.stringResource(context = context),
            fontSize = 14.sp,
            icon = Icons.Filled.Share
        )
    }
}

@Composable
fun MadeWithLove() {
    val context = LocalContext.current
    val brush = Brush.verticalGradient(listOf(Color.Black, Color.Red, Color.Yellow))
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {

        SobuuText(
            modifier = Modifier.padding(end = 2.dp),
            text = settings_about_madeWithLove.stringResource(context = context),
            textSize = 18.sp
        )
        Icon(
            modifier = Modifier
                .clip(HeartShape())
                .graphicsLayer(alpha = 0.99f)
                .drawWithCache {
                    onDrawWithContent {
                        drawContent()
                        drawRect(brush, blendMode = BlendMode.Difference)
                    }
                },
            imageVector = Icons.Filled.Favorite,
            contentDescription = settings_about_love.stringResource(context = context),
            tint = Color.Transparent
        )
        SobuuText(
            modifier = Modifier.padding(start = 2.dp),
            text = settings_about_inGermany.stringResource(context = context),
            textSize = 18.sp
        )
    }
}
