package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_searchBook
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun SearchBarTextField(
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearText: () -> Unit,
    contentDescription: String = ""
) {
    val focusManager = LocalFocusManager.current
    val focused = remember { mutableStateOf(false) }
    val keyboardController = LocalSoftwareKeyboardController.current


    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val placeholderColor = MaterialTheme.colorScheme.tertiary
    val errorColor = MaterialTheme.colorScheme.error
    val roundedCornerSearch = 5.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = searchFieldValue,
            onValueChange = onSearchFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(roundedCornerSearch),
                    color = backgroundColor,
                )
                .semantics { this.contentDescription = contentDescription }
                .onFocusChanged { focusState ->
                    focused.value = focusState.isFocused
                    onSearchFieldFocusChange(focusState.isFocused)
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
            keyboardActions = KeyboardActions(
                onSearch = {
                    onSearchButtonClick()
                    keyboardController?.hide()
                }
            ),
            placeholder = { PlaceholderTextLarge(
                placeholderText = home_main_searchBook.stringResource(context = LocalContext.current)
            ) },
            shape = RoundedCornerShape(roundedCornerSearch),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = Icons.Filled.Search.name,
                    tint = placeholderColor,
                )
            },
            trailingIcon = if(focused.value) {
                {
                    IconButton(
                        onClick = {
                            if (searchFieldValue.isBlank()) {
                                focusManager.clearFocus()
                            } else {
                                clearText()
                            }
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Close,
                            tint = elementColor,
                            contentDescription = Icons.Filled.Close.name,
                        )
                    }
                }
            } else {
                {}
            }
        )
    }
}


@Preview(showSystemUi = false, showBackground = true)
@Composable
fun SearchBarTextFieldPreview() {
    SearchBarTextField(
        searchFieldValue = "",
        onSearchButtonClick = {},
        onSearchFieldFocusChange = {},
        clearText = {},
        onSearchFieldValueChange = {}
    )
}
