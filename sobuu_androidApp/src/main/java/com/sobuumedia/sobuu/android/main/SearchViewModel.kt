package com.sobuumedia.sobuu.android.main

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.analytics.AnalyticsEvent
import com.sobuumedia.sobuu.analytics.events.ErrorEvent
import com.sobuumedia.sobuu.analytics.events.SearchBookEvent
import com.sobuumedia.sobuu.android.SobuuApp
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.models.bo_models.Book
import kotlinx.coroutines.launch
import org.acra.ACRA

class SearchViewModel(private val book: IBookRepository): ViewModel() {
    private var analytics = SobuuApp.analytics

    var state by mutableStateOf(SearchState())
    var booksList by mutableStateOf<List<Book>?>(emptyList())

    fun onEvent(event: SearchUIEvent) {
        when(event) {
            is SearchUIEvent.SearchTermChanged -> {
                state = state.copy(
                    searchTerm = event.value,
                    language = event.lang,
                    searchFurther = state.searchFurther,
                )
            }
            is SearchUIEvent.removeErrorState -> TODO()
            is SearchUIEvent.searchTerm -> search()
            is SearchUIEvent.cleanSearchTerm -> {
                state = state.copy(
                    searchTerm = "",
                    language = "",
                    emptyResult = false,
                    displaySearchFurther = false,
                )
                booksList = emptyList()
            }
            is SearchUIEvent.clickOnMoreInfo -> {
                state = state.copy(displayMoreInfo = !state.displayMoreInfo)
            }
        }
    }

    fun onAnalyticsEvent(event: AnalyticsEvent) {
        when (event) {
            is SearchBookEvent -> sendAnalyticData(event)
            is ErrorEvent -> sendAnalyticData(event)
        }
    }

    private fun handleError(error: BookError?) {
        if(error is BookError.UnknownError) {
            ACRA.errorReporter.putCustomData("Handle error in SearchViewModel", error.message ?: "Unknown error with empty message")

            val map = mapOf<String, String>()
            map.plus("Error" to error.message)

            onAnalyticsEvent(ErrorEvent(name = "Error searching books", otherVariables = map))
        }

        if(error != null) {
            onAnalyticsEvent(
                ErrorEvent(
                    name = "${error.javaClass.name} while searching books",
                    otherVariables = emptyMap()
                )
            )
        }

        state = state.copy(error = error)
    }

    private fun search() {
        viewModelScope.launch {
            state = state.copy(isLoading = true, displayMoreInfo = false)
            val result = book.searchBook(
                term = state.searchTerm,
                language = state.language,
            )

            if(result.data == null) {
                handleError(result.error)
            } else {
                if(result.data.isNullOrEmpty()) {
                    state = state.copy(
                        emptyResult = true,
                        displayWarningWithEmail = true
                    )
                } else {
                    booksList = result.data
                    state = state.copy(
                        emptyResult = false,
                        displayWarningWithEmail = true
                    )
                }
                state = state.copy(displaySearchFurther = true)
                handleError(null)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun sendAnalyticData(event: AnalyticsEvent) {
        viewModelScope.launch {
            analytics.track(event)
        }
    }
}