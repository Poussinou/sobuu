package com.sobuumedia.sobuu.android.main

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.BookStatusType
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import kotlinx.coroutines.launch
import org.acra.ACRA

private const val TAG = "HomeViewModel"

class HomeViewModel(private val book: IBookRepository, private val logs: SobuuLogs) : ViewModel() {

    var currentSection: BookStatusType by mutableStateOf(BookStatusType.CURRENTLY_READING)
    var state by mutableStateOf(HomeState())
    var currentlyReadingBooksList by mutableStateOf<List<BookWithProgress>?>(emptyList())
    var finishedBooksList by mutableStateOf<List<FinishedReadingBook>?>(emptyList())
    var giveUpBooksList by mutableStateOf<List<BookWithProgress>?>(emptyList())

    init {
        onEvent(HomeUIEvent.DisplayCurrentlyReading)
    }

    fun onEvent(event: HomeUIEvent) {
        when (event) {
            HomeUIEvent.DisplayCurrentlyReading -> getUserCurrentReadingBooks()
            HomeUIEvent.DisplayFinished -> getUserFinishedBooks()
            HomeUIEvent.DisplayGiveUp -> getUserGiveUpBooks()
            is HomeUIEvent.DisplaySearch -> {
                state = state.copy(
                    isOnSearch = !event.emptyTerm || event.focused,
                )
            }
        }
    }

    private fun handleError(error: BookError?) {

        logs.error(TAG, "Error: ${error?.errorMessage}", Exception())
        if(error is BookError.UnknownError) {
            ACRA.errorReporter.putCustomData("Handle error in HomeViewModel", error.message ?: "Unknown error with empty message")
        }
        state = state.copy(error = error)
    }

    private fun getUserCurrentReadingBooks() {
        currentSection = BookStatusType.CURRENTLY_READING

        logs.info(TAG, "Getting User Current Reading books")

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = book.getUserCurrentReadingBook()

            if (result.data.isNullOrEmpty()) {
                handleError(result.error)
            } else {

                logs.info(TAG, "User Current Reading books data: ${result.data}")
                currentlyReadingBooksList = result.data
                handleError(null)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun getUserFinishedBooks() {
        currentSection = BookStatusType.ALREADY_READ

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = book.getUserFinishedReadingBook()

            if (result.data.isNullOrEmpty()) {
                handleError(result.error)
            } else {
                finishedBooksList = result.data
                handleError(null)
            }

            state = state.copy(isLoading = false)
        }
    }

    private fun getUserGiveUpBooks() {
        currentSection = BookStatusType.GIVE_UP

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = book.getUserGiveUpBook()

            if (result.data.isNullOrEmpty()) {
                handleError(result.error)
            } else {
                giveUpBooksList = result.data
                handleError(null)
            }

            state = state.copy(isLoading = false)
        }
    }
}