package com.sobuumedia.sobuu.android.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.sobuumedia.sobuu.android.add_book.AddBookScreen
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.authentication.LongTextScreen
import com.sobuumedia.sobuu.android.authentication.SentEmailScreen
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.authentication.login.LoginScreen
import com.sobuumedia.sobuu.android.authentication.register.RegistrationScreen
import com.sobuumedia.sobuu.android.authentication.reset_pass.ResetPasswordScreen
import com.sobuumedia.sobuu.android.authentication.splash.SplashScreen
import com.sobuumedia.sobuu.android.book.BookCoverScreen
import com.sobuumedia.sobuu.android.book.BookScreen
import com.sobuumedia.sobuu.android.comments.AddNewCommentScreen
import com.sobuumedia.sobuu.android.comments.CommentsScreen
import com.sobuumedia.sobuu.android.currently_reading.CurrentlyReadingScreen
import com.sobuumedia.sobuu.android.main.HomeScreen
import com.sobuumedia.sobuu.android.profile.ProfileScreen
import com.sobuumedia.sobuu.android.settings.settings.AboutTheAppScreen
import com.sobuumedia.sobuu.android.settings.settings.SettingsScreen
import com.sobuumedia.sobuu.android.utils.toEmailType
import com.sobuumedia.sobuu.android.utils.toTextType
import com.sobuumedia.sobuu.core.SobuuLogs
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

const val TAG = "Navigation"

@Composable
fun Navigation(navController: NavHostController, logs: SobuuLogs) {

    fun sobuuLogI(log: String) = logs.info(tag = TAG, log = log)

    NavHost(navController = navController, startDestination = Screen.SplashScreen.route) {
        // Splash screen
        composable(route = Screen.SplashScreen.route) {
            SplashScreen(nav = navController)
        }

        // Login screen
        composable(route = Screen.LoginScreen.route) {
            sobuuLogI("Navigate to Login Screen")
            LoginScreen(
                navigateToHomeScreen = {
                    navController.navigate(Screen.HomeScreen.route) {
                        popUpTo(Screen.LoginScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToForgotPasswordScreen = {
                    navController.navigate(Screen.ForgotPasswordScreen.route)
                },
                navigateToRegistrationScreen = {
                    navController.navigate(Screen.RegistrationScreen.route)
                },
                navigateToLegalTextScreen = {
                    navController.navigate(Screen.LongTextScreen.withArgs(it.name))
                }
            )
        }

        // Legal text screen
        composable(
            route = Screen.LongTextScreen.route + "/{text_type}",
            arguments = listOf(
                navArgument("text_type") {
                    type = NavType.StringType
                    defaultValue = TextType.LICENSES.name
                    nullable = false
                }
            )
        ) {
            LongTextScreen(
                navigateBack = { navController.navigateUp() },
                textType = it.arguments?.getString("text_type").toTextType()
            )
        }

        // Registration screen
        composable(route = Screen.RegistrationScreen.route) {
            RegistrationScreen(
                navigateToSentEmailScreen = {
                    navController.navigate(Screen.SentEmailScreen.withArgs(it.name))
                },
                navigateBack = {
                    navController.navigateUp()
                },
                navigateToLegalText = {
                    navController.navigate(Screen.LongTextScreen.withArgs(it.name))
                }
            )
        }

        // Sent email screen
        composable(
            route = Screen.SentEmailScreen.route + "/{email_type}",
            arguments = listOf(
                navArgument("email_type") {
                    type = NavType.StringType
                    defaultValue = EmailType.VERIFICATION.name
                    nullable = false
                }
            )
        ) {
            SentEmailScreen(
                navToLogin = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.LoginScreen.route) {
                            inclusive = true
                        }
                    }
                },
                emailType = it.arguments?.getString("email_type").toEmailType()
            )
        }

        // Reset password screen
        composable(
            route = Screen.ForgotPasswordScreen.route,
        ) {
            ResetPasswordScreen(
                navigateToSentEmailScreen = {
                    navController.navigate(Screen.SentEmailScreen.withArgs(it.name))
                },
                navigateBack = { navController.navigateUp() }
            )
        }

        // Home screen
        composable(
            route = Screen.HomeScreen.route,
        ) {
            sobuuLogI("Navigate to home screen")
            HomeScreen(
                navigateToLoginScreen = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.HomeScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToProfileScreen = {
                    navController.navigate(Screen.ProfileScreen.route)
                },
                navigateToCurrentlyReadingBook = {
                    navController.navigate(Screen.CurrentlyReadingBookScreen.withArgs(it))
                },
                navigateToBookScreen = {
                    navController.navigate(Screen.BookScreen.withArgs("false", it))
                },
                navigateToAddBookScreen = {
                    navController.navigate(Screen.AddBookScreen.withArgs(it))
                },
                logs = logs
            )
        }

        // Current reading book screen
        composable(
            route = Screen.CurrentlyReadingBookScreen.route + "/{book_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            val bookId = it.arguments?.getString("book_id") ?: ""
            CurrentlyReadingScreen(
                navigateToHomeScreen = {
                    navController.navigate(Screen.HomeScreen.route) {
                        popUpTo(Screen.CurrentlyReadingBookScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToCommentsScreen = { idBook ->
                    navController.navigate(
                        Screen.CommentsScreen.withArgs(idBook)
                    )
                },
                navigateBack = { navController.navigateUp() },
                bookId = bookId,
                logs = logs
            )
        }

        // Comments screen
        composable(
            route = Screen.CommentsScreen.route + "/{book_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            CommentsScreen(
                navigateBackOrHome = {
                    if(!navController.navigateUp()) {
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToAddNewCommentScreen = { id, repliedCommentId ->
                    navController.navigate(
                        Screen.AddNewCommentScreen.withArgs(
                            id, repliedCommentId
                        )
                    )
                },
                bookId = it.arguments?.getString("book_id") ?: "",
                logs = logs
            )
        }

        // Add new comment screen
        composable(
            route = Screen.AddNewCommentScreen.route + "/{book_id}/{parent_comment_id}",
            arguments = listOf(
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                },
                navArgument("parent_comment_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                },
            )
        ) {
            AddNewCommentScreen(
                navigateBackOrHome = {
                    if(!navController.navigateUp()) {
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigatePopBackStack = {
                    navController.popBackStack()
                },
                bookId = it.arguments?.getString("book_id") ?: "",
                parentCommentId = it.arguments?.getString("parent_comment_id") ?: "",
                logs = logs
            )
        }

        // Book screen
        composable(
            route = Screen.BookScreen.route + "/{opened_after_saved}/{book_id}",
            arguments = listOf(
                navArgument("opened_after_saved") {
                    type = NavType.StringType
                    defaultValue = "false"
                    nullable = false
                },
                navArgument("book_id") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            BookScreen(
                navigateBackOrHome = {
                    val openedAfterSaved = it.arguments?.getString("opened_after_saved")?.equals("true") ?: false
                    sobuuLogI("OpenedAfterSaved: $openedAfterSaved")

                    if(openedAfterSaved) {
                        navController.navigate(Screen.HomeScreen.route)
                    } else {
                        navController.popBackStack()
                    }
                },
                navigateToHomeScreen = {
                    navController.navigate(Screen.HomeScreen.route)
                },
                navigateToBookCoverScreen = { coverUrl ->
                    val encodedUrl = URLEncoder.encode(coverUrl, StandardCharsets.UTF_8.toString())
                    navController.navigate(Screen.BookCoverScreen.withArgs(encodedUrl))
                },
                bookId = it.arguments?.getString("book_id") ?: ""
            )
        }

        // Book cover screen
        composable(
            route = Screen.BookCoverScreen.route + "/{cover_url}",
            arguments = listOf(
                navArgument("cover_url") {
                    type = NavType.StringType
                    defaultValue = ""
                    nullable = false
                }
            )
        ) {
            BookCoverScreen(
                nav = navController,
                cover = it.arguments?.getString("cover_url") ?: ""
            )
        }

        // Add book screen
        composable(
            route = Screen.AddBookScreen.route + "/{manually}",
            arguments = listOf(
                navArgument("manually") {
                    type = NavType.BoolType
                    defaultValue = false
                    nullable = false
                }
            )
        ) {
            AddBookScreen(
                navigateBackOrHome = {
                    if(!navController.navigateUp()) {
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToBookScreen = { openedAfterSaved, bookId ->
                    navController.navigate(Screen.BookScreen.withArgs(openedAfterSaved.toString(), bookId)) {
                        popUpTo(Screen.BookScreen.route) {
                            inclusive = true
                        }
                    }
                },
                isManually = it.arguments?.getBoolean("manually") ?: false
            )
        }

        // Profile screen
        composable(route = Screen.ProfileScreen.route) {
            ProfileScreen(
                navigateBackOrHome = {
                    if(!navController.navigateUp()) {
                        navController.navigate(Screen.HomeScreen.route)
                    }
                },
                navigateToLoginScreen = {
                    navController.navigate(Screen.LoginScreen.route) {
                        popUpTo(Screen.HomeScreen.route) {
                            inclusive = true
                        }
                    }
                },
                navigateToSettingsScreen = {
                    navController.navigate(Screen.SettingsScreen.route)
                }
            )
        }

        // Settings screen
        composable(route = Screen.SettingsScreen.route) {
            SettingsScreen(navigator = navController, navigateBack = { navController.navigateUp() })
        }

        // About the app screen
        composable(route = Screen.AboutScreen.route) {
            AboutTheAppScreen(navigateBack = { navController.navigateUp() })
        }
    }
}