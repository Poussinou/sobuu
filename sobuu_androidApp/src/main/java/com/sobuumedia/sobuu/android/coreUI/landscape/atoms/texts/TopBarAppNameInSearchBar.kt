package com.sobuumedia.sobuu.android.coreUI.landscape.atoms.texts

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.general_appName
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Solway
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun TopBarAppNameInSearchBar() {
    val context = LocalContext.current
    val textColor = MaterialTheme.colorScheme.secondary
    val backgroundColor = MaterialTheme.colorScheme.background
    val text = general_appName.stringResource(context = context)
    val paddingSize = 0.dp

    Text(
        text = text,
        style = Typography.bodyLarge.copy(color = textColor, fontFamily = Solway, fontWeight = FontWeight.Medium),
        modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor)
            .padding(vertical = paddingSize)
            .semantics { this.contentDescription = text },
        textAlign = TextAlign.Start,
    )
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun TopBarAppNameInSearchBarPreview() {
    SobuuTheme {
        TopBarAppNameInSearchBar()
    }
}