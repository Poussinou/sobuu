package com.sobuumedia.sobuu.android.add_book

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawing
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircleOutline
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.AssistChip
import androidx.compose.material3.AssistChipDefaults
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SelectableDates
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.google.accompanist.flowlayout.FlowRow
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_dateIsFutureError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_dateIsFutureTitleError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_missingRequiredDataError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_missingRequiredDataTitleError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_unknownError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_errors_unknownTitleError
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookAuthors
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookCredits
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookDescription
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookGenres
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookISBN
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookImage
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookIsSerie
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookLanguage
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookLanguageEnglish
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookLanguageGerman
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookLanguageSpanish
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookPublishedDate
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookPublisher
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookSerieName
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookSerieNumber
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookTitle
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_bookTotalPages
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_isItTranslated
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_searchBarPlaceholder
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_searchByISBN
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_takeTime
import com.sobuumedia.sobuu.SharedRes.strings.addBook_main_title
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.book_detailsError_emptyField
import com.sobuumedia.sobuu.SharedRes.strings.general_cancel
import com.sobuumedia.sobuu.SharedRes.strings.general_ok
import com.sobuumedia.sobuu.SharedRes.strings.search_main_addBookManually
import com.sobuumedia.sobuu.analytics.events.SaveBookManuallyEvent
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SearchBookListItem
import com.sobuumedia.sobuu.android.custom_widgets.CompleteRoundedOutlineTextFieldNoIcon
import com.sobuumedia.sobuu.android.custom_widgets.CompleteRoundedOutlineWideTextField
import com.sobuumedia.sobuu.android.custom_widgets.CustomTextButton
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.custom_widgets.SegmentText
import com.sobuumedia.sobuu.android.custom_widgets.SegmentedControl
import com.sobuumedia.sobuu.android.custom_widgets.SobuuText
import com.sobuumedia.sobuu.android.custom_widgets.TopAppBarWithMenu
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.getAllGenres
import com.sobuumedia.sobuu.android.utils.getDateFromMillis
import com.sobuumedia.sobuu.android.utils.getImageFile
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.book.remote.BookError
import org.koin.androidx.compose.koinViewModel
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

enum class BookLanguages(private val text: String) {
    ENGLISH(addBook_main_bookLanguageEnglish.stringResource()),
    GERMAN(addBook_main_bookLanguageGerman.stringResource()),
    SPANISH(addBook_main_bookLanguageSpanish.stringResource());

    fun getText(): String {
        return text
    }
}

enum class LoadingScreenType {
    SEARCH, STORE
}

@Composable
fun AddBookScreen(
    addBookViewModel: AddBookViewModel = koinViewModel(),
    navigateBackOrHome: () -> Unit,
    navigateToBookScreen: (openedAfterSaved: Boolean, String) -> Unit,
    isManually: Boolean = false,
) {
    val context = LocalContext.current
    val windowInsets = WindowInsets.safeDrawing
    val isIntroducedManually = remember { mutableStateOf(isManually) }

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            modifier = Modifier.windowInsetsPadding(windowInsets),
            topBar = {
                TopAppBarWithMenu(
                    navigateBackOrHome = navigateBackOrHome,
                    title = addBook_main_title.stringResource(context = context),
                    titleSize = 20.sp,
                    listItems =
                    if (isIntroducedManually.value) {
                        listOf(
                            MenuItemData(
                                text = addBook_main_title.stringResource(context = context),
                                icon = Icons.Filled.AddCircleOutline,
                                action = {
                                    addBookViewModel.onEvent(AddBookUIEvent.saveBookManuallyData)
                                    addBookViewModel.onAnalyticsEvent(
                                        SaveBookManuallyEvent(
                                            name = "SaveBookManually",
                                            otherVariables = mutableMapOf<String, String>()
                                                .plus("Title" to addBookViewModel.state.bookName)
                                                .plus("ISBN10" to addBookViewModel.state.bookISBN)
                                                .plus("Publisher" to addBookViewModel.state.bookPublisher)
                                        )
                                    )
                                }
                            )
                        )
                    } else {
                        listOf()
                    },
                    showCollapseMenu = false,
                )
            },
            content = {
                if (isIntroducedManually.value) {
                    ContentIntroducedManually(
                        navigateToBookScreen = navigateToBookScreen,
                        modifier = Modifier.padding(it),
                        addBookViewModel = addBookViewModel
                    )
                } else {
                    ContentISBN(
                        modifier = Modifier.padding(it),
                        navigateToBookScreen = navigateToBookScreen,
                        navigateToAddBookScreenManually = {
                            isIntroducedManually.value = true
                        },
                        addBookViewModel = addBookViewModel
                    )
                }
            },
        )
    }
}

// Screen that appears in case the ISBN search found nothing.
// It's to introduce the data of the book
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContentIntroducedManually(
    modifier: Modifier = Modifier,
    navigateToBookScreen: (openedAfterSaved: Boolean, String) -> Unit,
    addBookViewModel: AddBookViewModel = koinViewModel()
) {
    val context = LocalContext.current
    val state = addBookViewModel.state
    val keyboardController = LocalSoftwareKeyboardController.current

    if (state.manuallySavedBook != null) {
        val book = state.manuallySavedBook
        navigateToBookScreen(true, book.id)
    }

    if (state.displayErrorDialog) {
        val dialogTitle: String
        val dialogText: String

        when (state.error) {
            is BookError.DateIsFutureError -> {
                dialogTitle =
                    addBook_errors_dateIsFutureTitleError.stringResource(context = context)
                dialogText = addBook_errors_dateIsFutureError.stringResource(context = context)
            }

            is BookError.MissingRequiredDataError -> {
                dialogTitle =
                    addBook_errors_missingRequiredDataTitleError.stringResource(context = context)
                dialogText =
                    addBook_errors_missingRequiredDataError.stringResource(context = context)
            }

            else -> {
                dialogTitle = addBook_errors_unknownTitleError.stringResource(context = context)
                dialogText = addBook_errors_unknownError.stringResource(context = context)
            }
        }

        AlertDialog(
            onDismissRequest = {
                addBookViewModel.onEvent(AddBookUIEvent.closeErrorDialog)
            },
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = true
            ),
            title = { Text(text = dialogTitle) },
            containerColor = MaterialTheme.colorScheme.background,
            textContentColor = MaterialTheme.colorScheme.secondary,
            titleContentColor = MaterialTheme.colorScheme.secondary,
            shape = RoundedCornerShape(10.dp),
            text = { Text(text = dialogText) },
            confirmButton = {
                CustomTextButton(
                    modifier = Modifier.padding(10.dp),
                    onClick = {
                        addBookViewModel.onEvent(AddBookUIEvent.closeErrorDialog)
                    },
                    color = MaterialTheme.colorScheme.primary,
                    text = general_ok.stringResource(context = context),
                    fontSize = 20.sp
                )
            },
        )
    }

    when {
        state.isLoading -> {
            keyboardController?.hide()
            ScreenLoading(loadingType = LoadingScreenType.STORE)
        }

        else -> {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 15.dp)
                    .verticalScroll(state = rememberScrollState())
                    .then(modifier),
            ) {

                // Picture

                val pickPictureLauncher = rememberLauncherForActivityResult(
                    ActivityResultContracts.GetContent()
                ) { imageUri ->
                    if (imageUri != null) {
                        val imageFile = imageUri.getImageFile(context)
                        addBookViewModel.onEvent(AddBookUIEvent.BookPictureChanged(imageFile))
                    }
                }

                if (state.bookImage == null) {
                    ElevatedButton(
                        onClick = {
                            pickPictureLauncher.launch("image/*")
                        },
                        colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(32.dp)
                    ) {
                        Text(
                            text = addBook_main_bookImage.stringResource(context = context),
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.background,
                                fontSize = 16.sp,
                                fontFamily = SourceSans
                            )
                        )
                    }
                } else {
                    Image(
                        painter = rememberAsyncImagePainter(
                            ImageRequest
                                .Builder(LocalContext.current)
                                .data(data = state.bookImage)
                                .build()
                        ),
                        contentDescription = addBook_main_bookImage.stringResource(context = context),
                        modifier = Modifier
                            .align(CenterHorizontally)
                            .size(width = 300.dp, height = 450.dp)
                            .padding(vertical = 10.dp),
                        contentScale = ContentScale.Crop
                    )
                }

                // Title
                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = state.bookName,
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookTitleChanged(it))
                    },
                    placeholderText = addBook_main_bookTitle.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookName.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookName.isBlank()
                )

                // Authors
                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = state.bookAuthors,
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookAuthorsChanged(it))
                    },
                    placeholderText = addBook_main_bookAuthors.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookAuthors.isEmpty()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookAuthors.isEmpty()
                )

                // Publisher
                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = state.bookPublisher,
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookPublisherChanged(it))
                    },
                    placeholderText = addBook_main_bookPublisher.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookPublisher.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookPublisher.isBlank()
                )

                // Publisher Date
                val openDatePickerDialog = remember { mutableStateOf(false) }
                val datePickerState = rememberDatePickerState(selectableDates = object :
                    SelectableDates {
                    override fun isSelectableDate(utcTimeMillis: Long): Boolean {
                        return utcTimeMillis <= System.currentTimeMillis()
                    }
                })

                if (openDatePickerDialog.value) {
                    DatePickerDialog(
                        onDismissRequest = {
                            openDatePickerDialog.value = false
                        },
                        confirmButton = {
                            TextButton(
                                onClick = {
                                    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
                                    val instant = Instant.ofEpochMilli(
                                        datePickerState.selectedDateMillis ?: 0L
                                    )
                                    val date =
                                        LocalDateTime.ofInstant(instant, ZoneId.systemDefault())

                                    if (date.isAfter(LocalDateTime.now())) {
                                        addBookViewModel.onEvent(AddBookUIEvent.Error(BookError.DateIsFutureError))
                                    } else {
                                        addBookViewModel.onEvent(
                                            AddBookUIEvent.BookPublishedDateChanged(
                                                formatter.format(date)
                                            )
                                        )
                                    }

                                    openDatePickerDialog.value = false
                                },
                            ) {
                                Text(general_ok.stringResource(context = context))
                            }
                        },
                        dismissButton = {
                            TextButton(
                                onClick = {
                                    openDatePickerDialog.value = false
                                }
                            ) {
                                Text(general_cancel.stringResource(context = context))
                            }
                        }
                    ) {
                        DatePicker(
                            state = datePickerState,
                            headline = {
                                val selectedDate = datePickerState.selectedDateMillis.getDateFromMillis()
                                Text(
                                    modifier = Modifier.padding(horizontal = 50.dp),
                                    text = selectedDate,
                                    textAlign = TextAlign.Center,
                                    style = Typography.headlineSmall.copy(color = MaterialTheme.colorScheme.secondary)
                                )
                            },
                            showModeToggle = false
                        )
                    }
                }

                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = state.bookPublishedDate,
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookPublishedDateChanged(it))
                        openDatePickerDialog.value = false
                    },
                    placeholderText = addBook_main_bookPublishedDate.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookPublishedDate.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookPublishedDate.isBlank(),
                    fieldEnabled = false,
                    onFieldClicked = {
                        openDatePickerDialog.value = true
                    }
                )

                // Description (probably not the best in a phone app)
                CompleteRoundedOutlineWideTextField(
                    fieldValue = state.bookDescription,
                    onFieldValueChange = { newCommentText ->
                        addBookViewModel.onEvent(
                            AddBookUIEvent.BookDescriptionChanged(
                                newCommentText
                            )
                        )
                    },
                    placeholderText = addBook_main_bookDescription.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookDescription.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookDescription.isBlank()
                )

                // Total pages
                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = if (state.bookTotalPages <= 0) "" else state.bookTotalPages.toString(),
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookTotalPagesChanged(it))
                    },
                    placeholderText = addBook_main_bookTotalPages.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookTotalPages <= 0) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookTotalPages <= 0

                )

                // Is it translated?
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 32.dp, vertical = 5.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        modifier = Modifier.padding(vertical = 10.dp),
                        text = addBook_main_isItTranslated.stringResource(context = context),
                        style = TextStyle(
                            color = MaterialTheme.colorScheme.tertiary,
                            fontFamily = SourceSans,
                            fontSize = 16.sp,
                        ),
                    )
                    Switch(
                        modifier = Modifier
                            .semantics {
                                contentDescription = addBook_main_isItTranslated.stringResource()
                            },
                        checked = state.bookIsTranslated,
                        onCheckedChange = {
                            addBookViewModel.onEvent(AddBookUIEvent.BookTranslatedChanged(it))
                        })
                }

                // by who (The textfield appears if the last switcher is on yes)
                if (state.bookIsTranslated) {
                    CompleteRoundedOutlineTextFieldNoIcon(
                        fieldValue = state.bookTranslator,
                        onFieldValueChange = {
                            addBookViewModel.onEvent(AddBookUIEvent.BookTranslatorChanged(it))
                        },
                        placeholderText = addBook_main_bookCredits.stringResource(context = context),
                        placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookTranslator.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                        onKeyboardIme = ImeAction.Next,
                        isError = state.error == BookError.MissingRequiredDataError && state.bookTranslator.isBlank()
                    )
                }

                // ISBN
                CompleteRoundedOutlineTextFieldNoIcon(
                    fieldValue = state.bookISBN,
                    onFieldValueChange = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookISBNChanged(it))
                    },
                    placeholderText = addBook_main_bookISBN.stringResource(context = context),
                    placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookISBN.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    onKeyboardIme = ImeAction.Next,
                    isError = state.error == BookError.MissingRequiredDataError && state.bookISBN.isBlank()
                )

                // Belong to a serie?
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 32.dp, vertical = 5.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        modifier = Modifier.padding(vertical = 10.dp),
                        text = addBook_main_bookIsSerie.stringResource(context = context),
                        style = TextStyle(
                            color = MaterialTheme.colorScheme.tertiary,
                            fontFamily = SourceSans,
                            fontSize = 16.sp,
                        ),
                    )
                    Switch(
                        modifier = Modifier
                            .semantics {
                                contentDescription = addBook_main_bookIsSerie.stringResource()
                            },
                        checked = state.bookIsSerie,
                        onCheckedChange = {
                            addBookViewModel.onEvent(AddBookUIEvent.BookSerieChanged(it))
                        })
                }

                // Which one?
                if (state.bookIsSerie) {
                    CompleteRoundedOutlineTextFieldNoIcon(
                        fieldValue = state.bookSerieName,
                        onFieldValueChange = {
                            addBookViewModel.onEvent(AddBookUIEvent.BookSerieNameChanged(it))
                        },
                        placeholderText = addBook_main_bookSerieName.stringResource(context = context),
                        placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookSerieName.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                        onKeyboardIme = ImeAction.Next,
                        isError = state.error == BookError.MissingRequiredDataError && state.bookSerieName.isBlank()
                    )

                    // What number of book in the serie
                    CompleteRoundedOutlineTextFieldNoIcon(
                        fieldValue = state.bookSerieNumber,
                        onFieldValueChange = {
                            addBookViewModel.onEvent(AddBookUIEvent.BookSerieNumberChanged(it))
                        },
                        placeholderText = addBook_main_bookSerieNumber.stringResource(context = context),
                        placeholderTextColor = if (state.error == BookError.MissingRequiredDataError && state.bookSerieNumber.isBlank()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                        onKeyboardIme = ImeAction.Next,
                        isError = state.error == BookError.MissingRequiredDataError && state.bookSerieNumber.isBlank()
                    )
                }

                // Language
                val segments = remember {
                    listOf(
                        BookLanguages.ENGLISH,
                        BookLanguages.SPANISH,
                        BookLanguages.GERMAN
                    )
                }
                val locale = Locale.current
                var type by remember {
                    mutableStateOf(
                        when (locale.language.lowercase()) {
                            "en" -> segments.first()
                            "de" -> segments.last()
                            "es" -> segments[1]
                            else -> segments.first()
                        }
                    )
                }

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 32.dp, vertical = 5.dp)
                ) {
                    SobuuText(
                        modifier = Modifier.padding(vertical = 10.dp),
                        text = addBook_main_bookLanguage.stringResource(context = context),
                        textColor = MaterialTheme.colorScheme.tertiary,
                        textSize = 16.sp,
                    )
                    SegmentedControl(
                        modifier = Modifier.padding(
                            top = 5.dp, bottom = 15.dp,
                        ),
                        segments = segments,
                        selectedSegment = type,
                        onSegmentSelected = {
                            type = it
                            addBookViewModel.onEvent(AddBookUIEvent.BookLanguageChanged(it.getText()))
                        },
                    ) {
                        SegmentText(
                            it.getText(),
                            style = TextStyle(
                                fontFamily = SourceSans,
                                fontSize = 14.sp,
                                color = MaterialTheme.colorScheme.secondary,
                            )
                        )
                    }
                }

                // Genre (pick from a cloud of genres)
                SobuuText(
                    modifier = Modifier.padding(horizontal = 32.dp, vertical = 10.dp),
                    text = addBook_main_bookGenres.stringResource(context = context),
                    textColor = if (state.error == BookError.MissingRequiredDataError && state.bookGenres.isEmpty()) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.tertiary,
                    textSize = 16.sp
                )

                ChipGroup(
                    selectedGenres = state.bookGenres,
                    onSelectedChanged = {
                        addBookViewModel.onEvent(AddBookUIEvent.BookGenresChanged(it))
                    }
                )
            }
        }
    }
}

// First screen with ISBN search
@Composable
fun ContentISBN(
    modifier: Modifier = Modifier,
    navigateToBookScreen: (openedAfterSaved: Boolean, String) -> Unit,
    navigateToAddBookScreenManually: (Boolean) -> Unit,
    addBookViewModel: AddBookViewModel? = null
) {
    val state = addBookViewModel?.state
    val bookList = addBookViewModel?.booksList
    val keyboardController = LocalSoftwareKeyboardController.current

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 25.dp)
            .verticalScroll(state = rememberScrollState())
            .then(modifier),
    ) {

        // ISBN
        CompleteRoundedOutlineTextFieldNoIcon(
            fieldEnabled = state?.isLoading == false,
            fieldValue = state?.searchISBN ?: "",
            onFieldValueChange = {
                addBookViewModel?.onEvent(AddBookUIEvent.SearchISBNChanged(it))
            },
            placeholderText = addBook_main_searchBarPlaceholder.stringResource(context = LocalContext.current),
            onKeyboardActionClicked = {
                addBookViewModel?.onEvent(AddBookUIEvent.searchISBN)
                keyboardController?.hide()
            }
        )

        when {
            state?.isLoading == true -> {
                keyboardController?.hide()
                Spacer(modifier = Modifier.padding(top = 20.dp))
                ScreenLoading(loadingType = LoadingScreenType.SEARCH)
            }

            state?.error != null -> {
                addBookViewModel.onEvent(AddBookUIEvent.LogError("There was an error message: ${state.error.errorMessage}"))
                ButtonSearch(addBookViewModel = addBookViewModel)

                Spacer(modifier = Modifier.height(25.dp))

                Text(
                    getStringFromBookError(resultError = state.error),
                    modifier = Modifier
                        .padding(start = 32.dp, end = 32.dp),
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.error,
                        fontFamily = SourceSans,
                        fontSize = 20.sp
                    ),
                    textAlign = TextAlign.Center,
                )

            }

            !state?.bookId.isNullOrEmpty() -> {
                navigateToBookScreen(false, state?.bookId!!)
                addBookViewModel.onEvent(AddBookUIEvent.emptyBookIdData)
            }

            bookList?.isEmpty() == false && state?.displayBookListResult == true -> {
                Spacer(modifier = Modifier.height(25.dp))
                // Display result
                val book = bookList[0]
                Box(modifier = Modifier.padding(horizontal = 15.dp)) {
                    SearchBookListItem(
                        modifier = Modifier
                            .clickable {
                                addBookViewModel.onEvent(AddBookUIEvent.SaveBookData(book))
                            },
                        title = book.title,
                        author = book.authors.joinToString(", ") { it },
                        totalPages = book.totalPages,
                        description = book.bookDescription,
                        cover = book.thumbnail
                    )
                }
            }

            bookList?.isEmpty() == true && state?.displayBookListResult == true -> {
                Spacer(modifier = Modifier.padding(vertical = 15.dp))
                ElevatedButton(
                    onClick = { navigateToAddBookScreenManually(true) },
                    colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.primary),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Text(
                        text = search_main_addBookManually.stringResource(context = LocalContext.current),
                        style = TextStyle(
                            color = MaterialTheme.colorScheme.background,
                            fontSize = 16.sp,
                            fontFamily = SourceSans
                        )
                    )
                }
                Spacer(modifier = Modifier.padding(vertical = 15.dp))
            }

            else -> {
                ButtonSearch(addBookViewModel = addBookViewModel)
            }
        }
    }
}

@Composable
fun ButtonSearch(addBookViewModel: AddBookViewModel?) {
    ElevatedButton(
        onClick = {
            addBookViewModel?.onEvent(AddBookUIEvent.searchISBN)
        },
        colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary),
        modifier = Modifier
            .fillMaxWidth()
            .padding(32.dp)
    ) {
        Text(
            text = addBook_main_searchByISBN.stringResource(context = LocalContext.current),
            style = TextStyle(
                color = MaterialTheme.colorScheme.background,
                fontSize = 16.sp,
                fontFamily = SourceSans
            )
        )
    }
}

@Composable
fun ChipGroup(
    modifier: Modifier = Modifier,
    genres: List<String> = SharedRes.strings.getAllGenres(),
    selectedGenres: List<String> = emptyList(),
    onSelectedChanged: (String) -> Unit = {},
) {
    Column(modifier = Modifier.padding(horizontal = 32.dp, vertical = 10.dp)) {
        FlowRow(
            modifier = modifier,
            mainAxisSpacing = 2.dp,
            crossAxisSpacing = 1.dp,
        ) {
            genres.forEach { genre ->
                AssistChip(
                    onClick = { onSelectedChanged(genre) },
                    label = {
                        Text(
                            text = genre,
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.onError,
                                fontFamily = SourceSans,
                                fontSize = if (selectedGenres.contains(genre)) 20.sp else 16.sp,
                            )
                        )
                    },
                    colors = AssistChipDefaults.assistChipColors(
                        containerColor = if (selectedGenres.contains(genre))
                            MaterialTheme.colorScheme.primary
                        else
                            MaterialTheme.colorScheme.tertiary
                    ),
                    border = AssistChipDefaults.assistChipBorder(
                        enabled = true,
                        borderColor = if (selectedGenres.contains(genre))
                            MaterialTheme.colorScheme.primary
                        else
                            MaterialTheme.colorScheme.inverseOnSurface
                    )
                )
            }
        }
    }
}

@Composable
fun ScreenLoading(
    modifier: Modifier = Modifier,
    loadingType: LoadingScreenType
) {
    Spacer(modifier = Modifier.height(25.dp))
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .then(modifier),
        horizontalAlignment = CenterHorizontally
    ) {
        Text(
            text = addBook_main_takeTime.stringResource(context = LocalContext.current),
            style = TextStyle(
                color = MaterialTheme.colorScheme.secondary,
                fontSize = 16.sp,
                fontFamily = SourceSans
            )
        )

        Spacer(modifier = Modifier.height(15.dp))

        when (loadingType) {
            LoadingScreenType.STORE -> {
                Spacer(modifier = Modifier.height(25.dp))
                StoringAnimation()
            }

            else ->
                SearchingAnimation()
        }
    }
}

@Composable
fun SearchingAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.search))
    val progress by animateLottieCompositionAsState(
        composition = composition,
        iterations = LottieConstants.IterateForever
    )

    LottieAnimation(
        modifier = Modifier
            .width(200.dp)
            .height(200.dp),
        composition = composition,
        progress = { progress },
    )
}

@Composable
fun StoringAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loading_animation))
    val progress by animateLottieCompositionAsState(
        composition = composition,
        iterations = LottieConstants.IterateForever
    )

    LottieAnimation(
        modifier = Modifier
            .width(300.dp)
            .height(300.dp),
        composition = composition,
        progress = { progress },
    )
}

@Composable
private fun getStringFromBookError(resultError: BookError?): String {
    return when (resultError) {
        is BookError.TimeOutError -> {
            authorization_errors_timeout.stringResource(context = LocalContext.current)
        }

        is BookError.ApiRouteNotFoundError -> {
            authorization_errors_unknown.stringResource(context = LocalContext.current)
        }

        is BookError.InvalidISBNError -> {
            authorization_errors_unknown.stringResource(context = LocalContext.current)
        }

        is BookError.SearchBookError -> {
            authorization_errors_unknown.stringResource(context = LocalContext.current)
        }

        is BookError.EmptyField -> {
            book_detailsError_emptyField.stringResource(context = LocalContext.current)
        }

        else -> {
            authorization_errors_unknown.stringResource(context = LocalContext.current)
        }
    }
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun ComposableAddBookContentISBNPreview() {
    ContentISBN(
        navigateToBookScreen = { _, _ -> },
        navigateToAddBookScreenManually = {}
    )
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun ScreenLoadingPreview() {
    ScreenLoading(loadingType = LoadingScreenType.STORE)
}


