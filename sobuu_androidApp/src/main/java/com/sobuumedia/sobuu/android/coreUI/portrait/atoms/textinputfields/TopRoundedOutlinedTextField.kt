package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun TopRoundedOutlinedTextField(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    icon: ImageVector = Icons.Filled.Person,
    isError: Boolean = false,
) {
    val focusManager = LocalFocusManager.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    val roundedCorner = 10.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(
                        topStart = roundedCorner,
                        topEnd = roundedCorner
                    ),
                    color = backgroundColor
                )
                .semantics { this.contentDescription = placeholderText },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
            keyboardActions = KeyboardActions(
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                }
            ),
            isError = isError,
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            shape = RoundedCornerShape(
                topStart = roundedCorner,
                topEnd = roundedCorner
            ),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = icon,
                    contentDescription = Icons.Filled.Person.name,
                    tint = placeholderColor,
                )
            }
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun TopRoundedOutlinedTextFieldPreview() {
    TopRoundedOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Username"
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun TopRoundedOutlinedTextFieldErrorPreview() {
    TopRoundedOutlinedTextField(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Username",
        isError = true
    )
}