package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CompleteRoundedOutlineTextFieldNoIcon(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    onFieldClicked: () -> Unit = {},
    fieldEnabled: Boolean = true,
    contentDescription: String = "",
    isError: Boolean = false,
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    val roundedCorner = 10.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)

    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(roundedCorner),
                    color = backgroundColor
                )
                .clickable { onFieldClicked.invoke() }
                .semantics { this.contentDescription = contentDescription },
            enabled = fieldEnabled,
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(imeAction = onKeyboardIme),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            shape = RoundedCornerShape(roundedCorner),
            singleLine = true,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlineTextFieldNoIconPreview() {
    CompleteRoundedOutlineTextFieldNoIcon(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Title",
        onKeyboardActionClicked = {}
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlineTextFieldNoIconErrorPreview() {
    CompleteRoundedOutlineTextFieldNoIcon(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Title",
        onKeyboardActionClicked = {},
        isError = true
    )
}