package com.sobuumedia.sobuu.android.settings.settings

import android.content.pm.PackageManager
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.features.settings.repository.ISettingsRepository
import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions
import kotlinx.coroutines.launch


class SettingsViewModel(
    private val settingsRepo: ISettingsRepository,
): ViewModel() {

    var state by mutableStateOf(SettingsState())

    fun onEvent(event: SettingsUIEvent) {
        when(event) {
            is SettingsUIEvent.SetThemeMode -> setDisplayMode(event.value)
            is SettingsUIEvent.displayThemeModeDialog -> {
                state = if(state.displayThemeDialog) {
                    state.copy(displayThemeDialog = !state.displayThemeDialog)
                } else {
                    getDisplayMode()
                    state.copy(displayThemeDialog = true)
                }
            }
            is SettingsUIEvent.getThemeMode -> getDisplayMode()
            is SettingsUIEvent.SetAnalyticsEnabled -> setAnalyticsEnabled(event.value)
            is SettingsUIEvent.displayAnalyticsDialog -> {
                state = state.copy(displayAnalyticsExplanationDialog = !state.displayAnalyticsExplanationDialog)
            }
            is SettingsUIEvent.getAnalyticsEnabled -> getAnalyticsEnabled()
        }
    }

    private fun getDisplayMode() {
        viewModelScope.launch {
            val theme = settingsRepo.getAppTheme()

            state = state.copy(currentTheme = theme)
        }
    }

    private fun getDisplayModeAndOpenDialog() {
        viewModelScope.launch {
            val theme = settingsRepo.getAppTheme()

            state = state.copy(currentTheme = theme)
        }
    }

    private fun setDisplayMode(selectedMode: ThemeOptions) {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true,
                displayThemeDialog = false
            )

            settingsRepo.setAppTheme(selectedMode)

            state = state.copy(isLoading = false, currentTheme = selectedMode)
        }
    }

    private fun getAnalyticsEnabled() {
        viewModelScope.launch {
            val enabled = settingsRepo.getAnalyticsEnabled()

            state = state.copy(currentAnalyticsEnabled = enabled)
        }
    }

    private fun setAnalyticsEnabled(enabled: Boolean) {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true,
                displayAnalyticsExplanationDialog = false
            )

            settingsRepo.setAnalyticsEnabled(enabled)

            state = state.copy(isLoading = false, currentAnalyticsEnabled = enabled)
        }
    }

    fun isPackageInstalled(packageName: String, packageManager: PackageManager): Boolean {
        return try {
            packageManager.getPackageInfo(packageName, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
}