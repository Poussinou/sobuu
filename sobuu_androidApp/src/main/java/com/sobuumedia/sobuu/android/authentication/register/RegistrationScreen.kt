package com.sobuumedia.sobuu.android.authentication.register

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_createAccount
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emailAlreadyUsed
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyRegistrationFields
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_invalidRegistrationFields
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_registration
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_shortPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_usernameAlreadyTaken
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_weakPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_wrongEmailFormat
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.authentication.RegistrationViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SobuuTopAppBar
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication.RegistrationView
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import org.koin.androidx.compose.koinViewModel

@Composable
fun RegistrationScreen(
    navigateToSentEmailScreen: (type: EmailType) -> Unit,
    navigateBack: () -> Unit,
    navigateToLegalText: (TextType) -> Unit,
    viewModel: RegistrationViewModel = koinViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
    val configuration = LocalConfiguration.current

    LaunchedEffect(viewModel, context) {
        viewModel.registrationResult.collect { result ->
            if (result is AuthenticationResult.Registered) {
                navigateToSentEmailScreen(EmailType.VERIFICATION)
            } else {
                viewModel.handleError(result.error)
            }
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            SobuuTopAppBar(
                navigateBack = { navigateBack() },
                title = authorization_auth_createAccount.stringResource(context = context),
                backgroundColor = MaterialTheme.colorScheme.primary,
                titleColor = MaterialTheme.colorScheme.background,
                iconColor = MaterialTheme.colorScheme.background,
            )
        },
        content = {
            Box(modifier = Modifier.padding(it)) {
                when (configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        RegistrationViewLandscape(
                            usernameValue = state.registrationUsername,
                            usernameOnValueChange = { username ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationUsernameChanged(
                                        username
                                    )
                                )
                            },
                            emailValue = state.registrationEmail,
                            emailOnValueChange = { email ->
                                viewModel.onEvent(RegistrationUIEvent.RegistrationEmailChanged(email))
                            },
                            passwordValue = state.registrationPassword,
                            passwordOnValueChange = { password ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationPasswordChanged(
                                        password
                                    )
                                )
                            },
                            confirmationPasswordValue = state.registrationPasswordConfirmation,
                            confirmationPasswordOnValueChange = { password ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationPasswordConfirmationChanged(
                                        password
                                    )
                                )
                            },
                            firstNameValue = state.registrationFirstname,
                            firstNameOnValueChange = { firstName ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationFirstNameChanged(
                                        firstName
                                    )
                                )
                            },
                            lastNameValue = state.registrationLastname,
                            lastNameOnValueChange = { lastName ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationLastNameChanged(
                                        lastName
                                    )
                                )
                            },
                            nameHasError = state.usernameError,
                            emailHasError = state.emailError,
                            passwordHasError = state.passwordError,
                            confirmationPassHasError = state.confirmationPassError,
                            errorText = if (state.error != null) getStringFromError(error = state.error) else null,
                            isLoading = state.isLoading,
                            privacyPolicyConfirmationSwitchValue = state.privacyPolicyConfirmationSwitch,
                            privacyPolicyConfirmationSwitchOnValueChange = { policy ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.PrivacyPolicyConfirmationSwitchChanged(
                                        policy
                                    )
                                )
                            },
                            registerButtonOnClick = {
                                viewModel.onEvent(RegistrationUIEvent.registerUser)
                            },
                            termsButtonOnClick = { navigateToLegalText(TextType.TERMS_AND_CONDITIONS) },
                            privacyButtonOnClick = { navigateToLegalText(TextType.PRIVACY_POLICY) }
                        )
                    }

                    else -> {
                        RegistrationView(
                            usernameValue = state.registrationUsername,
                            usernameOnValueChange = { username ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationUsernameChanged(
                                        username
                                    )
                                )
                            },
                            emailValue = state.registrationEmail,
                            emailOnValueChange = { email ->
                                viewModel.onEvent(RegistrationUIEvent.RegistrationEmailChanged(email))
                            },
                            passwordValue = state.registrationPassword,
                            passwordOnValueChange = { password ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationPasswordChanged(
                                        password
                                    )
                                )
                            },
                            confirmationPasswordValue = state.registrationPasswordConfirmation,
                            confirmationPasswordOnValueChange = { password ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationPasswordConfirmationChanged(
                                        password
                                    )
                                )
                            },
                            firstNameValue = state.registrationFirstname,
                            firstNameOnValueChange = { firstName ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationFirstNameChanged(
                                        firstName
                                    )
                                )
                            },
                            lastNameValue = state.registrationLastname,
                            lastNameOnValueChange = { lastName ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.RegistrationLastNameChanged(
                                        lastName
                                    )
                                )
                            },
                            nameHasError = state.error != null,
                            emailHasError = state.error != null,
                            passwordHasError = state.error != null,
                            confirmationPassHasError = state.error != null,
                            errorText = if (state.error != null) getStringFromError(error = state.error) else null,
                            isLoading = state.isLoading,
                            privacyPolicyConfirmationSwitchValue = state.privacyPolicyConfirmationSwitch,
                            privacyPolicyConfirmationSwitchOnValueChange = { policy ->
                                viewModel.onEvent(
                                    RegistrationUIEvent.PrivacyPolicyConfirmationSwitchChanged(
                                        policy
                                    )
                                )
                            },
                            registerButtonOnClick = {
                                viewModel.onEvent(RegistrationUIEvent.registerUser)
                            },
                            termsButtonOnClick = { navigateToLegalText(TextType.TERMS_AND_CONDITIONS) },
                            privacyButtonOnClick = { navigateToLegalText(TextType.PRIVACY_POLICY) }
                        )
                    }
                }
            }
        }
    )
}

fun getStringFromError(error: AuthenticationError?): String {
    return when (error) {
        is AuthenticationError.InvalidCredentials -> {
            authorization_errors_invalidRegistrationFields.stringResource()
        }
        is AuthenticationError.EmptyCredentialsError -> {
            authorization_errors_emptyRegistrationFields.stringResource()
        }
        is AuthenticationError.TimeOutError -> {
            authorization_errors_timeout.stringResource()
        }
        is AuthenticationError.WrongEmailFormatError -> {
            authorization_errors_wrongEmailFormat.stringResource()
        }
        is AuthenticationError.PasswordTooShort -> {
            authorization_errors_shortPassword.stringResource()
        }
        is AuthenticationError.RegistrationError -> {
            authorization_errors_registration.stringResource()
        }
        is AuthenticationError.UsernameAlreadyTaken -> {
            authorization_errors_usernameAlreadyTaken.stringResource()
        }
        is AuthenticationError.WeakPassword -> {
            authorization_errors_weakPassword.stringResource()
        }
        is AuthenticationError.EmailAlreadyTaken -> {
            authorization_errors_emailAlreadyUsed.stringResource()
        }
        else -> {
            authorization_errors_unknown.stringResource()
        }
    }
}