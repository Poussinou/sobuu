package com.sobuumedia.sobuu.android.currently_reading

import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.times
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.AsyncImage
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize
import com.sobuumedia.sobuu.SharedRes.strings.general_no
import com.sobuumedia.sobuu.SharedRes.strings.general_yes
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_emptyRateNumber
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishButton
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishText1
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishText2
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_finishTitle
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpButton
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpText1
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpText2
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_giveUpTitle
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_rateBookHint
import com.sobuumedia.sobuu.SharedRes.strings.home_currentlyReading_updateProgressButton
import com.sobuumedia.sobuu.SharedRes.strings.home_main_finishedBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_gaveUpBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_main_reading
import com.sobuumedia.sobuu.SharedRes.strings.home_main_startedBookOn
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_errorNotValidNumberInProgressDialog
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogButton
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogPage
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogPercentage
import com.sobuumedia.sobuu.SharedRes.strings.home_updateBookProgress_progressDialogTitle
import com.sobuumedia.sobuu.android.R
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.LinealProgressIndicator
import com.sobuumedia.sobuu.android.custom_widgets.CustomTextButton
import com.sobuumedia.sobuu.android.custom_widgets.CustomTopAppBar
import com.sobuumedia.sobuu.android.custom_widgets.SegmentText
import com.sobuumedia.sobuu.android.custom_widgets.SegmentedControl
import com.sobuumedia.sobuu.android.custom_widgets.SobuuText
import com.sobuumedia.sobuu.android.custom_widgets.WideTextField
import com.sobuumedia.sobuu.android.main.toStringDateWithDayAndTime
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.getPage
import com.sobuumedia.sobuu.android.utils.getPercentage
import com.sobuumedia.sobuu.android.utils.limitToTwoDecimals
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.NapierLogs
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import org.koin.androidx.compose.koinViewModel
import kotlin.random.Random

const val TAG = "CurrentlyReadingScreen"

enum class DialogUpdateProgressType(private val text: String) {
    PAGE(home_updateBookProgress_progressDialogPage.stringResource()),
    PERCENTAGE(home_updateBookProgress_progressDialogPercentage.stringResource());

    fun getText(): String {
        return text
    }
}

@Composable
fun CurrentlyReadingScreen(
    navigateToHomeScreen: () -> Unit,
    navigateToCommentsScreen: (idBook: String) -> Unit,
    navigateBack: () -> Unit,
    bookId: String,
    viewModel: CurrentlyReadingViewModel = koinViewModel(),
    logs: SobuuLogs
) {
    val context = LocalContext.current

    LaunchedEffect(viewModel, context) {
        viewModel.onEvent(CurrentlyReadingUIEvent.StartScreen(bookId = bookId))
    }

    if (viewModel.state.gaveUp || viewModel.state.finished) {
        navigateToHomeScreen()
    }

    Scaffold(
        topBar = {
            CustomTopAppBar(
                navigateBack = { navigateBack() },
                title = home_main_reading.stringResource(context = context),
                titleSize = 24.sp,
            )
        },
        content = {
            if (viewModel.state.isLoading) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background),
                    contentAlignment = Alignment.Center,
                ) {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                }
            } else {
                CurrentlyReadingContent(
                    modifier = Modifier
                        .padding(it)
                        .clickable { navigateToCommentsScreen(bookId) },
                    book = viewModel.bookData,
                    viewModel = viewModel,
                    totalPages = viewModel.bookData?.book?.totalPages ?: 0,
                    currentProgress = viewModel.bookData?.bookProgress?.progressInPercentage,
                    currentProgressPage = viewModel.bookData?.bookProgress?.page,
                    currentProgressPercentage = viewModel.bookData?.bookProgress?.percentage,
                    onUpdateProgressDialogButtonClicked = {
                        viewModel.onEvent(
                            CurrentlyReadingUIEvent.UpdateProgress(
                                bookID = bookId,
                            )
                        )
                    },
                    onUpdateProgressDialogChanged = { percentage, page ->
                        viewModel.onEvent(
                            CurrentlyReadingUIEvent.UpdateProgressChanged(
                                percentage = percentage,
                                page = page,
                            )
                        )
                    },
                    logs = logs
                )
            }
        }
    )
}

@Composable
fun CurrentlyReadingContent(
    modifier: Modifier = Modifier,
    book: BookWithProgress?,
    totalPages: Int,
    currentProgressPage: Int?,
    currentProgressPercentage: Double?,
    currentProgress: Double?,
    onUpdateProgressDialogButtonClicked: () -> Unit,
    onUpdateProgressDialogChanged: (percentage: Double, page: Int) -> Unit,
    viewModel: CurrentlyReadingViewModel,
    logs: SobuuLogs
) {
    val context = LocalContext.current
    val displayUpdateProgressDialog = remember { mutableStateOf(false) }
    val displayGiveUpDialog = remember { mutableStateOf(false) }
    val displayFinishDialog = remember { mutableStateOf(false) }

    if (displayGiveUpDialog.value && book != null) {
        GiveUpBookDialog(
            bookTitle = book.book.title,
            onShowDialogState = displayGiveUpDialog,
            bookId = book.book.id,
            viewModel = viewModel
        )
    }

    if (displayFinishDialog.value && book != null) {
        FinishBookDialog(
            bookTitle = book.book.title,
            onShowDialogState = displayFinishDialog,
            bookId = book.book.id,
            viewModel = viewModel
        )
    }

    Box(
        contentAlignment = Alignment.Center
    ) {
        UpdateProgressDialog(
            onShowDialogState = displayUpdateProgressDialog,
            totalPages = totalPages,
            currentPage = currentProgressPage,
            currentPercentage = currentProgressPercentage,
            onButtonClick = onUpdateProgressDialogButtonClicked,
            onChanged = onUpdateProgressDialogChanged,
            logs = logs
        )

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.background)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            BookProgressCard(
                modifier = Modifier
                    .padding(20.dp)
                    .then(modifier),
                picture = book?.book?.picture ?: "",
                progress = currentProgress ?: 0.0,
                startedToRead = book?.bookProgress?.startedToRead,
                finishedToRead = null,
                title = book?.book?.title ?: "",
                authors = book?.book?.authors ?: emptyList(),
                finished = false,
                giveUp = false,
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                TextButton(
                    modifier = Modifier.padding(start = 4.dp),
                    onClick = {
                        displayUpdateProgressDialog.value = true
                    }
                ) {
                    SobuuText(
                        modifier = Modifier.width(100.dp),
                        text = home_currentlyReading_updateProgressButton.stringResource(context = context),
                        textSize = 16.sp,
                        textColor = MaterialTheme.colorScheme.secondary,
                        maxLines = 2,
                        textAlign = TextAlign.Center
                    )
                }

                TextButton(
                    onClick = { displayGiveUpDialog.value = true }
                ) {
                    SobuuText(
                        text = home_currentlyReading_giveUpButton.stringResource(context = context),
                        textSize = 16.sp,
                        textColor = MaterialTheme.colorScheme.secondary,
                    )
                }

                TextButton(
                    modifier = Modifier.padding(end = 5.dp),
                    onClick = { displayFinishDialog.value = true }
                ) {
                    SobuuText(
                        text = home_currentlyReading_finishButton.stringResource(context = context),
                        textSize = 16.sp,
                        textColor = MaterialTheme.colorScheme.secondary,
                    )
                }
            }
        }
    }
}

@Composable
fun BookProgressCard(
    modifier: Modifier = Modifier,
    picture: String,
    progress: Double,
    startedToRead: LocalDateTime?,
    finishedToRead: LocalDateTime?,
    title: String,
    authors: List<String>,
    finished: Boolean,
    giveUp: Boolean,
) {
    val context = LocalContext.current
    val config = LocalConfiguration.current
    val screenHeight = config.screenHeightDp.dp
    val screenWidth = config.screenWidthDp.dp

    Box(
        modifier = Modifier
            .then(modifier)
            .height((75 * screenHeight) / 100)
            .width(screenWidth - 70.dp)
            .border(
                border = BorderStroke(2.dp, color = MaterialTheme.colorScheme.secondary),
                shape = RoundedCornerShape(20.dp)
            )
            .clip(RoundedCornerShape(20.dp))
            .background(MaterialTheme.colorScheme.secondary)
            .padding(10.dp)
    ) {

        ConstraintLayout(modifier = Modifier.fillMaxSize()) {

            val (bookCover, bookTitle, bookAuthors, startedDate,
                progressIndicator, endDate) = createRefs()

            createVerticalChain(bookTitle, bookAuthors, chainStyle = ChainStyle.Packed)

            Text(
                text = title,
                style = TextStyle(
                    fontSize = 22.sp,
                    color = MaterialTheme.colorScheme.background,
                    fontFamily = SourceSans,
                    fontWeight = FontWeight.Medium,
                ),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .constrainAs(bookTitle) {
                        start.linkTo(parent.start, margin = 50.dp)
                        end.linkTo(parent.end, margin = 50.dp)
                        centerHorizontallyTo(parent)
                        linkTo(parent.top, bookAuthors.top, bias = 0.05f)
                    },
            )
            Text(
                text = authors.joinToString(", "),
                style = TextStyle(
                    fontSize = 18.sp,
                    color = MaterialTheme.colorScheme.background,
                    fontFamily = SourceSans
                ),
                modifier = Modifier
                    .padding(top = 18.dp)
                    .constrainAs(bookAuthors) {
                        start.linkTo(parent.start, margin = 50.dp)
                        end.linkTo(parent.end, margin = 50.dp)
                        centerHorizontallyTo(parent)
                        linkTo(bookTitle.bottom, parent.bottom, bias = 0.90f)
                    },
            )

            AsyncImage(
                model = picture,
                placeholder = painterResource(id = R.drawable.ic_cover_placeholder),
                contentDescription = null,
                modifier = Modifier
                    .height(screenHeight / 2.5f)
                    .constrainAs(bookCover) {
                        top.linkTo(bookAuthors.bottom, margin = 10.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        centerHorizontallyTo(parent)
                    },
            )
            LinealProgressIndicator(
                progress = progress,
                modifier = Modifier.constrainAs(progressIndicator) {
                    top.linkTo(bookCover.bottom, margin = 20.dp)
                    end.linkTo(parent.end, margin = 50.dp)
                    start.linkTo(parent.start, margin = 50.dp)
                }
            )
            Text(
                "${home_main_startedBookOn.stringResource(context = context)} ${
                    startedToRead?.toStringDateWithDayAndTime() ?: Clock.System.now()
                        .toLocalDateTime(TimeZone.currentSystemDefault())
                        .toStringDateWithDayAndTime()
                }",
                style = TextStyle(
                    fontFamily = SourceSans,
                    fontSize = 12.sp,
                    color = MaterialTheme.colorScheme.background,
                ),
                modifier = Modifier
                    .padding(top = 5.dp, bottom = 5.dp)
                    .constrainAs(startedDate) {
                        top.linkTo(progressIndicator.bottom)
                        start.linkTo(progressIndicator.start, margin = 10.dp)
                    },
            )
            if (finishedToRead != null && (finished || giveUp)) {
                Text(
                    "${
                        if (finished) {
                            home_main_finishedBookOn.stringResource(context = context)
                        } else {
                            home_main_gaveUpBookOn.stringResource(context = context)
                        }
                    } ${
                        finishedToRead.toStringDateWithDayAndTime()
                    }",
                    style = TextStyle(
                        fontFamily = SourceSans,
                        fontSize = 12.sp,
                        color = MaterialTheme.colorScheme.background,
                    ),
                    modifier = Modifier
                        .padding(bottom = 20.dp)
                        .constrainAs(endDate) {
                            top.linkTo(startedDate.bottom)
                            start.linkTo(startedDate.start)
                        },
                )
            }
        }
    }
}

@Composable
fun UpdateProgressDialog(
    modifier: Modifier = Modifier,
    onShowDialogState: MutableState<Boolean> = mutableStateOf(true),
    totalPages: Int,
    currentPage: Int?,
    currentPercentage: Double?,
    onChanged: (percentage: Double, page: Int) -> Unit,
    onButtonClick: () -> Unit,
    logs: SobuuLogs
) {
    val context = LocalContext.current
    var displayInvalidNumberError by remember { mutableStateOf(false) }

    val segments = remember {
        listOf(DialogUpdateProgressType.PAGE, DialogUpdateProgressType.PERCENTAGE)
    }
    var type by remember {
        mutableStateOf(
            if (currentPage == null) segments.last() else segments.first()
        )
    }

    logs.info(TAG, "CurrentPage: $currentPage, CurrentPercentage: $currentPercentage")

    val page = if(currentPage == null || (currentPage < 0 && (currentPercentage != null && currentPercentage > 0.0))) {
        currentPercentage?.getPage(totalPages = totalPages)
    } else {
        currentPage
    }

    val percentage = if(currentPercentage == null || (currentPercentage <= 0.0 && (currentPage != null && currentPage > 10))) {
        currentPage?.getPercentage(totalPages = totalPages)
    } else {
        currentPercentage
    }


    logs.info(TAG, "Page: $page, Percentage: $percentage")

    // The dialog is loaded once with the page and the percentage null and a second time with the right data
    // To avoid storing the null data in the remember fields, don't assign them when they are null
    if(page != null && percentage != null) {
        val textPercentageState = remember {
            mutableStateOf(
                TextFieldValue(
                    text = percentage.limitToTwoDecimals(),
                    selection = TextRange(0)
                )
            )
        }

        val textPageState = remember {
            mutableStateOf(
                TextFieldValue(
                    text = page.toString(),
                    selection = TextRange(0)
                )
            )
        }

        if (onShowDialogState.value) {
            Dialog(
                onDismissRequest = { onShowDialogState.value = false },
                DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .background(
                            MaterialTheme.colorScheme.background,
                            shape = RoundedCornerShape(8.dp)
                        )
                        .padding(10.dp)
                        .then(modifier),
                ) {
                    Text(
                        text = home_updateBookProgress_progressDialogTitle.stringResource(context = context),
                        style = TextStyle(
                            fontFamily = SourceSans,
                            fontSize = 18.sp,
                            color = MaterialTheme.colorScheme.secondary
                        ),
                    )

                    SegmentedControl(
                        modifier = Modifier.padding(
                            vertical = 5.dp,
                            horizontal = 20.dp
                        ),
                        segments = segments,
                        selectedSegment = type,
                        onSegmentSelected = {
                            type = it
                        },
                    ) {
                        SegmentText(
                            it.getText(),
                            style = TextStyle(
                                fontFamily = SourceSans,
                                fontSize = 16.sp,
                                color = MaterialTheme.colorScheme.secondary,
                            )
                        )
                    }

                    if (type == DialogUpdateProgressType.PAGE) {
                        BasicTextField(
                            modifier = Modifier
                                .width(IntrinsicSize.Min)
                                .padding(10.dp)
                                .onFocusChanged { focusState ->
                                    if (focusState.isFocused) {
                                        val text = textPageState.value.text
                                        textPageState.value = textPageState.value.copy(
                                            selection = TextRange(0, text.length)
                                        )
                                    }
                                },
                            value = textPageState.value,
                            maxLines = 1,
                            onValueChange = {
                                displayInvalidNumberError = false
                                textPageState.value = it
                                textPercentageState.value = it.copy(text = it.text.toInt()?.getPercentage(totalPages)?.limitToTwoDecimals().toString())
                                if (it.text.isNumber() && it.text.toInt() != null) {
                                    onChanged.invoke(
                                        // Percentage
                                        it.text.toInt()?.getPercentage(totalPages)?.limitToTwoDecimals()?.toDouble()!!,
                                        // Page
                                        it.text.toInt()!!
                                    )
                                } else {
                                    displayInvalidNumberError = true
                                }
                            },
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                            textStyle = TextStyle(
                                fontSize = 80.sp,
                                fontFamily = SourceSans,
                                color = MaterialTheme.colorScheme.secondary,
                            )
                        )
                    } else {
                        Row(
                            modifier = Modifier.padding(10.dp),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            BasicTextField(
                                modifier = Modifier
                                    .width(IntrinsicSize.Min)
                                    .onFocusChanged { focusState ->
                                        if (focusState.isFocused) {
                                            val text = textPercentageState.value.text
                                            textPercentageState.value =
                                                textPercentageState.value.copy(
                                                    selection = TextRange(0, text.length)
                                                )
                                        }
                                    },
                                value = textPercentageState.value,
                                maxLines = 1,
                                onValueChange = {
                                    val changedText = if(it.text.contains(',')) {
                                        it.copy(text = it.text.replace(',', '.'))
                                    } else if(it.text.isBlank() || it.text == "-") {
                                        it.copy(text = "0")
                                    } else {
                                        it
                                    }
                                    displayInvalidNumberError = false
                                    textPercentageState.value = changedText
                                    textPageState.value = changedText.copy(text = changedText.text.toDouble().getPage(totalPages).toString())
                                    if (changedText.text.isPercentNumber() && changedText.text.isNotBlank()) {
                                        onChanged.invoke(
                                            // Percentage
                                            changedText.text.toDouble().limitToTwoDecimals().toDouble(),
                                            // Page
                                            changedText.text.toDouble().getPage(totalPages)
                                        )
                                    } else {
                                        displayInvalidNumberError = true
                                    }
                                },
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Number
                                ),
                                textStyle = TextStyle(
                                    fontSize = 80.sp,
                                    fontFamily = SourceSans,
                                    color = MaterialTheme.colorScheme.secondary,
                                )
                            )
                            Text(
                                modifier = Modifier.padding(start = 10.dp),
                                text = "%",
                                style = TextStyle(
                                    fontSize = 70.sp,
                                    fontFamily = SourceSans,
                                    color = MaterialTheme.colorScheme.secondary,
                                    fontWeight = FontWeight.Bold,
                                )
                            )
                        }
                    }

                    if (displayInvalidNumberError) {
                        Text(
                            modifier = Modifier.padding(10.dp),
                            text = home_updateBookProgress_errorNotValidNumberInProgressDialog.stringResource(
                                context = context
                            ),
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.error,
                                fontSize = 14.sp,
                                fontFamily = SourceSans,
                            ),
                        )
                    }

                    FilledTonalButton(
                        modifier = Modifier
                            .clip(RoundedCornerShape(10.dp))
                            .padding(
                                bottom = 10.dp,
                                top = if (displayInvalidNumberError) 0.dp else 50.dp
                            ),
                        onClick = {
                            onButtonClick.invoke()
                            onShowDialogState.value = false
                        },
                        enabled = !displayInvalidNumberError,
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.error,
                            contentColor = MaterialTheme.colorScheme.background,
                            disabledContainerColor = MaterialTheme.colorScheme.tertiary,
                            disabledContentColor = MaterialTheme.colorScheme.background,
                        )
                    ) {
                        Text(
                            home_updateBookProgress_progressDialogButton.stringResource(context = context),
                            style = TextStyle(
                                fontSize = 14.sp,
                                color = MaterialTheme.colorScheme.background,
                                fontWeight = FontWeight.Normal,
                                fontFamily = SourceSans
                            ),
                            textAlign = TextAlign.Center,
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun GiveUpBookDialog(
    modifier: Modifier = Modifier,
    onShowDialogState: MutableState<Boolean> = mutableStateOf(true),
    bookTitle: String,
    bookId: String,
    viewModel: CurrentlyReadingViewModel
) {
    val context = LocalContext.current
    AlertDialog(
        onDismissRequest = {
            onShowDialogState.value = false
        },
        modifier = Modifier.then(modifier),
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
        title = { Text(text = "${home_currentlyReading_giveUpTitle.stringResource(context = context)} \"$bookTitle\"") },
        containerColor = MaterialTheme.colorScheme.background,
        textContentColor = MaterialTheme.colorScheme.secondary,
        titleContentColor = MaterialTheme.colorScheme.secondary,
        shape = RoundedCornerShape(10.dp),
        text = {
            val random = Random.nextInt(0, 100)

            if (random % 2 == 0) {
                Text(text = home_currentlyReading_giveUpText1.stringResource(context = context))
            } else {
                Text(text = home_currentlyReading_giveUpText2.stringResource(context = context))
            }
        },
        confirmButton = {
            CustomTextButton(
                modifier = Modifier.padding(10.dp),
                onClick = {
                    viewModel.onEvent(CurrentlyReadingUIEvent.GiveUpBook(bookId = bookId))
                    onShowDialogState.value = false
                },
                color = MaterialTheme.colorScheme.primary,
                text = general_yes.stringResource(context = context),
                fontSize = 20.sp
            )
        },
        dismissButton = {
            CustomTextButton(
                modifier = Modifier.padding(10.dp),
                onClick = { onShowDialogState.value = false },
                color = MaterialTheme.colorScheme.error,
                text = general_no.stringResource(context = context),
                fontSize = 20.sp
            )
        }
    )
}

@Composable
fun FinishBookDialog(
    modifier: Modifier = Modifier,
    onShowDialogState: MutableState<Boolean> = mutableStateOf(true),
    bookTitle: String,
    bookId: String,
    viewModel: CurrentlyReadingViewModel
) {
    val context = LocalContext.current
    var rating: Double by remember { mutableDoubleStateOf(0.0) }
    var ratingText: String by remember { mutableStateOf("") }
    var emptyRatingWarning: Boolean by remember { mutableStateOf(false) }

    Dialog(
        onDismissRequest = {
            onShowDialogState.value = false
        },
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = true),
        content = {
            Box {
                if (emptyRatingWarning) {
                    Toast.makeText(
                        context,
                        home_currentlyReading_emptyRateNumber.stringResource(context = context),
                        Toast.LENGTH_LONG
                    ).show()
                }
                Column(
                    Modifier
                        .fillMaxWidth()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(10.dp)
                        .clip(shape = RoundedCornerShape(10.dp))
                        .then(modifier),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    // Title
                    Text(
                        modifier = Modifier.padding(
                            bottom = 20.dp,
                            top = 10.dp,
                            start = 10.dp,
                            end = 10.dp
                        ),
                        text = "${home_currentlyReading_finishTitle.stringResource(context = context)} \"$bookTitle\"",
                        style = TextStyle(
                            fontFamily = SourceSans,
                            color = MaterialTheme.colorScheme.secondary,
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Medium
                        )
                    )

                    // Text 1
                    Text(
                        modifier = Modifier.padding(vertical = 10.dp),
                        text = home_currentlyReading_finishText1.stringResource(context = context),
                        style = TextStyle(
                            fontFamily = SourceSans,
                            color = MaterialTheme.colorScheme.secondary,
                            fontSize = 16.sp
                        )
                    )

                    // Text 2
                    Text(
                        modifier = Modifier.padding(vertical = 10.dp),
                        text = home_currentlyReading_finishText2.stringResource(context = context),
                        style = TextStyle(
                            fontFamily = SourceSans,
                            color = MaterialTheme.colorScheme.secondary,
                            fontSize = 16.sp
                        )
                    )

                    // Stars line
                    RatingBar(
                        modifier = modifier
                            .padding(top = 10.dp, bottom = 10.dp),
                        value = rating.toFloat(),
                        config = RatingBarConfig()
                            .activeColor(MaterialTheme.colorScheme.primary)
                            .inactiveColor(MaterialTheme.colorScheme.tertiary)
                            .stepSize(StepSize.HALF)
                            .numStars(5)
                            .size(40.dp)
                            .padding(8.dp)
                            .style(RatingBarStyle.Normal),

                        onValueChange = {
                            rating = it.toDouble()
                        },
                        onRatingChanged = {
                            println("Rating changed: $it")
                        }
                    )

                    // Text field
                    Box {
                        WideTextField(
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight(0.3f),
                            fieldValue = ratingText,
                            onFieldValueChange = {
                                ratingText = it
                            },
                            placeholderText = home_currentlyReading_rateBookHint.stringResource(
                                context = context
                            ),
                            onKeyboardActionClicked = {
                                ratingText = "${ratingText}\r\n"
                            })
                    }

                    // Yes - No Buttons
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.End,
                        verticalAlignment = Alignment.Bottom
                    ) {
                        CustomTextButton(
                            modifier = Modifier.padding(vertical = 10.dp),
                            onClick = { onShowDialogState.value = false },
                            color = MaterialTheme.colorScheme.error,
                            text = general_no.stringResource(context = context),
                            fontSize = 20.sp
                        )
                        CustomTextButton(
                            modifier = Modifier.padding(vertical = 10.dp),
                            onClick = {
                                if (rating <= 0) {
                                    emptyRatingWarning = true
                                } else {
                                    emptyRatingWarning = false
                                    viewModel.onEvent(
                                        CurrentlyReadingUIEvent.FinishBook(
                                            bookId = bookId,
                                            rate = rating.toFloat(),
                                            ratingText = ratingText,
                                        )
                                    )
                                    onShowDialogState.value = false
                                }
                            },
                            color = MaterialTheme.colorScheme.primary,
                            text = general_yes.stringResource(context = context),
                            fontSize = 20.sp
                        )
                    }
                }
            }
        }
    )
}

@Preview(group = "Done")
@Composable
fun BookProgressCardPreview() {
    BookProgressCard(
        picture = "",
        progress = 23.0,
        startedToRead = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
        title = "1984",
        authors = listOf("George Orwell"),
        finished = false,
        giveUp = false,
        finishedToRead = null,
    )
}

@Preview(group = "Done")
@Composable
fun LinealProgressIndicatorPreview() {
    LinealProgressIndicator(progress = 68.77)
}

@Preview(group = "Done")
@Composable
fun DialogUpdateProgressPreview() {
    UpdateProgressDialog(
        totalPages = 200,
        currentPage = null,
        currentPercentage = 100.0,
        onButtonClick = {},
        onChanged = { _,_ -> },
        logs = NapierLogs()
    )
}

fun String.isNumber(): Boolean {
    return this.toIntOrNull() != null
}

fun String.isPercentNumber(): Boolean {
    return this.toDoubleOrNull() != null
}

fun String.toInt(): Int? {
    return this.toIntOrNull()
}