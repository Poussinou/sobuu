package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ProfileIconButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.SearchBar
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.TopBarAppName
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme


@Composable
fun TopBarWithSearchAndProfile(
    modifier:Modifier = Modifier,
    navigateToProfileScreen: () -> Unit,
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearTextButtonClick: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
) {
    val backgroundColor = MaterialTheme.colorScheme.background
    val horizontalPaddingSeize = 10.dp

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor)
            .then(modifier),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopBarAppName()
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = horizontalPaddingSeize)
        ) {
            SearchBar(
                searchFieldValue = searchFieldValue,
                onSearchFieldValueChange = onSearchFieldValueChange,
                modifier = Modifier.weight(3f),
                onSearchButtonClick = onSearchButtonClick,
                clearText = clearTextButtonClick,
                onSearchFieldFocusChange = onSearchFieldFocusChange
            )

            ProfileIconButton(
                modifier = Modifier.weight(1f),
                navigateToProfileScreen = navigateToProfileScreen
            )
        }
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun TopBarWithSearchAndProfilePreview() {
    SobuuTheme {
        TopBarWithSearchAndProfile(
            navigateToProfileScreen = {},
            searchFieldValue = "",
            onSearchFieldValueChange = {},
            onSearchButtonClick = {},
            clearTextButtonClick = {},
            onSearchFieldFocusChange = {}
        )
    }
}