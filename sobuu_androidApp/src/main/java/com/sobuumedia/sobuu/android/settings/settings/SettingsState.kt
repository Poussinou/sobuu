package com.sobuumedia.sobuu.android.settings.settings

import com.sobuumedia.sobuu.features.settings.repository.ThemeOptions

data class SettingsState(
    val isLoading: Boolean = false,
    val error: String? = null,
    val displayThemeDialog: Boolean = false,
    val displayAnalyticsExplanationDialog: Boolean = false,
    val currentAnalyticsEnabled: Boolean = false,
    val currentTheme: ThemeOptions = ThemeOptions.LIGHT
)