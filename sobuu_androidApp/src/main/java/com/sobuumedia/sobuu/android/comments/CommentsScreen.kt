package com.sobuumedia.sobuu.android.comments

import android.graphics.Color.WHITE
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Message
import androidx.compose.material.icons.filled.Badge
import androidx.compose.material.icons.filled.EmojiSymbols
import androidx.compose.material.icons.filled.Group
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Sos
import androidx.compose.material.icons.filled.SportsKabaddi
import androidx.compose.material.icons.filled.Vaccines
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material.icons.filled.WarningAmber
import androidx.compose.material.icons.rounded.Warning
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.PopupProperties
import androidx.constraintlayout.compose.ConstraintLayout
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsOnPage
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsOnPercentage
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsOnPercentageOrPageMinus0
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsSpoilersWarning
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_commentsWarning
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_noComments
import com.sobuumedia.sobuu.SharedRes.strings.comments_comment_writeComment
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_emptyField
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_gettingComments
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidBookId
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidCommentId
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidPageNumber
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidPercentage
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_invalidUseOfPageAndPercentage
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_processingQuery
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_timeout
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_unauthorizedQuery
import com.sobuumedia.sobuu.SharedRes.strings.comments_error_unknown
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonOffensiveLanguage
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonOthers
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonPersonalDetails
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonRacism
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonSellingIllegalProducts
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonSpam
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reasonViolence
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reportComment
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reportError
import com.sobuumedia.sobuu.SharedRes.strings.comments_report_reportSuccess
import com.sobuumedia.sobuu.SharedRes.strings.general_ok
import com.sobuumedia.sobuu.android.custom_widgets.CustomTextButton
import com.sobuumedia.sobuu.android.custom_widgets.IconAndText
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.custom_widgets.TopAppBarWithMenu
import com.sobuumedia.sobuu.android.main.toStringDateWithDayAndTime
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.comments.remote.CommentError
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.ReportReason
import org.koin.androidx.compose.koinViewModel

private const val TAG = "CommentsScreen"

@Composable
fun CommentsScreen(
    navigateBackOrHome: () -> Unit,
    navigateToAddNewCommentScreen: (id: String, repliedCommentId: String) -> Unit,
    bookId: String,
    viewModel: CommentsViewModel = koinViewModel(),
    logs: SobuuLogs
) {
    val context = LocalContext.current
    val state = viewModel.state

    LaunchedEffect(viewModel, context) {
        logs.info(TAG, "---Running Launched Effect")
        viewModel.onEvent(
            CommentsUIEvents.DisplayCommentsScreen(
                bookId = bookId,
            )
        )
        viewModel.onEvent(
            CommentsUIEvents.GetBookData(
                bookId = bookId
            )
        )
    }

    Scaffold(
        topBar = {
            TopAppBarWithMenu(
                navigateBackOrHome = navigateBackOrHome,
                title = if (state.book?.bookProgress?.page != null) {
                    if ((state.book.bookProgress.page ?: 0) <= 0) {
                        comments_comment_commentsOnPercentageOrPageMinus0.stringResource(context = context)
                    } else {
                        "${comments_comment_commentsOnPage.stringResource(context = context)} ${state.book.bookProgress.page}"
                    }
                } else {
                    if ((state.book?.bookProgress?.percentage ?: 0.0) <= 0.0) {
                        comments_comment_commentsOnPercentageOrPageMinus0.stringResource(context = context)
                    } else {
                        "${comments_comment_commentsOnPercentage.stringResource(context = context)} ${state.book?.bookProgress?.percentage}%"
                    }
                },
                titleSize = 20.sp,
                listItems = listOf(
                    MenuItemData(
                        text = comments_comment_writeComment.stringResource(context = context),
                        icon = Icons.AutoMirrored.Filled.Message,
                        action = {
                            navigateToAddNewCommentScreen(
                                bookId,
                                "not_in_use", // Reply to comments is not supported yet,
                            )
                        }
                    )
                ),
                showCollapseMenu = false,
            )
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .background(MaterialTheme.colorScheme.background)
            ) {
                IconAndText(
                    modifier = Modifier.padding(horizontal = 20.dp),
                    text = comments_comment_commentsWarning.stringResource(context = context),
                    textMaxLines = 2,
                    fontSize = 12.sp,
                    icon = Icons.Rounded.Warning,
                    iconColor = MaterialTheme.colorScheme.error
                )

                when {
                    viewModel.state.isLoading -> {
                        LoadingBox()
                    }

                    viewModel.state.error != null -> {
                        val error: CommentError = viewModel.state.error!!
                        ErrorBox(error = error)
                    }

                    viewModel.state.listOFComments.isEmpty() -> {
                        NoComments()
                    }

                    else -> {
                        CommentsInPage(
                            page = state.book?.bookProgress?.page,
                            percentage = state.book?.bookProgress?.percentage,
                            comments = viewModel.state.listOFComments,
                            viewModel = viewModel
                        )
                    }
                }

                if (state.displayReportReasonsDialog.first) {
                    ReportReasonsDialog(
                        commentId = state.displayReportReasonsDialog.second,
                        viewModel = viewModel
                    )
                }

                if (state.displaySuccessDialog) {
                    ResultDialog(
                        text = comments_report_reportSuccess.stringResource(context = context),
                        viewModel
                    )
                } else if (state.displayErrorDialog) {
                    ResultDialog(text = getStringFromError(error = state.error), viewModel)
                }
            }
        }
    )
}

@Composable
fun CommentsInPage(
    modifier: Modifier = Modifier,
    page: Int? = null,
    percentage: Double? = null,
    comments: List<Comment>,
    viewModel: CommentsViewModel
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(
                top = 10.dp,
                bottom = 20.dp,
                start = 20.dp,
                end = 20.dp
            )
            .then(modifier)
            .border(
                width = 2.dp,
                color = MaterialTheme.colorScheme.secondary
            ),
    ) {
        val listState = rememberLazyListState()

        LazyColumn(
            state = listState,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            items(comments.size) { index ->
                val comment = comments[index]
                CommentItem(
                    commentId = comment.id,
                    username = comment.user.username,
                    date = comment.publishedDate.toStringDateWithDayAndTime(),
                    text = comment.text,
                    votes = comment.votesCounter.toInt(),
                    hasSpoilers = comment.hasSpoilers,
                    viewModel = viewModel,
                )
                Spacer(modifier = Modifier.padding(vertical = 10.dp))
                HorizontalDivider(
                    color = MaterialTheme.colorScheme.secondary.copy(alpha = 0.6f),
                    modifier = Modifier
                        .height(2.dp)
                        .width(60.dp)
                )
            }
        }

        Text(
            if (page != null) {
                "$page"
            } else {
                "$percentage%"
            },
            modifier = Modifier
                .align(if (page != null) Alignment.BottomEnd else Alignment.BottomStart)
                .padding(10.dp),
            style = TextStyle(
                fontSize = 20.sp,
                fontFamily = SourceSans,
                color = MaterialTheme.colorScheme.secondary
            )
        )
    }
}

@Composable
fun CommentItem(
    commentId: String,
    username: String,
    date: String,
    text: String,
    votes: Int,
    hasSpoilers: Boolean,
    viewModel: CommentsViewModel
) {
    val context = LocalContext.current
    val config = LocalConfiguration.current
    val screenWidth = config.screenWidthDp.dp
    var spoilers by remember { mutableStateOf(hasSpoilers) }
    var voteAvailable by remember { mutableStateOf(true) }
    var commentOptionMenu by remember { mutableStateOf(false) }

    val commentOptions: List<MenuItemData> = listOf(
        MenuItemData(
            text = comments_report_reportComment.stringResource(context = context),
            icon = Icons.Filled.WarningAmber,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.DisplayReportReasonsDialog(commentId = commentId)
                )
            }
        )
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.background)
    ) {

        BlurElement(
            modifier = Modifier.clickable(
                interactionSource = remember {
                    MutableInteractionSource()
                },
                indication = null
            ) {
                if (hasSpoilers) {
                    spoilers = !spoilers
                }
            },
            blur = spoilers,
            warningText = comments_comment_commentsSpoilersWarning.stringResource(context = context)
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 10.dp)
                    .background(MaterialTheme.colorScheme.background),
            ) {
                val (commentUsername, commentDate, commentText,
                    commentVotes, commentSettings) = createRefs()
                val nameGuideline = createGuidelineFromStart(0.6f)

                Text(
                    text = username,
                    style = TextStyle(
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.secondary,
                        fontFamily = SourceSans
                    ),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier
                        .width(screenWidth * 60 / 100)
                        .constrainAs(commentUsername) {
                            top.linkTo(parent.top, margin = 10.dp)
                            linkTo(parent.start, nameGuideline, bias = 0f, startMargin = 10.dp)
                        },
                )

                Text(
                    text = date,
                    style = TextStyle(
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.secondary,
                        fontFamily = SourceSans
                    ),
                    textAlign = TextAlign.Start,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.constrainAs(commentDate) {
                        top.linkTo(parent.top, margin = 10.dp)
                        end.linkTo(parent.end, margin = 10.dp)
                    },
                )

                Text(
                    text = text,
                    style = TextStyle(
                        fontSize = 18.sp,
                        color = MaterialTheme.colorScheme.secondary,
                        fontFamily = SourceSans
                    ),
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.constrainAs(commentText) {
                        top.linkTo(commentUsername.bottom, margin = 20.dp)
                        linkTo(
                            parent.start,
                            parent.end,
                            bias = 0f,
                            startMargin = 10.dp,
                            endMargin = 20.dp
                        )
                    },
                )

                /*  The votes is not totally secure, because there are not implementations for the user to
                don't vote twice. Control this may need a bit more of code. Think about it later.
                                Row(
                                    modifier = Modifier
                                        .width(screenWidth * 50 / 100)
                                        .height(Icons.Filled.ThumbUpAlt.defaultHeight)
                                        .constrainAs(commentVotes) {
                                            top.linkTo(commentText.bottom, margin = 20.dp)
                                            bottom.linkTo(parent.bottom, margin = 10.dp)
                                            start.linkTo(parent.start)
                                        },
                                ) {
                                    IconButton(
                                        onClick = {
                                            viewModel.onEvent(
                                                CommentsUIEvents.IncreaseVote(commentId = commentId)
                                            )
                                            voteAvailable = false
                                        },
                                        enabled = voteAvailable
                                    ) {
                                        Icon(
                                            imageVector = Icons.Filled.ThumbUpAlt,
                                            contentDescription = null,
                                            tint = MaterialTheme.colorScheme.secondary,
                                        )
                                    }
                                    Text(
                                        text = if (votes <= 999) {
                                            "$votes"
                                        } else {
                                            stringResource(id = R.string.more_than_999)
                                        },
                                        style = TextStyle(
                                            fontSize = 16.sp,
                                            color = MaterialTheme.colorScheme.secondary,
                                            fontFamily = SourceSans
                                        ),
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis,
                                        modifier = Modifier.padding(start = 5.dp, end = 5.dp),
                                    )
                                    IconButton(
                                        onClick = {
                                            viewModel.onEvent(
                                                CommentsUIEvents.DecreaseVote(commentId = commentId)
                                            )
                                            voteAvailable = false
                                        },
                                        enabled = voteAvailable
                                    ) {
                                        Icon(
                                            imageVector = Icons.Filled.ThumbDownAlt,
                                            contentDescription = null,
                                            tint = MaterialTheme.colorScheme.secondary,
                                        )
                                    }
                                }
                */
                IconButton(
                    onClick = {
                        commentOptionMenu = true
                    },
                    modifier = Modifier.constrainAs(commentSettings) {
                        top.linkTo(commentText.bottom)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end)
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.MoreVert,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary,
                    )
                }
            }
        }
        if (commentOptionMenu) {
            DropdownMenu(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .width(width = 200.dp),
                expanded = commentOptionMenu,
                onDismissRequest = {
                    commentOptionMenu = false
                },
                offset = DpOffset(x = (-102).dp, y = (-64).dp),
                properties = PopupProperties()
            ) {
                if (commentOptionMenu) {
                    commentOptions.forEach { menuItemData ->
                        if (!menuItemData.displayIt) {
                            return@forEach
                        }
                        DropdownMenuItem(
                            onClick = {
                                menuItemData.action()
                                commentOptionMenu = false
                            },
                            enabled = true,
                            text = {
                                Text(
                                    text = menuItemData.text,
                                    fontWeight = FontWeight.Normal,
                                    fontSize = 14.sp,
                                    color = MaterialTheme.colorScheme.secondary
                                )
                            },
                            leadingIcon = {
                                Icon(
                                    imageVector = menuItemData.icon,
                                    contentDescription = menuItemData.text,
                                    tint = MaterialTheme.colorScheme.secondary,
                                )
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun BlurElement(
    modifier: Modifier = Modifier,
    blur: Boolean,
    warningText: String,
    warningTextColor: Color = MaterialTheme.colorScheme.secondary,
    element: @Composable () -> Unit
) {
    val blurRadius = if (blur) 10.dp else 0.dp

    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .blur(blurRadius),
            contentAlignment = Alignment.Center
        ) {
            element()
        }

        if (blur) {
            Text(
                modifier = Modifier.padding(10.dp),
                text = warningText,
                style = Typography.bodyLarge.copy(color = warningTextColor, fontWeight = FontWeight.Medium),
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun ResultDialog(text: String, viewModel: CommentsViewModel) {
    AlertDialog(
        modifier = Modifier,
        onDismissRequest = {
            viewModel.onEvent(
                CommentsUIEvents.CloseResultDialog
            )
        },
        confirmButton = {},
        dismissButton = {
            CustomTextButton(
                onClick = {
                    viewModel.onEvent(CommentsUIEvents.CloseResultDialog)
                },
                color = MaterialTheme.colorScheme.error,
                text = general_ok.stringResource(context = LocalContext.current)
            )
        },
        containerColor = MaterialTheme.colorScheme.background,
        textContentColor = MaterialTheme.colorScheme.secondary,
        text = { Text(text = text) },
        shape = RoundedCornerShape(10.dp),
        properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = false,
        ),
    )
}

@Composable
fun ReportReasonsDialog(commentId: String, viewModel: CommentsViewModel) {
    val reportReasonsList = getReasonsList(commentId = commentId, viewModel = viewModel)

    Dialog(
        onDismissRequest = {
            viewModel.onEvent(
                CommentsUIEvents.DisplayReportReasonsDialog(commentId = commentId)
            )
        },
        content = {
            Column {
                LazyColumn(
                    modifier = Modifier
                        .padding(10.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(MaterialTheme.colorScheme.background),
                    content = {
                        this.items(reportReasonsList.size) {
                            val menuItemData = reportReasonsList[it]
                            if (!menuItemData.displayIt) {
                                return@items
                            }
                            DropdownMenuItem(
                                onClick = {
                                    menuItemData.action()
//                commentOptionMenu = false
                                },
                                enabled = true,
                                text = {
                                    Text(
                                        text = menuItemData.text,
                                        fontWeight = FontWeight.Normal,
                                        fontSize = 14.sp,
                                        color = MaterialTheme.colorScheme.secondary
                                    )
                                },
                                leadingIcon = {
                                    Icon(
                                        imageVector = menuItemData.icon,
                                        contentDescription = menuItemData.text,
                                        tint = MaterialTheme.colorScheme.secondary,
                                    )
                                }
                            )
                        }
                    }
                )
            }
        },
        properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = true,
        ),
    )
}

@Composable
private fun getReasonsList(commentId: String, viewModel: CommentsViewModel): List<MenuItemData> {
    val context = LocalContext.current
    return listOf(
        MenuItemData(
            text = comments_report_reasonOffensiveLanguage.stringResource(context = context),
            icon = Icons.Filled.EmojiSymbols,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(
                        commentId = commentId,
                        ReportReason.UseOfOffensiveLanguage
                    )
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonSpam.stringResource(context = context),
            icon = Icons.Filled.Warning,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(commentId = commentId, ReportReason.Spam)
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonRacism.stringResource(context = context),
            icon = Icons.Filled.Group,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(commentId = commentId, ReportReason.Racism)
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonViolence.stringResource(context = context),
            icon = Icons.Filled.SportsKabaddi,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(commentId = commentId, ReportReason.Violence)
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonPersonalDetails.stringResource(context = context),
            icon = Icons.Filled.Badge,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(
                        commentId = commentId,
                        ReportReason.PersonalDetails
                    )
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonSellingIllegalProducts.stringResource(context = context),
            icon = Icons.Filled.Vaccines,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(
                        commentId = commentId,
                        ReportReason.SellingIllegalProducts
                    )
                )
            }
        ),
        MenuItemData(
            text = comments_report_reasonOthers.stringResource(context = context),
            icon = Icons.Filled.Sos,
            action = {
                viewModel.onEvent(
                    CommentsUIEvents.ReportComment(commentId = commentId, ReportReason.Others)
                )
            }
        )
    )
}

@Composable
fun getStringFromError(error: CommentError?): String {
    val context = LocalContext.current
    return when (error) {
        is CommentError.EmptyFieldError -> comments_error_emptyField.stringResource(context = context)
        is CommentError.InvalidBookIdError -> comments_error_invalidBookId.stringResource(context = context)
        is CommentError.InvalidCommentIdError -> comments_error_invalidCommentId.stringResource(
            context = context
        )

        is CommentError.InvalidDoublePageAndPercentageNumberError -> comments_error_invalidUseOfPageAndPercentage.stringResource(
            context = context
        )

        is CommentError.InvalidPageNumberError -> comments_error_invalidPageNumber.stringResource(
            context = context
        )

        is CommentError.InvalidPercentageNumberError -> comments_error_invalidPercentage.stringResource(
            context = context
        )

        is CommentError.InvalidSessionTokenError -> comments_error_invalidSessionToken.stringResource(
            context = context
        )

        is CommentError.ProcessingQueryError -> comments_error_processingQuery.stringResource(
            context = context
        )

        is CommentError.GetPageOrPercentageCommentsError -> comments_error_gettingComments.stringResource(
            context = context
        )

        is CommentError.SaveNewCommentError -> comments_error_gettingComments.stringResource(context = context)
        is CommentError.TimeOutError -> comments_error_timeout.stringResource(context = context)
        is CommentError.UnauthorizedQueryError -> comments_error_unauthorizedQuery.stringResource(
            context = context
        )

        is CommentError.ReportError -> comments_report_reportError.stringResource(context = context)
        else -> comments_error_unknown.stringResource(context = context)
    }
}

@Composable
fun LoadingBox() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
    }
}

@Composable
fun ErrorBox(error: CommentError) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 18.dp)
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        Text(
            text = getStringFromError(error = error),
            style = TextStyle(
                color = MaterialTheme.colorScheme.error,
                fontSize = 18.sp
            )
        )
    }
}

@Composable
fun NoComments() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        Text(
            text = comments_comment_noComments.stringResource(context = LocalContext.current),
            style = TextStyle(
                color = MaterialTheme.colorScheme.secondary,
                fontSize = 18.sp
            )
        )
    }
}

@Preview
@Composable
fun CommentsInPagePreview() {
    CommentsInPage(
        page = 33,
        comments = emptyList(),
        viewModel = koinViewModel()
    )
}

@Preview
@Composable
fun CommentItemPreview() {
    CommentItem(
        commentId = "woijcew09jde",
        username = "Ayoze Pérez Rodríguez <De las nieves extranjeras>",
        date = "Sep 17, 2022, 21:10",
        text = "It's good this part",
        votes = 220000,
        hasSpoilers = true,
        viewModel = koinViewModel()
    )
}

@Preview(showSystemUi = true, showBackground = true, backgroundColor = WHITE.toLong())
@Composable
fun BlurryElementPreview() {
    BlurElement(
        blur = true,
        warningText = "This contains spoilers"
    ) {
        Column {
            Text(
                text = "Test",
                style = TextStyle(
                    fontSize = 160.sp,
                    color = Color.Cyan
                )
            )
        }
    }
}