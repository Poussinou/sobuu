package com.sobuumedia.sobuu.android.authentication.splash

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class SplashViewModel constructor(
    private val auth: IAuthenticationRepository,
    private val connectivityObserver: ConnectivityObserver
): ViewModel() {

    var state by mutableStateOf(SplashState())

    private val resultChannel = Channel<AuthenticationResult<Unit>>()
    val authResult = resultChannel.receiveAsFlow()

    init {
        checkConnectivity()
        authentication()
    }

    fun handleError(error: AuthenticationError?) {
        state = state.copy(error = error)
    }

    private fun authentication() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = auth.authenticate()
            resultChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun checkConnectivity() {
        viewModelScope.launch {
            val isConnected = connectivityObserver.isConnected()

            state = state.copy(
                disconnected = !isConnected
            )
        }
    }
}