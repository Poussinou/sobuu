package com.sobuumedia.sobuu.android.book

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import kotlinx.coroutines.launch
import org.acra.ACRA

class BookViewModel(
    private val bookRepo: IBookRepository,
//    private val shelfRepo: IShelfRepository,
    private val logs: SobuuLogs
) : ViewModel() {

    var state by mutableStateOf(BookState())

    fun onEvent(event: BookUIEvent) {
        when(event) {
            is BookUIEvent.AddBookToShelf -> addBookToShelf(event.bookId, event.shelfId)
            is BookUIEvent.DisplayUserShelves -> TODO()
            is BookUIEvent.StartToReadBook -> startToReadBook(
                bookId = event.bookId,
                bookTitle = event.bookTitle,
                isbn = event.bookISBN
            )
            is BookUIEvent.CancelAllErrors -> {
                state = state.copy(error = null)
            }
            is BookUIEvent.StartScreen -> getBookByIdFromDB(event.bookId)
            is BookUIEvent.ReportMissingDataInBook -> reportMissingData(
                bookId = event.bookId,
                bookTitle = event.bookTitle,
                isbn = event.bookISBN
            )
            is BookUIEvent.CloseReportedMissingDataDialog -> {
                state = state.copy(displayReportedMissingDataMessage = false)
            }
        }
    }

    fun handleError(error: BookError?) {
        if(error is BookError.UnknownError) {
            ACRA.errorReporter.putCustomData("Handle error in BookViewModel", error.message ?: "Unknown error with empty message")
        }
        state = state.copy(error = error)
    }

    private fun startToReadBook(bookId: String, bookTitle: String, isbn: String?) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.setBookCurrentlyReading(bookId = bookId, title = bookTitle, isbn = isbn)

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(
                    isLoading = false,
                    displayStartedToRead = true,
                    bookStartedReadingTitle = bookTitle,
                )
            }
        }
    }

    private fun reportMissingData(bookId: String, bookTitle: String, isbn: String?) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = bookRepo.reportMissingDataInBook(bookId = bookId, title = bookTitle, isbn = isbn)

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(isLoading = false, displayReportedMissingDataMessage = true)
            }
        }
    }

    private fun addBookToShelf(bookId: String, shelfId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
//            val result = shelfRepo.addBookToShelf(shelfId = shelfId, bookId = bookId)

//            if(result.error != null) {
//                handleError(result.error)
//            } else {
//                handleError(null)
//            }

            state = state.copy(isLoading = false)
        }
    }

    private fun getBookByIdFromDB(bookId: String) {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result  = bookRepo.getBookByIdFromDB(bookId)

            if(result.error != null) {
                handleError(result.error)
            } else {
                handleError(null)
                state = state.copy(book = result.data)
            }
            state = state.copy(isLoading = false)
        }
    }
}