package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CompleteRoundedOutlinedTextFieldWithIcon(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    onKeyboardActionClicked: () -> Unit,
    isError: Boolean = false,
    contentDescription: String = "",
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    val roundedCorner = 10.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(roundedCorner),
                    color = backgroundColor
                )
                .semantics {
                    this.contentDescription = contentDescription
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Go),
            keyboardActions = KeyboardActions(
                onNext = {
                    onKeyboardActionClicked()
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            shape = RoundedCornerShape(roundedCorner),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Mail,
                    contentDescription = Icons.Filled.Mail.name,
                    tint = placeholderColor,
                )
            }
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlinedTextFieldWithIconPreview() {
    CompleteRoundedOutlinedTextFieldWithIcon(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Password",
        onKeyboardActionClicked = {}
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlinedTextFieldWithIconErrorPreview() {
    CompleteRoundedOutlinedTextFieldWithIcon(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Password",
        onKeyboardActionClicked = {},
        isError = true
    )
}