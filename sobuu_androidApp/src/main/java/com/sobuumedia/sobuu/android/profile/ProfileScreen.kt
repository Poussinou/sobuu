package com.sobuumedia.sobuu.android.profile

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Logout
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_processingQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unauthorizedQuery
import com.sobuumedia.sobuu.SharedRes.strings.general_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.profile_error_invalidId
import com.sobuumedia.sobuu.SharedRes.strings.profile_error_user
import com.sobuumedia.sobuu.SharedRes.strings.profile_main_logout
import com.sobuumedia.sobuu.SharedRes.strings.profile_main_title
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_title
import com.sobuumedia.sobuu.android.custom_widgets.MenuItemData
import com.sobuumedia.sobuu.android.custom_widgets.TopAppBarWithMenu
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import com.sobuumedia.sobuu.models.bo_models.Profile
import org.koin.androidx.compose.koinViewModel

@Composable
fun ProfileScreen(
    navigateBackOrHome: () -> Unit,
    navigateToLoginScreen: () -> Unit,
    navigateToSettingsScreen: () -> Unit,
    viewModel: ProfileViewModel = koinViewModel()
) {
    val context = LocalContext.current
    val state = viewModel.state

    if(state.logout) {
        navigateToLoginScreen()
    }

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                TopAppBarWithMenu(
                    navigateBackOrHome = navigateBackOrHome,
                    title = profile_main_title.stringResource(context = context),
                    titleSize = 24.sp,
                    listItems = listOf(
                        MenuItemData(
                            text = settings_main_title.stringResource(context = context),
                            icon = Icons.Filled.Settings,
                            action = { navigateToSettingsScreen() }
                        ),
                        MenuItemData(
                            text = profile_main_logout.stringResource(context = context),
                            icon = Icons.AutoMirrored.Filled.Logout,
                            action = {
                                viewModel.onEvent(ProfileUIEvent.logout)
                            },
                        ),
                    ),
                )
            },
            content = { padding ->
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(padding),
                ) {
                    state.profileInfo?.let {
                        ProfileCard(
                            profile = it
                        )
                    }

                    if (state.isLoading) {
                        ProfileLoading()
                    }

                    state.error?.let {
                        Text(
                            text = getStringFromProfileError(error = it),
                            color = Color.Red,
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }
        )
    }
}

@Composable
fun ProfileCard(
    profile: Profile,
    modifier: Modifier = Modifier
) {
    Text(
        modifier = modifier
            .padding(start = 50.dp, top = 20.dp, bottom = 30.dp)
            .then(modifier),
        text = "${profile.firstName} ${profile.lastName}",
        style = TextStyle(
            fontSize = 25.sp,
            fontFamily = SourceSans,
            color = MaterialTheme.colorScheme.secondary
        )
    )
}

@Composable
fun ProfileLoading(
    modifier: Modifier = Modifier
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        CircularProgressIndicator(modifier = modifier)
    }
}

@Composable
private fun getStringFromProfileError(error: ProfileError?): String {
    val context = LocalContext.current
    if(error == null) return ""
    return when (error) {
        ProfileError.InvalidProfileIdError -> profile_error_invalidId.stringResource(context = context)
        ProfileError.InvalidSessionTokenError -> general_errors_invalidSessionToken.stringResource(context = context)
        ProfileError.ProcessingQueryError -> general_errors_processingQuery.stringResource(context = context)
        ProfileError.TimeOutError -> general_errors_timeout.stringResource(context = context)
        ProfileError.UnauthorizedQueryError -> general_errors_unauthorizedQuery.stringResource(context = context)
        ProfileError.GetUserProfileError -> profile_error_user.stringResource(context = context)
        else -> general_errors_unknown.stringResource(context = context)
    }
}