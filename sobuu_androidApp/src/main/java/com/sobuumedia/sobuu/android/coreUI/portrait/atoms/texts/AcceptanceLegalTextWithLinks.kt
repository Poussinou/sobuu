package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts

import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_privacyConfirmation1
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_privacyConfirmation2
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun AcceptanceLegalTextWithLinks(
    modifier: Modifier = Modifier,
    navigateToTerms: () -> Unit,
    navigateToPrivacy: () -> Unit
) {
    val context = LocalContext.current
    val linkColor = MaterialTheme.colorScheme.error
    val textColor = MaterialTheme.colorScheme.background

    val annotatedString = buildAnnotatedString {
        append("${authorization_auth_privacyConfirmation1.stringResource(context = context)} ")

        pushStringAnnotation(
            tag = "terms",
            annotation = "terms and conditions"
        )
        withStyle(style = SpanStyle(color = linkColor)) {
            append(settings_main_terms.stringResource(context = context))
        }
        pop()

        append(" ${authorization_auth_privacyConfirmation2.stringResource(context = context)} ")

        pushStringAnnotation(
            tag = "privacy",
            annotation = "privacy policy"
        )

        withStyle(style = SpanStyle(color = linkColor)) {
            append(settings_main_privacy.stringResource(context = context))
        }

        pop()

        append(" ${SharedRes.strings.authorization_auth_privacyConfirmation3.stringResource(context = context)}")
    }

    ClickableText(
        text = annotatedString,
        modifier = Modifier
            .semantics { this.contentDescription = annotatedString.text }
            .then(modifier),
        style = Typography.bodySmall.copy(color = textColor),
        onClick = { offset ->

            annotatedString.getStringAnnotations(
                tag = "terms",
                start = offset,
                end = offset
            ).firstOrNull()?.let {
                navigateToTerms()
            }

            annotatedString.getStringAnnotations(
                tag = "privacy",
                start = offset,
                end = offset
            ).firstOrNull()?.let {
                navigateToPrivacy()
            }
        })
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun AcceptanceLegalTextWithLinksPreview() {
    AcceptanceLegalTextWithLinks(
        navigateToTerms = {},
        navigateToPrivacy = {}
    )
}