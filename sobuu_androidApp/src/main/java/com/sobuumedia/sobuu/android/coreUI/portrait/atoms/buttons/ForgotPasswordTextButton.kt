package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_forgotPassword
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun ForgotPasswordTextButton(onClick: () -> Unit) {
    val elementColor = MaterialTheme.colorScheme.secondary

    TextButton(
        modifier = Modifier
            .fillMaxWidth()
            .semantics { this.contentDescription = authorization_auth_forgotPassword.stringResource() },
        onClick = onClick
    ) {
        Text(
            text = authorization_auth_forgotPassword.stringResource(context = LocalContext.current),
            style = Typography.bodyMedium.copy(color = elementColor),
            textDecoration = TextDecoration.Underline,
            textAlign = TextAlign.Center,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun ForgotPasswordTextButtonPreview() {
    ForgotPasswordTextButton(
       onClick = {}
    )
}