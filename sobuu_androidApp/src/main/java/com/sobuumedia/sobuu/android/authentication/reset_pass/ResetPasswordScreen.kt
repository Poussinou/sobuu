package com.sobuumedia.sobuu.android.authentication.reset_pass

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyResetPassEmail
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_resettingPassword
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_wrongEmailFormat
import com.sobuumedia.sobuu.android.authentication.EmailType
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.authentication.ResetPasswordViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SobuuTopAppBar
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication.ResetPasswordView
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import org.koin.androidx.compose.koinViewModel

@Composable
fun ResetPasswordScreen(
    navigateToSentEmailScreen: (type: EmailType) -> Unit,
    navigateBack: () -> Unit,
    viewModel: ResetPassViewModel = koinViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
    val configuration = LocalConfiguration.current

    LaunchedEffect(viewModel, context) {
        viewModel.authResult.collect { result ->
            if (result.error != null) {
                viewModel.handleError(result.error)
            } else {
                navigateToSentEmailScreen(EmailType.RESET_PASSWORD)
            }
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            SobuuTopAppBar(
                navigateBack = { navigateBack() },
                title = SharedRes.strings.authorization_auth_createAccount.stringResource(context = context),
                backgroundColor = MaterialTheme.colorScheme.primary,
                titleColor = MaterialTheme.colorScheme.background,
                iconColor = MaterialTheme.colorScheme.background,
            )
        },
        content = {
            Box(modifier = Modifier.padding(it)) {
                when (configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        ResetPasswordViewLandscape(
                            emailFieldValue = state.forgotEmail,
                            onEmailValueChange = { email ->
                                viewModel.onEvent(ResetPassUIEvent.ForgotPasswordEmailChanged(email))
                            },
                            navigateToSentEmailScreen = {
                                viewModel.onEvent(ResetPassUIEvent.resetPassword)
                            },
                            errorText = if(state.error != null) getStringFromError(state.error) else null,
                            isLoading = state.isLoading
                        )
                    }

                    else -> {
                        ResetPasswordView(
                            emailFieldValue = state.forgotEmail,
                            onEmailValueChange = { email ->
                                viewModel.onEvent(ResetPassUIEvent.ForgotPasswordEmailChanged(email))
                            },
                            navigateToSentEmailScreen = {
                                viewModel.onEvent(ResetPassUIEvent.resetPassword)
                            },
                            errorText = if(state.error != null) getStringFromError(state.error) else null,
                            isLoading = state.isLoading
                        )
                    }
                }
            }
        }
    )
}

fun getStringFromError(error: AuthenticationError?): String {
    return when (error) {
        is AuthenticationError.EmptyCredentialsError -> {
            authorization_errors_emptyResetPassEmail.stringResource()
        }
        is AuthenticationError.TimeOutError -> {
            authorization_errors_timeout.stringResource()
        }
        is AuthenticationError.WrongEmailFormatError -> {
            authorization_errors_wrongEmailFormat.stringResource()
        }
        is AuthenticationError.ResetPassword -> {
            authorization_errors_resettingPassword.stringResource()
        }
        else -> {
            authorization_errors_unknown.stringResource()
        }
    }
}