package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.PlaceholderTextLarge
import com.sobuumedia.sobuu.android.settings.theme.Typography

@Composable
fun CompleteRoundedOutlinedTextArea(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    contentDescription: String = "",
    isError: Boolean = false,
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    var offset by remember { mutableFloatStateOf(0f) }

    val backgroundColor = MaterialTheme.colorScheme.background
    val elementColor = MaterialTheme.colorScheme.secondary
    val errorColor = MaterialTheme.colorScheme.error
    val placeholderColor = if(isError) errorColor else MaterialTheme.colorScheme.tertiary
    val roundedCorner = 10.dp

    ProvideTextStyle(
        value = Typography.bodyLarge.copy(color = elementColor)
    ) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .scrollable(
                    orientation = Orientation.Horizontal,
                    state = rememberScrollableState { delta ->
                        offset += delta
                        delta
                    }
                )
                .semantics { this.contentDescription = contentDescription }
                .background(
                    shape = RoundedCornerShape(roundedCorner),
                    color = backgroundColor,
                ),
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = elementColor,
                unfocusedBorderColor = elementColor,
                errorBorderColor = errorColor,
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = onKeyboardIme,
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            textStyle = Typography.bodyLarge.copy(color = elementColor),
            placeholder = { PlaceholderTextLarge(placeholderText = placeholderText, isError = isError) },
            shape = RoundedCornerShape(roundedCorner),
            singleLine = false,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlinedTextAreaPreview() {
    CompleteRoundedOutlinedTextArea(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Long text description",
        onKeyboardActionClicked = {}
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun CompleteRoundedOutlinedTextAreaErrorPreview() {
    CompleteRoundedOutlinedTextArea(
        fieldValue = "",
        onFieldValueChange = {},
        placeholderText = "Long text description",
        onKeyboardActionClicked = {},
        isError = true
    )
}