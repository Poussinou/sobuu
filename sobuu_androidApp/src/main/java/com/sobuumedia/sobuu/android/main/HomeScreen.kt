package com.sobuumedia.sobuu.android.main

import android.app.Activity
import android.content.res.Configuration
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawing
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.intl.Locale
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.general_today
import com.sobuumedia.sobuu.SharedRes.strings.home_error_getFinishedBooksError
import com.sobuumedia.sobuu.SharedRes.strings.home_error_getGiveUpBooksError
import com.sobuumedia.sobuu.SharedRes.strings.home_error_getReadingBooksError
import com.sobuumedia.sobuu.analytics.events.SearchBookEvent
import com.sobuumedia.sobuu.android.authentication.login.LoginViewModel
import com.sobuumedia.sobuu.android.coreUI.landscape.molecules.TopBarWithSearchAndProfileLandscape
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.home.HomeViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.TopBarWithSearchAndProfile
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home.HomeView
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.core.SobuuLogs
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.book.remote.BookError
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import org.koin.androidx.compose.koinViewModel
import java.time.format.DateTimeFormatter

private const val TAG = "HomeScreen"

@Composable
fun HomeScreen(
    navigateToLoginScreen: () -> Unit,
    navigateToProfileScreen: () -> Unit,
    navigateToCurrentlyReadingBook: (String) -> Unit,
    navigateToBookScreen: (String) -> Unit,
    navigateToAddBookScreen: (Boolean) -> Unit,
    loginViewModel: LoginViewModel = koinViewModel(),
    searchViewModel: SearchViewModel = koinViewModel(),
    homeViewModel: HomeViewModel = koinViewModel(),
    logs: SobuuLogs
) {
    val context = LocalContext.current
    val searchState = searchViewModel.state
    val activity = (LocalContext.current as? Activity)
    val focus = LocalFocusManager.current
    val configuration = LocalConfiguration.current
    val windowInsets = WindowInsets.safeDrawing

    LaunchedEffect(loginViewModel, context) {
        loginViewModel.authResult.collect { result ->
            if (result == AuthenticationResult.LoggedOut<Unit>()) {
                navigateToLoginScreen()
            }
        }
    }

    LaunchedEffect(homeViewModel, context) {
        homeViewModel.onEvent(HomeUIEvent.DisplayCurrentlyReading)
    }

    SobuuTheme(useDarkTheme = false) {
        BackPressHandler(onBackPressed = {
            if (!homeViewModel.state.isOnSearch) {
                activity?.finish()
            } else {
                if (searchState.searchTerm.isNotBlank()) {
                    searchViewModel.onEvent(
                        SearchUIEvent.cleanSearchTerm
                    )
                } else {
                    focus.clearFocus()
                }
            }
        })

        Scaffold(
            modifier = Modifier.windowInsetsPadding(windowInsets),
            topBar = {
                when(configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        TopBarWithSearchAndProfileLandscape(
                            navigateToProfileScreen = { navigateToProfileScreen() },
                            onSearchFieldValueChange = {
                                searchViewModel.onEvent(
                                    SearchUIEvent.SearchTermChanged(it, Locale.current.language)
                                )
                            },
                            searchFieldValue = searchState.searchTerm,
                            onSearchButtonClick = {
                                searchViewModel.onEvent(SearchUIEvent.searchTerm)
                                searchViewModel.onAnalyticsEvent(
                                    SearchBookEvent(
                                        name = "Searching",
                                        otherVariables = mutableMapOf<String, String>().plus("Term" to searchState.searchTerm)
                                    )
                                )
                            },
                            clearTextButtonClick = { searchViewModel.onEvent(SearchUIEvent.cleanSearchTerm) },
                            onSearchFieldFocusChange = {
                                homeViewModel.onEvent(
                                    HomeUIEvent.DisplaySearch(it, searchState.searchTerm.isEmpty())
                                )
                            },
                        )
                    }
                    else -> {
                        TopBarWithSearchAndProfile(
                            navigateToProfileScreen = { navigateToProfileScreen() },
                            onSearchFieldValueChange = {
                                searchViewModel.onEvent(
                                    SearchUIEvent.SearchTermChanged(it, Locale.current.language)
                                )
                            },
                            searchFieldValue = searchState.searchTerm,
                            onSearchButtonClick = {
                                searchViewModel.onEvent(SearchUIEvent.searchTerm)
                                searchViewModel.onAnalyticsEvent(
                                    SearchBookEvent(
                                        name = "Searching",
                                        otherVariables = mutableMapOf<String, String>().plus("Term" to searchState.searchTerm)
                                    )
                                )
                            },
                            clearTextButtonClick = { searchViewModel.onEvent(SearchUIEvent.cleanSearchTerm) },
                            onSearchFieldFocusChange = {
                                homeViewModel.onEvent(
                                    HomeUIEvent.DisplaySearch(it, searchState.searchTerm.isEmpty())
                                )
                            },
                        )
                    }
                }

            },
            content = { padding ->
                logs.info(TAG, "Currently reading books: ${homeViewModel.currentlyReadingBooksList?.size}")
                if((homeViewModel.currentlyReadingBooksList?.size ?: 0) > 0) {
                    logs.info(
                        TAG,
                        "Currently reading books gave up?: ${
                            homeViewModel.currentlyReadingBooksList?.get(0)?.bookProgress?.giveUp
                        }"
                    )
                }
                when(configuration.orientation) {
                    Configuration.ORIENTATION_LANDSCAPE -> {
                        if (homeViewModel.state.isOnSearch) {
                            SearchListScreen(
                                modifier = Modifier.padding(padding),
                                navigateToBookScreen = { navigateToBookScreen(it) },
                                navigateToAddBookScreen = { navigateToAddBookScreen(it) }
                            )
                        } else {
                            HomeViewLandscape(
                                modifier = Modifier.padding(padding),
                                navigateToCurrentlyReadingBook = { navigateToCurrentlyReadingBook(it) },
                                currentSection = homeViewModel.currentSection,
                                currentlyReadingBookList = homeViewModel.currentlyReadingBooksList,
                                gaveUpBookList = homeViewModel.giveUpBooksList,
                                finishedBookList = homeViewModel.finishedBooksList,
                                displayCurrentlyReading = { homeViewModel.onEvent(HomeUIEvent.DisplayCurrentlyReading) },
                                displayFinished = { homeViewModel.onEvent(HomeUIEvent.DisplayFinished) },
                                displayGaveUp = { homeViewModel.onEvent(HomeUIEvent.DisplayGiveUp) },
                                isLoading = homeViewModel.state.isLoading,
                                errorText = getErrorString(homeViewModel.state.error)
                            )
                        }
                    }
                    else -> {
                        if (homeViewModel.state.isOnSearch) {
                            SearchListScreen(
                                modifier = Modifier.padding(padding),
                                navigateToBookScreen = { navigateToBookScreen(it) },
                                navigateToAddBookScreen = { navigateToAddBookScreen(it) }
                            )
                        } else {
                            HomeView(
                                modifier = Modifier.padding(padding),
                                navigateToCurrentlyReadingBook = { navigateToCurrentlyReadingBook(it) },
                                currentSection = homeViewModel.currentSection,
                                currentlyReadingBookList = homeViewModel.currentlyReadingBooksList,
                                gaveUpBookList = homeViewModel.giveUpBooksList,
                                finishedBookList = homeViewModel.finishedBooksList,
                                displayCurrentlyReading = { homeViewModel.onEvent(HomeUIEvent.DisplayCurrentlyReading) },
                                displayFinished = { homeViewModel.onEvent(HomeUIEvent.DisplayFinished) },
                                displayGaveUp = { homeViewModel.onEvent(HomeUIEvent.DisplayGiveUp) },
                                isLoading = homeViewModel.state.isLoading,
                                errorText = getErrorString(homeViewModel.state.error)
                            )
                        }
                    }
                }
            },
            // TODO Uncomment when there are more screens to navigate to
            //bottomBar = { BottomMenuBar() }
        )
    }
}

@Composable
fun BackPressHandler(
    backPressedDispatcher: OnBackPressedDispatcher? =
        LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher,
    onBackPressed: () -> Unit
) {
    val currentOnBackPressed by rememberUpdatedState(newValue = onBackPressed)

    val backCallback = remember {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                currentOnBackPressed()
            }
        }
    }

    DisposableEffect(key1 = backPressedDispatcher) {
        backPressedDispatcher?.addCallback(backCallback)

        onDispose {
            backCallback.remove()
        }
    }
}

private fun getErrorString(resultError: BookError?): String? {
    if(resultError == null) {
        return null
    }
    return when (resultError) {
        is BookError.TimeOutError -> {
            authorization_errors_timeout.stringResource()
        }
        is BookError.GetCurrentReadingBooksError -> {
            home_error_getReadingBooksError.stringResource()
        }
        is BookError.GetUserFinishedBooksError -> {
            home_error_getFinishedBooksError.stringResource()
        }
        is BookError.GetUserGiveUpBooksError -> {
            home_error_getGiveUpBooksError.stringResource()
        }
        else -> {
            authorization_errors_unknown.stringResource()
        }
    }
}

@Composable
fun LocalDateTime.toStringDateWithDayAndTime(): String {
    if(this.date == Clock.System.now().toLocalDateTime(TimeZone.UTC).date) {
        return general_today.stringResource(context = LocalContext.current)
    }

    val formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm")
    return this.toJavaLocalDateTime().format(formatter)
}

