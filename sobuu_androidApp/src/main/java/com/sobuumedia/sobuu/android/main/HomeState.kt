package com.sobuumedia.sobuu.android.main

import com.sobuumedia.sobuu.features.book.remote.BookError

data class HomeState (
    val isLoading: Boolean = false,
    val isOnSearch: Boolean = false,
    val error: BookError? = null,
)