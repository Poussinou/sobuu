package com.sobuumedia.sobuu.android.authentication

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_licenses
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_privacy
import com.sobuumedia.sobuu.SharedRes.strings.settings_main_terms
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication.LegalTextView
import com.sobuumedia.sobuu.android.coreUI.portrait.views.authentication.LegalTextViewLandscape
import com.sobuumedia.sobuu.android.custom_widgets.CustomTopAppBar
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.utils.stringResource

enum class TextType {
    TERMS_AND_CONDITIONS, PRIVACY_POLICY, LICENSES
}

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun LongTextScreen(
    navigateBack: () -> Unit,
    textType: TextType,
) {
    val context = LocalContext.current
    val configuration = LocalConfiguration.current

    // TODO Not ready for production
    SobuuTheme(useDarkTheme = false) {
        Scaffold(
            topBar = {
                CustomTopAppBar(
                    navigateBack = { navigateBack() },
                    title = when (textType) {
                        TextType.TERMS_AND_CONDITIONS -> settings_main_terms.stringResource(context = context)
                        TextType.PRIVACY_POLICY -> settings_main_privacy.stringResource(context = context)
                        TextType.LICENSES -> settings_main_licenses.stringResource(context = context)
                    },
                )
            },
            content = {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background)
                        .padding(it)
                        .padding(horizontal = 15.dp),
                ) {
                    when (configuration.orientation) {
                        Configuration.ORIENTATION_LANDSCAPE -> {
                            LegalTextViewLandscape(textType = textType)
                        }

                        else -> {
                            LegalTextView(textType = textType)
                        }
                    }
                }
            })
    }
}