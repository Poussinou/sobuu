package com.sobuumedia.sobuu.android.coreUI.portrait.atoms

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.gowtham.ratingbar.StepSize
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme

@Composable
fun RatingStarsBar(
    modifier: Modifier = Modifier,
    ratingNum: Double = 0.0
) {

    val backgroundColor = MaterialTheme.colorScheme.background
    val ratingColor = MaterialTheme.colorScheme.tertiary
    val padding = 10.dp

    var rating: Double by remember { mutableDoubleStateOf(ratingNum) }
    val ratingSize = 40.dp
    val numberOfRating = 5

    RatingBar(
        modifier = modifier
            .background(backgroundColor)
            .padding(vertical = padding),
        value = rating.toFloat(),
        config = RatingBarConfig()
            .activeColor(backgroundColor)
            .inactiveColor(ratingColor)
            .stepSize(StepSize.HALF)
            .numStars(numberOfRating)
            .size(ratingSize)
            .padding(padding)
            .style(RatingBarStyle.Normal),

        onValueChange = {
            rating = it.toDouble()
        },
        onRatingChanged = { }
    )
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun FinishBookDialogPreview() {
    SobuuTheme {
        RatingStarsBar()
    }
}
