package com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun CrossButtonIcon(
    modifier: Modifier = Modifier,
    onButtonClick: () -> Unit
) {
    val iconColor = MaterialTheme.colorScheme.background

    IconButton(
        modifier = Modifier
            .padding(12.dp)
            .then(modifier),
        onClick = onButtonClick
    ) {
        Icon(
            imageVector = Icons.Filled.Close,
            contentDescription = Icons.Filled.Close.name,
            tint = iconColor,
            modifier = Modifier.size(24.dp)
        )
    }
}


@Preview(showSystemUi = true, showBackground = true, group = "Done")
@Composable
fun CrossButtonIconPreview() {
    CrossButtonIcon(
        onButtonClick = {}
    )
}