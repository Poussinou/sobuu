package com.sobuumedia.sobuu.android.coreUI.portrait.molecules

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_login
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_password
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.LoginCircleButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.BottomRoundedOutlinedTextField
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.TopRoundedOutlinedTextField
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun LoginForm(
    modifier: Modifier = Modifier,
    nameFieldValue: String,
    onNameValueChange: (String) -> Unit,
    passwordFieldValue: String,
    onPasswordValueChange: (String) -> Unit,
    onLoginButtonClick: () -> Unit,
    nameIsError: Boolean = false,
    passwordIsError: Boolean = false,
) {
    val context = LocalContext.current
    
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        TopRoundedOutlinedTextField(
            fieldValue = nameFieldValue,
            onFieldValueChange = onNameValueChange,
            icon = Icons.Filled.Mail,
            placeholderText = authorization_auth_email.stringResource(context = context),
            isError = nameIsError
        )

        BottomRoundedOutlinedTextField(
            fieldValue = passwordFieldValue,
            onFieldValueChange = onPasswordValueChange,
            placeholderText = authorization_auth_password.stringResource(context = context),
            onKeyboardActionClicked = onLoginButtonClick,
            passwordField = true,
            isError = passwordIsError
        )

        Spacer(modifier = Modifier.height(15.dp))

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth().padding(horizontal = 5.dp)
        ) {
            Text(
                authorization_auth_login.stringResource(context = context),
                modifier = Modifier
                    .semantics {
                        this.contentDescription = authorization_auth_login.stringResource()
                    },
                style = Typography.titleLarge.copy(
                    color = MaterialTheme.colorScheme.background,
                    fontWeight = FontWeight.Bold
                )
            )
            LoginCircleButton (
                onLoginButtonClick = onLoginButtonClick
            )
        }
    }
}


@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LoginFormPreview() {
    LoginForm(
        nameFieldValue = "Sobuu",
        onNameValueChange = {},
        passwordFieldValue = "123456",
        onPasswordValueChange = {},
        onLoginButtonClick = {}
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LoginFormErrorPreview() {
    LoginForm(
        nameFieldValue = "Sobuu",
        onNameValueChange = {},
        passwordFieldValue = "123456",
        onPasswordValueChange = {},
        onLoginButtonClick = {},
        nameIsError = true
    )
}

@Preview(showSystemUi = false, showBackground = true)
@Composable
fun LoginFormErrorsPreview() {
    LoginForm(
        nameFieldValue = "Sobuu",
        onNameValueChange = {},
        passwordFieldValue = "123456",
        onPasswordValueChange = {},
        onLoginButtonClick = {},
        nameIsError = true,
        passwordIsError = true,
    )
}