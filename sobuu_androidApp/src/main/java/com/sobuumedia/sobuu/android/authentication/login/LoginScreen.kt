package com.sobuumedia.sobuu.android.authentication.login

import android.content.res.Configuration
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_emptyCredentials
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_invalidCredentials
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_invalidSessionToken
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_timeout
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_unknown
import com.sobuumedia.sobuu.SharedRes.strings.authorization_errors_wrongEmailFormat
import com.sobuumedia.sobuu.android.authentication.TextType
import com.sobuumedia.sobuu.android.coreUI.landscape.organisms.authentication.LoginViewLandscape
import com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication.LoginView
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import org.koin.androidx.compose.koinViewModel

@Composable
fun LoginScreen(
    navigateToHomeScreen: () -> Unit,
    navigateToForgotPasswordScreen: () -> Unit,
    navigateToRegistrationScreen: () -> Unit,
    navigateToLegalTextScreen: (type: TextType) -> Unit,
    viewModel: LoginViewModel = koinViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val configuration = LocalConfiguration.current

    LaunchedEffect(viewModel, context) {
        viewModel.authResult.collect { result ->
            if (result is AuthenticationResult.Authorized) {
                navigateToHomeScreen()
            } else {
                viewModel.handleError(result.error)
            }
        }
    }

    when (configuration.orientation) {
        Configuration.ORIENTATION_LANDSCAPE -> {
            LoginViewLandscape(
                usernameValue = state.loginUsername,
                usernameOnValueChange = { viewModel.onEvent(LoginUIEvent.LoginUsernameChanged(it)) },
                passwordValue = state.loginPassword,
                passwordOnValueChange = { viewModel.onEvent(LoginUIEvent.LoginPasswordChanged(it)) },
                onLoginButtonClicked = {
                    viewModel.onEvent(LoginUIEvent.loginUser)
                    keyboardController?.hide()
                },
                nameHasError = state.error != null,
                errorText = if (state.error != null) {
                    getStringFromError(error = state.error)
                } else {
                    null
                },
                isLoading = state.isLoading,
                forgotPasswordButtonOnClick = {
                    navigateToForgotPasswordScreen()
                },
                registerButtonOnClick = {
                    navigateToRegistrationScreen()
                },
                termsButtonOnClick = {
                    navigateToLegalTextScreen(TextType.TERMS_AND_CONDITIONS)
                },
                privacyButtonOnClick = {
                    navigateToLegalTextScreen(TextType.PRIVACY_POLICY)
                })
        }
        else -> {
            LoginView(
                usernameValue = state.loginUsername,
                usernameOnValueChange = { viewModel.onEvent(LoginUIEvent.LoginUsernameChanged(it)) },
                passwordValue = state.loginPassword,
                passwordOnValueChange = { viewModel.onEvent(LoginUIEvent.LoginPasswordChanged(it)) },
                onLoginButtonClicked = {
                    viewModel.onEvent(LoginUIEvent.loginUser)
                    keyboardController?.hide()
                },
                nameHasError = state.error != null,
                errorText = if (state.error != null) {
                    getStringFromError(error = state.error)
                } else {
                    null
                },
                isLoading = state.isLoading,
                forgotPasswordButtonOnClick = {
                    navigateToForgotPasswordScreen()
                },
                registerButtonOnClick = {
                    navigateToRegistrationScreen()
                },
                termsButtonOnClick = {
                    navigateToLegalTextScreen(TextType.TERMS_AND_CONDITIONS)
                },
                privacyButtonOnClick = {
                    navigateToLegalTextScreen(TextType.PRIVACY_POLICY)
                })
        }
    }
}

private fun getStringFromError(error: AuthenticationError?): String {
    return when (error) {
        is AuthenticationError.InvalidCredentials -> {
            authorization_errors_invalidCredentials.stringResource()
        }

        is AuthenticationError.EmptyCredentialsError -> {
            authorization_errors_emptyCredentials.stringResource()
        }

        is AuthenticationError.InvalidSessionToken -> {
            authorization_errors_invalidSessionToken.stringResource()
        }

        is AuthenticationError.TimeOutError -> {
            authorization_errors_timeout.stringResource()
        }

        is AuthenticationError.WrongEmailFormatError -> {
            authorization_errors_wrongEmailFormat.stringResource()
        }

        else -> {
            authorization_errors_unknown.stringResource()
        }
    }
}