package com.sobuumedia.sobuu.android.settings.theme

import android.app.Activity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

val DarkColorScheme = darkColorScheme(
    primary = DarkGreen,
    onPrimary = LightBlue,
    secondary = DarkCharcoalGray,
    onSecondary = LightBlue,
    tertiary = LightGray,
    error = DarkRed,
    onError = LightBlue,
    background = LightBlue,
    onBackground = LightBlue
)

val LightColorScheme = lightColorScheme(
    primary = GreenSheen,
    onPrimary = WhiteBlue,
    secondary = DarkLava,
    onSecondary = WhiteBlue,
    tertiary = SpanishGray,
    error = Vermilion,
    onError = WhiteBlue,
    background = WhiteBlue,
)

@Composable
fun SobuuAuthTheme(
    useDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        useDarkTheme -> DarkColorScheme
        else -> LightColorScheme
    }

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            window.navigationBarColor = colorScheme.secondary.toArgb()
            WindowCompat
                .getInsetsController(window, view)
                .isAppearanceLightStatusBars = useDarkTheme
            WindowCompat
                .getInsetsController(window, view)
                .isAppearanceLightNavigationBars = useDarkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}

@Composable
fun SobuuTheme(
    useDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        useDarkTheme -> DarkColorScheme
        else -> LightColorScheme
    }

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.background.toArgb()
            window.navigationBarColor = colorScheme.secondary.toArgb()
            WindowCompat
                .getInsetsController(window, view)
                .isAppearanceLightStatusBars = !useDarkTheme
            WindowCompat
                .getInsetsController(window, view)
                .isAppearanceLightNavigationBars = useDarkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}