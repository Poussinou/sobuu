package com.sobuumedia.sobuu.android.coreUI.landscape.organisms.home

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.ErrorIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.texts.GetMoreBooksText
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SearchBookListItem
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.SearchMoreInfoBlock
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.CreditsBO

@Composable
fun SearchListViewLandscape(
    modifier: Modifier = Modifier,
    isLoading: Boolean,
    errorText: String?,
    bookList: List<Book>?,
    emptyResult: Boolean,
    navigateToBookScreen: (String) -> Unit,
    navigateToAddBookScreen: (Boolean) -> Unit,
    onMoreInfoClick: () -> Unit,
    listState: LazyListState,
    displayMoreInfo: Boolean,
    displaySearchFurther: Boolean,
    displayWarningWithEmail: Boolean
) {
    val context = LocalContext.current
    val textColor = MaterialTheme.colorScheme.secondary

    val verticalPadding = 12.dp
    val moreBooksInfoPadding = 24.dp

    Column(
        modifier = Modifier.then(modifier).padding(top = verticalPadding)
    ) {
        when {
            isLoading -> CircularLoadingIndicator()
            !errorText.isNullOrEmpty() -> ErrorIndicator(errorText = errorText)
            emptyResult -> {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = verticalPadding),
                    text = SharedRes.strings.search_main_noBooksFound.stringResource(context = context),
                    style = Typography.bodyLarge.copy(color = textColor),
                    textAlign = TextAlign.Center,
                )

                GetMoreBooksText(
                    onClick = onMoreInfoClick,
                    displayMoreInfo = displayMoreInfo
                )
            }
            !bookList.isNullOrEmpty() -> {
                LazyColumn(
                    state = listState,
                    contentPadding = PaddingValues(5.dp)
                ) {
                    items(bookList.size) { position ->
                        val book = bookList[position]

                        SearchBookListItem(
                            title = book.title,
                            author = book.authors.joinToString(", "),
                            totalPages = book.totalPages,
                            description = book.bookDescription,
                            cover = book.thumbnail,
                            totalReviews = book.allReviews.size,
                            totalComments = book.totalComments,
                            peopleReadingTheBook = book.peopleReadingIt,
                            rate = book.totalRating,
                            userHasReadTheBook = book.readingStatus,
                            modifier = Modifier
                                .padding(verticalPadding)
                                .clickable {
                                    navigateToBookScreen(book.id)
                                }
                        )

                        if (position == bookList.size - 1) {
                            Column {
                                GetMoreBooksText(
                                    onClick = onMoreInfoClick,
                                    displayMoreInfo = displayMoreInfo
                                )

                                SearchMoreInfoBlock(
                                    modifier = Modifier.padding(moreBooksInfoPadding),
                                    navigateToAddBookScreen = { navigateToAddBookScreen(it) },
                                    displayMoreInfo = displayMoreInfo,
                                    displaySearchFurther = displaySearchFurther,
                                    displayWarningWithEmail = displayWarningWithEmail
                                )
                            }
                        }
                    }
                }
            }
        }

        SearchMoreInfoBlock(
            modifier = Modifier.padding(moreBooksInfoPadding),
            navigateToAddBookScreen = { navigateToAddBookScreen(it) },
            displayMoreInfo = displayMoreInfo,
            displaySearchFurther = displaySearchFurther,
            displayWarningWithEmail = displayWarningWithEmail
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, widthDp = 720, heightDp = 360)
@Composable
fun SearchListViewLandscapeDarkPreview() {
    val book = Book(
        title = "Test Book",
        authors = listOf("Flame McGuy"),
        picture = "",
        thumbnail = "",
        userRating = null,
        id = "",
        allReviews = emptyList(),
        credits = CreditsBO(),
        bookDescription = "",
        genres = listOf(),
        isbn = Pair("", ""),
        lang = "en",
        peopleReadingIt = 6,
        publishedDate = "",
        publisher = "",
        readingStatus = BookReadingStatus.NOT_READ,
        serie = "",
        serieNumber = -1,
        totalComments = 0,
        totalPages = 654,
        totalRating = 0.0
    )

    SobuuTheme {
        SearchListViewLandscape(
            errorText = null,
            isLoading = false,
            bookList = listOf(book),
            navigateToAddBookScreen = {},
            navigateToBookScreen = {},
            listState = rememberLazyListState(),
            emptyResult = false,
            displayMoreInfo = true,
            displaySearchFurther = true,
            displayWarningWithEmail = true,
            onMoreInfoClick = {}
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO, widthDp = 720, heightDp = 360)
@Composable
fun SearchListViewLandscapePreview() {
    SobuuTheme {
        SearchListViewLandscape(
            modifier = Modifier.background(Color.White),
            errorText = "Error when catching books",
            isLoading = false,
            bookList = null,
            navigateToAddBookScreen = {},
            navigateToBookScreen = {},
            listState = rememberLazyListState(),
            emptyResult = false,
            displayMoreInfo = false,
            displaySearchFurther = true,
            displayWarningWithEmail = true,
            onMoreInfoClick = {}
        )
    }
}