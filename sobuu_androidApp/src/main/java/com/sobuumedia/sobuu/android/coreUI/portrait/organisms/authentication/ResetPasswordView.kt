package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.authentication

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sobuumedia.sobuu.SharedRes.strings.authorization_auth_email
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.CircularLoadingIndicator
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.buttons.ResetPasswordButton
import com.sobuumedia.sobuu.android.coreUI.portrait.atoms.textinputfields.CompleteRoundedOutlinedTextFieldWithIcon
import com.sobuumedia.sobuu.android.settings.theme.SobuuAuthTheme
import com.sobuumedia.sobuu.android.settings.theme.Typography
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun ResetPasswordView(
    emailFieldValue: String,
    onEmailValueChange: (String) -> Unit,
    navigateToSentEmailScreen: () -> Unit,
    errorText: String?,
    isLoading: Boolean,
) {
    val context = LocalContext.current

    val backgroundColor = MaterialTheme.colorScheme.primary
    val errorColor = MaterialTheme.colorScheme.error

    if (isLoading) {
        CircularLoadingIndicator()
    } else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor)
                .padding(horizontal = 40.dp),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Spacer(modifier = Modifier.weight(0.5f))
            CompleteRoundedOutlinedTextFieldWithIcon(
                fieldValue = emailFieldValue,
                onFieldValueChange = onEmailValueChange,
                placeholderText = authorization_auth_email.stringResource(context = context),
                onKeyboardActionClicked = navigateToSentEmailScreen
            )

            if (errorText != null) {
                Spacer(modifier = Modifier.height(20.dp))

                Text(
                    text = errorText,
                    modifier = Modifier
                        .semantics { this.contentDescription = errorText },
                    style = Typography.bodyMedium.copy(color = errorColor),
                    textAlign = TextAlign.Center,
                )

                Spacer(modifier = Modifier.height(20.dp))
            } else {
                Spacer(modifier = Modifier.height(45.dp))
            }

            ResetPasswordButton(
                onClick = navigateToSentEmailScreen
            )

            Spacer(modifier = Modifier.weight(1.0f))
        }
    }
}



@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun ResetPasswordViewLightPreview() {
    SobuuAuthTheme {
        ResetPasswordView(
            emailFieldValue = "",
            onEmailValueChange = {},
            navigateToSentEmailScreen = {},
            errorText = null,
            isLoading = false,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun ResetPasswordViewDarkPreview() {
    SobuuAuthTheme {
        ResetPasswordView(
            emailFieldValue = "",
            onEmailValueChange = {},
            navigateToSentEmailScreen = {},
            errorText = null,
            isLoading = false,
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun ResetPasswordViewErrorDarkPreview() {
    SobuuAuthTheme {
        ResetPasswordView(
            emailFieldValue = "",
            onEmailValueChange = {},
            navigateToSentEmailScreen = {},
            errorText = "Long error text because this is here to test how the error looks like",
            isLoading = false,
        )
    }
}