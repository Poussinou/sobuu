package com.sobuumedia.sobuu.android.authentication.splash

import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError

data class SplashState (
    val isLoading: Boolean = false,
    val error: AuthenticationError? = null,
    val disconnected: Boolean = false
)