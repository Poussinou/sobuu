package com.sobuumedia.sobuu.android.analytics

import android.util.Log
import com.sobuumedia.sobuu.analytics.Analytics
import com.sobuumedia.sobuu.analytics.matomo_kit.MatomoDispatcherImpl
import com.sobuumedia.sobuu.analytics.plausible_kit.PlausibleDispatcherImpl
import org.koin.core.component.KoinComponent

class SobuuAnalytics: KoinComponent {

    fun startAnalytics(resolution: String, operativeSystem: String, osVersion: String,
                               appVersion: String, language: String): Analytics {

        // init analytics property. this is in charge of tracking all events
        return Analytics(
            MatomoDispatcherImpl(
                init = true,
                resolution = resolution,
                operativeSystem = operativeSystem,
                osVersion = osVersion,
                appVersion = appVersion,
                language = language
            ),
            PlausibleDispatcherImpl(
                init = false,
                resolution = resolution,
                operativeSystem = operativeSystem,
                osVersion = osVersion,
                appVersion = appVersion,
                language = language
            )
        ).also {
            // set an exception handler
            // either way, the analytics util won't crash your app
            it.exceptionHandler = object : Analytics.ExceptionHandler {
                override fun onException(e: Exception) {
                    // this is the exception, log it, send it or ignore it.
                    Log.w("Analytics", "Analytics Exception Raised")
                    e.printStackTrace()
                }
            }
        }
    }
}