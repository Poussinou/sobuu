package com.sobuumedia.sobuu.android.main

import com.sobuumedia.sobuu.features.book.remote.BookError

data class SearchState(
    val isLoading: Boolean = false,
    val searchTerm: String = "",
    val language: String = "en",
    val searchFurther: Boolean = false,
    val error: BookError? = null,
    val emptyResult: Boolean = false,
    val displaySearchFurther: Boolean = false,
    val displayWarningWithEmail: Boolean = false,
    val displayMoreInfo: Boolean = false,
)