package com.sobuumedia.sobuu.android.profile

import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import com.sobuumedia.sobuu.models.bo_models.Profile


data class ProfileState(
    val profileInfo: Profile? = null,
    val isLoading: Boolean = false,
    val error: ProfileError? = null,
    val logout: Boolean = false
    )
