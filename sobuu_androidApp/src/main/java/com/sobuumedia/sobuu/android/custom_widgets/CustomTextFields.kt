package com.sobuumedia.sobuu.android.custom_widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sobuumedia.sobuu.SharedRes.strings.home_main_searchBook
import com.sobuumedia.sobuu.android.settings.theme.SourceSans
import com.sobuumedia.sobuu.android.utils.stringResource

@Composable
fun CompleteRoundedOutlineTextField(
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    icon: ImageVector,
    onKeyboardActionClicked: () -> Unit,
    isError: Boolean = false,
    contentDescription: String = "",
) {
    val keyboardController = LocalSoftwareKeyboardController.current

    ProvideTextStyle(value = TextStyle(color = MaterialTheme.colorScheme.secondary)) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    shape = RoundedCornerShape(10.dp),
                    color = MaterialTheme.colorScheme.background
                )
                .semantics {
                    this.contentDescription = contentDescription
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = MaterialTheme.colorScheme.secondary,
                unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                errorBorderColor = MaterialTheme.colorScheme.error,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Go),
            keyboardActions = KeyboardActions(
                onNext = {
                    onKeyboardActionClicked()
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            placeholder = {
                Text(
                    text = placeholderText,
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.tertiary,
                        fontFamily = SourceSans,
                        fontSize = 20.sp,
                    )
                )
            },
            shape = RoundedCornerShape(10.dp),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = icon,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.tertiary,
                )
            }
        )
    }
}

@Composable
fun CompleteRoundedOutlineTextFieldNoIcon(
    modifier: Modifier = Modifier,
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    placeholderTextColor: Color = MaterialTheme.colorScheme.tertiary,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    onFieldClicked: () -> Unit = {},
    fieldEnabled: Boolean = true,
    isError: Boolean = false,
) {
    val keyboardController = LocalSoftwareKeyboardController.current

    ProvideTextStyle(value = TextStyle(color = MaterialTheme.colorScheme.secondary)) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp, vertical = 5.dp)
                .background(
                    shape = RoundedCornerShape(10.dp),
                    color = MaterialTheme.colorScheme.background
                )
                .clickable { onFieldClicked.invoke() }
                .then(modifier),
            enabled = fieldEnabled,
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = MaterialTheme.colorScheme.secondary,
                unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                errorBorderColor = MaterialTheme.colorScheme.error,
            ),
            keyboardOptions = KeyboardOptions(imeAction = onKeyboardIme),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            placeholder = {
                Text(
                    modifier = modifier
                        .then(modifier),
                    text = placeholderText,
                    style = TextStyle(
                        color = placeholderTextColor,
                        fontFamily = SourceSans,
                        fontSize = 16.sp,
                    )
                )
            },
            shape = RoundedCornerShape(10.dp),
            singleLine = true,
        )
    }
}

@Composable
fun CompleteRoundedOutlineWideTextField(
    modifier: Modifier = Modifier,
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    placeholderTextColor: Color = MaterialTheme.colorScheme.tertiary,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    isError: Boolean = false,
) {

    val keyboardController = LocalSoftwareKeyboardController.current
    var offset by remember { mutableFloatStateOf(0f) }

    ProvideTextStyle(value = TextStyle(color = MaterialTheme.colorScheme.secondary)) {
        OutlinedTextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp, vertical = 5.dp)
                .scrollable(
                    orientation = Orientation.Horizontal,
                    state = rememberScrollableState { delta ->
                        offset += delta
                        delta
                    }
                )
                .background(
                    shape = RoundedCornerShape(10.dp),
                    color = MaterialTheme.colorScheme.background,
                )
                .then(modifier),
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = MaterialTheme.colorScheme.secondary,
                unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                errorBorderColor = MaterialTheme.colorScheme.error,
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = onKeyboardIme,
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            isError = isError,
            textStyle = TextStyle(
                color = MaterialTheme.colorScheme.secondary,
                fontFamily = SourceSans,
                fontSize = 18.sp,
            ),
            placeholder = {
                Text(
                    modifier = modifier
                    .then(modifier),
                    text = placeholderText,
                    style = TextStyle(
                        color = placeholderTextColor,
                        fontFamily = SourceSans,
                        fontSize = 16.sp,
                    )
                )
            },
            shape = RoundedCornerShape(10.dp),
            singleLine = false,
        )
    }
}

@Composable
fun SearchBarTextField(
    searchFieldValue: String,
    onSearchFieldValueChange: (String) -> Unit,
    onSearchButtonClick: () -> Unit,
    clearText: () -> Unit,
    onSearchFieldFocusChange: (Boolean) -> Unit,
    modifier: Modifier,
) {
    val focusManager = LocalFocusManager.current
    val focused = remember { mutableStateOf(false) }
    val keyboardController = LocalSoftwareKeyboardController.current

    ProvideTextStyle(value = TextStyle(color = MaterialTheme.colorScheme.secondary)) {
        OutlinedTextField(
            value = searchFieldValue,
            onValueChange = onSearchFieldValueChange,
            modifier = Modifier
                .background(
                    shape = RoundedCornerShape(5.dp),
                    color = MaterialTheme.colorScheme.background,
                )
                .fillMaxWidth()
                .then(modifier)
                .onFocusChanged { focusState ->
                    focused.value = focusState.isFocused
                    onSearchFieldFocusChange(focusState.isFocused)
                },
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = MaterialTheme.colorScheme.secondary,
                unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                errorBorderColor = MaterialTheme.colorScheme.error,
            ),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
            keyboardActions = KeyboardActions(
                onSearch = {
                    onSearchButtonClick()
                    keyboardController?.hide()
                }
            ),
            placeholder = {
                Text(
                    modifier = Modifier.semantics { this.contentDescription = home_main_searchBook.stringResource() },
                    text = home_main_searchBook.stringResource(context = LocalContext.current),
                    style = TextStyle(
                        color = MaterialTheme.colorScheme.tertiary,
                        fontFamily = SourceSans,
                        fontSize = 18.sp,
                    ),
                )
            },
            shape = RoundedCornerShape(5.dp),
            singleLine = true,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.tertiary,
                )
            },
            trailingIcon = if(focused.value) {
                {
                    IconButton(
                        onClick = {
                            if (searchFieldValue.isBlank()) {
                                focusManager.clearFocus()
                            } else {
                                clearText()
                            }
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Close,
                            tint = MaterialTheme.colorScheme.secondary,
                            contentDescription = null,
                        )
                    }
                }
            } else {
                {}
            }
        )
    }
}

@Composable
fun WideTextField(
    modifier: Modifier = Modifier,
    fieldValue: String,
    onFieldValueChange: (String) -> Unit,
    placeholderText: String,
    placeholderTextColor: Color = MaterialTheme.colorScheme.tertiary,
    onKeyboardActionClicked: (() -> Unit)? = null,
    onKeyboardIme: ImeAction = ImeAction.Go,
    isError: Boolean = false)
{
    val keyboardController = LocalSoftwareKeyboardController.current
    var offset by remember { mutableFloatStateOf(0f) }

    ProvideTextStyle(value = TextStyle(color = MaterialTheme.colorScheme.secondary)) {
        TextField(
            value = fieldValue,
            onValueChange = onFieldValueChange,
            modifier = Modifier
                .scrollable(
                    orientation = Orientation.Horizontal,
                    state = rememberScrollableState { delta ->
                        offset += delta
                        delta
                    }
                )
                .background(
                    color = MaterialTheme.colorScheme.background,
                )
                .then(modifier),
            textStyle = TextStyle(
                color = MaterialTheme.colorScheme.secondary,
                fontFamily = SourceSans,
                fontSize = 18.sp,
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.show()
                },
                onGo = {
                    if(onKeyboardActionClicked != null) {
                        onKeyboardActionClicked()
                    } else {
                        this.defaultKeyboardAction(onKeyboardIme)
                    }
                    keyboardController?.hide()
                }
            ),
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = MaterialTheme.colorScheme.secondary,
                unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                errorBorderColor = MaterialTheme.colorScheme.error,
            ),
            isError = isError,
            placeholder = {
                Text(
                    placeholderText,
                    style = TextStyle(
                        color = placeholderTextColor,
                        fontFamily = SourceSans,
                        fontSize = 16.sp,
                    )
                )
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Default,
                keyboardType = KeyboardType.Text,
                capitalization = KeyboardCapitalization.Sentences
            ),
            singleLine = false,
        )
    }

}

@Preview
@Composable
private fun WideTextFieldPreview() {
    WideTextField(
        fieldValue = "",
        onFieldValueChange = ({}),
        placeholderText = "Write",
        onKeyboardActionClicked = { /*TODO*/ })
}