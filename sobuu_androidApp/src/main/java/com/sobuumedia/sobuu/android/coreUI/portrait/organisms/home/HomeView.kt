package com.sobuumedia.sobuu.android.coreUI.portrait.organisms.home

import android.content.res.Configuration
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.sobuumedia.sobuu.android.coreUI.portrait.molecules.BookSectionsBlock
import com.sobuumedia.sobuu.android.settings.theme.SobuuTheme
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookReadingStatus
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

enum class BookStatusType {
    CURRENTLY_READING, ALREADY_READ, GIVE_UP
}

@Composable
fun HomeView(
    modifier: Modifier = Modifier,
    navigateToCurrentlyReadingBook: (String) -> Unit,
    currentSection: BookStatusType,
    currentlyReadingBookList: List<BookWithProgress>?,
    gaveUpBookList: List<BookWithProgress>?,
    finishedBookList: List<FinishedReadingBook>?,
    displayCurrentlyReading: () -> Unit,
    displayFinished: () -> Unit,
    displayGaveUp: () -> Unit,
    isLoading: Boolean,
    errorText: String?
) {
    BookSectionsBlock(
        modifier = modifier,
        isLoading = isLoading,
        errorText = errorText,
        currentSection = currentSection,
        onCurrentBookClicked = navigateToCurrentlyReadingBook,
        onDisplayCurrentlyReadingMenuClicked = displayCurrentlyReading,
        onDisplayFinishedMenuClicked = displayFinished,
        onDisplayGaveUpMenuClicked = displayGaveUp,
        currentlyReadingBookList = currentlyReadingBookList,
        gaveUpBookList = gaveUpBookList,
        finishedBooksList = finishedBookList
    )
}


@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun HomeViewDarkPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        HomeView(
            navigateToCurrentlyReadingBook = {},
            errorText = null,
            isLoading = false,
            gaveUpBookList = emptyList(),
            currentlyReadingBookList = listOf(book, book),
            currentSection = BookStatusType.CURRENTLY_READING,
            displayCurrentlyReading = {},
            displayFinished = {},
            displayGaveUp = {},
            finishedBookList = emptyList()
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun HomeViewPreview() {
    val book = BookWithProgress(
        bookProgress = BookProgress(
            id = "",
            giveUp = false,
            finishedToRead = null,
            finished = false,
            startedToRead = Clock.System.now().toLocalDateTime(TimeZone.UTC),
            percentage = 0.0,
            page = 0,
            progressInPercentage = 0.0
        ),
        book = Book(
            title = "Test Book",
            authors = listOf("Flame McGuy"),
            picture = "",
            thumbnail = "",
            userRating = null,
            id = "",
            allReviews = emptyList(),
            credits = CreditsBO(),
            bookDescription = "",
            genres = listOf(),
            isbn = Pair("", ""),
            lang = "en",
            peopleReadingIt = 6,
            publishedDate = "",
            publisher = "",
            readingStatus = BookReadingStatus.NOT_READ,
            serie = "",
            serieNumber = -1,
            totalComments = 0,
            totalPages = 654,
            totalRating = 0.0
        ),
        bookProgressComments = listOf()
    )

    SobuuTheme {
        HomeView(
            navigateToCurrentlyReadingBook = {},
            errorText = null,
            isLoading = false,
            gaveUpBookList = emptyList(),
            currentlyReadingBookList = listOf(book, book),
            currentSection = BookStatusType.CURRENTLY_READING,
            displayCurrentlyReading = {},
            displayFinished = {},
            displayGaveUp = {},
            finishedBookList = emptyList()
        )
    }
}

@Preview(showSystemUi = false, showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Composable
fun HomeViewEmptyPreview() {
    SobuuTheme {
        HomeView(
            navigateToCurrentlyReadingBook = {},
            errorText = null,
            isLoading = false,
            gaveUpBookList = emptyList(),
            currentlyReadingBookList = emptyList(),
            currentSection = BookStatusType.CURRENTLY_READING,
            displayCurrentlyReading = {},
            displayFinished = {},
            displayGaveUp = {},
            finishedBookList = emptyList()
        )
    }
}