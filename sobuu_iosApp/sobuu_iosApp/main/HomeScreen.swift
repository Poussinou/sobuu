import SwiftUI
import shared

enum BookStatusType {
    case CURRENTLY_READING
    case ALREADY_READ
    case GIVE_UP
}

struct HomeScreen: View {
    let appName: String = SharedRes.strings().general_appName.stringResource()
    var body: some View {
        NavigationView {
            VStack {
                Text("Book Title")
                Image(systemName: "book")
            }
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: .infinity,
                alignment: .topLeading
            )
            .overlay(
                ProfileView()
                    .padding(.trailing, 20)
                    .offset(x: 0, y: -50),
                alignment: .topTrailing
            )
            .background(ColorToken.greenSheen)
            .navigationTitle(
                Text(appName)
                    .foregroundColor(ColorToken.darkLava)
                    .font(.SolwayAppLogo)
            )
        }
        .navigationBarHidden(true)
    }
}

struct ProfileView: View {
    var body: some View {
        Image(systemName: "book")
            .resizable()
            .scaledToFit()
            .frame(width: 40, height: 40)
            .clipShape(Circle())
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
    }
}
