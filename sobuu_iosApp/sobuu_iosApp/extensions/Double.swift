import Foundation

extension Double {
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 15 //maximum digits in Double after dot (maximum precision)
        return String(formatter.string(from: number) ?? "")
    }
    
    func removeZerosFromEnd() -> Double {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 15 //maximum digits in Double after dot (maximum precision)
        return Double(truncating: number)
    }
}
