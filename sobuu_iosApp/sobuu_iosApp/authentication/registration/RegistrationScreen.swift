import SwiftUI
import shared

struct RegistrationScreen: View {
    
    @StateObject private var viewModel = LoginViewModel()

    
    var body: some View {
        
        NavigationView {
            if viewModel.showLoading {
                ProgressView()
                    .scaleEffect(2)
                    .font(.title)
                    .progressViewStyle(CircularProgressViewStyle(tint: ColorToken.greenSheen))
                    .edgesIgnoringSafeArea(.all)
                    .background(ColorToken.whiteBlue)
            } else {
                VStack {
                    NavigationLink(destination: HomeScreen(), isActive: $viewModel.authorized) { EmptyView() }
                                    
                    RegistrationForm()
                }
                .frame(
                    maxWidth: .infinity,
                    maxHeight: .infinity,
                    alignment: .top
                )
                .background(ColorToken.greenSheen.ignoresSafeArea())
                .navigationTitle(SharedRes.strings().authorization_auth_createAccount.stringResource())
                .navigationBarBackButtonHidden(false)
                .toolbar {
                    ToolbarItem(placement: .topBarLeading) {
                        Button(action: {
                            // TODO
                        }) {
                            Label("Back", systemImage: "arrow.left")
                                .foregroundColor(ColorToken.whiteBlue)
                        }
                    }
                }
            }
        }
        .navigationBarHidden(false)
    }
}

struct RegistrationForm: View {
    
    @State var username = ""
    @State var email = ""
    @State var password = ""
    @State var passwordConfirmation = ""
    @State var errorText: String? = nil
    
    var body: some View {
        VStack {
            TextField(SharedRes.strings().authorization_auth_username.stringResource(), text: $username)
                .keyboardType(.alphabet)
            TextField(SharedRes.strings().authorization_auth_email.stringResource(), text: $email)
                .keyboardType(.emailAddress)
            SecureField(SharedRes.strings().authorization_auth_password.stringResource(), text: $password)
            SecureField(SharedRes.strings().authorization_auth_confirmPassword.stringResource(), text: $passwordConfirmation)
        }
        .font(Font.SourceSans)
        .lineLimit(0)
        .background(ColorToken.greenSheen)
        .autocapitalization(.none)
        .textFieldStyle(.roundedBorder)
        .padding(.vertical, 50)
        .padding(.horizontal, 50)
    }
}

struct IconTextField: View {
    
    @State var stateText: Binding<String>
    var icon = ""
    var hint = ""
    
    var body: some View {
        HStack {
            Image(systemName: icon)
            TextField(hint, text: $stateText.wrappedValue)
        }
    }
}

#Preview {
    RegistrationScreen()
}
