import Foundation
import shared

class LoginViewModel: ObservableObject {
    
    @Published var credentials = Credentials()
    @Published var showLoading = false
    @Published var authorized = false
    @Published var errorText: String? = nil
    
    private var authRepo: IAuthenticationRepository?
    
    init() {
        authRepo = KoinHelper(context: NSObject()).authRepo
    }
/*
    func updatedCredentials(email: String, password: String) {
        credentials.email = email
        credentials.password = password
    }
*/
    func login () async {
        showLoading = true
        
        let task = Task.detached { // Runs on a background thread
            try? await self.authRepo?.loginUser(username: self.credentials.email, password: self.credentials.password)
        }

        let authResult = await task.value
         
        
        if (authResult != nil) {
            if(authResult?.error != nil) {
                self.errorText = self.handleErrors(error: authResult?.error)
            } else {
                self.errorText = nil
                self.authorized = true
            }
        
        } else {
            self.errorText = self.handleErrors(error: AuthenticationError.UnknownError())
        }
        
        self.showLoading = false
    }
    
    private func handleErrors(error: AuthenticationError?) -> String {
        guard let resultError = error else {
            return SharedRes.strings().authorization_errors_unknown.stringResource()
        }
        
        switch resultError {
        case .EmailAlreadyTaken():
            return SharedRes.strings().authorization_errors_emailAlreadyUsed.stringResource()
        case .EmptyCredentialsError():
            return SharedRes.strings().authorization_errors_emptyCredentials.stringResource()
        case .InvalidCredentials():
            return SharedRes.strings().authorization_errors_invalidCredentials.stringResource()
        case .InvalidSessionToken():
            return SharedRes.strings().authorization_errors_invalidSessionToken.stringResource()
        case .TimeOutError():
            return SharedRes.strings().authorization_errors_timeout.stringResource()
        case .ResetPassword():
            return SharedRes.strings().authorization_errors_resettingPassword.stringResource()
        case .PasswordTooShort():
            return SharedRes.strings().authorization_errors_shortPassword.stringResource()
        case .InvalidEmailError():
            return SharedRes.strings().authorization_errors_wrongEmailFormat.stringResource()
        case .UsernameAlreadyTaken():
            return SharedRes.strings().authorization_errors_usernameAlreadyTaken.stringResource()
        case .WrongEmailFormatError():
            return SharedRes.strings().authorization_errors_wrongEmailFormat.stringResource()
        case .RegistrationError():
            return SharedRes.strings().authorization_errors_registration.stringResource()
        default:
            return SharedRes.strings().authorization_errors_unknown.stringResource()
        }
    }
}
