import SwiftUI
import shared

struct LoginScreen: View {
    @State private var isPortrait = false
    @StateObject private var viewModel = LoginViewModel()
    @State private var selection: Int?
    
    var body: some View {
        LoginView(
            username: viewModel.credentials.email,
            password: viewModel.credentials.password,
            isUserError: viewModel.errorText != nil,
            isPasswordError: viewModel.errorText != nil,
            errorText: viewModel.errorText,
            isLoading: viewModel.showLoading,
            onLoginClick: {
                let _ = task {
                    await viewModel.login()
                }
            },
            onForgotPasswordClick: {
                //self.selection = 1
            },
            onRegisterClick: {
                //self.selection = 2
            },
            onTermsClick: {
                //self.selection = 3
            },
            onPrivacyClick: {
                //self.selection = 4
            }
        ).loadCustomFonts()
    }
}

struct LoginScreen_Previews: PreviewProvider {
    static var previews: some View {
        LoginScreen()
            .loadCustomFonts()
    }
}
