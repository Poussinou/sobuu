import Foundation
import SwiftUI
import shared

extension Binding {
     func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}

extension ResourcesStringResource {
    func stringResource() -> String {
        return StringsHelper().get(id: self, args: [])
    }
}

public extension ResourcesResourceContainer {
    static var Res: ResourcesResourceContainer {
        return SharedRes.strings()
    }
}
