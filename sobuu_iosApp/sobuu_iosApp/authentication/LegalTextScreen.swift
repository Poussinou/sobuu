import SwiftUI

enum TextType {
    case TERMS_AND_CONDITIONS
    case PRIVACY_POLICY
    case LICENSES
}

struct LegalTextScreen: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    LegalTextScreen()
}
