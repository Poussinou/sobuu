import SwiftUI
import shared

struct SearchListView: View {
    
    let isLoading: Bool
    let errorText: String?
    let bookList: [Book]?
    let emptyResult: Bool
    let navigateToBookScreen: (String) -> Void
    let navigateToAddBookScreen: (Bool) -> Void
    let onMoreInfoClick: () -> Void
    let displayMoreInfo: Bool
    let displaySearchFurther: Bool
    let displayWarningWithEmail: Bool
    
    let verticalPadding: CGFloat = 12
    let moreBooksInfoPadding: CGFloat = 24
    
    let textColor = ColorToken.darkLava
    let backgroundColor = ColorToken.whiteBlue
    
    var body: some View {
        
        if(isLoading) {
            CircularLoadingIndicator()
        } else if (errorText != nil && !errorText!.isEmpty) {
            ErrorIndicator(errorText: errorText)
        } else if(emptyResult) {
            VStack {
                Text(SharedRes.strings().search_main_noBooksFound.stringResource())
                    .frame(maxWidth: .infinity)
                    .padding(.vertical, verticalPadding)
                    .font(CustomFont.bodyLarge)
                    .foregroundStyle(textColor)
                
                GetMoreBooksText(
                    onClick: onMoreInfoClick,
                    displayMoreInfo: displayMoreInfo
                )
                .padding(.vertical, verticalPadding)
                
                SearchMoreInfoBlock(
                    navigateToAddBookScreen: { isManual in
                        navigateToAddBookScreen(isManual)
                    },
                    displayMoreInfo: displayMoreInfo,
                    displaySearchFurther: displaySearchFurther,
                    displayWarningWithEmail: displayWarningWithEmail
                )
            }
            .frame(maxWidth: 350)
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        } else if(bookList != nil && !bookList!.isEmpty) {
            
            VStack {
                ScrollView(showsIndicators: false) {
                    ForEach(0..<bookList!.count, id: \.self) { i in
                        let book = bookList![i]
                        
                        SearchBookListItem(
                            peopleReadingTheBook: Int(book.peopleReadingIt),
                            title: book.title,
                            author: book.authors.joined(separator: ", "),
                            totalPages: Int(book.totalPages),
                            totalReviews: Int(book.totalRating),
                            totalComments: Int(book.totalComments),
                            rate: Double(book.allReviews.count),
                            description: book.bookDescription,
                            cover: book.thumbnail,
                            userHasReadTheBook: book.readingStatus
                        )
                        .listRowInsets(EdgeInsets(top: 3, leading: 3, bottom: 3, trailing: 3))
                        .listRowBackground(backgroundColor)
                        
                        
                        if(i == bookList!.count - 1) {
                            GetMoreBooksText(
                                onClick: onMoreInfoClick,
                                displayMoreInfo: displayMoreInfo
                            )
                            .padding(.vertical, verticalPadding)
                            
                        }
                        
                    }
                    
                    SearchMoreInfoBlock(
                        navigateToAddBookScreen: { isManual in
                            navigateToAddBookScreen(isManual)
                        },
                        displayMoreInfo: displayMoreInfo,
                        displaySearchFurther: displaySearchFurther,
                        displayWarningWithEmail: displayWarningWithEmail
                    )
                    
                    Spacer()
                }
            }
            .frame(maxWidth: 350)
        }
    }
}

#Preview {
    SearchListView(
        isLoading: false,
        errorText: nil,
        bookList: [],
        emptyResult: true,
        navigateToBookScreen: { _ in },
        navigateToAddBookScreen: { _ in},
        onMoreInfoClick: {},
        displayMoreInfo: true,
        displaySearchFurther: true,
        displayWarningWithEmail: true
    ).loadCustomFonts()
}

#Preview {
    SearchListView(
        isLoading: false,
        errorText: "There was an error while fetching the data",
        bookList: [],
        emptyResult: true,
        navigateToBookScreen: { _ in },
        navigateToAddBookScreen: { _ in},
        onMoreInfoClick: {},
        displayMoreInfo: true,
        displaySearchFurther: true,
        displayWarningWithEmail: true
    ).loadCustomFonts()
}

#Preview {
    SearchListView(
        isLoading: false,
        errorText: nil,
        bookList: [getPreviewBook(), getPreviewBook(), getPreviewBook(), getPreviewBook()],
        emptyResult: false,
        navigateToBookScreen: { _ in },
        navigateToAddBookScreen: { _ in},
        onMoreInfoClick: {},
        displayMoreInfo: true,
        displaySearchFurther: true,
        displayWarningWithEmail: true
    ).loadCustomFonts()
}
private func getPreviewBook() -> Book {
    return Book(
        id: "",
        title: "Test book",
        authors: ["Glenn Swanson"],
        bookDescription: "This is an example description to test that if there is a long description it will manage it and display it well",
        picture: "",
        thumbnail: "",
        publisher: "",
        credits: CreditsBO(),
        totalPages: 642,
        isbn: KotlinPair(first: "", second: ""),
        publishedDate: "",
        genres: [],
        totalComments: 0,
        peopleReadingIt: 6,
        readingStatus: BookReadingStatus.notRead,
        allReviews: [],
        userRating: nil,
        totalRating: 6,
        serie: "",
        serieNumber: -1,
        lang: "en"
    )
}
