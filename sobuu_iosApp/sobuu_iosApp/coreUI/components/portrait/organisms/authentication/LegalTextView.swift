import SwiftUI

struct LegalTextView: View {
    @State var isPresenter = true
    let textType: TextType
    
    let backgroundColor: Color = ColorToken.whiteBlue
    
    var body: some View {
        ZStack {
            backgroundColor.ignoresSafeArea()
            
            let path: String = switch(textType) {
                case TextType.LICENSES: "app-licenses"
                case TextType.TERMS_AND_CONDITIONS: "app-terms"
                case TextType.PRIVACY_POLICY: "app-privacy"
            }
            
            let url = "https://getsobuu.com/\(path)"
            
            ZStack {}
                .sheet(isPresented: $isPresenter) {
                    NavigationView {
                        WebView(url: URL(string: url)!).ignoresSafeArea()
                    }
                    .padding()
                }
        }
        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    LegalTextView(textType: TextType.PRIVACY_POLICY)
}
