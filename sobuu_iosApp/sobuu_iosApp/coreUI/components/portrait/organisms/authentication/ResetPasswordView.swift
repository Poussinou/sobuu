import SwiftUI
import shared

struct ResetPasswordView: View {
    
    let onButtonClick: () -> Void
    @State var email: String
    @State var errorText: String?
    @State var isLoading: Bool
    
    var errorSpacing: CGFloat = 100
    
    init(onButtonClick: @escaping () -> Void, email: String, errorText: String? = nil, isLoading: Bool) {
        self.onButtonClick = onButtonClick
        self.email = email
        self.errorText = errorText
        self.isLoading = isLoading
        
        
        errorSpacing = errorText != nil ? 40 : 100
    }
    
    
    var body: some View {
        if isLoading {
            CircularLoadingIndicator()
        } else {
            ZStack {
                ColorToken.greenSheen.ignoresSafeArea()
                
                VStack (spacing: errorSpacing) {
                    Spacer()
                    CompleteRoundedOutlinedTextFieldWithIcon(
                        text: email,
                        hintText: SharedRes.strings().authorization_auth_email.stringResource(),
                        isError: false,
                        icon: "envelope.fill"
                    )
                    
                    if errorText != nil {
                        Text(errorText!)
                            .foregroundColor(ColorToken.vermilion)
                            .multilineTextAlignment(.center)
                    }
                    
                    ResetPasswordButton(didClickButton: onButtonClick)
                        .padding(.horizontal, 5)
                    
                    Spacer()
                }
                .font(CustomFont.bodyMedium)
                .frame(width: 300)
            }
        }
    }
}

#Preview {
    ResetPasswordView(
        onButtonClick: {},
        email: "",
        isLoading: false
    ).loadCustomFonts()
}

#Preview {
    ResetPasswordView(
        onButtonClick: {},
        email: "",
        errorText: "There is an error very long, so it needs a very long error text",
        isLoading: false
    ).loadCustomFonts()
}
