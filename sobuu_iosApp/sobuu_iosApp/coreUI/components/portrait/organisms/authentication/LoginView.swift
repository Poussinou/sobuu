import SwiftUI

struct LoginView: View {
    @State var username: String
    @State var password: String
    @State var isUserError: Bool
    @State var isPasswordError: Bool
    @State var errorText: String?
    @State var isLoading: Bool
    let onLoginClick: () -> Void
    let onForgotPasswordClick: () -> Void
    let onRegisterClick: () -> Void
    let onTermsClick: () -> Void
    let onPrivacyClick: () -> Void
    
    var body: some View {
        if isLoading {
            CircularLoadingIndicator()
        } else {
            ZStack {
                ColorToken.greenSheen.ignoresSafeArea()
                VStack {
                    Spacer()
                    
                    SobuuBigLogo()
                    LoginForm(
                        username: username,
                        password: password,
                        nameIsError: isUserError,
                        passIsError: isPasswordError,
                        onLoginButtonClick: onLoginClick
                    )
                    .padding(.horizontal, 35)
                    
                    if errorText != nil {
                        Text(errorText!)
                            .padding(EdgeInsets(top: 15, leading: 25, bottom: 0, trailing: 25))
                            .foregroundColor(ColorToken.vermilion)
                    }
                    Spacer()
                    
                    ForgotPasswordTextButton(action: onForgotPasswordClick)
                    
                    RegisterButton(didClickButton: onRegisterClick)
                        .padding(.horizontal, 25)
                    
                    Spacer()
                    
                    LegalButtons(
                        onTermsAndConditionsButtonClick: onTermsClick,
                        onPrivacyPolicyButtonClick: onPrivacyClick
                    )
                    .padding(.horizontal, 20)
                    
                    Spacer()
                }
            }
        }
    }
}

#Preview {
    LoginView(
        username: "",
        password: "",
        isUserError: false,
        isPasswordError: false,
        errorText: nil,
        isLoading: false,
        onLoginClick: {},
        onForgotPasswordClick: {},
        onRegisterClick: {},
        onTermsClick: {},
        onPrivacyClick: {}
    ).loadCustomFonts()
}

#Preview {
    LoginView(
        username: "",
        password: "",
        isUserError: false,
        isPasswordError: false,
        errorText: "There was an error while login the user. Please, try again",
        isLoading: false,
        onLoginClick: {},
        onForgotPasswordClick: {},
        onRegisterClick: {},
        onTermsClick: {},
        onPrivacyClick: {}
    ).loadCustomFonts()
}

#Preview {
    LoginView(
        username: "",
        password: "",
        isUserError: false,
        isPasswordError: false,
        errorText: nil,
        isLoading: true,
        onLoginClick: {},
        onForgotPasswordClick: {},
        onRegisterClick: {},
        onTermsClick: {},
        onPrivacyClick: {}
    ).loadCustomFonts()
}
