import SwiftUI
import shared

struct SearchBookListItem: View {
    
    let peopleReadingTheBook: Int
    let title: String
    let author: String
    let totalPages: Int
    let totalReviews: Int
    let totalComments: Int
    let rate: Double
    let description: String
    let cover: String
    let userHasReadTheBook: BookReadingStatus
    
    let backgroundColor = ColorToken.spanishGrey
    let dataTextColor = ColorToken.darkLava
    let textColor = ColorToken.whiteBlue
    
    let heightSize: CGFloat = 250
    let verticalPaddingSize = 5
    
    var readStatusText: String
    var peopleReadingItemText: String
    var totalReviewsText: String
    var totalCommentsText: String
    
    init(peopleReadingTheBook: Int, title: String, author: String, totalPages: Int, totalReviews: Int, totalComments: Int, rate: Double, description: String, cover: String, userHasReadTheBook: BookReadingStatus) {
        self.peopleReadingTheBook = peopleReadingTheBook
        self.title = title
        self.author = author
        self.totalPages = totalPages
        self.totalReviews = totalReviews
        self.totalComments = totalComments
        self.rate = rate
        self.description = description
        self.cover = cover
        self.userHasReadTheBook = userHasReadTheBook
        
        readStatusText = switch(userHasReadTheBook) {
        case BookReadingStatus.notRead: SharedRes.strings().search_main_userHasNotReadBook.stringResource()
        case BookReadingStatus.reading: SharedRes.strings().search_main_userIsReadingBook.stringResource()
        case BookReadingStatus.finished: SharedRes.strings().search_main_userHasAlreadyReadBook.stringResource()
        case BookReadingStatus.giveUp: SharedRes.strings().search_main_userGaveUpBook.stringResource()
        }
        
        
        if(peopleReadingTheBook == 0) {
            peopleReadingItemText = SharedRes.strings().search_main_nobodyReadingBook.stringResource()
        } else if(peopleReadingTheBook == 1) {
            peopleReadingItemText = "\(peopleReadingTheBook) \(SharedRes.strings().search_main_onePersonReadingBook.stringResource())"
        } else if (peopleReadingTheBook > 999) {
            peopleReadingItemText = "\(SharedRes.strings().search_main_moreThan999.stringResource()) \(SharedRes.strings().search_main_pluralPersonsReadingBook.stringResource())"
        } else {
            peopleReadingItemText = "\(peopleReadingTheBook) \(SharedRes.strings().search_main_pluralPersonsReadingBook.stringResource())"
        }
        
        if(totalReviews == 1) {
            totalReviewsText = "\(totalReviews) \(SharedRes.strings().search_main_review.stringResource())"
        } else if (totalReviews > 999) {
            totalReviewsText = "\(SharedRes.strings().search_main_moreThan999.stringResource()) \(SharedRes.strings().search_main_reviews.stringResource())"
        } else {
            totalReviewsText = "\(totalReviews) \(SharedRes.strings().search_main_reviews.stringResource())"
        }
        
        if(totalComments == 1) {
            totalCommentsText = "\(totalComments) \(SharedRes.strings().search_main_comment.stringResource())"
        } else if (totalComments > 999) {
            totalCommentsText = "\(SharedRes.strings().search_main_moreThan999.stringResource()) \(SharedRes.strings().search_main_comments.stringResource())"
        } else {
            totalCommentsText = "\(totalComments) \(SharedRes.strings().search_main_comments.stringResource())"
        }
    }
    
    var body: some View {
        ZStack(alignment: .leading) {
            VStack {
                VStack(alignment: .leading) {
                    HStack {
                        Spacer()
                        Text(readStatusText)
                        Spacer()
                        IconAndText(
                            text: peopleReadingItemText,
                            icon: CurrentlyReadingIcon()
                        )
                        Spacer()
                    }
                    
                    HStack {
                        Spacer()
                        IconAndText(
                            text: "\(rate)",
                            icon: IconToken.star
                        )
                        Spacer()
                        Text(totalReviewsText)
                        Spacer()
                        IconAndText(
                            text: totalCommentsText,
                            icon: IconToken.comment
                        )
                        Spacer()
                    }
                    .font(CustomFont.bodyMedium)
                    .padding(.top, 10)
                }
                .font(CustomFont.bodySmall)
                .padding(.leading, 90)
                .padding(.top, 5)
                
                VStack(alignment: .leading) {
                    Text(title)
                        .font(CustomFont.bodyLarge)
                        .foregroundStyle(textColor)
                        .lineLimit(1)
                        .padding(.all, 3)
                    Text(author)
                        .font(CustomFont.bodyMedium)
                        .foregroundStyle(dataTextColor)
                        .lineLimit(1)
                        .padding(.all, 3)
                    Text(description)
                        .font(CustomFont.bodySmall)
                        .foregroundStyle(textColor)
                        .padding(.all, 3)
                        .padding(.bottom, 10)
                }
                .frame(maxWidth: .infinity)
                .padding(.leading, 100)
                .background(backgroundColor)
                .borderRadius(backgroundColor, width: 1, cornerRadius: 20, corners: [.allCorners])
            }
            VStack {
                BookThumbnail(imageURL: cover)
                    .frame(maxWidth: 80, maxHeight: 120)
                
                IconAndText(
                    text: "\(totalPages)",
                    icon: IconToken.book
                        .foregroundStyle(textColor)
                )
                .font(CustomFont.bodySmall)
                .foregroundStyle(dataTextColor)
                .lineLimit(1)
                .padding(.all, 3)
            }
            .padding(15)
        }
        .borderRadius(backgroundColor, width: 3, cornerRadius: 20, corners: .allCorners)
    }
}

#Preview {
    SearchBookListItem(
        peopleReadingTheBook: 2000,
        title: "The final empire of flies",
        author: "Mark Swanson",
        totalPages: 633,
        totalReviews: 1000,
        totalComments: 1000,
        rate: 5.0,
        description: "This is a description. It's not the best description, but it should be enough.",
        cover: "",
        userHasReadTheBook: BookReadingStatus.notRead
    ).loadCustomFonts()
}
