import SwiftUI
import shared

struct SearchFurtherBlock: View {
    let navigateToAddBookScreenByISBN: () -> Void
    
    let buttonColor = ColorToken.greenSheen
    let textColor = ColorToken.whiteBlue
    
    var body: some View {
        Button(action: navigateToAddBookScreenByISBN) {
                Label(
                    title:{ Text(SharedRes.strings().search_main_addBookByISBN.stringResource())},
                    icon: {}
                )
                .font(CustomFont.bodyMedium)
                .foregroundColor(textColor)
                .frame(maxWidth: .infinity)
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            }
        .frame(maxWidth: .infinity)
        .background(buttonColor)
        .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
    }
}

#Preview {
    SearchFurtherBlock(navigateToAddBookScreenByISBN: {})
}
