import SwiftUI
import shared

struct LegalButtons: View {
    let onTermsAndConditionsButtonClick: () -> Void
    let onPrivacyPolicyButtonClick: () -> Void
    
    var body: some View {
        HStack {
            CustomTextButton(
                text: SharedRes.strings().settings_main_terms.stringResource(),
                color: ColorToken.whiteBlue,
                action: onTermsAndConditionsButtonClick
            )
            Spacer()
            CustomTextButton(
                text: SharedRes.strings().settings_main_privacy.stringResource(),
                color: ColorToken.whiteBlue,
                action: onPrivacyPolicyButtonClick
            )
        }
        .padding(.horizontal, 30)
    }
}

#Preview {
    BaseContentView {
        LegalButtons(
            onTermsAndConditionsButtonClick: {},
            onPrivacyPolicyButtonClick: {}
        ).loadCustomFonts()
    }
}
