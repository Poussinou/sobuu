import SwiftUI
import shared

struct LoginForm: View {
    @State var username: String
    @State var password: String
    @State var nameIsError: Bool
    @State var passIsError: Bool
    let onLoginButtonClick: () -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            TopRoundedOutlinedTextField(
                text: username,
                hintText: SharedRes.strings().authorization_auth_email.stringResource(),
                icon: "person.fill",
                isError: nameIsError
            ).keyboardType(.emailAddress)
            BottomRoundedOutlinedTextField(
                isSecuredField: true,
                text: password,
                hintText: SharedRes.strings().authorization_auth_password.stringResource(),
                isError: passIsError
            )
            
            HStack {
                Text(SharedRes.strings().authorization_auth_login.stringResource())
                    .font(CustomFont.titleLarge)
                    .foregroundColor(ColorToken.whiteBlue)
                Spacer()
                LoginCircleButton(didClickLoginButton: onLoginButtonClick)
            }.padding(.all, 15)
        }
    }
}

#Preview {
    BaseContentView {
        LoginForm(
            username: "",
            password: "",
            nameIsError: false,
            passIsError: false,
            onLoginButtonClick: {}
        ).loadCustomFonts()
    }
}
