import SwiftUI
import shared

struct BookSectionButtonBar: View {
    let displayCurrentlyReading: () -> Void
    let displayFinished: () -> Void
    let displayGaveUp: () -> Void
    let currentSection: BookStatusType
    
    let selectedTextColor = ColorToken.darkLava
    let unselectedTextColor = ColorToken.spanishGrey
    
    let selectedTextStyle = CustomFont.bodyLarge
    let unselectedTextStyle = CustomFont.bodySmall
    
    var body: some View {
        HStack(spacing: 20) {
            Text(SharedRes.strings().home_main_reading.stringResource())
                .font(currentSection == BookStatusType.CURRENTLY_READING ? selectedTextStyle : unselectedTextStyle)
                .foregroundStyle(currentSection == BookStatusType.CURRENTLY_READING ? selectedTextColor : unselectedTextColor)
                .onTapGesture { displayCurrentlyReading() }
            
            Text(SharedRes.strings().home_main_alreadyRead.stringResource())
                .font(currentSection == BookStatusType.ALREADY_READ ? selectedTextStyle : unselectedTextStyle)
                .foregroundStyle(currentSection == BookStatusType.ALREADY_READ ? selectedTextColor : unselectedTextColor)
                .onTapGesture { displayFinished() }
            
            Text(SharedRes.strings().home_main_giveUp.stringResource())
                .font(currentSection == BookStatusType.GIVE_UP ? selectedTextStyle : unselectedTextStyle)
                .foregroundStyle(currentSection == BookStatusType.GIVE_UP ? selectedTextColor : unselectedTextColor)
                .onTapGesture { displayGaveUp() }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(10)
    }
}

#Preview {
    BookSectionButtonBar(
        displayCurrentlyReading: {},
        displayFinished: {},
        displayGaveUp: {},
        currentSection: BookStatusType.ALREADY_READ
    ).loadCustomFonts()
}
