import SwiftUI
import shared

struct BookSectionsBlock: View {

    var isLoading: Bool
    var errorText: String?
    var currentSection: BookStatusType
    var onCurrentBookClicked: (String) -> Void
    var onDisplayCurrentlyReadingMenuClicked: () -> Void
    var onDisplayFinishedMenuClicked: () -> Void
    var onDisplayGaveUpMenuClicked: () -> Void
    var currentlyReadingBookList: Array<BookWithProgress>?
    var gaveUpBookList: Array<BookWithProgress>?
    var finishedBooksList: Array<FinishedReadingBook>?
        
    var body: some View {
        
        if(isLoading) {
            CircularLoadingIndicator()
        } else if (errorText != nil) {
            ErrorIndicator(errorText: errorText)
        } else {
            VStack {
                BookSectionButtonBar(
                    displayCurrentlyReading: onDisplayCurrentlyReadingMenuClicked,
                    displayFinished: onDisplayFinishedMenuClicked,
                    displayGaveUp: onDisplayGaveUpMenuClicked,
                    currentSection: currentSection
                )
                                
                if(currentSection == BookStatusType.CURRENTLY_READING) {
                    CurrentlyReadingBooksSection(
                        bookList: currentlyReadingBookList,
                        onBookClick: onCurrentBookClicked
                    )
                } else if(currentSection == BookStatusType.ALREADY_READ) {
                    FinishedBooksSection(bookList: finishedBooksList)
                } else if(currentSection == BookStatusType.GIVE_UP) {
                    GaveUpBooksSection(bookList: gaveUpBookList)
                }
            }
        }
    }
}

#Preview {
    BookSectionsBlock(
        isLoading: false,
        errorText: nil,
        currentSection: BookStatusType.ALREADY_READ,
        onCurrentBookClicked: {_ in },
        onDisplayCurrentlyReadingMenuClicked: {},
        onDisplayFinishedMenuClicked: {},
        onDisplayGaveUpMenuClicked: {},
        currentlyReadingBookList: [getPreviewBook(), getPreviewBook(), getPreviewBook()],
        gaveUpBookList: [getPreviewBook(), getPreviewBook(), getPreviewBook()],
        finishedBooksList: [getPreviewFinishedBook(), getPreviewFinishedBook(), getPreviewFinishedBook()]
    )
}

private func getPreviewBook() -> BookWithProgress {
    let book = BookWithProgress(
        book: Book(
            id: "",
            title: "Test book",
            authors: ["Glenn Swanson"],
            bookDescription: "",
            picture: "",
            thumbnail: "",
            publisher: "",
            credits: CreditsBO(),
            totalPages: 642,
            isbn: KotlinPair(first: "", second: ""),
            publishedDate: "",
            genres: [],
            totalComments: 0,
            peopleReadingIt: 6,
            readingStatus: BookReadingStatus.notRead,
            allReviews: [],
            userRating: nil,
            totalRating: 6,
            serie: "",
            serieNumber: -1,
            lang: "en"
        ),
        bookProgress: BookProgress(
            id: "",
            percentage: 0.0,
            page: 0,
            progressInPercentage: 0.0,
            finished: false,
            giveUp: false,
            startedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate()),
            finishedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate())
        ),
        bookProgressComments: []
    )
    return book
}

private func getPreviewFinishedBook() -> FinishedReadingBook {
    let book = FinishedReadingBook(
        bookWithProgress: BookWithProgress(
            book: Book(
                id: "",
                title: "Test book",
                authors: ["Glenn Swanson"],
                bookDescription: "",
                picture: "",
                thumbnail: "",
                publisher: "",
                credits: CreditsBO(),
                totalPages: 642,
                isbn: KotlinPair(first: "", second: ""),
                publishedDate: "",
                genres: [],
                totalComments: 0,
                peopleReadingIt: 6,
                readingStatus: BookReadingStatus.notRead,
                allReviews: [],
                userRating: nil,
                totalRating: 6,
                serie: "",
                serieNumber: -1,
                lang: "en"
            ),
            bookProgress: BookProgress(
                id: "",
                percentage: 0.0,
                page: 0,
                progressInPercentage: 0.0,
                finished: false,
                giveUp: false,
                startedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate()),
                finishedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate())
            ),
            bookProgressComments: []
        ),
        userRating: nil
    )
    
    return book
}
