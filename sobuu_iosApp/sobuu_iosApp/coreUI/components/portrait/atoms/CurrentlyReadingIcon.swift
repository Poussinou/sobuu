import SwiftUI

struct CurrentlyReadingIcon: View {
    
    let backIconColor = ColorToken.vermilion
    let frontIconColor = ColorToken.greenSheen
    let backIconSize: CGFloat = 16
    let frontIconSize: CGFloat = 10
    
    var body: some View {
        ZStack {
            IconToken.book
                .foregroundColor(backIconColor)
                .font(.system(size: backIconSize))
            IconToken.group
                .foregroundColor(frontIconColor)
                .font(.system(size: frontIconSize))
        }
    }
}

#Preview {
    CurrentlyReadingIcon()
}
