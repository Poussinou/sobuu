import SwiftUI

struct BookThumbnail: View {
    let imageURL: String?
    
    var body: some View {
        if(imageURL == nil || imageURL?.isEmpty ?? true) {
            // TODO add placeholder image in this project
            Image("book_cover_placeholder")
                .resizable()
        } else {
            AsyncImage(url: URL(string: imageURL!))
        }
    }
}

#Preview {
    BookThumbnail(imageURL: "")
}
