import SwiftUI
import shared

struct GetMoreBooksText: View {
    
    let onClick: () -> Void
    let displayMoreInfo: Bool
    
    
    let textColor = ColorToken.darkLava
    let paddingSize: CGFloat = 10
    
    var body: some View {
        HStack() {
            Text(SharedRes.strings().search_main_getMoreBooks.stringResource())
            
            if(displayMoreInfo) {
                IconToken.arrowUp
                    .foregroundStyle(textColor)
            } else {
                IconToken.arrowDown
                    .foregroundStyle(textColor)
            }
        }
        .font(CustomFont.bodySmall)
        .foregroundStyle(textColor)
        .frame(maxWidth: .infinity, alignment: .trailing)
        .padding(.horizontal, paddingSize)
    }
}

#Preview {
    GetMoreBooksText(
        onClick: {},
        displayMoreInfo: true
    ).loadCustomFonts()
}
