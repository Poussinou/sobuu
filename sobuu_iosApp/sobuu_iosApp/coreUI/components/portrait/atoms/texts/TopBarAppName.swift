import SwiftUI
import shared

struct TopBarAppName: View {
    
    let textColor = ColorToken.darkLava
    let backgroundColor = ColorToken.whiteBlue
    let text: String = SharedRes.strings().general_appName.stringResource()
    let font = CustomFont.headlineSmall
    let paddingSize: CGFloat = 10
    
    var body: some View {
        Text(text)
            .frame(maxWidth: .infinity)
            .font(font)
            .padding(paddingSize)
            .background(backgroundColor)
            .foregroundStyle(textColor)
    }
}

#Preview {
    TopBarAppName().loadCustomFonts()
}
