import SwiftUI

struct ErrorIndicator: View {
    let errorText: String?
    
    let horizontalPadding: CGFloat = 32
    let textColor = ColorToken.vermilion
    let textFont = CustomFont.bodyMedium
    
    var body: some View {
        Text(errorText ?? "")
            .font(textFont)
            .foregroundStyle(textColor)
            .padding(.horizontal, horizontalPadding)
    }
}

#Preview {
    ErrorIndicator(errorText: "Error while fetching the data from the books").loadCustomFonts()
}
