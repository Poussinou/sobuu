import SwiftUI
import shared

struct SobuuBigLogo: View {
    let contentColor:Color = ColorToken.darkLava
    var body: some View {
        Text(SharedRes.strings().general_appName.stringResource())
            .padding(EdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24))
            .font(CustomFont.logoFont)
            .foregroundColor(contentColor)
            .lineLimit(1)
            .frame(width: .infinity, alignment: .center)
    }
}

#Preview {
    SobuuBigLogo()
        .loadCustomFonts()
}
