import SwiftUI

struct CompleteRoundedOutlinedTextArea: View {
    @State var text: String
    var hintText: String
    var isError: Bool
    
    let radius: CGFloat = 10
    let contentColor: Color
    
    init(text: String, hintText: String, isError: Bool) {
        self.text = text
        self.hintText = hintText
        self.isError = isError
        
        if(text.isEmpty) {
            self.text = hintText
        } else {
            self.text = text
        }
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        TextEditor(text: $text)
        .font(CustomFont.bodyLarge)
        .foregroundColor(self.text == hintText ? ColorToken.spanishGrey : contentColor)
        .lineLimit(nil)
        .multilineTextAlignment(.leading)
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .background(ColorToken.whiteBlue)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.allCorners])
        .colorMultiply(ColorToken.whiteBlue)
        .onTapGesture {
            if self.text == hintText {
                self.text = ""
            }
        }
    }
}

#Preview {
    CompleteRoundedOutlinedTextArea(
        text: "",
        hintText: "Long text description",
        isError: false
    ).loadCustomFonts()
}
