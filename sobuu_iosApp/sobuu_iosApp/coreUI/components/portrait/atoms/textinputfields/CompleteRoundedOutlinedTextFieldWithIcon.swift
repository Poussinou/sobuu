import SwiftUI

struct CompleteRoundedOutlinedTextFieldWithIcon: View {
    @State var text: String
    var hintText: String
    var isError: Bool
    let icon: String
    
    let radius: CGFloat = 10
    
    let contentColor: Color
    let iconColor: Color = ColorToken.spanishGrey
    let iconSize: CGFloat = 24
    
    init(text: String, hintText: String, isError: Bool, icon: String) {
        self.text = text
        self.hintText = hintText
        self.isError = isError
        self.icon = icon
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundStyle(iconColor)
                .frame(width: iconSize, height: iconSize)
            
            TextField(
                hintText,
                text: $text
            )
                .font(CustomFont.bodyLarge)
                .foregroundColor(contentColor)
                .lineLimit(1)
        }
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .background(ColorToken.whiteBlue)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.allCorners])
    }
}

#Preview {
    CompleteRoundedOutlinedTextFieldWithIcon(
        text: "",
        hintText: "Last name",
        isError: false,
        icon: "person.fill"
    ).loadCustomFonts()
}

#Preview {
    CompleteRoundedOutlinedTextFieldWithIcon(
        text: "Wrong",
        hintText: "Last name",
        isError: true,
        icon: "person.fill"
    ).loadCustomFonts()
}
