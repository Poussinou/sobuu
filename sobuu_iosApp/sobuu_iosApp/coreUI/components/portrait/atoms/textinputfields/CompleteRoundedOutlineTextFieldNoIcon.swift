import SwiftUI

struct CompleteRoundedOutlineTextFieldNoIcon: View {
    @State var text: String
    var hintText: String
    var isError: Bool
    
    let radius: CGFloat = 10
    
    let contentColor: Color
    
    init(text: String, hintText: String, isError: Bool) {
        self.text = text
        self.hintText = hintText
        self.isError = isError
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
        
    var body: some View {
        TextField(
            hintText,
            text: $text
        )
            .font(CustomFont.bodyLarge)
            .foregroundColor(contentColor)
            .lineLimit(nil)
            .multilineTextAlignment(.leading)
            .frame(width: .infinity, alignment: .leading)
            .padding(.all, 10)
            .background(ColorToken.whiteBlue)
            .borderRadius(contentColor, cornerRadius: radius, corners: [.allCorners])
    }
}

#Preview {
    CompleteRoundedOutlineTextFieldNoIcon(
        text: "",
        hintText: "Title",
        isError: false
    ).loadCustomFonts()
}

#Preview {
    CompleteRoundedOutlineTextFieldNoIcon(
        text: "Wrong",
        hintText: "Title",
        isError: true
    ).loadCustomFonts()
}
