import SwiftUI

struct TopRoundedOutlinedTextField: View {
    var hintText: String
    var isError: Bool
    @State var text: String
    
    let iconColor: Color = ColorToken.spanishGrey
    let contentColor: Color
    let iconSize: CGFloat = 24
    let radius: CGFloat = 10
    let icon: String
    
    init(text: String, hintText: String, icon: String, isError: Bool) {
        self.hintText = hintText
        self.text = text
        self.isError = isError
        self.icon = icon
        
        contentColor = if(isError) {
            ColorToken.vermilion
        } else {
            ColorToken.darkLava
        }
    }
    
    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundStyle(iconColor)
                .frame(width: iconSize, height: iconSize)
            
            TextField(
                hintText,
                text: $text
            )
            .font(CustomFont.bodyLarge)
            .foregroundColor(contentColor)
        }
        .frame(width: .infinity, alignment: .leading)
        .padding(.all, 10)
        .background(ColorToken.whiteBlue)
        .borderRadius(contentColor, cornerRadius: radius, corners: [.topLeft, .topRight])
    }
}

#Preview {
    TopRoundedOutlinedTextField(
        text: "",
        hintText: "Username",
        icon: "person.fill",
        isError: false
    )
    .loadCustomFonts()
}

#Preview {
    TopRoundedOutlinedTextField(
        text: "Wrong",
        hintText: "Username",
        icon: "person.fill",
        isError: true
    )
    .loadCustomFonts()
}
