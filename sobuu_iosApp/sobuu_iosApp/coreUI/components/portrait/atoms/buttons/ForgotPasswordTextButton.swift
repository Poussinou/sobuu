import SwiftUI
import shared

struct ForgotPasswordTextButton: View {
    let action: () -> Void
    
    init(
        action: @escaping () -> Void
    ) {
        self.action = action
    }

    var body: some View {
        Button(action: action) {
            Text(SharedRes.strings().authorization_auth_forgotPassword.stringResource())
                .font(CustomFont.bodyMedium)
                .foregroundColor(ColorToken.darkLava)
                .underline()
                .frame(maxWidth: .infinity, alignment: .center)
        }
    }
}

#Preview {
    BaseContentView {
        ForgotPasswordTextButton(action: {})
            .loadCustomFonts()
    }
}
