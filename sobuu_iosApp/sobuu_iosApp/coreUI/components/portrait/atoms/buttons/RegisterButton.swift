import SwiftUI
import shared

struct RegisterButton: View {
    let textColor = ColorToken.whiteBlue
    let buttonColor = ColorToken.darkLava
    
    var didClickButton: () -> Void
    
    init(didClickButton: @escaping () -> Void) {
        self.didClickButton = didClickButton
    }
    
    var body: some View {
        Button(action: didClickButton) {
                Label(
                    title: {Text(SharedRes.strings().authorization_auth_createNewAccount.stringResource())},
                    icon: {}
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(textColor)
                .frame(maxWidth: .infinity)
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            }
        .frame(maxWidth: .infinity)
        .background(buttonColor)
        .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
        
    }
}

#Preview {
    RegisterButton(didClickButton: {})
}
