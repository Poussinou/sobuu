import SwiftUI
import shared

struct LoginCircleButton: View {
    let iconColor = ColorToken.whiteBlue
    let buttonColor = ColorToken.vermilion
    
    let buttonSize: CGFloat = 62
    let iconSize: CGFloat = 32
    
    var didClickLoginButton: () -> Void
    
    init(didClickLoginButton: @escaping () -> Void) {
        self.didClickLoginButton = didClickLoginButton
    }
    
    var body: some View {
        HStack {
            Button(action: didClickLoginButton) {
                Image(systemName: "arrow.right")
                    .resizable()
                    .frame(width: iconSize, height: iconSize)
                    .foregroundColor(iconColor)
            }
            .frame(width: buttonSize, height: buttonSize)
            .background(buttonColor)
            .clipShape(Circle())
        }
    }
}

#Preview {
    LoginCircleButton(didClickLoginButton: {})
}
