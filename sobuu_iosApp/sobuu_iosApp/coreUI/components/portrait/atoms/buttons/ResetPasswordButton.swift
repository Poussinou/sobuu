import SwiftUI
import shared

struct ResetPasswordButton: View {
    var didClickButton: () -> Void
    
    let textColor: Color = ColorToken.whiteBlue
    let buttonColor: Color = ColorToken.vermilion
    
    
    init(didClickButton: @escaping () -> Void) {
        self.didClickButton = didClickButton
    }
    
    var body: some View {
        Button(action: didClickButton) {
                Label(
                    title: { Text(SharedRes.strings().authorization_auth_resetPassword.stringResource()) },
                    icon: {}
                )
                .font(CustomFont.bodyLarge)
                .foregroundColor(textColor)
                .frame(maxWidth: .infinity)
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            }
        .buttonStyle(.plain)
        .frame(maxWidth: .infinity)
        .background(buttonColor)
        .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
    }
}

#Preview {
    ResetPasswordButton(didClickButton: {})
}
