import SwiftUI
import shared

struct BookCardLandscape: View {
    let picture: String?
    let progress: Double
    let startedToRead: Date?
    let finishedToRead: Date?
    let title: String
    let authors: String
    let finished: Bool
    let giveUp: Bool
    
    let backgroundColor: Color = ColorToken.darkLava
    let textColor: Color = ColorToken.whiteBlue
    
    var startedDateText: String = ""
    var finishedDateText: String = ""
    let cardCornerRadius: CGFloat = 20
    
    init(picture: String?, progress: Double, startedToRead: Date?, finishedToRead: Date?, title: String, authors: String, finished: Bool, giveUp: Bool) {
        self.picture = picture
        self.progress = progress
        self.startedToRead = startedToRead
        self.finishedToRead = finishedToRead
        self.title = title
        self.authors = authors
        self.finished = finished
        self.giveUp = giveUp
        
        startedDateText = "\(SharedRes.strings().home_main_startedBookOn.stringResource()) \(startedToRead.toStringDateWithDayAndTime())"
        
        if (finished || giveUp) {
            if(finished) {
                finishedDateText = "\(SharedRes.strings().home_main_finishedBookOn.stringResource()) \(finishedToRead.toStringDateWithDayAndTime())"
            } else {
                finishedDateText = "\(SharedRes.strings().home_main_gaveUpBookOn.stringResource()) \(finishedToRead.toStringDateWithDayAndTime())"
            }
        } else {
            finishedDateText = ""
        }
    }
    
    var body: some View {
        HStack {
            Spacer()
            
            ZStack {
                BookThumbnail(imageURL: picture)
                    .frame(maxWidth: 250, maxHeight: 400)
                    .overlay(
                        CircularProgressIndicator(progress: progress)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 30, trailing: 10)),
                        alignment: .bottomTrailing
                    )
            }
            .padding(10)
            
            VStack {
                Text(startedDateText)
                    .font(CustomFont.bodySmall)
                    .foregroundStyle(textColor)
                    .frame(maxWidth: .infinity, alignment: Alignment.leading)
                    .padding(EdgeInsets(top: 30, leading: 10, bottom: 1, trailing: 10))
                
                if(finishedToRead != nil && (finished || giveUp)) {
                    Text(finishedDateText)
                        .font(CustomFont.bodySmall)
                        .foregroundStyle(textColor)
                        .frame(maxWidth: .infinity, alignment: Alignment.leading)
                        .padding(EdgeInsets(top: 0, leading: 10, bottom: 30, trailing: 10))
                }
                
                Text(title)
                    .font(CustomFont.bodyLarge)
                    .foregroundStyle(textColor)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, alignment: Alignment.center)
                    .padding(EdgeInsets(top: 30, leading: 10, bottom: 30, trailing: 10))
                
                Text(authors)
                    .font(CustomFont.bodyMedium)
                    .foregroundStyle(textColor)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, alignment: Alignment.center)
                    .padding(EdgeInsets(top: 30, leading: 10, bottom: 30, trailing: 10))
                
                Spacer()
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(backgroundColor)
        .cornerRadius(cardCornerRadius)
    }
}

#Preview {
    BookCardLandscape(
        picture: "",
        progress: 93.0,
        startedToRead: Date(),
        finishedToRead: Date(),
        title: "Testing how long can be a book title without looking uncomfortable",
        authors: "Ryan Conwail",
        finished: true,
        giveUp: false
    )
}
