import SwiftUI
import shared

struct GaveUpBooksSectionLandscape: View {
    let bookList: [BookWithProgress]?
    
    @State var currentIndex: Int = 0
    @GestureState var dragOffset: CGFloat = 0
    
    var body: some View {
        GeometryReader { geo in
            ZStack() {
                ForEach(0..<bookList!.count, id: \.self) { index in
                    let book = bookList![index].book
                    let bookProgress = bookList![index].bookProgress
                    
                    let finishedDate = bookProgress.finishedToRead
                    let startingDate = bookProgress.startedToRead
                    BookCardLandscape(
                        picture: book.picture,
                        progress: bookProgress.progressInPercentage,
                        startedToRead: DateTimeHelper<NSDate>().fromLocalDateTime(dateTime: startingDate) as Date?,
                        finishedToRead: finishedDate != nil ? DateTimeHelper<NSDate>().fromLocalDateTime(dateTime: finishedDate!) as Date? : nil,
                        title: book.title,
                        authors: book.authors.joined(separator: ","),
                        finished: bookProgress.finished,
                        giveUp: bookProgress.giveUp
                    )
                    .frame(width: geo.size.width - 80)
                    .opacity(self.currentIndex == index ? 1.0 : 0.5)
                    .scaleEffect(self.currentIndex == index ? 1.0 : 0.8)
                    .offset(x: CGFloat(index - self.currentIndex) * 600 + self.dragOffset, y: 0)
                }
            }
            .padding(.top, 20)
            .gesture(
                DragGesture()
                    .onEnded({ value in
                        let threshold: CGFloat = 50
                        if (value.translation.width > threshold) {
                            withAnimation {
                                currentIndex = max(0, self.currentIndex - 1)
                            }
                        } else if(value.translation.width < -threshold) {
                            withAnimation {
                                currentIndex = min(self.bookList!.count - 1, self.currentIndex + 1)
                            }
                        }
                    })
            )
        }
    }
}

#Preview {
    GaveUpBooksSectionLandscape(
        bookList: [getPreviewBook(), getPreviewBook(), getPreviewBook()]
    )
}
private func getPreviewBook() -> BookWithProgress {
    let book = BookWithProgress(
        book: Book(
            id: "",
            title: "Test book",
            authors: ["Glenn Swanson"],
            bookDescription: "",
            picture: "",
            thumbnail: "",
            publisher: "",
            credits: CreditsBO(),
            totalPages: 642,
            isbn: KotlinPair(first: "", second: ""),
            publishedDate: "",
            genres: [],
            totalComments: 0,
            peopleReadingIt: 6,
            readingStatus: BookReadingStatus.notRead,
            allReviews: [],
            userRating: nil,
            totalRating: 6,
            serie: "",
            serieNumber: -1,
            lang: "en"
        ),
        bookProgress: BookProgress(
            id: "",
            percentage: 0.0,
            page: 0,
            progressInPercentage: 0.0,
            finished: false,
            giveUp: false,
            startedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate()),
            finishedToRead: DateTimeHelper().toLocalDateTime(dateTime: NSDate())
        ),
        bookProgressComments: []
    )
    return book
}
