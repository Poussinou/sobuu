import SwiftUI
import shared

struct HomeViewLandscape: View {
    var isLoading: Bool
    var errorText: String?
    var currentSection: BookStatusType
    var onCurrentBookClicked: (String) -> Void
    var onDisplayCurrentlyReadingMenuClicked: () -> Void
    var onDisplayFinishedMenuClicked: () -> Void
    var onDisplayGaveUpMenuClicked: () -> Void
    var currentlyReadingBookList: Array<BookWithProgress>?
    var gaveUpBookList: Array<BookWithProgress>?
    var finishedBooksList: Array<FinishedReadingBook>?
    
    var body: some View {
        BookSectionsBlockLandscape(
            isLoading: isLoading,
            errorText: errorText,
            currentSection: currentSection,
            onCurrentBookClicked: onCurrentBookClicked,
            onDisplayCurrentlyReadingMenuClicked: onDisplayCurrentlyReadingMenuClicked,
            onDisplayFinishedMenuClicked: onDisplayFinishedMenuClicked,
            onDisplayGaveUpMenuClicked: onDisplayGaveUpMenuClicked,
            currentlyReadingBookList: currentlyReadingBookList,
            gaveUpBookList: gaveUpBookList,
            finishedBooksList: finishedBooksList
        )
    }
}

#Preview {
    HomeViewLandscape(
        isLoading: false,
        currentSection: BookStatusType.ALREADY_READ,
        onCurrentBookClicked: {_ in },
        onDisplayCurrentlyReadingMenuClicked: {},
        onDisplayFinishedMenuClicked: {},
        onDisplayGaveUpMenuClicked: {}
    ).loadCustomFonts()
}
