import SwiftUI
import shared

struct SentEmailViewLandscape: View {
    let onButtonClick: () -> Void
    let emailType: EmailType
    
    var body: some View {
        
        let backgroundColor: Color = ColorToken.greenSheen
        let textColor: Color = ColorToken.whiteBlue
        
        ZStack {
            backgroundColor.ignoresSafeArea()
            VStack(spacing: 0) {
                CrossButtonIcon(onButtonClick: onButtonClick)
                    .frame(maxWidth: .infinity, alignment: .topTrailing)
                    .padding(22)
                
                // TODO Add Lotti animation
                
                Spacer()
                
                ZStack {
                    if(emailType == EmailType.VERIFICATION) {
                        Text(SharedRes.strings().authorization_auth_verificationEmail.stringResource())
                    } else {
                        Text(SharedRes.strings().authorization_auth_resetPasswordEmail.stringResource())
                    }
                }
                .font(CustomFont.bodyLarge)
                .padding(.horizontal, 50)
                .multilineTextAlignment(.center)
                .foregroundStyle(textColor)
                
                
                Spacer()
            }
        }
    }
}

#Preview {
    SentEmailViewLandscape(
        onButtonClick: {},
        emailType: EmailType.RESET_PASSWORD
    ).loadCustomFonts()
}
