import SwiftUI

enum Solway: String, CaseIterable {
    case regular = "Solway-Regular"
    case medium = "Solway-Medium"
    case semibold = "Solway-ExtraBold"
    case bold = "Solway-Bold"
}

enum SourceSans: String, CaseIterable {
    case regular = "SourceSans3-Regular"
    case medium = "SourceSans3-Black"
    case semibold = "SourceSans3-Semibold"
    case bold = "SourceSans3-Bold"
}

public enum CustomFont {
    public static func registerFonts() {
        SourceSans.allCases.forEach {
            registerFont(bundle: .main, fontName: $0.rawValue, fontExtension: "ttf")
        }
        Solway.allCases.forEach {
            registerFont(bundle: .main, fontName: $0.rawValue, fontExtension: "ttf")
        }
    }
    
    fileprivate static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) {
        guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension),
              let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
              let font = CGFont(fontDataProvider) else {
            fatalError("Couldn't create font from filename: \(fontName) with extension \(fontExtension)")
        }
        
        var error: Unmanaged<CFError>?
        CTFontManagerRegisterGraphicsFont(font, &error)
    }
    
    private static func makeFont(_ font: SourceSans, size: CGFloat) -> Font {
        .custom(font.rawValue, size: size, relativeTo: .body)
    }
    
    private static func makeFontSolway(_ font: Solway, size: CGFloat) -> Font {
        .custom(font.rawValue, size: size, relativeTo: .body)
    }
    
    static let heading1 = makeFont(.bold, size: 24)
    static let heading2 = makeFont(.bold, size: 20)
    static let title1 = makeFont(.semibold, size: 18)
    static let title2 = makeFont(.semibold, size: 16)
    static let title3 = makeFont(.semibold, size: 14)
    static let body1 = makeFont(.regular, size: 16)
    static let body2 = makeFont(.regular, size: 14)
    static let caption1 = makeFont(.regular, size: 12)
    static let caption2 = makeFont(.regular, size: 10)
    static let button = makeFont(.medium, size: 14)
    static let logoFont = makeFontSolway(.regular, size: 100)
    
    static let bodyLarge = makeFont(.regular, size: 20)
    static let bodyMedium = makeFont(.regular, size: 16)
    static let bodySmall = makeFont(.regular, size: 12)
    static let titleLarge = makeFont(.medium, size: 32)
    static let headlineSmall = makeFontSolway(.medium, size: 38)
}

extension View {
    /// Attach this to any Xcode Preview's view to have custom fonts displayed
    /// Note: Not needed for the actual app
    public func loadCustomFonts() -> some View {
        CustomFont.registerFonts()
        return self
    }
}
