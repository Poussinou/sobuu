import SwiftUI

enum ColorToken {
    static var greenSheen: Color {
        return Color("GreenSheen")
    }
    
    static var  darkLava: Color {
        return Color("DarkLava")
    }
    
    static var  vermilion: Color {
        return Color("Vermilion")
    }
    
    static var  spanishGrey: Color {
        return Color("SpanishGrey")
    }
    
    static var  whiteBlue: Color {
        return Color("WhiteBlue")
    }
}
