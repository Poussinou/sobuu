import Foundation
import SwiftUI

extension Font {
    static var SolwayTopBarTitle: Font {
        return Font.custom("Solway-Regular", size: 20)
    }
    
    static var SolwayAppLogo: Font {
        return Font.custom("Solway-Regular", size: 88)
    }
    
    static var Solway: Font {
        return Font.custom("Solway-Regular", size: 16)
    }
    
    static var SourceSans: Font {
        return Font.custom("SourceSans3-Regular", size: 16)
    }
    
    static var SourceSansSmall: Font {
        return Font.custom("SourceSans3-Regular", size: 14)
    }
    
    static var SourceSansBigBold: Font {
        return Font.custom("SourceSans3-Bold", size: 32)
    }
    
    static var SourceSansBigRegular: Font {
        return Font.custom("SourceSans3-Regular", size: 24)
    }
}
