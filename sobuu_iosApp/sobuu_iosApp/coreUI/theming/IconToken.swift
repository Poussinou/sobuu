import Foundation
import SwiftUI


enum IconToken {
    static var searchIcon: Image {
        return Image(systemName: "magnifyingglass")
    }
    
    static var  cancelIcon: Image {
        return Image(systemName: "xmark.circle")
    }
    
    static var  person: Image {
        return Image(systemName: "person.fill")
    }
    
    static var  profile: Image {
        return Image(systemName: "person.circle.fill")
    }
    
    static var  star: Image {
        return Image(systemName: "star.fill")
    }
    
    static var  book: Image {
        return Image(systemName: "book.fill")
    }
    
    static var  group: Image {
        return Image(systemName: "person.3.fill")
    }
    
    static var  comment: Image {
        return Image(systemName: "bubble.right")
    }
    
    static var  arrowUp: Image {
        return Image(systemName: "chevron.up")
    }
    
    static var  arrowDown: Image {
        return Image(systemName: "chevron.down")
    }
}
