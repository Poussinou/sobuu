package com.sobuumedia.sobuu.analytics.matomo_kit.remote

interface IMatomoRemoteData {
    suspend fun sendAnalyticsData(actionName: String, resolution: String, operativeSystem: String,
                                  osVersion: String, appVersion: String, language: String,
                                  otherVariables: Map<String, String>)
}