package com.sobuumedia.sobuu.analytics.plausible_kit.remote

import de.jensklingenberg.ktorfit.http.Field
import de.jensklingenberg.ktorfit.http.FormUrlEncoded
import de.jensklingenberg.ktorfit.http.Headers
import de.jensklingenberg.ktorfit.http.POST

interface PlausibleApi {

    @Headers("Content-Type:application/json")
    @POST("api/event")
    @FormUrlEncoded
    suspend fun sendTrackingEvent(
        @Field("domain") siteId: String = "sobuu",
        @Field("url") url: String,
        @Field("name") actionName: String,
    )
}