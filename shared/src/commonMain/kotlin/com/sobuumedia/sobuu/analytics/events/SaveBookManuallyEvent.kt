package com.sobuumedia.sobuu.analytics.events

import com.sobuumedia.sobuu.analytics.AnalyticsEvent

class SaveBookManuallyEvent(
    override val name: String,
    override val otherVariables: Map<String, String>
) : AnalyticsEvent {
    override fun getEventName(): String {
        return name
    }
}