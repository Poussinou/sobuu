package com.sobuumedia.sobuu.analytics.exceptions

import com.sobuumedia.sobuu.analytics.events.base.Event


class UnsupportedEventException(event: Event) : UnsupportedOperationException() {
    override val message: String = "couldn't fire \"${event.name}\" event"
}