package com.sobuumedia.sobuu.analytics.exceptions

import com.sobuumedia.sobuu.analytics.AnalyticsDispatcher
import com.sobuumedia.sobuu.analytics.events.base.Event

class EventNotTrackedException(message: String?, cause: Throwable?) : RuntimeException(message, cause) {
    constructor(dispatcher: AnalyticsDispatcher, event: Event, t: Throwable) : this("${dispatcher.dispatcherName} dispatcher couldn't fire \"${event.name}\" event", t)
}