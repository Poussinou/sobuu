package com.sobuumedia.sobuu.analytics

/**
 * Represents a service, such as Answers or Firebase
 */
interface AnalyticsKit {
    val name: String

}