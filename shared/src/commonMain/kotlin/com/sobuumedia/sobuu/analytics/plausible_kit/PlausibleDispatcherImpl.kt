package com.sobuumedia.sobuu.analytics.plausible_kit

import com.sobuumedia.sobuu.analytics.AnalyticsContentView
import com.sobuumedia.sobuu.analytics.AnalyticsDispatcher
import com.sobuumedia.sobuu.analytics.AnalyticsEvent
import com.sobuumedia.sobuu.analytics.AnalyticsKit
import com.sobuumedia.sobuu.analytics.plausible_kit.remote.IPlausibleRemoteData
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PlausibleDispatcherImpl (override val init: Boolean,
                               override val resolution: String,
                               override val operativeSystem: String,
                               override val osVersion: String,
                               override val appVersion: String,
                               override val language: String
) : AnalyticsDispatcher, KoinComponent {
    override val dispatcherName: String = "PlausibleDispatcher"
    private val plausibleRemote: IPlausibleRemoteData by inject()

    constructor() : this(true, "", "", "","", "")

    override val kit: AnalyticsKit = PlausibleKit.instance

    override fun initDispatcher() {
        // call custom analytics initiation function
    }

    override fun trackContentView(contentView: AnalyticsContentView) {
        // track content view
    }

    override fun trackCustomEvent(event: AnalyticsEvent) {
        // track event
        if(init) {
            MainScope().launch {
                plausibleRemote.sendAnalyticsData(
                    resolution = resolution,
                    operativeSystem = operativeSystem,
                    osVersion = osVersion,
                    appVersion = appVersion,
                    language = language,
                    actionName = event.getEventName(),
                    otherVariables = event.otherVariables
                )
            }
        }
    }
}
