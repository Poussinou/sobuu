package com.sobuumedia.sobuu.analytics.matomo_kit

import com.sobuumedia.sobuu.analytics.AnalyticsKit

class MatomoKit : AnalyticsKit {
    override val name: String = "Matomo Dispatcher"

    private object Holder {
        val INSTANCE = MatomoKit()
    }

    companion object {
        val instance: MatomoKit by lazy { Holder.INSTANCE }
    }
}


