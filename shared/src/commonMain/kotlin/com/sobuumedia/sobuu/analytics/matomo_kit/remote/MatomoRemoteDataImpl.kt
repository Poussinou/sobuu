package com.sobuumedia.sobuu.analytics.matomo_kit.remote

import com.sobuumedia.sobuu.analytics.utils.toAnalyticsString
import com.sobuumedia.sobuu.analytics.utils.toJsonObject

class MatomoRemoteDataImpl(private val api: MatomoApi): IMatomoRemoteData {
    override suspend fun sendAnalyticsData(
        actionName: String, resolution: String,
        operativeSystem: String, osVersion: String,
        appVersion: String, language: String,
        otherVariables: Map<String, String>
    ) {

        var variablesMap: Map<String, String> = mutableMapOf()
        variablesMap = variablesMap.plus("os" to operativeSystem)
        variablesMap = variablesMap.plus("osVersion" to osVersion)
        variablesMap = variablesMap.plus("appVersion" to appVersion)
        variablesMap = variablesMap.plus("lang" to language)
        variablesMap = variablesMap.plus(otherVariables)

        val customVariablesString = variablesMap.toAnalyticsString()
        val customVariables = variablesMap.toJsonObject()

        api.sendTrackingEvent(
            actionName = "[resolution: $resolution * $customVariablesString]",
            url = "app://sobuu/$actionName",
            resolution = resolution,
            customVariables = customVariables
        )
    }
}