package com.sobuumedia.sobuu.analytics.plausible_kit.remote

import com.sobuumedia.sobuu.analytics.utils.toAnalyticsString

class PlausibleRemoteDataImpl(private val api: PlausibleApi): IPlausibleRemoteData {
    override suspend fun sendAnalyticsData(actionName: String, resolution: String,
                                           operativeSystem: String, osVersion: String,
                                           appVersion: String, language: String,
                                           otherVariables: Map<String, String>) {

        var variablesMap: Map<String, String> = mutableMapOf()
        variablesMap = variablesMap.plus("os" to operativeSystem)
        variablesMap = variablesMap.plus("osVersion" to osVersion)
        variablesMap = variablesMap.plus("appVersion" to appVersion)
        variablesMap = variablesMap.plus("lang" to language)
        variablesMap = variablesMap.plus(otherVariables)

        val customVariables = variablesMap.toAnalyticsString()

        api.sendTrackingEvent(
            actionName = actionName,
            url = "app://sobuu/$customVariables"
        )
    }
}