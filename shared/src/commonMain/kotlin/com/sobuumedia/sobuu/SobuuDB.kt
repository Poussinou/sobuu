package com.sobuumedia.sobuu

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.driver.bundled.BundledSQLiteDriver
import com.sobuumedia.sobuu.core.database.BookDBApi
import com.sobuumedia.sobuu.core.database.BookProgressDBApi
import com.sobuumedia.sobuu.core.database.BookRatingDBApi
import com.sobuumedia.sobuu.core.database.CommentDBApi
import com.sobuumedia.sobuu.core.database.CreditsDBApi
import com.sobuumedia.sobuu.core.database.ListConverter
import com.sobuumedia.sobuu.models.db_models.BookDB
import com.sobuumedia.sobuu.models.db_models.BookProgressDB
import com.sobuumedia.sobuu.models.db_models.CommentDB
import com.sobuumedia.sobuu.models.db_models.CreditsDB
import com.sobuumedia.sobuu.models.db_models.ProfileDB
import com.sobuumedia.sobuu.models.db_models.UserBookRatingDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO

internal const val dbFileName = "sobuuDB.db"

@Database(entities = [BookDB::class, BookProgressDB::class, CommentDB::class, CreditsDB::class,
                     ProfileDB::class, UserBookRatingDB::class], version = 1, exportSchema = true)
@TypeConverters(ListConverter::class)
abstract class SobuuDB : RoomDatabase() {
    abstract fun getBookDao(): BookDBApi
    abstract fun getBookProgressDao(): BookProgressDBApi
    abstract fun getCommentDao(): CommentDBApi
    abstract fun getCreditsDao(): CreditsDBApi
    abstract fun getBookRatingsDao(): BookRatingDBApi
}

fun getRoomDatabase(
    builder: RoomDatabase.Builder<SobuuDB>
): SobuuDB {
    return builder
        // Info about migrations: https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
        //.addMigrations(MIGRATIONS)
        .fallbackToDestructiveMigrationOnDowngrade(false)
        .setDriver(BundledSQLiteDriver())
        .setQueryCoroutineContext(Dispatchers.IO)
        .build()
}
