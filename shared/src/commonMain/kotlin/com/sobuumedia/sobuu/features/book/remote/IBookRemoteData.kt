package com.sobuumedia.sobuu.features.book.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook

interface IBookRemoteData {

    suspend fun getUserCurrentReadingBook(sessionToken: String?) : BookResult<List<BookWithProgress>>

    suspend fun getUserAlreadyReadBooks(sessionToken: String?) : BookResult<List<FinishedReadingBook>>

    suspend fun getUserGiveUpBooks(sessionToken: String?) : BookResult<List<BookWithProgress>>

    suspend fun searchBook(sessionToken: String?, term: String, language: String) : BookResult<List<Book>>

//    suspend fun getCommentsFromPage(sessionToken: String?, bookId: String, pageNum: Number) : BookResult<List<Comment>>

//    suspend fun getCommentsFromPercentage(sessionToken: String?, bookId: String, percentage: Number) : BookResult<List<Comment>>

    suspend fun setBookCurrentlyReading(sessionToken: String?, bookId: String, title: String, isbn: String?) : BookResult<Unit>

    suspend fun reportMissingDataInBook(sessionToken: String?, bookId: String, title: String, isbn: String?) : BookResult<Unit>

    suspend fun getBookById(sessionToken: String?, bookId: String) : BookResult<Book>

    suspend fun getBookProgress(sessionToken: String?, bookId: String) : BookResult<BookWithProgress>

    suspend fun updateBookProgress(sessionToken: String?, bookId: String, percentage: Double?,
                                   page: Int?, finished: Boolean, giveUp: Boolean) : BookResult<BookWithProgress>

    suspend fun searchISBN(sessionToken: String?, isbn: String) : BookResult<List<Book>>

//    suspend fun rateBook(sessionToken: String?, bookId: String, rate: Double, reviewText: String) : BookResult<Book>

//    suspend fun getUserRatingInBook(sessionToken: String?, bookId: String) : BookResult<UserBookRating>

//    suspend fun getRatingListFromBook(sessionToken: String?, bookId: String) : BookResult<List<UserBookRating>>

//    suspend fun removeRating(sessionToken: String?, rateId: String) : BookResult<Unit>

    suspend fun finishBook(sessionToken: String?, bookId: String,
                           rateNumber: Float, rateText: String) : BookResult<Boolean>

    suspend fun giveUpWithBook(sessionToken: String?, bookId: String) : BookResult<Boolean>

    suspend fun saveBook(sessionToken: String?, title: String, authors: List<String>,
                         description: String, picture: String, thumbnail: String, publisher: String,
                         credits: CreditsBO, totalPages: Int, isbn: Pair<String, String>,
                         publishedDate: String, genres: List<String>, language: String,
                         serie: String, seriesNumber: Int) : BookResult<Book>

    suspend fun saveBookManually(sessionToken: String?, title: String, authors: List<String>,
                                 description: String?, picture: ByteArray, thumbnail: ByteArray, publisher: String,
                                 credits: CommonItemApi.CreditsItemApi?, totalPages: Int, isbn: Pair<String, String>, publishedDate: String,
                                 genres: List<String>, language: String, serie: String, seriesNumber: String) : BookResult<Book>
}