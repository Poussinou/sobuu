package com.sobuumedia.sobuu.features.comments.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi.CommentItemApi
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.ProfileItemApi
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.Profile
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

// region Converters
fun List<CommentItemApi>.toCommentsList(): List<Comment>? {
    return this.map {
        it.toComment()
    }
}

/**
 * Convert single comment api item to comment business object
 */
fun CommentItemApi.toComment(): Comment {
    return Comment(
        id = this.id,
        user = this.profile.toProfile(),
        publishedDate =  Instant.parse(this.createdAt)
            .toLocalDateTime(TimeZone.currentSystemDefault()),
        text = this.commentText,
        hasSpoilers = this.hasSpoilers,
        votesCounter = this.votes,
        percentage = this.percentage,
        pageNumber = this.pageNumber,
        parentCommentId = this.parentCommentID
    )
}

fun List<CommentItemApi>.toCommentList(): List<Comment> {
    return this.map {
        it.toComment()
    }
}

fun ProfileItemApi.toProfile(): Profile {
    return Profile(
        id = this.id,
        firstName = this.firstname ?: "",
        lastName = this.lastname ?: "",
        username = this.username,
        email = "", // Here is not needed to display the email
        createdAt = this.createdAt ?: ""
    )
}

// endregion