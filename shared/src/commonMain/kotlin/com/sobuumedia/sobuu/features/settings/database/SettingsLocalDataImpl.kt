package com.sobuumedia.sobuu.features.settings.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager

class SettingsLocalDataImpl(
    private val prefs: KMMStorage
): ISettingsLocalData, SessionTokenManager(prefs) {

    object SettingsConstants {
        const val THEME: String = "theme"
        const val ANALYTICS: String = "analytics"
    }

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun getRefreshToken(): String {
        return super.obtainRefreshToken()
    }

    override suspend fun setSessionToken(token: String?) {
        prefs.putString(Constants.SESSION_TOKEN_KEY, token)
    }

    override suspend fun setRefreshToken(token: String?) {
        prefs.putString(Constants.REFRESH_TOKEN_KEY, token)
    }

    override suspend fun getThemeMode(): Int {
        return prefs.getInt(SettingsConstants.THEME)
    }

    override suspend fun setThemeMode(mode: Int) {
        prefs.putInt(SettingsConstants.THEME, mode)
    }

    override suspend fun getAnalyticsEnabled(): Boolean {
        return prefs.getBoolean(SettingsConstants.ANALYTICS)
    }

    override suspend fun setAnalyticsEnabled(enabled: Boolean) {
        prefs.putBoolean(SettingsConstants.ANALYTICS, enabled)
    }
}