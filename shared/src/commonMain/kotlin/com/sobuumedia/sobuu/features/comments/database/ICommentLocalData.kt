package com.sobuumedia.sobuu.features.comments.database

import com.sobuumedia.sobuu.features.comments.remote.CommentResult
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment

interface ICommentLocalData {
    suspend fun getSessionToken(): String?

    suspend fun saveCommentDataInDB(bookId: String,comment: Comment)

    suspend fun saveCommentListDataInDB(bookId: String,comments: List<Comment>)

    suspend fun getCommentByID(commentId: String): CommentResult<Comment>

    suspend fun getCommentsByBookID(bookId: String): CommentResult<List<Comment>>

    suspend fun getAllComments(): CommentResult<List<Comment>>

    suspend fun getBookById(bookId: String): CommentResult<Book>

    suspend fun getBookProgressById(bookId: String): CommentResult<BookWithProgress>
}