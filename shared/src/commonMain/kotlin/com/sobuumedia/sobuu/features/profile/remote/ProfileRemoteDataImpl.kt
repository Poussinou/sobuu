package com.sobuumedia.sobuu.features.profile.remote

import co.touchlab.skie.configuration.annotations.SuspendInterop
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.ProfileItemApi
import com.sobuumedia.sobuu.models.bo_models.Profile
import kotlinx.serialization.Serializable

@Serializable
data class ProfileDataRequest(val type: String = "GetUserProfile")

@Serializable
data class LogoutUserRequest(val type: String = "LogoutUser")

class ProfileRemoteDataImpl(
    private val api: ProfileApi
): IProfileRemoteData {

    @SuspendInterop.Enabled
    override suspend fun getUserProfile(sessionToken: String?): ProfileResult<Profile> {
        val result = execute(sessionToken) {
            api.getUserProfile(
                sessionToken = it,
                data = ProfileDataRequest()
            )
        }

        return if(result.data != null) {
            ProfileResult.Success(data = result.data.toProfile())
        } else {
            ProfileResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun logout(sessionToken: String?): ProfileResult<Boolean> {
        val result = execute(sessionToken) {
            api.logout(
                sessionToken = it,
                data = LogoutUserRequest()
            )
        }

        return if(result.data != null) {
            ProfileResult.Success(data = result.data)
        } else {
            ProfileResult.Error(error = result.error)
        }
    }

    /*
//    override suspend fun getProfileFromId(
//        sessionToken: String?,
//        profileId: String
//    ): ProfileResult<Profile> {
//        if (profileId.isBlank()) return ProfileResult.Error(ProfileError.InvalidProfileIdError)
//        return execute(sessionToken) {
//            api.getProfileFromId(it, profileId)
//        }
//    }
//
//    override suspend fun followProfile(
//        sessionToken: String?,
//        profileId: String
//    ): ProfileResult<Profile> {
//        if (profileId.isBlank()) return ProfileResult.Error(ProfileError.InvalidProfileIdError)
//        return execute(sessionToken) {
//            api.followProfile(it, profileId)
//        }
//    }
//
//    override suspend fun unfollowProfile(
//        sessionToken: String?,
//        profileId: String
//    ): ProfileResult<Profile> {
//        if (profileId.isBlank()) return ProfileResult.Error(ProfileError.InvalidProfileIdError)
//        return execute(sessionToken) {
//            api.unfollowProfile(it, profileId)
//        }
//    }
//
//    override suspend fun getFollowingProfiles(sessionToken: String?): ProfileResult<List<Profile>> = execute(sessionToken) {
//        api.getFollowingProfiles(sessionToken = it)
//    }
//
     */

    @SuspendInterop.Enabled
    private suspend fun <T>execute(
        sessionToken: String?,
        func: suspend (sessionToken: String) -> T
    ): ProfileResult<T> {
        return try {
            if (sessionToken.isNullOrBlank()) return ProfileResult.Error(ProfileError.InvalidSessionTokenError)

            val result = func("Bearer $sessionToken")

            return ProfileResult.Success(data = result)
        } catch(e: Exception) {
           handleResponseError(e)
        }
    }

    private fun <T>handleResponseError(errorBody: Exception?): ProfileResult<T> {
        if (errorBody == null) return ProfileResult.Error(error = ProfileError.UnknownError())
        if (!errorBody.cause?.message.isNullOrEmpty()) {
            return ProfileResult.Error(error = ProfileError.UnknownError(errorBody.cause?.message))
        }
        val response = errorBody.message ?: return ProfileResult.Error(error = ProfileError.UnknownError())

        return when {
            response.contains("GetUserProfileError") -> ProfileResult.Error(ProfileError.GetUserProfileError)
            response.contains("ApiRouteNotFoundError") -> ProfileResult.Error(ProfileError.ApiRouteNotFoundError)
            else -> ProfileResult.Error(ProfileError.UnknownError(errorBody.message))
        }
    }

// Converters

    private fun ProfileItemApi.toProfile(): Profile {
        return Profile(
            id = this.id,
            firstName = this.firstname ?: "",
            lastName = this.lastname ?: "",
            email = this.email ?: "",
            username = this.username,
            createdAt = this.createdAt ?: ""
        )
    }

//    private fun GetUserProfile.toProfile(): Profile {
//        return Profile(
//            id = this.result.id,
//            firstName = this.result.firstName,
//            lastName = this.result.lastName,
//            following = emptyList(),
//            userShelves = this.result.userShelves.toShelfList(),
//            bookProgress = this.result.bookProgress.toBookProgressList(),
//            giveUp = emptyList(),
//            alreadyRead = emptyList()
//        )
//    }
//
//    private fun List<GetUserProfile>.toProfileList(): List<Profile> {
//        return this.map {
//            it.toProfile()
//        }
//    }
//
//    private fun UserShelf.toShelf(): Shelf {
//        return Shelf(
//            id = this.id,
//            books = emptyList(),
//            name = this.name,
//            description = this.description ?: "",
//            isPublic = this.isPublic,
//        )
//    }
//
//    private fun List<UserShelf>.toShelfList(): List<Shelf> {
//        return this.map {
//            it.toShelf()
//        }
//    }
//
//    private fun bpApiModel.toBookProgress(): BookProgress {
//        return BookProgress(
//            id = this.id,
//            percentage = this.percentage,
//            page = this.page,
//            finished = this.finished,
//            giveUp = this.giveUp,
//        )
//    }
//
//    private fun List<bpApiModel>.toBookProgressList(): List<BookProgress> {
//        return this.map {
//            it.toBookProgress()
//        }
//    }
}