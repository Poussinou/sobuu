package com.sobuumedia.sobuu.features.comments.remote

import co.touchlab.skie.configuration.annotations.SuspendInterop
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.ReportReason

class CommentRemoteDataImpl(
    private val api: CommentApi
): ICommentRemoteData {

    @SuspendInterop.Enabled
    override suspend fun addComment(
        sessionToken: String?,
        bookId: String,
        text: String?,
        page: Int?,
        percentage: Double?,
        parentCommentId: String?,
        hasSpoilers: Boolean,
        bookTotalPages: Int
    ): CommentResult<List<Comment>> {
        if (bookId.isBlank()) return CommentResult.Error(CommentError.InvalidBookIdError)
        if (text.isNullOrBlank()) return CommentResult.Error(CommentError.EmptyFieldError)
        if (page == null && percentage == null) return CommentResult.Error(CommentError.InvalidDoublePageAndPercentageNumberError)
        if (page != null && page.toInt() < 0) return CommentResult.Error(CommentError.InvalidPageNumberError)
        if (percentage != null && (percentage < 0 || percentage > 100)) return CommentResult.Error(
            CommentError.InvalidPercentageNumberError
        )
        val result = execute(sessionToken) {
            api.addComment(
                sessionToken = it,
                data = AddNewCommentRequest(
                    content = NewCommentData(
                        bookId = bookId,
                        pageNumber = page,
                        percentage = percentage,
                        commentText = text,
                        parentCommentId = parentCommentId,
                        hasSpoilers = hasSpoilers,
                        bookTotalPages = bookTotalPages
                    )
                )
            )
        }

        return if (result.data != null) {
            CommentResult.Success(data = result.data.toCommentsList())
        } else {
            CommentResult.Error(error = result.error)
        }
    }
/*
//    override suspend fun removeComment(
//        sessionToken: String?,
//        bookId: String,
//        commentId: String
//    ): CommentResult<Unit> {
//        if (bookId.isBlank()) return CommentResult.Error(CommentError.InvalidBookIdError)
//        if (commentId.isBlank()) return CommentResult.Error(CommentError.InvalidCommentIdError)
//        return execute(sessionToken) {
//            api.removeComment(
//                sessionToken = it,
//                bookId = bookId,
//                commentId = commentId,
//            )
//        }
//    }
//
//    override suspend fun editComment(
//        sessionToken: String?,
//        bookId: String,
//        commentId: String,
//        text: String?,
//        hasSpoilers: Boolean
//    ): CommentResult<Comment> {
//        if (bookId.isBlank()) return CommentResult.Error(CommentError.InvalidBookIdError)
//        if (commentId.isBlank()) return CommentResult.Error(CommentError.InvalidCommentIdError)
//        if (text.isNullOrBlank()) return CommentResult.Error(CommentError.EmptyFieldError)
//        return execute(sessionToken) {
//            api.editComment(
//                sessionToken = it,
//                bookId = bookId,
//                commentId = commentId,
//                text = text,
//                hasSpoilers = hasSpoilers,
//            )
//        }
//    }
 */

    @SuspendInterop.Enabled
    override suspend fun getCommentsInPageOrPercentage(
        sessionToken: String?,
        bookId: String,
        page: Int?,
        percentage: Double?,
        bookTotalPages: Int,
    ): CommentResult<List<Comment>> {

        // The one not used is -1
        if (bookId.isBlank()) return CommentResult.Error(CommentError.InvalidBookIdError)
        if (page == null && percentage == null) return CommentResult.Error(CommentError.InvalidPageNumberError)
        if (page != null && page.toInt() < 0 && percentage == null) return CommentResult.Error(
            CommentError.InvalidPageNumberError
        )
        if (percentage != null && percentage > 100) return CommentResult.Error(CommentError.InvalidPercentageNumberError)
        val result = execute(sessionToken) {
            api.getCommentsFromPageOrPercentage(
                sessionToken = it,
                data = BookCommentsInPageOrPercentageRequest(
                    content = BookCommentData(
                        bookId = bookId,
                        percentage = percentage,
                        page = page,
                        totalPages = bookTotalPages,
                    )
                ),
            )
        }

        return if (result.data != null) {
            CommentResult.Success(data = result.data.toCommentsList())
        } else {
            CommentResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun increaseCommentVoteCounter(
        sessionToken: String?,
        commentId: String
    ): CommentResult<List<Comment>> {
        if (commentId.isBlank()) return CommentResult.Error(CommentError.InvalidCommentIdError)
        val result =  execute(sessionToken) {
            api.increaseVoteCounterComment(
                sessionToken = it,
                data = BookCommentIncreaseVoteRequest(
                    content = CommentIdData(
                        commentId = commentId,
                        up = true,
                        down = false
                    )
                ),
            )
        }

        return if(result.data != null) {
            CommentResult.Success(data = result.data.toCommentList())
        } else {
            CommentResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun decreaseCommentVoteCounter(
        sessionToken: String?,
        commentId: String
    ): CommentResult<List<Comment>> {
        if (commentId.isBlank()) return CommentResult.Error(CommentError.InvalidCommentIdError)
        val result = execute(sessionToken) {
            api.decreaseVoteCounterComment(
                sessionToken = it,
                data = BookCommentDecreaseVoteRequest(
                    content = CommentIdData(
                        commentId = commentId,
                        up = true,
                        down = false
                    )
                ),
            )
        }

        return if(result.data != null) {
            CommentResult.Success(data = result.data.toCommentList())
        } else {
            CommentResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun reportComment(
        sessionToken: String?,
        commentId: String,
        reason: ReportReason
    ): CommentResult<Boolean> {
        if (commentId.isBlank()) return CommentResult.Error(CommentError.InvalidCommentIdError)
        val result = execute(sessionToken) {
            api.reportComment(
                sessionToken = it,
                data = ReportCommentRequest(
                    content = ReportCommentData(
                        commentId = commentId,
                        reason = reason.ordinal.toString()
                    )
                )
            )
        }

        return if (result.data != null) {
            if(result.data) {
                CommentResult.Success(true)
            } else {
                CommentResult.Error(CommentError.ReportError)
            }
        } else {
            CommentResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    private suspend fun <T>execute(sessionToken: String?, func: suspend (sessionToken: String) -> T): CommentResult<T> {
        return try {
            if (sessionToken.isNullOrBlank()) return CommentResult.Error(CommentError.InvalidSessionTokenError)

            val result = func("Bearer $sessionToken")

            return CommentResult.Success(data = result)
        } catch (e: Exception) {
            handleResponseError(e)
        }
    }

    private fun <T>handleResponseError(errorBody: Exception?): CommentResult<T> {
        if (errorBody == null) return CommentResult.Error(error = CommentError.UnknownError())

        if (!errorBody.cause?.message.isNullOrEmpty()) {
            return CommentResult.Error(error = CommentError.UnknownError(errorBody.cause?.message))
        }

        val response = errorBody.message ?: return CommentResult.Error(error = CommentError.UnknownError())

        return when {
            response.contains("GetBookCommentsError") -> CommentResult.Error(CommentError.GetPageOrPercentageCommentsError)
            response.contains("ApiRouteNotFoundError") -> CommentResult.Error(CommentError.ApiRouteNotFoundError)
            response.contains("SaveNewCommentError") -> CommentResult.Error(CommentError.SaveNewCommentError)
            else -> CommentResult.Error(CommentError.UnknownError(errorBody.message))
        }
    }
}