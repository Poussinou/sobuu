package com.sobuumedia.sobuu.features.settings.database

interface ISettingsLocalData {
    suspend fun getSessionToken(): String

    suspend fun getRefreshToken(): String

    suspend fun setSessionToken(token: String?)

    suspend fun setRefreshToken(token: String?)

    suspend fun getThemeMode(): Int

    suspend fun setThemeMode(mode: Int)

    suspend fun getAnalyticsEnabled(): Boolean

    suspend fun setAnalyticsEnabled(enabled: Boolean)
}