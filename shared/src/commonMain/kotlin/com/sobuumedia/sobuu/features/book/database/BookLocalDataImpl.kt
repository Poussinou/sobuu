package com.sobuumedia.sobuu.features.book.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager
import com.sobuumedia.sobuu.core.database.BookDBApi
import com.sobuumedia.sobuu.core.database.BookProgressDBApi
import com.sobuumedia.sobuu.core.database.BookRatingDBApi
import com.sobuumedia.sobuu.core.database.CommentDBApi
import com.sobuumedia.sobuu.core.database.CreditsDBApi
import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.features.comments.database.toCommentBO
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.UserBookRating
import com.sobuumedia.sobuu.models.db_models.UserBookRatingDB

class BookLocalDataImpl(
    prefs: KMMStorage,
    private val dbApi: BookDBApi,
    private val dbProgressApi: BookProgressDBApi,
    private val dbReviewApi: BookRatingDBApi,
    private val dbCreditsApi: CreditsDBApi,
    private val commentDBApi: CommentDBApi
): IBookLocalData, SessionTokenManager(prefs) {

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun saveBookDataInDB(book: Book) {
        val currentBook = dbApi.getData(book.id)
        if(currentBook != null) {
            dbApi.updateData(currentBook.copy(
                totalComments = book.totalComments,
                description = book.bookDescription,
                peopleReadingIt = book.peopleReadingIt,
                readingStatus = book.readingStatus.value,
                totalRating = book.totalRating
            ))
        } else {
            dbApi.saveData(book.toBookDB())
        }

        dbReviewApi.saveDataList(book.allReviews.map { it.toUserBookRatingDB(bookId = book.id) })

        val currentUserReview = dbReviewApi.getDataByBookId(book.id)
        if(currentUserReview != null) {
            if(book.userRating != null) {
                dbReviewApi.updateData(
                    currentUserReview.copy(
                        date = book.userRating.date.toString(),
                        review = book.userRating.review ?: "",
                        rating = book.userRating.rating,
                        user = book.userRating.user?.id
                    )
                )
            }
        } else {
            dbReviewApi.saveData(book.userRating?.toUserBookRatingDB(bookId = book.id) ?: UserBookRatingDB())
        }
    }

    override suspend fun saveProgressDataInDB(bookId: String, progress: BookProgress) {
        val currentProgress = dbProgressApi.getDataByBookId(bookId)
        if(currentProgress != null) {
            dbProgressApi.updateData(currentProgress.copy(
                page = progress.page,
                percentage = progress.percentage,
                progressInPercentage = progress.progressInPercentage,
                finished = progress.finished,
                giveUp = progress.giveUp,
                finishedToRead = if(progress.finishedToRead != null) progress.finishedToRead.toString() else null
            ))
        } else {
            dbProgressApi.saveData(progress.toBookProgressDB(bookId))
        }
    }

    override suspend fun saveReviewDataInDB(bookId: String, rating: UserBookRating?) {
        if(rating != null) {
            dbReviewApi.saveData(rating.toUserBookRatingDB(bookId))
        }
    }

    override suspend fun saveCreditsDataInDB(bookId: String, credits: CreditsBO) {
        dbCreditsApi.saveData(credits.toCreditDB(bookId))
    }

    override suspend fun saveBooksDataInDB(books: List<Book>) {
        books.forEach {
            saveBookDataInDB(it)
        }
    }

    override suspend fun getBookDataByID(bookId: String): BookResult<Book> {
        val data = dbApi.getData(bookId)
        val credits = dbApi.getBookWithCredits(bookId).credits
        var userRating = dbApi.getBookWithRating(bookId).userRating
        val allReviews = dbApi.getBookWithReviews(bookId).map { it.allReviews }[0]

        if(userRating?.rating == -1.0) {
           userRating = null
        }

        return if(data != null) {
            BookResult.Success(data.toBook(credits, userRating, allReviews))
        } else {
            BookResult.Error()
        }
    }

    override suspend fun getBookWithProgressByIdFromDB(bookId: String): BookResult<BookWithProgress> {
        val book = dbApi.getData(bookId)
        val credits = dbApi.getBookWithCredits(bookId).credits
        var userRating = dbApi.getBookWithRating(bookId).userRating
        val allReviews = dbApi.getBookWithReviews(bookId).map { it.allReviews }[0]
        val progress = dbProgressApi.getDataByBookId(bookId)
        val comments = commentDBApi.getDataByBookId(bookId)

        if(userRating?.rating == -1.0) {
            userRating = null
        }

        return if(book == null || progress == null) {
            BookResult.Error()
        } else {
            val bookWithProgress = BookWithProgress(
                book = book.toBook(credits, userRating, allReviews),
                bookProgress = progress.toBookProgress(),
                bookProgressComments = comments.map { it.toCommentBO() }
            )

            BookResult.Success(bookWithProgress)
        }
    }

    override suspend fun giveUpBook(bookId: String) {
        val progress = dbProgressApi.getDataByBookId(bookId)

        if(progress != null) {
            progress.giveUp = true
            dbProgressApi.saveData(progress)
        }
    }

    override suspend fun finishBook(bookId: String) {
        val progress = dbProgressApi.getDataByBookId(bookId)
        if(progress != null) {
            progress.finished = true
            progress.progressInPercentage = 100.0
            progress.percentage = 100.0
            dbProgressApi.saveData(progress)
        }
    }
}