package com.sobuumedia.sobuu.features.profile.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi.ProfileItemApi
import de.jensklingenberg.ktorfit.http.Body
import de.jensklingenberg.ktorfit.http.Header
import de.jensklingenberg.ktorfit.http.POST

interface ProfileApi {

    @POST("main")
    suspend fun getUserProfile(
        @Header("Authorization") sessionToken: String,
        @Body data: ProfileDataRequest,
    ): ProfileItemApi

    @POST("main")
    suspend fun logout(
        @Header("Authorization") sessionToken: String,
        @Body data: LogoutUserRequest,
    ): Boolean

//    @FormUrlEncoded
//    @POST("functions/getProfileFromId")
//    suspend fun getProfileFromId(
//        @Field("sessionToken") sessionToken: String,
//        @Field("profileId") profileId: String,
//    ): Response<Profile>
//
//    @FormUrlEncoded
//    @POST("functions/followProfile")
//    suspend fun followProfile(
//        @Field("sessionToken") sessionToken: String,
//        @Field("profileId") profileId: String,
//    ): Response<Profile>
//
//    @FormUrlEncoded
//    @POST("functions/unfollowProfile")
//    suspend fun unfollowProfile(
//        @Field("sessionToken") sessionToken: String,
//        @Field("profileId") profileId: String,
//    ): Response<Profile>
//
//    @FormUrlEncoded
//    @POST("functions/getFollowingProfiles")
//    suspend fun getFollowingProfiles(
//        @Field("sessionToken") sessionToken: String,
//    ): Response<List<Profile>>
}