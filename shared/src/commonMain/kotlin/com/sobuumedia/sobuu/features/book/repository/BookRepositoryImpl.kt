package com.sobuumedia.sobuu.features.book.repository

import com.sobuumedia.sobuu.features.book.database.IBookLocalData
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook

class BookRepositoryImpl(
    private val bookRemoteData: IBookRemoteData,
    private val bookLocalData: IBookLocalData
): IBookRepository {
    // region DB
    override suspend fun getBookByIdFromDB(bookId: String): BookResult<Book> {
        return bookLocalData.getBookDataByID(bookId)
    }

    override suspend fun getBookWithProgressByIdFromDB(bookId: String): BookResult<BookWithProgress> {
        return bookLocalData.getBookWithProgressByIdFromDB(bookId)
    }

    override suspend fun saveBookInDB(book: Book) {
        bookLocalData.saveBookDataInDB(book)
    }

    override suspend fun saveBookListInDB(bookList: List<Book>) {
        bookLocalData.saveBooksDataInDB(bookList)
    }

    // endregion

    // region Remote

    override suspend fun getUserCurrentReadingBook(): BookResult<List<BookWithProgress>> = execute {
        val result = bookRemoteData.getUserCurrentReadingBook(it)

        result.data?.map { bookWithProgress ->
            bookLocalData.saveBookDataInDB(bookWithProgress.book)
            bookLocalData.saveProgressDataInDB(bookWithProgress.book.id, bookWithProgress.bookProgress)
        }

        result
    }

    override suspend fun getUserFinishedReadingBook(): BookResult<List<FinishedReadingBook>> = execute {
        bookRemoteData.getUserAlreadyReadBooks(it)
    }

    override suspend fun getUserGiveUpBook(): BookResult<List<BookWithProgress>> = execute {
        bookRemoteData.getUserGiveUpBooks(it)
    }

    override suspend fun searchBook(term: String, language: String): BookResult<List<Book>> = execute {
        val result = bookRemoteData.searchBook(
            sessionToken = it,
            term = term,
            language = language,
        )

        result.data?.map { book ->
            bookLocalData.saveBookDataInDB(book)
            bookLocalData.saveReviewDataInDB(book.id, book.userRating)
            bookLocalData.saveCreditsDataInDB(book.id, book.credits)
            book.allReviews.map { rating ->
                bookLocalData.saveReviewDataInDB(book.id, rating)
            }
        }

        result
    }

    override suspend fun getBookProgress(bookId: String): BookResult<BookWithProgress> = execute {
        val result = bookRemoteData.getBookProgress(sessionToken = it, bookId = bookId)

        result.data?.let { book ->
            bookLocalData.saveBookDataInDB(book.book)
            bookLocalData.saveReviewDataInDB(book.book.id, book.book.userRating)
            bookLocalData.saveCreditsDataInDB(book.book.id, book.book.credits)
            book.book.allReviews.map { rating ->
                bookLocalData.saveReviewDataInDB(book.book.id, rating)
            }
            bookLocalData.saveProgressDataInDB(book.book.id, book.bookProgress)
        }

        result
    }

    override suspend fun searchISBN(isbn: String): BookResult<List<Book>> = execute {
        val result = bookRemoteData.searchISBN(sessionToken = it, isbn = isbn)

        result.data?.map { book ->
            bookLocalData.saveBookDataInDB(book)
            bookLocalData.saveReviewDataInDB(book.id, book.userRating)
            bookLocalData.saveCreditsDataInDB(book.id, book.credits)
            book.allReviews.map { rating ->
                bookLocalData.saveReviewDataInDB(book.id, rating)
            }
        }

        result
    }

    override suspend fun updateBookProgress(
        bookId: String,
        percentage: Double?,
        page: Int?,
        finished: Boolean,
        giveUp: Boolean
    ): BookResult<BookWithProgress> = execute {
        val result = bookRemoteData.updateBookProgress(
            sessionToken = it,
            bookId = bookId,
            percentage = percentage,
            page = page,
            finished = finished,
            giveUp = giveUp,
        )

        result.data.let { bookWithProgress ->
            if(bookWithProgress != null) {
                bookLocalData.saveBookDataInDB(bookWithProgress.book)
                bookLocalData.saveProgressDataInDB(
                    bookId, bookWithProgress.bookProgress
                )
            }
        }

        result
    }

    override suspend fun setBookCurrentlyReading(bookId: String, title: String, isbn: String?): BookResult<Unit> = execute {
        bookRemoteData.setBookCurrentlyReading(
            sessionToken = it,
            bookId = bookId,
            title = title,
            isbn = isbn
        )
    }

    override suspend fun reportMissingDataInBook(
        bookId: String,
        title: String,
        isbn: String?
    ): BookResult<Unit> = execute {
        bookRemoteData.reportMissingDataInBook(
            sessionToken = it,
            bookId = bookId,
            title = title,
            isbn = isbn
        )
    }

    override suspend fun getBookById(bookId: String): BookResult<Book> = execute {
        bookRemoteData.getBookById(
            sessionToken = it,
            bookId = bookId
        )
    }

    /*
        //    override suspend fun getCommentsFromPage(
        //        bookId: String,
        //        pageNum: Number
        //    ): BookResult<List<Comment>> = execute {
        //       bookRemoteData.getCommentsFromPage(
        //           sessionToken = it,
        //           bookId = bookId,
        //           pageNum = pageNum,
        //       )
        //    }
        //
        //    override suspend fun getCommentsFromPercentage(
        //        bookId: String,
        //        percentage: Number
        //    ): BookResult<List<Comment>> = execute {
        //        bookRemoteData.getCommentsFromPercentage(
        //            sessionToken = it,
        //            bookId = bookId,
        //            percentage = percentage,
        //        )
        //    }
        //
        //    override suspend fun rateBook(
        //        bookId: String,
        //        rate: Double,
        //        reviewText: String
        //    ): BookResult<Book> = execute {
        //        bookRemoteData.rateBook(
        //            sessionToken = it,
        //            bookId = bookId,
        //            rate = rate,
        //            reviewText = reviewText,
        //        )
        //    }
        //
        //    override suspend fun getUserRatingInBook(bookId: String): BookResult<UserBookRating> = execute {
        //        bookRemoteData.getUserRatingInBook(
        //            sessionToken = it,
        //            bookId = bookId,
        //        )
        //    }
        //
        //    override suspend fun getRatingListFromBook(bookId: String): BookResult<List<UserBookRating>> = execute {
        //        bookRemoteData.getRatingListFromBook(
        //            sessionToken = it,
        //            bookId = bookId,
        //        )
        //    }
        //
        //    override suspend fun removeRating(rateId: String): BookResult<Unit> = execute {
        //        bookRemoteData.removeRating(
        //            sessionToken = it,
        //            rateId = rateId,
        //        )
        //    }
        */
    override suspend fun finishBook(bookId: String, rateNumber: Float,
                                    rateText: String): BookResult<Boolean> = execute {
        bookRemoteData.finishBook(
            sessionToken = it,
            bookId = bookId,
            rateText = rateText,
            rateNumber = rateNumber
        )
    }

    override suspend fun giveUpWithBook(bookId: String): BookResult<Boolean> = execute {
        bookRemoteData.giveUpWithBook(
            sessionToken = it,
            bookId = bookId,
        )
    }

    override suspend fun saveBook(
        title: String,
        authors: List<String>,
        description: String,
        picture: String,
        thumbnail: String,
        publisher: String,
        credits: CreditsBO,
        totalPages: Int,
        isbn: Pair<String, String>,
        publishedDate: String,
        genres: List<String>,
        language: String,
        serie: String,
        seriesNumber: Int
    ): BookResult<Book> = execute {
        val result = bookRemoteData.saveBook(
            sessionToken = it,
            title, authors, description, picture, thumbnail, publisher, credits, totalPages, isbn,
            publishedDate, genres, language, serie, seriesNumber
        )

        result.data?.let { book ->
            bookLocalData.saveBookDataInDB(book)
            bookLocalData.saveReviewDataInDB(book.id, book.userRating)
            bookLocalData.saveCreditsDataInDB(book.id, book.credits)
            book.allReviews.map { rating ->
                bookLocalData.saveReviewDataInDB(book.id, rating)
            }
        }

        result
    }

    override suspend fun saveBookManually(
        title: String,
        authors: List<String>,
        description: String,
        picture: ByteArray,
        thumbnail: ByteArray,
        publisher: String,
        credits: CommonItemApi.CreditsItemApi?,
        totalPages: Int,
        isbn: Pair<String, String>,
        publishedDate: String,
        genres: List<String>,
        language: String,
        serie: String,
        seriesNumber: String
    ): BookResult<Book> = execute {
        val result = bookRemoteData.saveBookManually(
            sessionToken = it,
            title, authors, description, picture, thumbnail, publisher, credits, totalPages, isbn,
            publishedDate, genres, language, serie, seriesNumber
        )

        result.data?.let { book ->
            bookLocalData.saveBookDataInDB(book)
            bookLocalData.saveReviewDataInDB(book.id, book.userRating)
            bookLocalData.saveCreditsDataInDB(book.id, book.credits)
            book.allReviews.map { rating ->
                bookLocalData.saveReviewDataInDB(book.id, rating)
            }
        }

        result
    }

    private suspend fun <T>execute(func: suspend (sessionToken: String) -> BookResult<T>) : BookResult<T> {
        val sessionToken = bookLocalData.getSessionToken() ?: return BookResult.Error(
            BookError.InvalidSessionTokenError)
        return func(sessionToken)
    }
    // endregion
}