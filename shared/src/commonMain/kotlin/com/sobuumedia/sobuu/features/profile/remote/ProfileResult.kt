package com.sobuumedia.sobuu.features.profile.remote

import co.touchlab.skie.configuration.annotations.SealedInterop

@SealedInterop.Enabled
sealed class ProfileResult<T>(val data: T? = null, val error: ProfileError? = null) {
    class Success<T>(data: T? = null) : ProfileResult<T>(data = data)
    class Error<T>(error: ProfileError? = null) : ProfileResult<T>(error = error)
}

@SealedInterop.Enabled
sealed class ProfileError (val errorMessage: String? = null) {
    object UnauthorizedQueryError : ProfileError()
    object InvalidSessionTokenError : ProfileError()
    object InvalidProfileIdError : ProfileError()
    object ProcessingQueryError : ProfileError()
    object TimeOutError : ProfileError()
    object ApiRouteNotFoundError : ProfileError()
    object GetUserProfileError : ProfileError()
    data class UnknownError(val message: String? = null) : ProfileError(errorMessage = message)
}