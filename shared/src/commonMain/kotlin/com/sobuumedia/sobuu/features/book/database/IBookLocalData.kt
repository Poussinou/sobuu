package com.sobuumedia.sobuu.features.book.database

import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookProgress
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.UserBookRating

interface IBookLocalData {
    suspend fun getSessionToken(): String?

    suspend fun saveBooksDataInDB(books: List<Book>)

    suspend fun saveBookDataInDB(book: Book)

    suspend fun saveProgressDataInDB(bookId: String, progress: BookProgress)

    suspend fun saveReviewDataInDB(bookId: String, rating: UserBookRating?)

    suspend fun saveCreditsDataInDB(bookId: String, credits: CreditsBO)

    suspend fun getBookDataByID(bookId: String): BookResult<Book>

    suspend fun getBookWithProgressByIdFromDB(bookId: String): BookResult<BookWithProgress>

    suspend fun giveUpBook(bookId: String)

    suspend fun finishBook(bookId: String)
}