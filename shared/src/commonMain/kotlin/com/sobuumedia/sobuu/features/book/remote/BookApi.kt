package com.sobuumedia.sobuu.features.book.remote

import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.api_models.FinishedBookItemApi
import com.sobuumedia.sobuu.models.api_models.GiveUpBookItemApi
import com.sobuumedia.sobuu.models.api_models.ReadingBooksItemApi
import com.sobuumedia.sobuu.models.api_models.SearchBookItemApi
import de.jensklingenberg.ktorfit.http.Body
import de.jensklingenberg.ktorfit.http.Header
import de.jensklingenberg.ktorfit.http.POST
import io.ktor.client.request.forms.MultiPartFormDataContent

interface BookApi {

    @POST("main")
    suspend fun getUserCurrentReadingBook(
        @Header("Authorization") sessionToken: String,
        @Body data: UserReadingBooksRequest,
    ): List<ReadingBooksItemApi>

    @POST("main")
    suspend fun getUserAlreadyReadBooks(
        @Header("Authorization") sessionToken: String,
        @Body request: UserFinishedBooksRequest,
    ): List<FinishedBookItemApi>

    @POST("main")
    suspend fun getUserGiveUpBooks(
        @Header("Authorization") sessionToken: String,
        @Body request: UserGiveUpBooksRequest,
    ): List<GiveUpBookItemApi>

    @POST("main")
    suspend fun searchBook(
        @Header("Authorization") sessionToken: String,
        @Body data: SearchBookRequest,
    ): List<SearchBookItemApi>

    @POST("main")
    suspend fun getBookProgress(
        @Header("Authorization") sessionToken: String,
        @Body data: UserBookProgressRequest,
    ): ReadingBooksItemApi

    @POST("main")
    suspend fun getBookById(
        @Header("Authorization") sessionToken: String,
        @Body data: GetBookByIdRequest,
    ): CommonItemApi.BookItemApi

    @POST("main")
    suspend fun searchISBN(
        @Header("Authorization") sessionToken: String,
        @Body data: SearchByISBNRequest,
    ): List<SearchBookItemApi>

    @POST("main")
    suspend fun addBook(
        @Header("Authorization") sessionToken: String,
        @Body data: AddBookToDatabaseRequest,
    ): List<SearchBookItemApi>

//    @Multipart
//    @POST("main")
//    suspend fun addBookManually(
//        @Header("Authorization") sessionToken: String,
//        @Part("type") type: String,
//        @Part("title") title: String,
//        @Part("authors") authors: List<String>,
//        @Part("publisher") publisher: String,
////        @Part("cover") cover: ByteArray,
//        @Part("cover") cover: List<PartData>,
//        @Part("translators") translators: List<String>,
//        @Part("description") description: String,
//        @Part("publishedDate") publishedDate: String,
//        @Part("genres") genres: List<String>,
//        @Part("serieName") serieName: String,
//        @Part("serieNumber") serieNumber: String,
//        @Part("isbn10") isbn10: String,
//        @Part("isbn13") isbn13: String,
//        @Part("language") language: String,
//        @Part("pages") pages: Int
//    ): List<SearchBookApi>
//
    @POST("main")
    suspend fun addBookManually(
        @Header("Authorization") sessionToken: String,
        @Body map: MultiPartFormDataContent
    ): List<SearchBookItemApi>

    @POST("main")
    suspend fun setBookAsCurrentlyReading(
        @Header("Authorization") sessionToken: String,
        @Body data: StartToReadBookRequest,
    )

    @POST("main")
    suspend fun reportMissingDataInBook(
        @Header("Authorization") sessionToken: String,
        @Body data: ReportMissingInBookDataRequest,
    )

    @POST("main")
    suspend fun updateProgressBook(
        @Header("Authorization") sessionToken: String,
        @Body data: UserBookUpdateProgressRequest,
    ): ReadingBooksItemApi

//    @FormUrlEncoded
//    @POST("functions/addRating")
//    suspend fun ratingBook(
//        @Field("sessionToken") sessionToken: String,
//        @Field("bookId") bookId: String,
//        @Field("rating") rating: Double,
//        @Field("reviewText") reviewText: String,
//    ): Response<Book>
//
//    @FormUrlEncoded
//    @POST("functions/getUserRatingInBook")
//    suspend fun getUserRatingInBook(
//        @Field("sessionToken") sessionToken: String,
//        @Field("bookId") bookId: String,
//    ): Response<UserBookRating>
//
//    @FormUrlEncoded
//    @POST("functions/getRatingListFromBook")
//    suspend fun getRatingListFromBook(
//        @Field("sessionToken") sessionToken: String,
//        @Field("bookId") bookId: String,
//    ): Response<List<UserBookRating>>
//
//    @FormUrlEncoded
//    @POST("functions/removeRating")
//    suspend fun removeRating(
//        @Field("sessionToken") sessionToken: String,
//        @Field("ratingId") ratingId: String,
//    ): Response<Unit>

    @POST("main")
    suspend fun finishBook(
        @Header("Authorization") sessionToken: String,
        @Body data: FinishBookRequest,
    ): ReadingBooksItemApi

    @POST("main")
    suspend fun giveUpWithBook(
        @Header("Authorization") sessionToken: String,
        @Body data: GiveUpBookRequest,
    ): ReadingBooksItemApi
}