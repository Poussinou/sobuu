package com.sobuumedia.sobuu.features.authentication.repository

import com.sobuumedia.sobuu.features.authentication.database.IAuthenticationLocalData
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationError
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationResult
import com.sobuumedia.sobuu.features.authentication.remote.IAuthenticationRemoteData

class AuthenticationRepositoryImpl(
    private val authRemoteData: IAuthenticationRemoteData,
    private val authLocalData: IAuthenticationLocalData
) : IAuthenticationRepository {

    override suspend fun loginUser(username: String, password: String): AuthenticationResult<Unit> {
        return when(val result = authRemoteData.login(username, password)) {
            is AuthenticationResult.Authorized -> {
                val token = authRemoteData.login(username, password).data
                setSessionToken(token?.accessToken)
                setRefreshToken(token?.refreshToken)
                AuthenticationResult.Authorized()
            }
            is AuthenticationResult.Unauthorized -> {
                AuthenticationResult.Unauthorized()
            }
            is AuthenticationResult.LoggedOut -> {
                setSessionToken(null)
                setRefreshToken(null)
                AuthenticationResult.LoggedOut()
            }
            is AuthenticationResult.Error -> {
                AuthenticationResult.Error(result.error)
            }
            is AuthenticationResult.Registered -> {
                AuthenticationResult.Registered()
            }
            is AuthenticationResult.ResetPassword -> {
                AuthenticationResult.ResetPassword()
            }
        }
    }

    override suspend fun registerUser(username: String, email: String,
                                      password: String, firstname: String,
                                      lastname: String, language: String): AuthenticationResult<Unit> {
        return authRemoteData.register(
            username = username, email = email,
            password = password, firstname = firstname,
            lastname = lastname, language = language)
    }

    override suspend fun authenticate(): AuthenticationResult<Unit> {
        try {
            val token = authLocalData.getSessionToken()
            val refresh = authLocalData.getRefreshToken()

            if(token.isEmpty() && refresh.isEmpty()) {
                return AuthenticationResult.Unauthorized()
            }

            val result = authRemoteData.authenticate(token)

            return if(result is AuthenticationResult.Error) {
                when(result.error) {
                    AuthenticationError.InvalidSessionToken -> {
                        try {
                            val refreshedResult = authRemoteData.refreshSession(refresh)

                            setSessionToken(refreshedResult.data?.accessToken)
                            setRefreshToken(refreshedResult.data?.refreshToken)

                            AuthenticationResult.Authorized()
                        } catch (e: Exception) {
                            AuthenticationResult.Unauthorized()
                        }
                    }
                    else -> {
                        setSessionToken(null)
                        setRefreshToken(null)
                        AuthenticationResult.Unauthorized()
                    }
                }
            } else {
                result
            }
        }catch(e: Exception) {
            return AuthenticationResult.Unauthorized()
        }
    }

    override suspend fun resetPassword(email: String?): AuthenticationResult<Unit> {
        return authRemoteData.resetPassword(email)
    }

    override suspend fun getSessionToken(username: String, password: String): String? {
        return authLocalData.getSessionToken()
    }

    override suspend fun setSessionToken(token: String?) {
        authLocalData.setSessionToken(token)
    }

    override suspend fun setRefreshToken(token: String?) {
        authLocalData.setRefreshToken(token)
    }
}