package com.sobuumedia.sobuu.features.authentication.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager
import com.sobuumedia.sobuu.core.SessionTokenManager.Constants.REFRESH_TOKEN_KEY
import com.sobuumedia.sobuu.core.SessionTokenManager.Constants.SESSION_TOKEN_KEY

class AuthenticationLocalDataImpl(
    private val prefs: KMMStorage
): IAuthenticationLocalData, SessionTokenManager(prefs) {

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun getRefreshToken(): String {
        return super.obtainRefreshToken()
    }

    override suspend fun setSessionToken(token: String?) {
        prefs.putString(SESSION_TOKEN_KEY, token)
    }

    override suspend fun setRefreshToken(token: String?) {
        prefs.putString(REFRESH_TOKEN_KEY, token)
    }
}