package com.sobuumedia.sobuu.features.comments.database

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.core.SessionTokenManager
import com.sobuumedia.sobuu.core.database.BookDBApi
import com.sobuumedia.sobuu.core.database.BookProgressDBApi
import com.sobuumedia.sobuu.core.database.CommentDBApi
import com.sobuumedia.sobuu.features.book.database.toBook
import com.sobuumedia.sobuu.features.book.database.toBookProgress
import com.sobuumedia.sobuu.features.comments.remote.CommentResult
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment

class CommentLocalDataImpl(
    prefs: KMMStorage,
    private val dbCommentApi: CommentDBApi,
    private val dbBookApi: BookDBApi,
    private val dbProgressApi: BookProgressDBApi
) : ICommentLocalData, SessionTokenManager(prefs) {

    override suspend fun getSessionToken(): String {
        return super.obtainSessionToken()
    }

    override suspend fun saveCommentDataInDB(bookId: String, comment: Comment) {
        val currentComment = dbCommentApi.getData(comment.id)
        if (currentComment != null) {
            dbCommentApi.updateData(
                currentComment.copy(
                    text = comment.text,
                    publishedDate = comment.publishedDate.toString(),
                    percentage = comment.percentage,
                    pageNumber = comment.pageNumber,
                    hasSpoilers = comment.hasSpoilers
                )
            )
        } else {
            dbCommentApi.saveData(comment.toCommentDB(bookId))
        }
    }

    override suspend fun saveCommentListDataInDB(bookId: String, comments: List<Comment>) {
        comments.forEach {
            saveCommentDataInDB(bookId, it)
        }
    }

    override suspend fun getCommentsByBookID(bookId: String): CommentResult<List<Comment>> {
        return CommentResult.Success(dbCommentApi.getDataByBookId(bookId).map { it.toCommentBO() })
    }

    override suspend fun getCommentByID(commentId: String): CommentResult<Comment> {
        return CommentResult.Success(dbCommentApi.getData(commentId)?.toCommentBO())
    }

    override suspend fun getAllComments(): CommentResult<List<Comment>> {
        return CommentResult.Success(dbCommentApi.getAllData().map { it.toCommentBO() })
    }

    override suspend fun getBookById(bookId: String): CommentResult<Book> {
        val data = dbBookApi.getData(bookId)
        val credits = dbBookApi.getBookWithCredits(bookId).credits
        val userRating = dbBookApi.getBookWithRating(bookId).userRating
        val allReviews = dbBookApi.getBookWithReviews(bookId).map { it.allReviews }[0]

        return if (data != null) {
            CommentResult.Success(data.toBook(credits, userRating, allReviews))
        } else {
            CommentResult.Error()
        }
    }

    override suspend fun getBookProgressById(bookId: String): CommentResult<BookWithProgress> {
        val book = dbBookApi.getData(bookId)
        val credits = dbBookApi.getBookWithCredits(bookId).credits
        var userRating = dbBookApi.getBookWithRating(bookId).userRating
        val allReviews = dbBookApi.getBookWithReviews(bookId).map { it.allReviews }[0]
        val progress = dbProgressApi.getDataByBookId(bookId)
        val comments = dbCommentApi.getDataByBookId(bookId)

        if (userRating?.rating == -1.0) {
            userRating = null
        }

        return if (book == null || progress == null) {
            CommentResult.Error()
        } else {
            val bookWithProgress = BookWithProgress(
                book = book.toBook(credits, userRating, allReviews),
                bookProgress = progress.toBookProgress(),
                bookProgressComments = comments.map { it.toCommentBO() }
            )

            CommentResult.Success(bookWithProgress)
        }
    }
}