package com.sobuumedia.sobuu.features.shelf.remote

import co.touchlab.skie.configuration.annotations.SealedInterop

@SealedInterop.Enabled
sealed class ShelfResult<T>(val data: T? = null, val error: ShelfError? = null) {
    class Success<T>(data: T? = null) : ShelfResult<T>(data = data)
    class Error<T>(error: ShelfError? = null) : ShelfResult<T>(error = error)
}

@SealedInterop.Enabled
sealed class ShelfError {
    object UnauthorizedQueryError : ShelfError()
    object InvalidSessionTokenError : ShelfError()
    object EmptyTerm : ShelfError()
    object EmptyShelfId : ShelfError()
    object EmptyBookId : ShelfError()
    object InvalidShelfId : ShelfError()
    object EmptyName : ShelfError()
    object EmptyDescription : ShelfError()
    object ProcessingQueryError : ShelfError()
    object TimeOutError : ShelfError()
    object UnknownError : ShelfError()
}