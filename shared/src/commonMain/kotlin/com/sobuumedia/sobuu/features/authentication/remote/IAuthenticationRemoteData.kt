package com.sobuumedia.sobuu.features.authentication.remote

interface IAuthenticationRemoteData {

    suspend fun login(username: String, password: String): AuthenticationResult<AccessTokens>

    suspend fun register(username: String, email: String, password: String, firstname: String, lastname: String, language: String): AuthenticationResult<Unit>

    suspend fun authenticate(sessionToken: String?): AuthenticationResult<Unit>

    suspend fun refreshSession(refreshToken: String?): AuthenticationResult<AccessTokens>

    suspend fun resetPassword(email: String?): AuthenticationResult<Unit>
}