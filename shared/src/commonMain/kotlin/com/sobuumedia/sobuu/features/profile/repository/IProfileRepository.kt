package com.sobuumedia.sobuu.features.profile.repository

import com.sobuumedia.sobuu.features.profile.remote.ProfileResult
import com.sobuumedia.sobuu.models.bo_models.Profile

interface IProfileRepository {
    suspend fun getUserProfile(): ProfileResult<Profile>

    suspend fun logout(): ProfileResult<Unit>

//    suspend fun getProfileFromId(profileId: String): ProfileResult<Profile>
//
//    suspend fun followProfile(profileId: String): ProfileResult<Profile>
//
//    suspend fun unfollowProfile(profileId: String): ProfileResult<Profile>
//
//    suspend fun getFollowingProfiles(): ProfileResult<List<Profile>>
}