package com.sobuumedia.sobuu.features.comments.remote

import co.touchlab.skie.configuration.annotations.SealedInterop

@SealedInterop.Enabled
sealed class CommentResult<T>(val data: T? = null, val error: CommentError? = null) {
    class Success<T>(data: T? = null) : CommentResult<T>(data = data)
    class Error<T>(error: CommentError? = null) : CommentResult<T>(error = error)
}

@SealedInterop.Enabled
sealed class CommentError(val errorMessage: String? = null) {
    object UnauthorizedQueryError : CommentError()
    object InvalidSessionTokenError : CommentError()
    object InvalidBookIdError : CommentError()
    object InvalidCommentIdError : CommentError()
    object ReportError : CommentError()
    object BookDataNotFound : CommentError()
    object GetPageOrPercentageCommentsError : CommentError()
    object ApiRouteNotFoundError : CommentError()
    object SaveNewCommentError : CommentError()
    object EmptyFieldError : CommentError()
    object InvalidPageNumberError : CommentError()
    object InvalidPercentageNumberError : CommentError()
    object InvalidDoublePageAndPercentageNumberError : CommentError()
    object ProcessingQueryError : CommentError()
    object TimeOutError : CommentError()
    data class UnknownError(val message: String? = null) : CommentError(errorMessage = message)
}