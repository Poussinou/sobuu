package com.sobuumedia.sobuu.features.book.remote

import co.touchlab.skie.configuration.annotations.SuspendInterop
import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders

class BookRemoteDataImpl(
    private val api: BookApi
) : IBookRemoteData {

    @SuspendInterop.Enabled
    override suspend fun getUserCurrentReadingBook(sessionToken: String?): BookResult<List<BookWithProgress>> {
        val result = execute(sessionToken) {
            api.getUserCurrentReadingBook(
                it,
                UserReadingBooksRequest()
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toCurrentReadingBookList())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun getUserAlreadyReadBooks(sessionToken: String?): BookResult<List<FinishedReadingBook>> {
        val result = execute(sessionToken) {
            api.getUserAlreadyReadBooks(
                it,
                request = UserFinishedBooksRequest()
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data
                .toFinishedBookList()
                .sortedByDescending { finishedReadingBook ->
                    finishedReadingBook.bookWithProgress.bookProgress.finishedToRead
                }
            )
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun getUserGiveUpBooks(sessionToken: String?): BookResult<List<BookWithProgress>> {
        val result = execute(sessionToken) {
            api.getUserGiveUpBooks(
                it,
                request = UserGiveUpBooksRequest()
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data
                .toGiveUpBookList()
                .sortedByDescending { gaveUpBook ->
                    gaveUpBook.bookProgress.finishedToRead
                }
            )
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun searchBook(
        sessionToken: String?,
        term: String,
        language: String
    ): BookResult<List<Book>> {
        if (term.isBlank()) return BookResult.Error(BookError.EmptySearchTermError)

        val result = execute(sessionToken) {
            api.searchBook(
                it,
                SearchBookRequest(
                    content = SearchData(
                        term = term
                    )
                )
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toBookList())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun getBookProgress(
        sessionToken: String?,
        bookId: String
    ): BookResult<BookWithProgress> {
        if (bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
        val result = execute(sessionToken) {
            api.getBookProgress(
                sessionToken = it,
                data = UserBookProgressRequest(content = BookIdData(bookId = bookId)),
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toBookWithProgress())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun searchISBN(sessionToken: String?, isbn: String): BookResult<List<Book>> {
        if (isbn.isBlank()) return BookResult.Error(BookError.EmptySearchTermError)

        val result = execute(sessionToken) {
            api.searchISBN(
                it,
                SearchByISBNRequest(
                    content = ISBN(isbn = isbn.trim())
                )
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toBookList())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun updateBookProgress(
        sessionToken: String?,
        bookId: String,
        percentage: Double?,
        page: Int?,
        finished: Boolean,
        giveUp: Boolean
    ): BookResult<BookWithProgress> {
        if (percentage == null && page == null) return BookResult.Error(BookError.InvalidPageNumberError)
        if (page != null && page.toInt() < 0) return BookResult.Error(BookError.InvalidPageNumberError)
        if (percentage != null && (percentage > 100 || percentage < 0)) return BookResult.Error(
            BookError.InvalidPercentageNumberError
        )
        if (finished && giveUp) return BookResult.Error(BookError.InvalidFinishedAndGiveUpBookValuesError)
        val result = execute(sessionToken) {
            api.updateProgressBook(
                sessionToken = it,
                data = UserBookUpdateProgressRequest(
                    content = UpdateBookProgress(
                        bookId = bookId,
                        percentage = percentage,
                        page = page,
                        finished = finished,
                        giveUp = giveUp
                    )
                )
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toBookWithProgress())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun setBookCurrentlyReading(
        sessionToken: String?,
        bookId: String,
        title: String,
        isbn: String?
    ): BookResult<Unit> {
        if (isbn.isNullOrBlank()) return BookResult.Error(BookError.InvalidISBNError)
        return execute(sessionToken) {
            api.setBookAsCurrentlyReading(
                sessionToken = it,
                data = StartToReadBookRequest(
                    content = StartToReadBookData(
                        bookId = bookId,
                        title = title,
                        isbn = isbn
                    )
                ),
            )
        }
    }

    /**
     * Send the data from the book to the server, so an email is sent and I can complete the book
     * data missing
     */
    override suspend fun reportMissingDataInBook(
        sessionToken: String?,
        bookId: String,
        title: String,
        isbn: String?
    ): BookResult<Unit> {
        return execute(sessionToken) {
            api.reportMissingDataInBook(
                sessionToken = it,
                data = ReportMissingInBookDataRequest(
                    content = ReportMissingInBookData(
                        bookId = bookId,
                        title = title,
                        isbn = isbn ?: ""
                    )
                ),
            )
        }
    }

    override suspend fun getBookById(
        sessionToken: String?,
        bookId: String
    ): BookResult<Book> {
        val result = execute(sessionToken) {
            api.getBookById(
                sessionToken = it,
                data = GetBookByIdRequest(
                    content = BookId(id = bookId)
                ),
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = result.data.toBook())
        } else {
            BookResult.Error(error = result.error)
        }
    }

    /*
    //    override suspend fun getCommentsFromPage(
    //        sessionToken: String?,
    //        bookId: String,
    //        pageNum: Number
    //    ): BookResult<List<Comment>> {
    //        if(bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
    //        if(pageNum.toInt() < 0) return BookResult.Error(BookError.InvalidPageNumberError)
    //        return execute(sessionToken) {
    //            api.getCommentsFromPage(
    //                sessionToken = it,
    //                bookId = bookId,
    //                pageNum = pageNum
    //            )
    //        }
    //    }
    //
    //    override suspend fun getCommentsFromPercentage(
    //        sessionToken: String?,
    //        bookId: String,
    //        percentage: Number
    //    ): BookResult<List<Comment>> {
    //        if(bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
    //        if(percentage < 0 || percentage > 100) return BookResult.Error(BookError.InvalidPercentageNumberError)
    //        return execute(sessionToken) {
    //            api.getCommentsFromPercentage(
    //                sessionToken = it,
    //                bookId = bookId,
    //                percentage = percentage
    //            )
    //        }
    //    }
    //
    //    override suspend fun rateBook(
    //        sessionToken: String?,
    //        bookId: String,
    //        rate: Double,
    //        reviewText: String
    //    ): BookResult<Book> {
    //        if(bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
    //        if(rate < 0 || rate > 10) return BookResult.Error(BookError.InvalidRateNumberError)
    //        return execute(sessionToken) {
    //            api.ratingBook(
    //                sessionToken = it,
    //                bookId = bookId,
    //                rating = rate,
    //                reviewText = reviewText
    //            )
    //        }
    //    }

    //    override suspend fun getUserRatingInBook(
    //        sessionToken: String?,
    //        bookId: String
    //    ): BookResult<UserBookRating> {
    //        if(bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
    //        return execute(sessionToken) {
    //            api.getUserRatingInBook(
    //                sessionToken = it,
    //                bookId = bookId
    //            )
    //        }
    //    }

    //    override suspend fun getRatingListFromBook(
    //        sessionToken: String?,
    //        bookId: String
    //    ): BookResult<List<UserBookRating>> {
    //        if(bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
    //        return execute(sessionToken) {
    //            api.getRatingListFromBook(
    //                sessionToken = it,
    //                bookId = bookId,
    //            )
    //        }
    //    }

    //    override suspend fun removeRating(sessionToken: String?, rateId: String): BookResult<Unit> {
    //        if(rateId.isBlank()) return BookResult.Error(BookError.InvalidRateIdError)
    //        return execute(sessionToken) {
    //            api.removeRating(
    //                sessionToken = it,
    //                ratingId = rateId,
    //            )
    //        }
    //    }
    */

    @SuspendInterop.Enabled
    override suspend fun finishBook(
        sessionToken: String?,
        bookId: String,
        rateNumber: Float,
        rateText: String
    ): BookResult<Boolean> {
        if (bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
        val result = execute(sessionToken) {
            api.finishBook(
                sessionToken = it,
                data = FinishBookRequest(
                    content = FinishBookData(
                        bookId = bookId,
                        rateNumber = rateNumber,
                        rateText = rateText
                    )
                )
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = true)
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun giveUpWithBook(
        sessionToken: String?,
        bookId: String
    ): BookResult<Boolean> {
        if (bookId.isBlank()) return BookResult.Error(BookError.InvalidBookIdError)
        val result = execute(sessionToken) {
            api.giveUpWithBook(
                sessionToken = it,
                data = GiveUpBookRequest(
                    content = GiveUpBookData(bookId = bookId)
                )
            )
        }

        return if (result.data != null) {
            BookResult.Success(data = true)
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun saveBook(
        sessionToken: String?,
        title: String,
        authors: List<String>,
        description: String,
        picture: String,
        thumbnail: String,
        publisher: String,
        credits: CreditsBO,
        totalPages: Int,
        isbn: Pair<String, String>,
        publishedDate: String,
        genres: List<String>,
        language: String,
        serie: String,
        seriesNumber: Int
    ): BookResult<Book> {
        if (title.isBlank()
            || authors.isEmpty()
            || totalPages <= 1
            || (isbn.first.isBlank() && isbn.second.isBlank())
        ) return BookResult.Error(BookError.EmptyField)

        val result = execute(sessionToken) {
            api.addBook(
                sessionToken = it,
                data = AddBookToDatabaseRequest(
                    content = BookData(
                        title = title,
                        authors = authors,
                        description = description,
                        picture_url = picture,
                        thumbnail_url = thumbnail,
                        publisher = publisher,
                        published_date = publishedDate,
                        credits = credits.convertToCreditsAPI(),
                        total_pages = totalPages,
                        genres = genres,
                        language = language,
                        serie = serie,
                        serie_number = seriesNumber,
                        isbn10 = isbn.first,
                        isbn13 = isbn.second,
                        returnRequest = true,
                    )
                )
            )
        }

        return if (result.data != null) {
            val book = result.data.toBookList().filter { it.title == title }[0]

            BookResult.Success(data = book)
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    override suspend fun saveBookManually(
        sessionToken: String?,
        title: String,
        authors: List<String>,
        description: String?,
        picture: ByteArray,
        thumbnail: ByteArray,
        publisher: String,
        credits: CommonItemApi.CreditsItemApi?,
        totalPages: Int,
        isbn: Pair<String, String>,
        publishedDate: String,
        genres: List<String>,
        language: String,
        serie: String,
        seriesNumber: String
    ): BookResult<Book> {

        if (picture.isEmpty()) {
            println("------------------Byte array for picture is empty")
        }

        if (title.isBlank()
            || authors.isEmpty()
            || totalPages <= 0
            || (isbn.first.isBlank() && isbn.second.isBlank())
        ) return BookResult.Error(BookError.EmptyField)

        val result = execute(sessionToken) {
            val re = "[^A-Za-z0-9 ]".toRegex()
            val coverName = re.replace(title, "")

            val multipart = MultiPartFormDataContent(formData {
                append("type", "SaveBookManually")
                append("title", title)
                append("authors", authors.joinToString(", "))
                append("publisher", publisher)
                append("publishedDate", publishedDate)
                append("translators", credits?.translators?.joinToString(", ") ?: "")
                append("pages", totalPages)
                append("genres", genres.joinToString(", "))
                append("language", language)
                append("serieName", serie)
                append("serieNumber", seriesNumber)
                append("isbn10", isbn.first)
                append("isbn13", isbn.second)
                append("description", description ?: "")
                append("cover", picture, Headers.build {
                    append(HttpHeaders.ContentType, "image/png")
                    append(HttpHeaders.ContentDisposition, "filename=$coverName.png")
                })
            })

            api.addBookManually(
                sessionToken = it,
                map = multipart
            )
        }

        return if (result.data != null) {
            val book = result.data.toBookList().filter { it.title == title }[0]

            BookResult.Success(data = book)
        } else {
            BookResult.Error(error = result.error)
        }
    }

    @SuspendInterop.Enabled
    private suspend fun <T> execute(
        sessionToken: String?,
        func: suspend (sessionToken: String) -> T
    ): BookResult<T> {
        return try {
            if (sessionToken.isNullOrBlank()) return BookResult.Error(BookError.InvalidSessionTokenError)

            val result = func("Bearer $sessionToken")

            BookResult.Success(data = result)
        } catch (e: Exception) {
            handleResponseError(e)
        }
    }

    private fun <T> handleResponseError(errorBody: Exception?): BookResult<T> {
        if (errorBody == null) return BookResult.Error(error = BookError.UnknownError())

        if (!errorBody.cause?.message.isNullOrEmpty()) {
            return BookResult.Error(error = BookError.UnknownError(errorBody.cause?.message))
        }

        val response = errorBody.message
            ?: return BookResult.Error(error = BookError.UnknownError())

        return when {
            response.contains("GetUserCurrentReadingBooksError") -> BookResult.Error(BookError.GetCurrentReadingBooksError)
            response.contains("GetUserAlreadyReadBooksError") -> BookResult.Error(BookError.InvalidBookIdError)
            response.contains("GetUserFinishedBooksError") -> BookResult.Error(BookError.GetUserFinishedBooksError)
            response.contains("GetUserGiveUpBooksError") -> BookResult.Error(BookError.GetUserGiveUpBooksError)
            response.contains("GetUserBookProgressError") -> BookResult.Error(BookError.InvalidBookIdError)
            response.contains("ApiRouteNotFoundError") -> BookResult.Error(BookError.ApiRouteNotFoundError)
            response.contains("SearchBookError") -> BookResult.Error(BookError.SearchBookError)
            else -> BookResult.Error(BookError.UnknownError(errorBody.message))
        }
    }
}