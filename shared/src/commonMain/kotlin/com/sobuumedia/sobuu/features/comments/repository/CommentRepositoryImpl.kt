package com.sobuumedia.sobuu.features.comments.repository

import com.sobuumedia.sobuu.features.comments.database.ICommentLocalData
import com.sobuumedia.sobuu.features.comments.remote.CommentError
import com.sobuumedia.sobuu.features.comments.remote.CommentResult
import com.sobuumedia.sobuu.features.comments.remote.ICommentRemoteData
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.Comment
import com.sobuumedia.sobuu.models.bo_models.ReportReason

class CommentRepositoryImpl(
    private val commentRemoteData: ICommentRemoteData,
    private val commentLocalData: ICommentLocalData
): ICommentRepository {

    // region DB

    override suspend fun getBookInfoById(bookId: String): CommentResult<BookWithProgress> = execute {
        commentLocalData.getBookProgressById(bookId)
    }

    // endregion

    // region Remote

    override suspend fun addComment(
        bookId: String,
        text: String?,
        page: Int?,
        percentage: Double?,
        parentCommentId: String?,
        hasSpoilers: Boolean,
        bookTotalPages: Int
    ): CommentResult<List<Comment>> = execute {
        val result = commentRemoteData.addComment(
            sessionToken = it,
            bookId = bookId,
            text = text,
            page = page,
            percentage = percentage,
            parentCommentId = parentCommentId,
            hasSpoilers = hasSpoilers,
            bookTotalPages = bookTotalPages
        )

        result.data?.map { comment ->
            commentLocalData.saveCommentDataInDB(bookId, comment)
        }

        result
    }

    /*
    //    override suspend fun removeComment(bookId: String, commentId: String): CommentResult<Unit> = execute {
    //        commentRemoteData.removeComment(
    //            sessionToken = it,
    //            bookId = bookId,
    //            commentId = commentId,
    //        )
    //    }
    //
    //    override suspend fun editComment(
    //        bookId: String,
    //        commentId: String,
    //        text: String?,
    //        hasSpoilers: Boolean
    //    ): CommentResult<Comment> = execute {
    //        commentRemoteData.editComment(
    //            sessionToken = it,
    //            bookId = bookId,
    //            commentId = commentId,
    //            text = text,
    //            hasSpoilers = hasSpoilers,
    //        )
    //    }
    */

    override suspend fun getCommentsInPageOrPercentage(
        bookId: String,
        page: Int?,
        totalPages: Int,
        percentage: Double?,
    ): CommentResult<List<Comment>> = execute {
        commentRemoteData.getCommentsInPageOrPercentage(
            sessionToken = it,
            bookId = bookId,
            page = page,
            percentage = percentage,
            bookTotalPages = totalPages
        )
    }


    override suspend fun increaseCommentVoteCounter(commentId: String): CommentResult<List<Comment>> =
        execute {
            commentRemoteData.increaseCommentVoteCounter(
                sessionToken = it,
                commentId = commentId,
            )
    }

    override suspend fun decreaseCommentVoteCounter(commentId: String): CommentResult<List<Comment>> =
        execute {
        commentRemoteData.decreaseCommentVoteCounter(
            sessionToken = it,
            commentId = commentId,
        )
    }

    override suspend fun reportComment(
        commentId: String,
        reason: ReportReason
    ): CommentResult<Boolean> = execute {
        commentRemoteData.reportComment(
            sessionToken = it,
            commentId = commentId,
            reason = reason,
        )
    }

    private suspend fun <T>execute(func: suspend (sessionToken: String) -> CommentResult<T>) : CommentResult<T> {
        val sessionToken = commentLocalData.getSessionToken() ?: return CommentResult.Error(
            CommentError.InvalidSessionTokenError)
        return func(sessionToken)
    }

    // endregion
}