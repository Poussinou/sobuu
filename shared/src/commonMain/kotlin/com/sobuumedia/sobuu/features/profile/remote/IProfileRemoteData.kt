package com.sobuumedia.sobuu.features.profile.remote

import com.sobuumedia.sobuu.models.bo_models.Profile

interface IProfileRemoteData {
    suspend fun getUserProfile(sessionToken: String?): ProfileResult<Profile>

    suspend fun logout(sessionToken: String?): ProfileResult<Boolean>
//
//    suspend fun getProfileFromId(sessionToken: String?, profileId: String): ProfileResult<Profile>
//
//    suspend fun followProfile(sessionToken: String?, profileId: String): ProfileResult<Profile>
//
//    suspend fun unfollowProfile(sessionToken: String?, profileId: String): ProfileResult<Profile>
//
//    suspend fun getFollowingProfiles(sessionToken: String?): ProfileResult<List<Profile>>
}