package com.sobuumedia.sobuu.features.book.repository

import com.sobuumedia.sobuu.features.book.remote.BookResult
import com.sobuumedia.sobuu.models.api_models.CommonItemApi
import com.sobuumedia.sobuu.models.bo_models.Book
import com.sobuumedia.sobuu.models.bo_models.BookWithProgress
import com.sobuumedia.sobuu.models.bo_models.CreditsBO
import com.sobuumedia.sobuu.models.bo_models.FinishedReadingBook

interface IBookRepository {

    // region DB
    suspend fun getBookByIdFromDB(bookId: String): BookResult<Book>

    suspend fun getBookWithProgressByIdFromDB(bookId: String): BookResult<BookWithProgress>

    suspend fun saveBookInDB(book: Book)

    suspend fun saveBookListInDB(bookList: List<Book>)

    // endregion


    suspend fun getUserCurrentReadingBook() : BookResult<List<BookWithProgress>>

    suspend fun getUserFinishedReadingBook() : BookResult<List<FinishedReadingBook>>

    suspend fun getUserGiveUpBook() : BookResult<List<BookWithProgress>>

    suspend fun getBookById(bookId: String): BookResult<Book>

    suspend fun searchBook(term: String, language: String) : BookResult<List<Book>>

//    suspend fun getCommentsFromPage(bookId: String, pageNum: Number) : BookResult<List<Comment>>
//
//    suspend fun getCommentsFromPercentage(bookId: String, percentage: Number) : BookResult<List<Comment>>

    suspend fun setBookCurrentlyReading(bookId: String, title: String, isbn: String?) : BookResult<Unit>

    suspend fun reportMissingDataInBook(bookId: String, title: String, isbn: String?) : BookResult<Unit>

    suspend fun getBookProgress(bookId: String) : BookResult<BookWithProgress>

    suspend fun searchISBN(isbn: String) : BookResult<List<Book>>

    suspend fun updateBookProgress(bookId: String, percentage: Double?,
                                   page: Int?, finished: Boolean, giveUp: Boolean) : BookResult<BookWithProgress>

//    suspend fun rateBook(bookId: String, rate: Double, reviewText: String) : BookResult<Book>
//
//    suspend fun getUserRatingInBook(bookId: String) : BookResult<UserBookRating>
//
//    suspend fun getRatingListFromBook(bookId: String) : BookResult<List<UserBookRating>>
//
//    suspend fun removeRating(rateId: String) : BookResult<Unit>

    suspend fun finishBook(bookId: String, rateNumber: Float, rateText: String) : BookResult<Boolean>

    suspend fun giveUpWithBook(bookId: String) : BookResult<Boolean>

    suspend fun saveBook(title: String, authors: List<String>, description: String, picture: String,
                         thumbnail: String, publisher: String, credits: CreditsBO,
                         totalPages: Int, isbn: Pair<String, String>, publishedDate: String,
                         genres: List<String>, language: String, serie: String,
                         seriesNumber: Int) : BookResult<Book>

    suspend fun saveBookManually(
        title: String, authors: List<String>, description: String,
        picture: ByteArray, thumbnail: ByteArray,
        publisher: String, credits: CommonItemApi.CreditsItemApi?, totalPages: Int,
        isbn: Pair<String, String>, publishedDate: String,
        genres: List<String>, language: String, serie: String,
        seriesNumber: String) : BookResult<Book>
}