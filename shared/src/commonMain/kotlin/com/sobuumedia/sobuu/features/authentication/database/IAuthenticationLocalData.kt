package com.sobuumedia.sobuu.features.authentication.database

interface IAuthenticationLocalData {

    suspend fun getSessionToken(): String

    suspend fun getRefreshToken(): String

    suspend fun setSessionToken(token: String?)

    suspend fun setRefreshToken(token: String?)
}