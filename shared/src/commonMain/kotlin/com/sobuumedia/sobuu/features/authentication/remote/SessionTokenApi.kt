package com.sobuumedia.sobuu.features.authentication.remote

import kotlinx.serialization.Serializable

@Serializable
data class SessionTokenApi(val sessionToken: String)
data class AuthSessionTokenApi(val sessionToken: String)