package com.sobuumedia.sobuu.features.profile.repository

import com.sobuumedia.sobuu.features.profile.database.IProfileLocalData
import com.sobuumedia.sobuu.features.profile.remote.IProfileRemoteData
import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import com.sobuumedia.sobuu.features.profile.remote.ProfileResult
import com.sobuumedia.sobuu.models.bo_models.Profile


class ProfileRepositoryImpl(
    private val profileRemoteData: IProfileRemoteData,
    private val profileLocalData: IProfileLocalData
) : IProfileRepository {

    override suspend fun getUserProfile(): ProfileResult<Profile> = execute {
        profileRemoteData.getUserProfile(
            sessionToken = it,
        )
    }

    override suspend fun logout(): ProfileResult<Unit> {
        val result = execute {
           profileRemoteData.logout(sessionToken = it)
        }

        return if(result.data == true) {
            profileLocalData.setRefreshToken(null)
            profileLocalData.setSessionToken(null)

            ProfileResult.Success()
        } else if (result.error != null){
            ProfileResult.Error(result.error)
        } else {
            ProfileResult.Error(ProfileError.UnknownError())
        }
    }

//    override suspend fun getProfileFromId(profileId: String): ProfileResult<Profile> = execute {
//        profileRemoteData.getProfileFromId(
//            sessionToken = it,
//            profileId = profileId,
//        )
//    }
//
//    override suspend fun followProfile(profileId: String): ProfileResult<Profile> = execute {
//        profileRemoteData.followProfile(
//            sessionToken = it,
//            profileId = profileId,
//        )
//    }
//
//    override suspend fun unfollowProfile(profileId: String): ProfileResult<Profile> = execute {
//        profileRemoteData.unfollowProfile(
//           sessionToken = it,
//           profileId = profileId,
//        )
//    }
//
//    override suspend fun getFollowingProfiles(): ProfileResult<List<Profile>> = execute {
//        profileRemoteData.getFollowingProfiles(
//            sessionToken = it,
//        )
//    }

    private suspend fun <T>execute(func: suspend (sessionToken: String) -> ProfileResult<T>) : ProfileResult<T> {
        val sessionToken = profileLocalData.getSessionToken()
        return func(sessionToken)
    }
}