package com.sobuumedia.sobuu.features.settings.repository

interface ISettingsRepository {

    suspend fun getAppTheme(): ThemeOptions

    suspend fun setAppTheme(value: ThemeOptions)

    suspend fun getAnalyticsEnabled(): Boolean

    suspend fun setAnalyticsEnabled(value: Boolean)
}