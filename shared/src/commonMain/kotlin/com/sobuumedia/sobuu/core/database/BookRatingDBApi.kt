package com.sobuumedia.sobuu.core.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.sobuumedia.sobuu.models.db_models.UserBookRatingDB

@Dao
interface BookRatingDBApi: DBApi<UserBookRatingDB> {

    @Insert
    override suspend fun saveData(data: UserBookRatingDB)

    @Insert
    override suspend fun saveDataList(data: List<UserBookRatingDB>)

    @Update
    override suspend fun updateData(data: UserBookRatingDB)

    @Query("SELECT * FROM UserBookRatingDB WHERE id LIKE :id")
    override suspend fun getData(id: String): UserBookRatingDB?

    @Query("SELECT * FROM UserBookRatingDB WHERE bookId LIKE :bookId")
    suspend fun getDataByBookId(bookId: String): UserBookRatingDB?

    @Query("SELECT * FROM UserBookRatingDB")
    override suspend fun getAllData(): List<UserBookRatingDB>

    @Query("DELETE FROM UserBookRatingDB")
    override suspend fun removeAll()

    @Query("DELETE FROM UserBookRatingDB WHERE bookId = :id")
    override suspend fun removeById(id: String)

    @Delete
    override suspend fun remove(data: UserBookRatingDB)
}