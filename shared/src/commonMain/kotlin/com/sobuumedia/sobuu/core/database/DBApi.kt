package com.sobuumedia.sobuu.core.database

interface DBApi<T> {

    suspend fun saveData(data: T)
    suspend fun saveDataList(data: List<T>)
    suspend fun updateData(data: T)
    suspend fun getData(id: String): T?
    suspend fun getAllData(): List<T>
    suspend fun remove(data: T)
    suspend fun removeById(id: String)
    suspend fun removeAll()
}