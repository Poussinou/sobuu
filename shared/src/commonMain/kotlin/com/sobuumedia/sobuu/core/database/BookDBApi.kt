package com.sobuumedia.sobuu.core.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.sobuumedia.sobuu.models.db_models.BookDB
import com.sobuumedia.sobuu.models.db_models.BookWithAllReviewsDB
import com.sobuumedia.sobuu.models.db_models.BookWithCreditsDB
import com.sobuumedia.sobuu.models.db_models.BookWithUserRatingDB

@Dao
interface BookDBApi: DBApi<BookDB> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveData(data: BookDB)

    @Insert
    override suspend fun saveDataList(data: List<BookDB>)

    @Update
    override suspend fun updateData(data: BookDB)

    @Query("SELECT * FROM BookDB WHERE bookId LIKE :id")
    override suspend fun getData(id: String): BookDB?

    @Query("SELECT * FROM BookDB")
    override suspend fun getAllData(): List<BookDB>

    @Delete
    override suspend fun remove(data: BookDB)

    @Query("DELETE FROM BookDB WHERE bookId = :id")
    override suspend fun removeById(id: String)

    @Query("DELETE FROM BookDB")
    override suspend fun removeAll()

    // Transaction
    @Transaction
    @Query("SELECT * FROM BookDB WHERE bookId LIKE :id")
    suspend fun getBookWithCredits(id: String): BookWithCreditsDB

    @Transaction
    @Query("SELECT * FROM BookDB WHERE bookId LIKE :id")
    suspend fun getBookWithRating(id: String): BookWithUserRatingDB

    @Transaction
    @Query("SELECT * FROM BookDB WHERE bookId LIKE :id")
    suspend fun getBookWithReviews(id: String): List<BookWithAllReviewsDB>
}