package com.sobuumedia.sobuu.core.database

import androidx.room.TypeConverter

class ListConverter {

    @TypeConverter
    fun fromStringArrayList(value: List<String>): String {
        return value.joinToString("##")
    }

    @TypeConverter
    fun toStringArrayList(value: String): List<String> {
        return try {
            value.split("##")
        } catch (e: Exception) {
            arrayListOf()
        }
    }
}