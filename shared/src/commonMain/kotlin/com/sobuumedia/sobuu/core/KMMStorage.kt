package com.sobuumedia.sobuu.core

import com.sobuumedia.sobuu.SPref
import com.sobuumedia.sobuu.getBoolean
import com.sobuumedia.sobuu.getInt
import com.sobuumedia.sobuu.getString
import com.sobuumedia.sobuu.setBoolean
import com.sobuumedia.sobuu.setInt
import com.sobuumedia.sobuu.setString

/**
 * Source: https://atul-sharma-94062.medium.com/userdefault-and-sharedpreference-in-kotlin-multiplatform-mobile-kmm-490494ba44a6
 */
class KMMStorage(val context: SPref) {

    fun getInt(key: String): Int {
        return context.getInt(key)
    }

    fun putInt(key: String, value: Int) {
        context.setInt(key, value)
    }

    fun getString(key: String): String {
        return context.getString(key)
    }

    fun putString(key: String, value: String?) {
        context.setString(key, value ?: "")
    }

    fun getBoolean(key: String): Boolean {
        return context.getBoolean(key)
    }

    fun putBoolean(key: String, value: Boolean?) {
        context.setBoolean(key, value ?: false)
    }
}