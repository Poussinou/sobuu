package com.sobuumedia.sobuu.core.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.sobuumedia.sobuu.models.db_models.BookProgressDB

@Dao
interface BookProgressDBApi: DBApi<BookProgressDB> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveData(data: BookProgressDB)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveDataList(data: List<BookProgressDB>)

    @Update
    override suspend fun updateData(data: BookProgressDB)

    @Query("SELECT * FROM BookProgressDB WHERE id LIKE :id")
    override suspend fun getData(id: String): BookProgressDB?

    @Query("SELECT * FROM BookProgressDB WHERE bookId LIKE :bookId")
    suspend fun getDataByBookId(bookId: String): BookProgressDB?

    @Query("SELECT * FROM BookProgressDB")
    override suspend fun getAllData(): List<BookProgressDB>

    @Query("DELETE FROM BookProgressDB")
    override suspend fun removeAll()

    @Query("DELETE FROM BookProgressDB WHERE bookId = :id")
    override suspend fun removeById(id: String)

    @Delete
    override suspend fun remove(data: BookProgressDB)
}