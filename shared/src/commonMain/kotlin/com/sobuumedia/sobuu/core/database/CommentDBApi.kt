package com.sobuumedia.sobuu.core.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.sobuumedia.sobuu.models.db_models.CommentDB

@Dao
interface CommentDBApi: DBApi<CommentDB> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveData(data: CommentDB)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override suspend fun saveDataList(data: List<CommentDB>)

    @Update
    override suspend fun updateData(data: CommentDB)

    @Query("SELECT * FROM CommentDB WHERE commentId LIKE :id")
    override suspend fun getData(id: String): CommentDB?

    @Query("SELECT * FROM CommentDB WHERE bookId LIKE :id")
    suspend fun getDataByBookId(id: String): List<CommentDB>

    @Query("SELECT * FROM CommentDB")
    override suspend fun getAllData(): List<CommentDB>

    @Query("DELETE FROM CommentDB")
    override suspend fun removeAll()

    @Query("DELETE FROM CommentDB WHERE commentId = :id")
    override suspend fun removeById(id: String)

    @Delete
    override suspend fun remove(data: CommentDB)
}