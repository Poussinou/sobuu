package com.sobuumedia.sobuu.di


import com.sobuumedia.sobuu.getRoomDatabase
import org.koin.dsl.module

val databaseModule = module {
    single { getRoomDatabase(get()) }
}

