package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.SobuuDB
import com.sobuumedia.sobuu.core.database.CommentDBApi
import com.sobuumedia.sobuu.features.comments.database.CommentLocalDataImpl
import com.sobuumedia.sobuu.features.comments.database.ICommentLocalData
import com.sobuumedia.sobuu.features.comments.remote.CommentApi
import com.sobuumedia.sobuu.features.comments.remote.CommentRemoteDataImpl
import com.sobuumedia.sobuu.features.comments.remote.ICommentRemoteData
import com.sobuumedia.sobuu.features.comments.repository.CommentRepositoryImpl
import com.sobuumedia.sobuu.features.comments.repository.ICommentRepository
import de.jensklingenberg.ktorfit.Ktorfit
import org.koin.core.qualifier.named
import org.koin.dsl.module

val commentModule = module {
    single { get<Ktorfit.Builder>(named("main")).build().create<CommentApi>() }

    single<CommentDBApi> { get<SobuuDB>().getCommentDao() }

    single<ICommentRemoteData> { CommentRemoteDataImpl(get()) }

    single<ICommentLocalData> { CommentLocalDataImpl(get(), get(), get(), get()) }

    single<ICommentRepository> { CommentRepositoryImpl(get(), get()) }
}