package com.sobuumedia.sobuu.di

fun appModule() = listOf(
    networkModule,
    authenticationModule,
    bookModule,
    commentModule,
    profileModule,
    settingsModule,
    analyticsModule,
    databaseModule,
    logsModule
)