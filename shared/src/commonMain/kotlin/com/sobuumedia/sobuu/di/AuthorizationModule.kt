package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.features.authentication.database.AuthenticationLocalDataImpl
import com.sobuumedia.sobuu.features.authentication.database.IAuthenticationLocalData
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationApi
import com.sobuumedia.sobuu.features.authentication.remote.AuthenticationRemoteDataImpl
import com.sobuumedia.sobuu.features.authentication.remote.IAuthenticationRemoteData
import com.sobuumedia.sobuu.features.authentication.repository.AuthenticationRepositoryImpl
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import de.jensklingenberg.ktorfit.Ktorfit
import org.koin.core.qualifier.named
import org.koin.dsl.module

val authenticationModule = module {
    single { get<Ktorfit.Builder>(named("main")).build().create<AuthenticationApi>() }

    single<IAuthenticationRemoteData> { AuthenticationRemoteDataImpl(authApi = get()) }

    single<IAuthenticationLocalData> { AuthenticationLocalDataImpl(prefs = get()) }

    single<IAuthenticationRepository> { AuthenticationRepositoryImpl(authLocalData = get(), authRemoteData = get()) }
}