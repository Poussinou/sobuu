package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.features.profile.database.IProfileLocalData
import com.sobuumedia.sobuu.features.profile.database.ProfileLocalDataImpl
import com.sobuumedia.sobuu.features.profile.remote.IProfileRemoteData
import com.sobuumedia.sobuu.features.profile.remote.ProfileApi
import com.sobuumedia.sobuu.features.profile.remote.ProfileRemoteDataImpl
import com.sobuumedia.sobuu.features.profile.repository.IProfileRepository
import com.sobuumedia.sobuu.features.profile.repository.ProfileRepositoryImpl
import de.jensklingenberg.ktorfit.Ktorfit
import org.koin.core.qualifier.named
import org.koin.dsl.module

val profileModule = module {
    single { get<Ktorfit.Builder>(named("main")).build().create<ProfileApi>() }

    single<IProfileRemoteData> { ProfileRemoteDataImpl(get()) }

    single<IProfileLocalData> { ProfileLocalDataImpl(get()) }

    single<IProfileRepository> { ProfileRepositoryImpl(get(), get()) }
}