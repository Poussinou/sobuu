package com.sobuumedia.sobuu.di

import com.sobuumedia.sobuu.SobuuDB
import com.sobuumedia.sobuu.core.database.BookDBApi
import com.sobuumedia.sobuu.core.database.BookProgressDBApi
import com.sobuumedia.sobuu.core.database.BookRatingDBApi
import com.sobuumedia.sobuu.core.database.CreditsDBApi
import com.sobuumedia.sobuu.features.book.database.BookLocalDataImpl
import com.sobuumedia.sobuu.features.book.database.IBookLocalData
import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.repository.BookRepositoryImpl
import com.sobuumedia.sobuu.features.book.repository.IBookRepository
import de.jensklingenberg.ktorfit.Ktorfit
import org.koin.core.qualifier.named
import org.koin.dsl.module

val bookModule = module {
    single { get<Ktorfit.Builder>(named("main")).build().create<BookApi>() }

    single<BookDBApi> { get<SobuuDB>().getBookDao() }

    single<BookProgressDBApi> { get<SobuuDB>().getBookProgressDao() }

    single<BookRatingDBApi> { get<SobuuDB>().getBookRatingsDao() }

    single<CreditsDBApi> { get<SobuuDB>().getCreditsDao() }

    single<IBookRemoteData> { BookRemoteDataImpl(get()) }

    single<IBookLocalData> { BookLocalDataImpl(get(), get(), get(), get(), get(), get()) }

    single<IBookRepository> { BookRepositoryImpl(get(), get()) }
}