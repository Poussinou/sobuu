package com.sobuumedia.sobuu.models.bo_models

import kotlinx.datetime.LocalDateTime

data class UserBookRating(
    val id: String,
    val user: Profile?,
    val rating: Double?,
    val review: String?,
    val date: LocalDateTime?
)
