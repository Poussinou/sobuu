package com.sobuumedia.sobuu.models.db_models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class UserBookRatingDB(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var bookId: String = "",
    var userBookRatingId: String = "",
    var user: String? = null,
    var rating: Double? = -1.0,
    var review: String = "",
    var date: String = ""
)

data class BookRatingWithUserProfileDB(
    @Embedded val bookRating: UserBookRatingDB,
    @Relation(
        parentColumn = "user",
        entityColumn = "profileId"
    )
    val profile: ProfileDB?
)