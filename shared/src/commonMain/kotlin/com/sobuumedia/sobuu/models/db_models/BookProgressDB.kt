package com.sobuumedia.sobuu.models.db_models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class BookProgressDB(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    var bookId: String = "",
    var progressId: String = "",
    var percentage: Double? = null,
    var page: Int? = null,
    var progressInPercentage: Double = -1.0,
    var finished: Boolean = false,
    var giveUp: Boolean = false,
    var startedToRead: String = "",
    var finishedToRead: String? = null
)