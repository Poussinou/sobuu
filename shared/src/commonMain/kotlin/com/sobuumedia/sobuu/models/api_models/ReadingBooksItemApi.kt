package com.sobuumedia.sobuu.models.api_models

import com.sobuumedia.sobuu.models.api_models.CommonItemApi.BookItemApi
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.CommentItemApi
import kotlinx.serialization.Serializable

@Serializable
data class ReadingBooksItemApi(
    val book: BookItemApi,
    val bookProgress: BookProgressItemApi,
    val comments: List<CommentItemApi>? = null
)

@Serializable
data class BookProgressItemApi(
    val id: String,
    val pageNumber: Int,
    val percentage: Double,
    val gaveup: Boolean,
    val finished: Boolean,
    val createdAt: String,
)