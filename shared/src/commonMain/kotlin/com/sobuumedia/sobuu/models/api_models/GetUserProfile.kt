package com.sobuumedia.sobuu.models.api_models

import kotlinx.serialization.Serializable


@Serializable
data class GetUserProfile(
    val result: Result
)