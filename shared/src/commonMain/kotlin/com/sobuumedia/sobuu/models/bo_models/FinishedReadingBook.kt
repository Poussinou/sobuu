package com.sobuumedia.sobuu.models.bo_models

data class FinishedReadingBook (
    val bookWithProgress: BookWithProgress,
    val userRating: UserBookRating? = null
)