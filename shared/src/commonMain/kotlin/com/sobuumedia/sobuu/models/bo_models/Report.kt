package com.sobuumedia.sobuu.models.bo_models

import co.touchlab.skie.configuration.annotations.EnumInterop

/**
 * Reason to report:
 * -(0)Polemic comment
 * -(1)Use of offensive language
 * -(2)Spam
 * -(3)Racism
 * -(4)Violence
 * -(5)Personal Details
 * -(6)Selling illegal products
 * -(7)Others
 */
@EnumInterop.Enabled
enum class ReportReason(reason: Byte) {
    PolemicComment(0),
    UseOfOffensiveLanguage(1),
    Spam(2),
    Racism(3),
    Violence(4),
    PersonalDetails(5),
    SellingIllegalProducts(6),
    Others(7),
}

data class Report(
    val id: String,
    val comment: Comment,
    val reason: ReportReason
)
