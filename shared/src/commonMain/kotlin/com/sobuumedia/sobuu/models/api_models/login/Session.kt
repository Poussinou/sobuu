package com.sobuumedia.sobuu.models.api_models.login

import kotlinx.serialization.Serializable

@Serializable
data class Session(
    val access_token: String,
    val expires_at: Int,
    val expires_in: Int,
    val refresh_token: String,
    val token_type: String,
    val user: User
)