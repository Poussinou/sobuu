package com.sobuumedia.sobuu.models.api_models

import kotlinx.serialization.Serializable

@Serializable
data class Result(
//    val bookProgress: List<BookProgress>,
    val firstName: String,
//    val following: List<Result>,
    val id: String,
    val lastName: String,
//    val userShelves: List<UserShelf>
)