package com.sobuumedia.sobuu.models.api_models

import com.sobuumedia.sobuu.models.api_models.CommonItemApi.BookItemApi
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.ProfileItemApi
import kotlinx.serialization.Serializable

@Serializable
data class FinishedBookItemApi(
    val book: BookItemApi,
    val pageNumber: Int = -1,
    val percentage: Double = -1.0,
    val createdAt: String,
    val updatedAt: String,
    val rating: ReviewItemApi?
)

@Serializable
data class ReviewItemApi(
    val id: String = "",
    val ratingText: String? = "",
    val rating: Double?,
    val createdAt: String?,
    val user: ProfileItemApi?
)
