package com.sobuumedia.sobuu.models.bo_models

import kotlinx.serialization.Serializable

@Serializable
data class CreditsBO (
    val contributors: Array<String> = emptyArray(),
    val illustrators: Array<String> = emptyArray(),
    val others: Array<String> = emptyArray(),
    val translators: Array<String> = emptyArray()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as CreditsBO

        if (!contributors.contentEquals(other.contributors)) return false
        if (!illustrators.contentEquals(other.illustrators)) return false
        if (!others.contentEquals(other.others)) return false
        if (!translators.contentEquals(other.translators)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = contributors.contentHashCode()
        result = 31 * result + illustrators.contentHashCode()
        result = 31 * result + others.contentHashCode()
        result = 31 * result + translators.contentHashCode()
        return result
    }
}