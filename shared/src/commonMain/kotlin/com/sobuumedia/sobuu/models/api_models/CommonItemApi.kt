package com.sobuumedia.sobuu.models.api_models

import kotlinx.serialization.Serializable

class CommonItemApi {

    @Serializable
    data class BookItemApi(
        val id: String,
        val title: String,
        val authors: List<String>,
        val picture_url: String?,
        val thumbnail_url: String?,
        val genres: List<String>,
        val picture: String?,
        val publisher: String?,
        val published_date: String?,
        val description: String,
        val total_pages: Int,
        val isbn10: String? = null,
        val isbn13: String? = null,
        val serie: String?,
        val serie_number: Int,
        val lang: String,
        val credits: CreditsItemApi? = null
    )

    @Serializable
    data class CommentItemApi(
        val id: String,
        val hasSpoilers: Boolean,
        val commentText: String,
        val createdAt: String,
        val votes: Long,
        val percentage: Double? = -1.0,
        val pageNumber: Int? = -1,
        val bookID: String,
        val parentCommentID: String? = null,
        val profile: ProfileItemApi,
    )

    @Serializable
    data class ProfileItemApi(
        val id: String,
        val username: String,
        val firstname: String? = "",
        val lastname: String? = "",
        val email: String? = "",
        val createdAt: String? = ""
    )

    @Serializable
    data class CreditsItemApi(
        val translators: List<String>,
        val illustrators: List<String>,
        val contributors: List<String>,
        val others: List<String>
    )
}