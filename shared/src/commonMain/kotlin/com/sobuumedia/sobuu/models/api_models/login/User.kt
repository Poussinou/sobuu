package com.sobuumedia.sobuu.models.api_models.login

@kotlinx.serialization.Serializable
data class User(
    val email: String,
    val id: String,
)