package com.sobuumedia.sobuu.models.api_models

import com.sobuumedia.sobuu.models.api_models.CommonItemApi.BookItemApi
import kotlinx.serialization.Serializable

@Serializable
data class GiveUpBookItemApi(
    val book: BookItemApi,
    val pageNumber: Int = -1,
    val percentage: Double = -1.0,
    val createdAt: String,
    val updatedAt: String
)