package com.sobuumedia.sobuu.book

import com.sobuumedia.sobuu.features.book.remote.BookApi
import com.sobuumedia.sobuu.features.book.remote.BookError
import com.sobuumedia.sobuu.features.book.remote.BookRemoteDataImpl
import com.sobuumedia.sobuu.features.book.remote.IBookRemoteData
import com.sobuumedia.sobuu.features.book.remote.UserReadingBooksRequest
import com.sobuumedia.sobuu.models.api_models.ReadingBooksItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

internal class GetCurrentReadingBooksTest {

    @Mock
    private val api = mock(classOf<BookApi>())

    private lateinit var bookRemoteData: IBookRemoteData
    private val sessionToken = "w09wiohnkwe"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        bookRemoteData = BookRemoteDataImpl(api)
    }

    @Test
    fun testGetCurrentReadingBooksWhenTheUserIsNotReadingNothingCurrently() = runTest {
        // Given
        val requestBody = UserReadingBooksRequest()
        val response = emptyList<ReadingBooksItemApi>()

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", requestBody) }
            .returnsMany(response)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue(currentReadingBooks.data != null)
        assertTrue(currentReadingBooks.data?.size == 0)
    }

    @Test
    fun testShouldGetInvalidSessionTokenErrorIfSessionTokenIsEmpty() = runTest {
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook("")
        assertTrue(currentReadingBooks.error != null)
        assertTrue(currentReadingBooks.error is BookError.InvalidSessionTokenError)
    }

    @Test
    fun testGetCurrentReadingBooksWithProperData() = runTest {
        // Given
        val serverResponse = """
            [
              {
                "book": {
                  "id": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                  "title": "Eat",
                  "authors": [
                    "Steph James"
                  ],
                  "picture_url": "https://foo.test/image",
                  "thumbnail_url": "https://foo.test/image",
                  "picture": null,
                  "genres": [
                    "Classics",
                    "Fantasy",
                    "Fiction"
                  ],
                  "publisher": "New Testing World",
                  "published_date": "15/9/1986",
                  "description": "Just some test",
                  "total_pages": 1116,
                  "isbn10": "5550411005",
                  "isbn13": "7890450411343",
                  "serie": null,
                  "serie_number": -1,
                  "lang": "en",
                  "credits": {
                        "translators": [],
                        "illustrators": [],
                        "contributors": [],
                        "others": []
                    }
                },
                "bookProgress": {
                  "id": "76894b10-a41e-4ade-849c-f402cf39baab",
                  "pageNumber": -1,
                  "percentage": 2,
                  "gaveup": false,
                  "finished": false,
                  "createdAt": "2023-04-23T17:40:14.037147+00:00"
                },
                "comments": [
                    {
                        "id": "2c890475-a1f0-4e88-a3cf-49d745b78528",
                        "hasSpoilers": false,
                        "commentText": "nice",
                        "percentage": 2,
                        "bookID": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                        "createdAt": "2023-05-04T08:18:06.835467+00:00",
                        "votes": 0,
                        "profile": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Joen Testen",
                            "firstname": "Joen",
                            "lastname": "Testen"
                        }
                    },
                    {
                        "id": "7efa98f3-c79b-408c-8ebe-0d5f8326755d",
                        "hasSpoilers": false,
                        "commentText": "notvbir",
                        "percentage": 2,
                        "bookID": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                        "createdAt": "2023-05-04T08:18:17.349404+00:00",
                        "votes": 0,
                        "profile": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Brocken",
                            "firstname": "Brock",
                            "lastname": "Enteil"
                        }
                    }
                ]
              }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<ReadingBooksItemApi>>(serverResponse)

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", UserReadingBooksRequest()) }
            .returns(response)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue { currentReadingBooks.data != null }
        assertTrue { currentReadingBooks.data?.size == 1 }
        assertTrue { currentReadingBooks.data?.get(0)?.book?.title == "Eat" }
        assertTrue { currentReadingBooks.data?.get(0)?.bookProgressComments?.size == 2 }
        assertTrue { currentReadingBooks.data?.get(0)?.bookProgressComments?.get(0)?.text == "nice" }
    }

    @Test
    fun testShouldGetErrorIfRequestTypeDoesNotExistsAndResponseIsAnError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "ApiRouteNotFoundError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", UserReadingBooksRequest()) }
            .throws(errorResponse)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue(currentReadingBooks.error != null)
        assertTrue(currentReadingBooks.error is BookError.ApiRouteNotFoundError)
    }

    @Test
    fun testShouldGetGetCurrentReadingBooksErrorIfRequestReturnsThatError() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "GetUserCurrentReadingBooksError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", UserReadingBooksRequest()) }
            .throws(errorResponse)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue(currentReadingBooks.error != null)
        assertTrue(currentReadingBooks.error is BookError.GetCurrentReadingBooksError)
    }

    @Test
    fun testShouldGetUnknownErrorIfTheRequestReturnAnErrorThatIsNotExpected() = runTest {
        // Given
        val serverResponse = """
            {
            	"error": {
            		"name": "TestError"
            	}
            }
        """.trimIndent()
        val errorResponse = Exception(serverResponse)

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", UserReadingBooksRequest()) }
            .throws(errorResponse)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue(currentReadingBooks.error != null)
        assertTrue(currentReadingBooks.error is BookError.UnknownError)
    }

    @Test
    fun testGetCurrentReadingBooksWithProperDataAndCredits() = runTest {
        // Given
        val serverResponse = """
            [
              {
                "book": {
                  "id": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                  "title": "Eat",
                  "authors": [
                    "Steph James"
                  ],
                  "picture_url": "https://foo.test/image",
                  "thumbnail_url": "https://foo.test/image",
                  "picture": null,
                  "genres": [
                    "Classics",
                    "Fantasy",
                    "Fiction"
                  ],
                  "publisher": "New Testing World",
                  "published_date": "15/9/1986",
                  "description": "Just some test",
                  "total_pages": 1116,
                  "isbn10": "5550411005",
                  "isbn13": "7890450411343",
                  "serie": null,
                  "serie_number": -1,
                  "lang": "en",
                  "credits": {
                        "translators": ["Emma Scars"],
                        "illustrators": ["Lord Geist"],
                        "contributors": [],
                        "others": []
                    }
                },
                "bookProgress": {
                  "id": "76894b10-a41e-4ade-849c-f402cf39baab",
                  "pageNumber": -1,
                  "percentage": 2,
                  "gaveup": false,
                  "finished": false,
                  "createdAt": "2023-04-23T17:40:14.037147+00:00"
                },
                "comments": [
                    {
                        "id": "2c890475-a1f0-4e88-a3cf-49d745b78528",
                        "hasSpoilers": false,
                        "commentText": "nice",
                        "percentage": 2,
                        "bookID": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                        "createdAt": "2023-05-04T08:18:06.835467+00:00",
                        "votes": 0,
                        "profile": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Joen Testen",
                            "firstname": "Joen",
                            "lastname": "Testen"
                        }
                    },
                    {
                        "id": "7efa98f3-c79b-408c-8ebe-0d5f8326755d",
                        "hasSpoilers": false,
                        "commentText": "notvbir",
                        "percentage": 2,
                        "bookID": "702fd9c6-b907-41c1-aac8-02a9a6ebb123",
                        "createdAt": "2023-05-04T08:18:17.349404+00:00",
                        "votes": 0,
                        "profile": {
                            "id": "1d650563-1a8c-4634-96ab-683098d66321",
                            "username": "Brocken",
                            "firstname": "Brock",
                            "lastname": "Enteil"
                        }
                    }
                ]
              }
            ]
        """.trimIndent()
        val response = Json.decodeFromString<List<ReadingBooksItemApi>>(serverResponse)

        coEvery { api.getUserCurrentReadingBook("Bearer $sessionToken", UserReadingBooksRequest()) }
            .returns(response)

        // When
        val currentReadingBooks = bookRemoteData.getUserCurrentReadingBook(sessionToken)

        // Then
        assertTrue { currentReadingBooks.data != null }
        assertTrue { currentReadingBooks.data?.size == 1 }
        assertTrue { currentReadingBooks.data?.get(0)?.book?.title == "Eat" }
        assertTrue { currentReadingBooks.data?.get(0)?.bookProgressComments?.size == 2 }
    }
}