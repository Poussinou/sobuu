package com.sobuumedia.sobuu.profile

import com.sobuumedia.sobuu.features.profile.remote.IProfileRemoteData
import com.sobuumedia.sobuu.features.profile.remote.ProfileApi
import com.sobuumedia.sobuu.features.profile.remote.ProfileDataRequest
import com.sobuumedia.sobuu.features.profile.remote.ProfileError
import com.sobuumedia.sobuu.features.profile.remote.ProfileRemoteDataImpl
import com.sobuumedia.sobuu.models.api_models.CommonItemApi.ProfileItemApi
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.coEvery
import io.mockative.configure
import io.mockative.mock
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class GetUserProfileTest {

    @Mock
    private val api = mock(classOf<ProfileApi>())

    private lateinit var profileRemoteData: IProfileRemoteData
    private val sessionToken = "w09wiohnkwe"

    @BeforeTest
    fun setup() {
        configure(api) { stubsUnitByDefault = false }
        profileRemoteData = ProfileRemoteDataImpl(api)
    }

    @Test
    fun testShouldGetInvalidSessionTokenErrorIfSessionTokenIsEmpty() = runTest {
        val userProfile = profileRemoteData.getUserProfile("")
        assertTrue(userProfile.error != null)
        assertTrue(userProfile.error is ProfileError.InvalidSessionTokenError)
    }

    @Test
    fun testGetUserProfileWithProperData() = runTest {
        // Given
        val serverResponse = """
            {
                "id": "sd9ijenwed98hi3",
                "username": "JoinSobuu",
                "firstname": "John",
                "lastname": "Doe Test",
                "email": "test.email@testing.com",
                "createdAt": "2022-12-26T11:46:30.923779+00:00"
            }
    """.trimIndent()
        val response = Json.decodeFromString<ProfileItemApi>(serverResponse)

        coEvery { api.getUserProfile("Bearer $sessionToken", ProfileDataRequest()) }
            .returns(response)

        // When
        val userProfile = profileRemoteData.getUserProfile(sessionToken)

        // Then
        assertTrue { userProfile.data != null }
        assertTrue { userProfile.data?.username == "JoinSobuu" }
        assertTrue { userProfile.data?.email == "test.email@testing.com" }
        assertTrue { userProfile.data?.firstName == "John" }
        assertTrue { userProfile.data?.createdAt?.isNotEmpty() == true }
    }

    @Test
    fun testGetUserProfileWithoutAllTheData() = runTest {
        // Given
        val serverResponse = """
            {
                "id": "sd9ijenwed98hi3",
                "username": "JoinSobuu",
                "email": "test.email@testing.com",
                "createdAt": "2022-12-26T11:46:30.923779+00:00"
            }
         """.trimIndent()
        val response = Json.decodeFromString<ProfileItemApi>(serverResponse)

        coEvery { api.getUserProfile("Bearer $sessionToken", ProfileDataRequest()) }
            .returns(response)

        // When
        val userProfile = profileRemoteData.getUserProfile(sessionToken)

        // Then
        assertTrue { userProfile.data != null }
        assertTrue { userProfile.data?.username == "JoinSobuu" }
        assertTrue { userProfile.data?.id?.isNotEmpty() == true }
        assertTrue { userProfile.data?.email == "test.email@testing.com" }
        assertTrue { userProfile.data?.firstName == "" }
        assertTrue { userProfile.data?.createdAt?.isNotEmpty() == true }
    }
}