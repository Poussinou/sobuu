package com.sobuumedia.sobuu.utils

import android.content.Context
import com.sobuumedia.sobuu.SharedRes.strings.genres_Adult
import com.sobuumedia.sobuu.SharedRes.strings.genres_Adult_Fiction
import com.sobuumedia.sobuu.SharedRes.strings.genres_Adventure
import com.sobuumedia.sobuu.SharedRes.strings.genres_Anthologies
import com.sobuumedia.sobuu.SharedRes.strings.genres_Chick_Lit
import com.sobuumedia.sobuu.SharedRes.strings.genres_Classic
import com.sobuumedia.sobuu.SharedRes.strings.genres_Contemporary
import com.sobuumedia.sobuu.SharedRes.strings.genres_Crime
import com.sobuumedia.sobuu.SharedRes.strings.genres_Dark
import com.sobuumedia.sobuu.SharedRes.strings.genres_Dark_Fantasy
import com.sobuumedia.sobuu.SharedRes.strings.genres_Epic
import com.sobuumedia.sobuu.SharedRes.strings.genres_Epic_Fantasy
import com.sobuumedia.sobuu.SharedRes.strings.genres_Fantasy
import com.sobuumedia.sobuu.SharedRes.strings.genres_Fiction
import com.sobuumedia.sobuu.SharedRes.strings.genres_Generic
import com.sobuumedia.sobuu.SharedRes.strings.genres_High_Fantasy
import com.sobuumedia.sobuu.SharedRes.strings.genres_Historical
import com.sobuumedia.sobuu.SharedRes.strings.genres_Historical_Fiction
import com.sobuumedia.sobuu.SharedRes.strings.genres_LGBT
import com.sobuumedia.sobuu.SharedRes.strings.genres_Magic
import com.sobuumedia.sobuu.SharedRes.strings.genres_Magical_Realism
import com.sobuumedia.sobuu.SharedRes.strings.genres_Music
import com.sobuumedia.sobuu.SharedRes.strings.genres_Mystery
import com.sobuumedia.sobuu.SharedRes.strings.genres_Novella
import com.sobuumedia.sobuu.SharedRes.strings.genres_Novels
import com.sobuumedia.sobuu.SharedRes.strings.genres_Queer
import com.sobuumedia.sobuu.SharedRes.strings.genres_Romance
import com.sobuumedia.sobuu.SharedRes.strings.genres_Science_Fiction
import com.sobuumedia.sobuu.SharedRes.strings.genres_Science_Fiction_Fantasy
import com.sobuumedia.sobuu.SharedRes.strings.genres_Short_Stories
import com.sobuumedia.sobuu.SharedRes.strings.genres_Space
import com.sobuumedia.sobuu.SharedRes.strings.genres_Speculative_Fiction
import com.sobuumedia.sobuu.SharedRes.strings.genres_Sports
import com.sobuumedia.sobuu.SharedRes.strings.genres_Study_Aids
import com.sobuumedia.sobuu.SharedRes.strings.genres_Suspense
import com.sobuumedia.sobuu.SharedRes.strings.genres_Thriller
import com.sobuumedia.sobuu.SharedRes.strings.genres_Time_Travel
import com.sobuumedia.sobuu.SharedRes.strings.genres_War
import com.sobuumedia.sobuu.SharedRes.strings.genres_Westerns
import com.sobuumedia.sobuu.SharedRes.strings.genres_Young_Adult
import dev.icerock.moko.resources.StringResource

class RemoteDataUtils {
    fun translateGenres(context: Context, genres: List<String>?): List<String> {
            if (genres.isNullOrEmpty()) {
                return emptyList()
            }
            return genres
                .map {
                    when (it.trim()) {
                        "Fiction" -> genres_Fiction
                        "Study Aids" -> genres_Study_Aids
                        "Romance" -> genres_Romance
                        "Historical Fiction" -> genres_Historical_Fiction
                        "LGBT" -> genres_LGBT
                        "Contemporary" -> genres_Contemporary
                        "Adult" -> genres_Adult
                        "Sports" -> genres_Sports
                        "Short Stories" -> genres_Short_Stories
                        "Novella" -> genres_Novella
                        "Chick Lit" -> genres_Chick_Lit
                        "Music" -> genres_Music
                        "Historical" -> genres_Historical
                        "Science Fiction" -> genres_Science_Fiction
                        "Young Adult" -> genres_Young_Adult
                        "Fantasy" -> genres_Fantasy
                        "Space" -> genres_Space
                        "Adventure" -> genres_Adventure
                        "Epic Fantasy" -> genres_Epic_Fantasy
                        "High Fantasy" -> genres_High_Fantasy
                        "Science Fiction Fantasy" -> genres_Science_Fiction_Fantasy
                        "Magic" -> genres_Magic
                        "Epic" -> genres_Epic
                        "Adult Fiction" -> genres_Adult_Fiction
                        "Dark Fantasy" -> genres_Dark_Fantasy
                        "War" -> genres_War
                        "Westerns" -> genres_Westerns
                        "Anthologies" -> genres_Anthologies
                        "Novels" -> genres_Novels
                        "Queer" -> genres_Queer
                        "Time Travel" -> genres_Time_Travel
                        "Speculative Fiction" -> genres_Speculative_Fiction
                        "Suspense" -> genres_Suspense
                        "Thriller" -> genres_Thriller
                        "Dark" -> genres_Dark
                        "Classic" -> genres_Classic
                        "Crime" -> genres_Crime
                        "Magical Realism" -> genres_Magical_Realism
                        "Mystery" -> genres_Mystery
                        else -> genres_Generic
                    }
                }
                .map { string -> context.getString(string.resourceId) }
                .filter { it.isNotEmpty() }
                .filter { it != context.getString(genres_Generic.resourceId) }
                .toList()
        }
}

fun StringResource.getResString(context: Context): String {
    return context.getString(this.resourceId)
}