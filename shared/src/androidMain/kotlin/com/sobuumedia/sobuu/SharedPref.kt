package com.sobuumedia.sobuu

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

const val PREF_NAME = "SobuuShared"

actual typealias SPref = Application

actual fun SPref.getInt(key: String) : Int {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    return prefs.getInt(key, -1)
}

actual fun SPref.setInt(key: String, value: Int) {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    val editor = prefs.edit()
    editor.putInt(key,value)
    editor.apply()
}

actual fun SPref.getString(key: String ) : String {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    return prefs.getString(key, "") ?: ""
}

actual fun SPref.setString(key: String, value: String) {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    val editor = prefs.edit()
    editor.putString(key,value)
    editor.apply()
}

actual fun SPref.getBoolean(key: String) : Boolean {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    return prefs.getBoolean(key, false)
}

actual fun SPref.setBoolean(key: String, value: Boolean) {
    val prefs: SharedPreferences = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
    val editor = prefs.edit()
    editor.putBoolean(key,value)
    editor.apply()
}