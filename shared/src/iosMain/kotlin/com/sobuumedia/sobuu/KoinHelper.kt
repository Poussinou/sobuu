package com.sobuumedia.sobuu

import com.sobuumedia.sobuu.core.KMMStorage
import com.sobuumedia.sobuu.di.appModule
import com.sobuumedia.sobuu.features.authentication.database.AuthenticationLocalDataImpl
import com.sobuumedia.sobuu.features.authentication.database.IAuthenticationLocalData
import com.sobuumedia.sobuu.features.authentication.remote.IAuthenticationRemoteData
import com.sobuumedia.sobuu.features.authentication.repository.AuthenticationRepositoryImpl
import com.sobuumedia.sobuu.features.authentication.repository.IAuthenticationRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin

class KoinHelper(context: SPref) : KoinComponent {

    private val remoteAuth: IAuthenticationRemoteData by inject()
    private val kmmStorage = KMMStorage(context = context)
    private val localAuth: IAuthenticationLocalData = AuthenticationLocalDataImpl(kmmStorage)

//    private val remoteBook: IBookRemoteData by inject()
//    private val localBook: IBookLocalData by inject()

    val authRepo: IAuthenticationRepository = AuthenticationRepositoryImpl(
        authRemoteData = remoteAuth,
        authLocalData = localAuth
    )

//    val homeRepo: IBookRepository = BookRepositoryImpl(
//        remoteBook, localBook
//    )
}

fun initKoin(){
    startKoin {
        modules(appModule())
    }.koin
}