package com.sobuumedia.sobuu.utils

import com.sobuumedia.sobuu.SharedRes
import com.sobuumedia.sobuu.get

class RemoteDataUtils {
    companion object {
        fun translateGenres(genres: List<String>?): List<String> {
            if (genres.isNullOrEmpty()) {
                return emptyList()
            }
            return genres
                .map {
                    when (it.trim()) {
                        "Fiction" -> SharedRes.strings.genres_Fiction.get()
                        "Study Aids" -> SharedRes.strings.genres_Study_Aids.get()
                        "Romance" -> SharedRes.strings.genres_Romance.get()
                        "Historical Fiction" -> SharedRes.strings.genres_Historical_Fiction.get()
                        "LGBT" -> SharedRes.strings.genres_LGBT.get()
                        "Contemporary" -> SharedRes.strings.genres_Contemporary.get()
                        "Adult" -> SharedRes.strings.genres_Adult.get()
                        "Sports" -> SharedRes.strings.genres_Sports.get()
                        "Short Stories" -> SharedRes.strings.genres_Short_Stories.get()
                        "Novella" -> SharedRes.strings.genres_Novella.get()
                        "Chick Lit" -> SharedRes.strings.genres_Chick_Lit.get()
                        "Music" -> SharedRes.strings.genres_Music.get()
                        "Historical" -> SharedRes.strings.genres_Historical.get()
                        "Science Fiction" -> SharedRes.strings.genres_Science_Fiction.get()
                        "Young Adult" -> SharedRes.strings.genres_Young_Adult.get()
                        "Fantasy" -> SharedRes.strings.genres_Fantasy.get()
                        "Space" -> SharedRes.strings.genres_Space.get()
                        "Adventure" -> SharedRes.strings.genres_Adventure.get()
                        "Epic Fantasy" -> SharedRes.strings.genres_Epic_Fantasy.get()
                        "High Fantasy" -> SharedRes.strings.genres_High_Fantasy.get()
                        "Science Fiction Fantasy" -> SharedRes.strings.genres_Science_Fiction_Fantasy.get()
                        "Magic" -> SharedRes.strings.genres_Magic.get()
                        "Epic" -> SharedRes.strings.genres_Epic.get()
                        "Adult Fiction" -> SharedRes.strings.genres_Adult_Fiction.get()
                        "Dark Fantasy" -> SharedRes.strings.genres_Dark_Fantasy.get()
                        "War" -> SharedRes.strings.genres_War.get()
                        "Westerns" -> SharedRes.strings.genres_Westerns.get()
                        "Anthologies" -> SharedRes.strings.genres_Anthologies.get()
                        "Novels" -> SharedRes.strings.genres_Novels.get()
                        "Queer" -> SharedRes.strings.genres_Queer.get()
                        "Time Travel" -> SharedRes.strings.genres_Time_Travel.get()
                        "Speculative Fiction" -> SharedRes.strings.genres_Speculative_Fiction.get()
                        else -> ""
                    }
                }
                .filter { it.isNotEmpty() }
                .toList()
        }
    }
}