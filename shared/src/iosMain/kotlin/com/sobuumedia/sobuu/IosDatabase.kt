package com.sobuumedia.sobuu

import androidx.room.Room
import androidx.room.RoomDatabase
import platform.Foundation.NSHomeDirectory

fun getDatabaseBuilder(): RoomDatabase.Builder<SobuuDB> {
    val dbFilePath = NSHomeDirectory() + "/" + dbFileName
    return Room.databaseBuilder<SobuuDB>(
        name = dbFilePath,
        factory = { SobuuDB::class.instantiateImpl() }
    )
}