import co.touchlab.skie.configuration.DefaultArgumentInterop
import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import com.codingfeline.buildkonfig.compiler.FieldSpec.Type.STRING
import org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSetTree

plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("com.codingfeline.buildkonfig")
    kotlin("plugin.serialization") version "1.9.22"
    id("com.google.devtools.ksp") version "1.9.22-1.0.17"
    id("de.jensklingenberg.ktorfit") version "1.12.0"
    id("io.kotest.multiplatform") version "5.8.0"
    id("io.realm.kotlin") version "1.11.0"
    id("dev.icerock.mobile.multiplatform-resources")
    id("co.touchlab.skie") version "0.6.2"
}

kotlin {
    // This is needed to run the Android app
    androidTarget {
        @OptIn(ExperimentalKotlinGradlePluginApi::class)
        instrumentedTestVariant {
            sourceSetTree.set(KotlinSourceSetTree.test)
        }
    }

    jvmToolchain(17)

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
            isStatic = true
        }
    }

    task("testClasses")

    sourceSets {
        commonMain.dependencies {
            // Database
            api(libs.room.runtime)
            api(libs.sqlite.bundled)

            // Others
            implementation(libs.kotlinx.datetime)
            // Logger
            implementation(libs.napier)

            // Ktor
            implementation(libs.kotlinx.serialization.json)
            implementation(libs.ktor.client.content.negotiation)
            implementation(libs.ktor.serialization.kotlinx.json)
            implementation(libs.ktor.client.logging)
            implementation(libs.ktorfit.lib)

            // Koin
            implementation(libs.koin.core)
            // This has been added temporally to avoid a problem starting iOS project
            implementation("co.touchlab:stately-common:2.0.6")
            implementation(libs.skie.configuration.annotations)

            // Coroutines
            implementation(libs.kotlinx.coroutines.core)

            // Share resources
            implementation(libs.moko.resources)
        }
        commonTest.dependencies {
            implementation(kotlin("test"))
            implementation(kotlin("test-common"))
            implementation(kotlin("test-annotations-common"))
            implementation(libs.mockative)
            implementation(libs.kotlinx.coroutines.test)
            implementation(libs.ktor.test.mock)
            implementation(libs.moko.test.resources)
        }
        androidMain.dependencies {
            implementation(libs.ktor.ktor.client.okhttp)
        }
        //val androidInstrumentedTest by getting
        iosMain.dependencies {
            implementation(libs.ktor.ktor.client.darwin)
        }

        getByName("androidMain") {
            kotlin.srcDir("build/generated/moko/androidMain/src")
        }
    }
}

dependencies {
    add("kspCommonMainMetadata", "de.jensklingenberg.ktorfit:ktorfit-ksp:1.14.0")
    add("kspAndroid", "de.jensklingenberg.ktorfit:ktorfit-ksp:1.14.0")
    add("kspIosArm64", "de.jensklingenberg.ktorfit:ktorfit-ksp:1.14.0")
    add("kspIosX64", "de.jensklingenberg.ktorfit:ktorfit-ksp:1.14.0")
    add("kspIosSimulatorArm64", "de.jensklingenberg.ktorfit:ktorfit-ksp:1.14.0")
    add("kspAndroid", libs.room.compiler)
    add("kspIosSimulatorArm64", libs.room.compiler)
    add("kspIosX64", libs.room.compiler)
    add("kspIosArm64", libs.room.compiler)
    configurations
        .filter { it.name.startsWith("ksp") && it.name.contains("Test") }
        .forEach {
            add(it.name, "io.mockative:mockative-processor:2.2.0")
        }
}

ksp {
    arg("mockative.stubsUnitByDefault", "false")
}

multiplatformResources {
    multiplatformResourcesPackage = "com.sobuumedia.sobuu"
    multiplatformResourcesClassName = "SharedRes"
}

skie {
    analytics {
        disableUpload.set(true)
        enabled.set(false)
    }
    features {
        group {
            DefaultArgumentInterop.Enabled(true)
        }
    }
}

buildkonfig {
    packageName = "com.sobuumedia.sobuu.shared"

    defaultConfigs {
        buildConfigField(
            STRING,
            "API_KEY",
            gradleLocalProperties(rootDir).getProperty("DEV_API_KEY")
        )
        buildConfigField(
            STRING,
            "API_URL",
            gradleLocalProperties(rootDir).getProperty("DEV_API_URL")
        )

        buildConfigField(
            STRING,
            "MATOMO_API_URL",
            gradleLocalProperties(rootDir).getProperty("MATOMO_API_URL")
        )

        buildConfigField(
            STRING,
            "PLAUSIBLE_API_URL",
            gradleLocalProperties(rootDir).getProperty("PLAUSIBLE_API_URL")
        )
    }
    defaultConfigs("prod") {
        buildConfigField(
            STRING,
            "API_KEY",
            gradleLocalProperties(rootDir).getProperty("PROD_API_KEY")
        )
        buildConfigField(
            STRING,
            "API_URL",
            gradleLocalProperties(rootDir).getProperty("PROD_API_URL")
        )
    }

    targetConfigs { // dev target to pass BuildConfig to iOS
        create("ios") {
            buildConfigField(
                STRING,
                "API_KEY",
                gradleLocalProperties(rootDir).getProperty("DEV_API_KEY")
            )
            buildConfigField(
                STRING,
                "API_URL",
                gradleLocalProperties(rootDir).getProperty("DEV_API_URL")
            )
        }
    }

    targetConfigs("prod") {
        create("android") {
            buildConfigField(
                STRING,
                "API_KEY",
                gradleLocalProperties(rootDir).getProperty("PROD_API_KEY")
            )
            buildConfigField(
                STRING,
                "API_URL",
                gradleLocalProperties(rootDir).getProperty("PROD_API_URL")
            )
        }

        create("ios") {
            buildConfigField(
                STRING,
                "API_KEY",
                gradleLocalProperties(rootDir).getProperty("PROD_API_KEY")
            )
            buildConfigField(
                STRING,
                "API_URL",
                gradleLocalProperties(rootDir).getProperty("PROD_API_URL")
            )
        }
    }
}

android {
    namespace = "com.sobuumedia.sobuu"
    compileSdk = 34
    defaultConfig {
        minSdk = 26
    }
    lint {
        baseline = file("lint-baseline.xml")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

tasks.configureEach {
    if (name == "kspKotlinIosSimulatorArm64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosSimulatorArm64Main"))
    }
    if (name == "kspKotlinIosX64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosX64Main"))
    }
    if (name == "kspKotlinIosArm64") {
        mustRunAfter(tasks.getByName("generateMRcommonMain"))
        mustRunAfter(tasks.getByName("generateMRiosArm64Main"))
    }
}